﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace PPSkin
{
    public partial class PPMessageForm : PPForm
    {
        private string MessageInfo = string.Empty;
        private MessageBoxButtons BtnType = MessageBoxButtons.OK;
        private MessageBoxIcon IconType = MessageBoxIcon.None;
        private PPMessageBox.Language Lang = PPMessageBox.Language.ENGLISH;
        private int timerInterval = 0;
        private int maxWidth = 500;//

        public PPMessageForm(string Text)
        {
            InitializeComponent();
            MessageInfo = Text;
        }

        public PPMessageForm(string Text, MessageBoxButtons button)
        {
            InitializeComponent();
            MessageInfo = Text;
            BtnType = button;
        }

        public PPMessageForm(string Text, MessageBoxButtons button, MessageBoxIcon icon)
        {
            InitializeComponent();
            MessageInfo = Text;
            BtnType = button;
            IconType = icon;
        }

        public PPMessageForm(string Text, string title)
        {
            InitializeComponent();
            MessageInfo = Text;
            this.Text = title;
        }

        public PPMessageForm(string Text, string title, MessageBoxButtons button)
        {
            InitializeComponent();
            MessageInfo = Text;
            BtnType = button;
            this.Text = title;
        }

        public PPMessageForm(string Text, string title, MessageBoxButtons button, MessageBoxIcon icon)
        {
            InitializeComponent();
            MessageInfo = Text;
            BtnType = button;
            IconType = icon;
            this.Text = title;
        }

        public PPMessageForm(string Text, int millisecond)
        {
            InitializeComponent();
            MessageInfo = Text;
            timerInterval = millisecond;
        }

        public PPMessageForm(string Text, string title, int millisecond)
        {
            InitializeComponent();
            MessageInfo = Text;
            this.Text = title;
            timerInterval = millisecond;
        }

        public PPMessageForm(string Text, PPMessageBox.Language lang)
        {
            InitializeComponent();
            MessageInfo = Text;
            Lang = lang;
        }

        public PPMessageForm(string Text, MessageBoxButtons button, PPMessageBox.Language lang)
        {
            InitializeComponent();
            MessageInfo = Text;
            BtnType = button;
            Lang = lang;
        }

        public PPMessageForm(string Text, MessageBoxButtons button, MessageBoxIcon icon, PPMessageBox.Language lang)
        {
            InitializeComponent();
            MessageInfo = Text;
            BtnType = button;
            IconType = icon;
            Lang = lang;
        }

        public PPMessageForm(string Text, string title, PPMessageBox.Language lang)
        {
            InitializeComponent();
            MessageInfo = Text;
            this.Text = title;
            Lang = lang;
        }

        public PPMessageForm(string Text, string title, MessageBoxButtons button, PPMessageBox.Language lang)
        {
            InitializeComponent();
            MessageInfo = Text;
            BtnType = button;
            this.Text = title;
            Lang = lang;
        }

        public PPMessageForm(string Text, string title, MessageBoxButtons button, MessageBoxIcon icon, PPMessageBox.Language lang)
        {
            InitializeComponent();
            MessageInfo = Text;
            BtnType = button;
            IconType = icon;
            this.Text = title;
            Lang = lang;
        }

        public PPMessageForm(string Text, int millisecond, PPMessageBox.Language lang)
        {
            InitializeComponent();
            MessageInfo = Text;
            timerInterval = millisecond;
            Lang = lang;
        }

        public PPMessageForm(string Text, string title, int millisecond, PPMessageBox.Language lang)
        {
            InitializeComponent();
            MessageInfo = Text;
            this.Text = title;
            timerInterval = millisecond;
            Lang = lang;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.EnableDoubleCilckSizeChange = false;

            switch (IconType)
            {
                case MessageBoxIcon.Information: this.Icon = SystemIcons.Information; this.ShowTitleIcon = true; break;
                case MessageBoxIcon.Exclamation: this.Icon = SystemIcons.Exclamation; this.ShowTitleIcon = true; break;
                case MessageBoxIcon.Error: this.Icon = SystemIcons.Error; this.ShowTitleIcon = true; break;
                case MessageBoxIcon.Question: this.Icon = SystemIcons.Question; this.ShowTitleIcon = true; break;
                case MessageBoxIcon.None: this.ShowTitleIcon = false; break;
            }
            Graphics g = this.CreateGraphics();
            SizeF fontsize = g.MeasureString(MessageInfo, this.Font);
            int width = this.Width;
            if (fontsize.Width > this.Width - 30)
            {
                width = (int)fontsize.Width + 30;
                if (width > maxWidth)
                {
                    this.Width = maxWidth;
                }
                else
                {
                    this.Width = width;
                }
            }
            //Graphics g = this.CreateGraphics();
            //if (icon == null)
            //{
            //    SizeF fontsize = g.MeasureString(MessageInfo, this.Font);
            //    string str = MessageInfo;
            //    if (fontsize.Width > this.Width - 30)
            //    {
            //        str = "";
            //        foreach (char c in MessageInfo)
            //        {
            //            str += c;
            //            fontsize = g.MeasureString(str, this.Font);
            //            if (fontsize.Width > this.Width - 30)
            //            {
            //                str = str.Substring(0, str.Length - 1);
            //                str += '\n';
            //                str += c;
            //            }
            //        }
            //    }
            //    MessageInfo = str;
            //}
            //else
            //{
            //    SizeF fontsize = g.MeasureString(MessageInfo, this.Font);
            //    string str = MessageInfo;
            //    if (fontsize.Width > this.Width - 70)
            //    {
            //        str = "";
            //        foreach (char c in MessageInfo)
            //        {
            //            str += c;
            //            fontsize = g.MeasureString(str, this.Font);
            //            if (fontsize.Width > this.Width - 70)
            //            {
            //                str = str.Substring(0, str.Length - 1);
            //                str += '\n';
            //                str += c;
            //            }
            //        }
            //    }
            //    MessageInfo = str;
            //}

            if (Lang == PPMessageBox.Language.CHINESE)
            {
                ppButton_OK.Text = "确定";
                ppButton_YES.Text = "是";
                ppButton_NO.Text = "否";
                ppButton_CANCLE.Text = "取消";
                ppButton_IGNORE.Text = "忽略";
                ppButton_ABORT.Text = "中止";
                ppButton_RETRY.Text = "重试";
            }

            switch (BtnType)
            {
                case MessageBoxButtons.OK:
                    ShowOK();
                    break;

                case MessageBoxButtons.YesNo:
                    ShowYESNO();
                    break;

                case MessageBoxButtons.AbortRetryIgnore:
                    ShowABORTRETRYIGNORE();
                    break;

                case MessageBoxButtons.OKCancel:
                    ShowOKCANCLE();
                    break;

                case MessageBoxButtons.RetryCancel:
                    ShowRETRYCANCLE();
                    break;

                case MessageBoxButtons.YesNoCancel:
                    ShowYESNOCANCLE();
                    break;
            }
            ppButton_OK.Parent = this;
            ppButton_YES.Parent = this;
            ppButton_NO.Parent = this;
            ppButton_CANCLE.Parent = this;
            ppButton_ABORT.Parent = this;
            ppButton_RETRY.Parent = this;
            ppButton_IGNORE.Parent = this;

            ppButton_OK.BringToFront();
            ppButton_YES.BringToFront();
            ppButton_NO.BringToFront();
            ppButton_CANCLE.BringToFront();
            ppButton_ABORT.BringToFront();
            ppButton_RETRY.BringToFront();
            ppButton_IGNORE.BringToFront();

            if (timerInterval != 0)
            {
                timer.Interval = timerInterval;
                timer.Enabled = true;
            }

            this.TopMost = true;
            this.CenterToParent();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            Graphics g = e.Graphics;
            //g.Clear(this.BackColor);
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.Default;
            g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;

            SizeF fontsize = g.MeasureString(MessageInfo, this.Font);

            Rectangle rect = new Rectangle(15, this.TitleHeight, this.Width - 30, this.Height - this.TitleHeight - 40);
            StringFormat stringFormat = new StringFormat();
            stringFormat.Alignment = StringAlignment.Center;
            stringFormat.LineAlignment = StringAlignment.Center;
            stringFormat.Trimming = StringTrimming.EllipsisPath;
            SolidBrush solidBrush = new SolidBrush(this.ForeColor);
            g.DrawString(MessageInfo, this.Font, solidBrush, rect, stringFormat);

            //if (icon == null)
            //{
            //    SizeF fontsize = g.MeasureString(MessageInfo, this.Font);
            //    PointF p = new PointF((this.Width - fontsize.Width) / 2, (this.Height - fontsize.Height) / 2);
            //    g.DrawString(MessageInfo, this.Font, new SolidBrush(this.ForeColor), p);
            //}
            //else
            //{
            //    SizeF fontsize = g.MeasureString(MessageInfo, this.Font);
            //    Point icoPoint = new Point((this.Width - 35 - (int)fontsize.Width) / 2, (this.Height - 30) / 2);
            //    g.DrawIcon(icon, new Rectangle(icoPoint, new Size(30, 30)));

            //    PointF p = new PointF(icoPoint.X + 35, (this.Height - fontsize.Height) / 2);
            //    g.DrawString(MessageInfo, this.Font, new SolidBrush(this.ForeColor), p);
            //}
        }

        private void ShowOK()
        {
            Font font = this.Font;
            Size fontsize = TextRenderer.MeasureText(MessageInfo, font);

            ppButton_OK.Visible = true;
            ppButton_YES.Visible = false;
            ppButton_NO.Visible = false;
            ppButton_ABORT.Visible = false;
            ppButton_CANCLE.Visible = false;
            ppButton_IGNORE.Visible = false;
            ppButton_RETRY.Visible = false;

            ppButton_OK.Location = new Point(this.Width - 100, this.Height - 40);

            //panel_base.Invalidate();
        }

        private void ShowYESNO()
        {
            Font font = this.Font;
            Size fontsize = TextRenderer.MeasureText(MessageInfo, font);

            ppButton_OK.Visible = false;
            ppButton_YES.Visible = true;
            ppButton_NO.Visible = true;
            ppButton_ABORT.Visible = false;
            ppButton_CANCLE.Visible = false;
            ppButton_IGNORE.Visible = false;
            ppButton_RETRY.Visible = false;

            ppButton_YES.Location = new Point(this.Width / 4 - 40, this.Height - 50);
            ppButton_NO.Location = new Point(3 * this.Width / 4 - 40, this.Height - 50);

            //panel_base.Invalidate();
        }

        private void ShowOKCANCLE()
        {
            Font font = this.Font;
            Size fontsize = TextRenderer.MeasureText(MessageInfo, font);

            ppButton_OK.Visible = true;
            ppButton_YES.Visible = false;
            ppButton_NO.Visible = false;
            ppButton_ABORT.Visible = false;
            ppButton_CANCLE.Visible = true;
            ppButton_IGNORE.Visible = false;
            ppButton_RETRY.Visible = false;

            ppButton_OK.Location = new Point(this.Width / 4 - 40, this.Height - 50);
            ppButton_CANCLE.Location = new Point(3 * this.Width / 4 - 40, this.Height - 50);

            // panel_base.Invalidate();
        }

        private void ShowRETRYCANCLE()
        {
            Font font = this.Font;
            Size fontsize = TextRenderer.MeasureText(MessageInfo, font);

            ppButton_OK.Visible = false;
            ppButton_YES.Visible = false;
            ppButton_NO.Visible = false;
            ppButton_ABORT.Visible = false;
            ppButton_CANCLE.Visible = true;
            ppButton_IGNORE.Visible = false;
            ppButton_RETRY.Visible = true;

            ppButton_RETRY.Location = new Point(this.Width / 4 - 40, this.Height - 50);
            ppButton_CANCLE.Location = new Point(3 * this.Width / 4 - 40, this.Height - 50);

            //panel_base.Invalidate();
        }

        private void ShowYESNOCANCLE()
        {
            Font font = this.Font;
            Size fontsize = TextRenderer.MeasureText(MessageInfo, font);

            ppButton_OK.Visible = false;
            ppButton_YES.Visible = true;
            ppButton_NO.Visible = true;
            ppButton_ABORT.Visible = false;
            ppButton_CANCLE.Visible = true;
            ppButton_IGNORE.Visible = false;
            ppButton_RETRY.Visible = false;

            ppButton_YES.Location = new Point(this.Width / 6 - 40, this.Height - 50);
            ppButton_NO.Location = new Point(3 * this.Width / 6 - 40, this.Height - 50);
            ppButton_CANCLE.Location = new Point(5 * this.Width / 6 - 40, this.Height - 50);

            //panel_base.Invalidate();
        }

        private void ShowABORTRETRYIGNORE()
        {
            Font font = this.Font;
            Size fontsize = TextRenderer.MeasureText(MessageInfo, font);

            ppButton_OK.Visible = false;
            ppButton_YES.Visible = false;
            ppButton_NO.Visible = false;
            ppButton_ABORT.Visible = true;
            ppButton_CANCLE.Visible = false;
            ppButton_IGNORE.Visible = true;
            ppButton_RETRY.Visible = true;

            ppButton_ABORT.Location = new Point(this.Width / 6 - 40, this.Height - 50);
            ppButton_RETRY.Location = new Point(3 * this.Width / 6 - 40, this.Height - 50);
            ppButton_IGNORE.Location = new Point(5 * this.Width / 6 - 40, this.Height - 50);

            //panel_base.Invalidate();
        }

        private void ppButton_OK_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void ppButton_YES_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Yes;
            this.Close();
        }

        private void ppButton_NO_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }

        private void ppButton_RETRY_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Retry;
            this.Close();
        }

        private void ppButton_CANCLE_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void ppButton_IGNORE_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Ignore;
            this.Close();
        }

        private void ppButton_ABORT_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Abort;
            this.Close();
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            this.timer.Enabled = false;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }

    public partial class PPMessageBox
    {
        public enum Language
        {
            ENGLISH,
            CHINESE,
        }

        public static DialogResult Show(string text, IWin32Window win32Window = null)
        {
            PPMessageForm mf = new PPMessageForm(text);
            if (win32Window == null || (Form.FromHandle(win32Window.Handle) as Form).WindowState == FormWindowState.Minimized)
            {
                mf.StartPosition = FormStartPosition.CenterScreen;
                return mf.ShowDialog();
            }
            else
            {
                return mf.ShowDialog(win32Window);
            }
        }

        public static DialogResult Show(string text, int millisecond, IWin32Window win32Window = null)
        {
            PPMessageForm mf = new PPMessageForm(text, millisecond);
            if (win32Window == null || (Form.FromHandle(win32Window.Handle) as Form).WindowState == FormWindowState.Minimized)
            {
                mf.StartPosition = FormStartPosition.CenterScreen;
                return mf.ShowDialog();
            }
            else
            {
                return mf.ShowDialog(win32Window);
            }
        }

        public static DialogResult Show(string text, MessageBoxButtons button, IWin32Window win32Window = null)
        {
            PPMessageForm mf = new PPMessageForm(text, button);
            if (win32Window == null || (Form.FromHandle(win32Window.Handle) as Form).WindowState == FormWindowState.Minimized)
            {
                mf.StartPosition = FormStartPosition.CenterScreen;
                return mf.ShowDialog();
            }
            else
            {
                return mf.ShowDialog(win32Window);
            }
        }

        public static DialogResult Show(string text, MessageBoxButtons button, MessageBoxIcon icon, IWin32Window win32Window = null)
        {
            PPMessageForm mf = new PPMessageForm(text, button, icon);
            if (win32Window == null || (Form.FromHandle(win32Window.Handle) as Form).WindowState == FormWindowState.Minimized)
            {
                mf.StartPosition = FormStartPosition.CenterScreen;
                return mf.ShowDialog();
            }
            else
            {
                return mf.ShowDialog(win32Window);
            }
        }

        public static DialogResult Show(string text, string title, IWin32Window win32Window = null)
        {
            PPMessageForm mf = new PPMessageForm(text, title);
            if (win32Window == null || (Form.FromHandle(win32Window.Handle) as Form).WindowState == FormWindowState.Minimized)
            {
                mf.StartPosition = FormStartPosition.CenterScreen;
                return mf.ShowDialog();
            }
            else
            {
                return mf.ShowDialog(win32Window);
            }
        }

        public static DialogResult Show(string text, string title, int millisecond, IWin32Window win32Window = null)
        {
            PPMessageForm mf = new PPMessageForm(text, title, millisecond);
            if (win32Window == null || (Form.FromHandle(win32Window.Handle) as Form).WindowState == FormWindowState.Minimized)
            {
                mf.StartPosition = FormStartPosition.CenterScreen;
                return mf.ShowDialog();
            }
            else
            {
                return mf.ShowDialog(win32Window);
            }
        }

        public static DialogResult Show(string text, string title, MessageBoxButtons button, IWin32Window win32Window = null)
        {
            PPMessageForm mf = new PPMessageForm(text, title, button);
            if (win32Window == null || (Form.FromHandle(win32Window.Handle) as Form).WindowState == FormWindowState.Minimized)
            {
                mf.StartPosition = FormStartPosition.CenterScreen;
                return mf.ShowDialog();
            }
            else
            {
                return mf.ShowDialog(win32Window);
            }
        }

        public static DialogResult Show(string text, string title, MessageBoxButtons button, MessageBoxIcon icon, IWin32Window win32Window = null)
        {
            PPMessageForm mf = new PPMessageForm(text, title, button, icon);
            if (win32Window == null || (Form.FromHandle(win32Window.Handle) as Form).WindowState == FormWindowState.Minimized)
            {
                mf.StartPosition = FormStartPosition.CenterScreen;
                return mf.ShowDialog();
            }
            else
            {
                return mf.ShowDialog(win32Window);
            }
        }

        public static DialogResult Show(string text, Language lang, IWin32Window win32Window = null)
        {
            PPMessageForm mf = new PPMessageForm(text, lang);
            if (win32Window == null || (Form.FromHandle(win32Window.Handle) as Form).WindowState == FormWindowState.Minimized)
            {
                mf.StartPosition = FormStartPosition.CenterScreen;
                return mf.ShowDialog();
            }
            else
            {
                return mf.ShowDialog(win32Window);
            }
        }

        public static DialogResult Show(string text, int millisecond, Language lang, IWin32Window win32Window = null)
        {
            PPMessageForm mf = new PPMessageForm(text, millisecond, lang);
            if (win32Window == null || (Form.FromHandle(win32Window.Handle) as Form).WindowState == FormWindowState.Minimized)
            {
                mf.StartPosition = FormStartPosition.CenterScreen;
                return mf.ShowDialog();
            }
            else
            {
                return mf.ShowDialog(win32Window);
            }
        }

        public static DialogResult Show(string text, MessageBoxButtons button, Language lang, IWin32Window win32Window = null)
        {
            PPMessageForm mf = new PPMessageForm(text, button, lang);
            if (win32Window == null || (Form.FromHandle(win32Window.Handle) as Form).WindowState == FormWindowState.Minimized)
            {
                mf.StartPosition = FormStartPosition.CenterScreen;
                return mf.ShowDialog();
            }
            else
            {
                return mf.ShowDialog(win32Window);
            }
        }

        public static DialogResult Show(string text, MessageBoxButtons button, MessageBoxIcon icon, Language lang, IWin32Window win32Window = null)
        {
            PPMessageForm mf = new PPMessageForm(text, button, icon, lang);
            if (win32Window == null || (Form.FromHandle(win32Window.Handle) as Form).WindowState == FormWindowState.Minimized)
            {
                mf.StartPosition = FormStartPosition.CenterScreen;
                return mf.ShowDialog();
            }
            else
            {
                return mf.ShowDialog(win32Window);
            }
        }

        public static DialogResult Show(string text, string title, Language lang, IWin32Window win32Window = null)
        {
            PPMessageForm mf = new PPMessageForm(text, title, lang);
            if (win32Window == null || (Form.FromHandle(win32Window.Handle) as Form).WindowState == FormWindowState.Minimized)
            {
                mf.StartPosition = FormStartPosition.CenterScreen;
                return mf.ShowDialog();
            }
            else
            {
                return mf.ShowDialog(win32Window);
            }
        }

        public static DialogResult Show(string text, string title, int millisecond, Language lang, IWin32Window win32Window = null)
        {
            PPMessageForm mf = new PPMessageForm(text, title, millisecond, lang);
            if (win32Window == null || (Form.FromHandle(win32Window.Handle) as Form).WindowState == FormWindowState.Minimized)
            {
                mf.StartPosition = FormStartPosition.CenterScreen;
                return mf.ShowDialog();
            }
            else
            {
                return mf.ShowDialog(win32Window);
            }
        }

        public static DialogResult Show(string text, string title, MessageBoxButtons button, Language lang, IWin32Window win32Window = null)
        {
            PPMessageForm mf = new PPMessageForm(text, title, button, lang);
            if (win32Window == null || (Form.FromHandle(win32Window.Handle) as Form).WindowState == FormWindowState.Minimized)
            {
                mf.StartPosition = FormStartPosition.CenterScreen;
                return mf.ShowDialog();
            }
            else
            {
                return mf.ShowDialog(win32Window);
            }
        }

        public static DialogResult Show(string text, string title, MessageBoxButtons button, MessageBoxIcon icon, Language lang, IWin32Window win32Window = null)
        {
            PPMessageForm mf = new PPMessageForm(text, title, button, icon, lang);
            if (win32Window == null || (Form.FromHandle(win32Window.Handle) as Form).WindowState == FormWindowState.Minimized)
            {
                mf.StartPosition = FormStartPosition.CenterScreen;
                return mf.ShowDialog();
            }
            else
            {
                return mf.ShowDialog(win32Window);
            }
        }
    }
}