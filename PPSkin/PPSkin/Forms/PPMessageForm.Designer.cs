﻿namespace PPSkin
{
    partial class PPMessageForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ppButton_OK = new PPSkin.PPButton();
            this.ppButton_YES = new PPSkin.PPButton();
            this.ppButton_NO = new PPSkin.PPButton();
            this.ppButton_RETRY = new PPSkin.PPButton();
            this.ppButton_CANCLE = new PPSkin.PPButton();
            this.ppButton_IGNORE = new PPSkin.PPButton();
            this.ppButton_ABORT = new PPSkin.PPButton();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // ppButton_OK
            // 
            this.ppButton_OK.BackColor = System.Drawing.Color.White;
            this.ppButton_OK.DialogResult = System.Windows.Forms.DialogResult.None;
            this.ppButton_OK.IcoDown = null;
            this.ppButton_OK.IcoIn = null;
            this.ppButton_OK.IcoRegular = null;
            this.ppButton_OK.IcoSize = new System.Drawing.Size(5, 5);
            this.ppButton_OK.LineWidth = 1;
            this.ppButton_OK.Location = new System.Drawing.Point(21, 41);
            this.ppButton_OK.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.ppButton_OK.MouseDownBaseColor = System.Drawing.Color.LightGray;
            this.ppButton_OK.MouseDownLineColor = System.Drawing.Color.Silver;
            this.ppButton_OK.MouseDownTextColor = System.Drawing.Color.Black;
            this.ppButton_OK.MouseInBaseColor = System.Drawing.Color.LightGray;
            this.ppButton_OK.MouseInLineColor = System.Drawing.Color.Silver;
            this.ppButton_OK.MouseInTextColor = System.Drawing.Color.Black;
            this.ppButton_OK.Name = "ppButton_OK";
            this.ppButton_OK.Radius = 3;
            this.ppButton_OK.RegularBaseColor = System.Drawing.Color.White;
            this.ppButton_OK.RegularLineColor = System.Drawing.Color.LightGray;
            this.ppButton_OK.RegularTextColor = System.Drawing.Color.Black;
            this.ppButton_OK.Size = new System.Drawing.Size(70, 30);
            this.ppButton_OK.TabIndex = 3;
            this.ppButton_OK.Text = "OK";
            this.ppButton_OK.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.ppButton_OK.TextDrawMode = PPSkin.Enums.DrawMode.Anti;
            this.ppButton_OK.TipAutoCloseTime = 5000;
            this.ppButton_OK.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppButton_OK.TipCloseOnLeave = true;
            this.ppButton_OK.TipDeviation = new System.Drawing.Size(0, 0);
            this.ppButton_OK.TipFontSize = 10;
            this.ppButton_OK.TipFontText = null;
            this.ppButton_OK.TipForeColor = System.Drawing.Color.White;
            this.ppButton_OK.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.ppButton_OK.TipText = null;
            this.ppButton_OK.TipTopMoust = true;
            this.ppButton_OK.Enabled = false;
            this.ppButton_OK.Xoffset = 0;
            this.ppButton_OK.XoffsetIco = 0;
            this.ppButton_OK.Yoffset = 0;
            this.ppButton_OK.YoffsetIco = 0;
            this.ppButton_OK.Click += new System.EventHandler(this.ppButton_OK_Click);
            // 
            // ppButton_YES
            // 
            this.ppButton_YES.BackColor = System.Drawing.Color.White;
            this.ppButton_YES.DialogResult = System.Windows.Forms.DialogResult.None;
            this.ppButton_YES.IcoDown = null;
            this.ppButton_YES.IcoIn = null;
            this.ppButton_YES.IcoRegular = null;
            this.ppButton_YES.IcoSize = new System.Drawing.Size(5, 5);
            this.ppButton_YES.LineWidth = 1;
            this.ppButton_YES.Location = new System.Drawing.Point(128, 41);
            this.ppButton_YES.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.ppButton_YES.MouseDownBaseColor = System.Drawing.Color.LightGray;
            this.ppButton_YES.MouseDownLineColor = System.Drawing.Color.Silver;
            this.ppButton_YES.MouseDownTextColor = System.Drawing.Color.Black;
            this.ppButton_YES.MouseInBaseColor = System.Drawing.Color.LightGray;
            this.ppButton_YES.MouseInLineColor = System.Drawing.Color.Silver;
            this.ppButton_YES.MouseInTextColor = System.Drawing.Color.Black;
            this.ppButton_YES.Name = "ppButton_YES";
            this.ppButton_YES.Radius = 3;
            this.ppButton_YES.RegularBaseColor = System.Drawing.Color.White;
            this.ppButton_YES.RegularLineColor = System.Drawing.Color.LightGray;
            this.ppButton_YES.RegularTextColor = System.Drawing.Color.Black;
            this.ppButton_YES.Size = new System.Drawing.Size(70, 30);
            this.ppButton_YES.TabIndex = 4;
            this.ppButton_YES.Text = "YES";
            this.ppButton_YES.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.ppButton_YES.TextDrawMode = PPSkin.Enums.DrawMode.Anti;
            this.ppButton_YES.TipAutoCloseTime = 5000;
            this.ppButton_YES.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppButton_YES.TipCloseOnLeave = true;
            this.ppButton_YES.TipDeviation = new System.Drawing.Size(0, 0);
            this.ppButton_YES.TipFontSize = 10;
            this.ppButton_YES.TipFontText = null;
            this.ppButton_YES.TipForeColor = System.Drawing.Color.White;
            this.ppButton_YES.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.ppButton_YES.TipText = null;
            this.ppButton_YES.TipTopMoust = true;
            this.ppButton_YES.Enabled = false;
            this.ppButton_YES.Xoffset = 0;
            this.ppButton_YES.XoffsetIco = 0;
            this.ppButton_YES.Yoffset = 0;
            this.ppButton_YES.YoffsetIco = 0;
            this.ppButton_YES.Click += new System.EventHandler(this.ppButton_YES_Click);
            // 
            // ppButton_NO
            // 
            this.ppButton_NO.BackColor = System.Drawing.Color.White;
            this.ppButton_NO.DialogResult = System.Windows.Forms.DialogResult.None;
            this.ppButton_NO.IcoDown = null;
            this.ppButton_NO.IcoIn = null;
            this.ppButton_NO.IcoRegular = null;
            this.ppButton_NO.IcoSize = new System.Drawing.Size(5, 5);
            this.ppButton_NO.LineWidth = 1;
            this.ppButton_NO.Location = new System.Drawing.Point(230, 41);
            this.ppButton_NO.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.ppButton_NO.MouseDownBaseColor = System.Drawing.Color.LightGray;
            this.ppButton_NO.MouseDownLineColor = System.Drawing.Color.Silver;
            this.ppButton_NO.MouseDownTextColor = System.Drawing.Color.Black;
            this.ppButton_NO.MouseInBaseColor = System.Drawing.Color.Tomato;
            this.ppButton_NO.MouseInLineColor = System.Drawing.Color.Silver;
            this.ppButton_NO.MouseInTextColor = System.Drawing.Color.Black;
            this.ppButton_NO.Name = "ppButton_NO";
            this.ppButton_NO.Radius = 3;
            this.ppButton_NO.RegularBaseColor = System.Drawing.Color.White;
            this.ppButton_NO.RegularLineColor = System.Drawing.Color.LightGray;
            this.ppButton_NO.RegularTextColor = System.Drawing.Color.Black;
            this.ppButton_NO.Size = new System.Drawing.Size(70, 30);
            this.ppButton_NO.TabIndex = 5;
            this.ppButton_NO.Text = "NO";
            this.ppButton_NO.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.ppButton_NO.TextDrawMode = PPSkin.Enums.DrawMode.Anti;
            this.ppButton_NO.TipAutoCloseTime = 5000;
            this.ppButton_NO.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppButton_NO.TipCloseOnLeave = true;
            this.ppButton_NO.TipDeviation = new System.Drawing.Size(0, 0);
            this.ppButton_NO.TipFontSize = 10;
            this.ppButton_NO.TipFontText = null;
            this.ppButton_NO.TipForeColor = System.Drawing.Color.White;
            this.ppButton_NO.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.ppButton_NO.TipText = null;
            this.ppButton_NO.TipTopMoust = true;
            this.ppButton_NO.Enabled = false;
            this.ppButton_NO.Xoffset = 0;
            this.ppButton_NO.XoffsetIco = 0;
            this.ppButton_NO.Yoffset = 0;
            this.ppButton_NO.YoffsetIco = 0;
            this.ppButton_NO.Click += new System.EventHandler(this.ppButton_NO_Click);
            // 
            // ppButton_RETRY
            // 
            this.ppButton_RETRY.BackColor = System.Drawing.Color.White;
            this.ppButton_RETRY.DialogResult = System.Windows.Forms.DialogResult.None;
            this.ppButton_RETRY.IcoDown = null;
            this.ppButton_RETRY.IcoIn = null;
            this.ppButton_RETRY.IcoRegular = null;
            this.ppButton_RETRY.IcoSize = new System.Drawing.Size(5, 5);
            this.ppButton_RETRY.LineWidth = 1;
            this.ppButton_RETRY.Location = new System.Drawing.Point(21, 81);
            this.ppButton_RETRY.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.ppButton_RETRY.MouseDownBaseColor = System.Drawing.Color.LightGray;
            this.ppButton_RETRY.MouseDownLineColor = System.Drawing.Color.Silver;
            this.ppButton_RETRY.MouseDownTextColor = System.Drawing.Color.Black;
            this.ppButton_RETRY.MouseInBaseColor = System.Drawing.Color.LightGray;
            this.ppButton_RETRY.MouseInLineColor = System.Drawing.Color.Silver;
            this.ppButton_RETRY.MouseInTextColor = System.Drawing.Color.Black;
            this.ppButton_RETRY.Name = "ppButton_RETRY";
            this.ppButton_RETRY.Radius = 3;
            this.ppButton_RETRY.RegularBaseColor = System.Drawing.Color.White;
            this.ppButton_RETRY.RegularLineColor = System.Drawing.Color.LightGray;
            this.ppButton_RETRY.RegularTextColor = System.Drawing.Color.Black;
            this.ppButton_RETRY.Size = new System.Drawing.Size(70, 30);
            this.ppButton_RETRY.TabIndex = 6;
            this.ppButton_RETRY.Text = "Retry";
            this.ppButton_RETRY.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.ppButton_RETRY.TextDrawMode = PPSkin.Enums.DrawMode.Anti;
            this.ppButton_RETRY.TipAutoCloseTime = 5000;
            this.ppButton_RETRY.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppButton_RETRY.TipCloseOnLeave = true;
            this.ppButton_RETRY.TipDeviation = new System.Drawing.Size(0, 0);
            this.ppButton_RETRY.TipFontSize = 10;
            this.ppButton_RETRY.TipFontText = null;
            this.ppButton_RETRY.TipForeColor = System.Drawing.Color.White;
            this.ppButton_RETRY.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.ppButton_RETRY.TipText = null;
            this.ppButton_RETRY.TipTopMoust = true;
            this.ppButton_RETRY.Enabled = false;
            this.ppButton_RETRY.Xoffset = 0;
            this.ppButton_RETRY.XoffsetIco = 0;
            this.ppButton_RETRY.Yoffset = 0;
            this.ppButton_RETRY.YoffsetIco = 0;
            this.ppButton_RETRY.Click += new System.EventHandler(this.ppButton_RETRY_Click);
            // 
            // ppButton_CANCLE
            // 
            this.ppButton_CANCLE.BackColor = System.Drawing.Color.White;
            this.ppButton_CANCLE.DialogResult = System.Windows.Forms.DialogResult.None;
            this.ppButton_CANCLE.IcoDown = null;
            this.ppButton_CANCLE.IcoIn = null;
            this.ppButton_CANCLE.IcoRegular = null;
            this.ppButton_CANCLE.IcoSize = new System.Drawing.Size(5, 5);
            this.ppButton_CANCLE.LineWidth = 1;
            this.ppButton_CANCLE.Location = new System.Drawing.Point(128, 81);
            this.ppButton_CANCLE.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.ppButton_CANCLE.MouseDownBaseColor = System.Drawing.Color.LightGray;
            this.ppButton_CANCLE.MouseDownLineColor = System.Drawing.Color.Silver;
            this.ppButton_CANCLE.MouseDownTextColor = System.Drawing.Color.Black;
            this.ppButton_CANCLE.MouseInBaseColor = System.Drawing.Color.Tomato;
            this.ppButton_CANCLE.MouseInLineColor = System.Drawing.Color.Silver;
            this.ppButton_CANCLE.MouseInTextColor = System.Drawing.Color.Black;
            this.ppButton_CANCLE.Name = "ppButton_CANCLE";
            this.ppButton_CANCLE.Radius = 3;
            this.ppButton_CANCLE.RegularBaseColor = System.Drawing.Color.White;
            this.ppButton_CANCLE.RegularLineColor = System.Drawing.Color.LightGray;
            this.ppButton_CANCLE.RegularTextColor = System.Drawing.Color.Black;
            this.ppButton_CANCLE.Size = new System.Drawing.Size(70, 30);
            this.ppButton_CANCLE.TabIndex = 7;
            this.ppButton_CANCLE.Text = "Cancle";
            this.ppButton_CANCLE.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.ppButton_CANCLE.TextDrawMode = PPSkin.Enums.DrawMode.Anti;
            this.ppButton_CANCLE.TipAutoCloseTime = 5000;
            this.ppButton_CANCLE.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppButton_CANCLE.TipCloseOnLeave = true;
            this.ppButton_CANCLE.TipDeviation = new System.Drawing.Size(0, 0);
            this.ppButton_CANCLE.TipFontSize = 10;
            this.ppButton_CANCLE.TipFontText = null;
            this.ppButton_CANCLE.TipForeColor = System.Drawing.Color.White;
            this.ppButton_CANCLE.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.ppButton_CANCLE.TipText = null;
            this.ppButton_CANCLE.TipTopMoust = true;
            this.ppButton_CANCLE.Enabled = false;
            this.ppButton_CANCLE.Xoffset = 0;
            this.ppButton_CANCLE.XoffsetIco = 0;
            this.ppButton_CANCLE.Yoffset = 0;
            this.ppButton_CANCLE.YoffsetIco = 0;
            this.ppButton_CANCLE.Click += new System.EventHandler(this.ppButton_CANCLE_Click);
            // 
            // ppButton_IGNORE
            // 
            this.ppButton_IGNORE.BackColor = System.Drawing.Color.White;
            this.ppButton_IGNORE.DialogResult = System.Windows.Forms.DialogResult.None;
            this.ppButton_IGNORE.IcoDown = null;
            this.ppButton_IGNORE.IcoIn = null;
            this.ppButton_IGNORE.IcoRegular = null;
            this.ppButton_IGNORE.IcoSize = new System.Drawing.Size(5, 5);
            this.ppButton_IGNORE.LineWidth = 1;
            this.ppButton_IGNORE.Location = new System.Drawing.Point(230, 81);
            this.ppButton_IGNORE.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.ppButton_IGNORE.MouseDownBaseColor = System.Drawing.Color.LightGray;
            this.ppButton_IGNORE.MouseDownLineColor = System.Drawing.Color.Silver;
            this.ppButton_IGNORE.MouseDownTextColor = System.Drawing.Color.Black;
            this.ppButton_IGNORE.MouseInBaseColor = System.Drawing.Color.LightGray;
            this.ppButton_IGNORE.MouseInLineColor = System.Drawing.Color.Silver;
            this.ppButton_IGNORE.MouseInTextColor = System.Drawing.Color.Black;
            this.ppButton_IGNORE.Name = "ppButton_IGNORE";
            this.ppButton_IGNORE.Radius = 3;
            this.ppButton_IGNORE.RegularBaseColor = System.Drawing.Color.White;
            this.ppButton_IGNORE.RegularLineColor = System.Drawing.Color.LightGray;
            this.ppButton_IGNORE.RegularTextColor = System.Drawing.Color.Black;
            this.ppButton_IGNORE.Size = new System.Drawing.Size(70, 30);
            this.ppButton_IGNORE.TabIndex = 8;
            this.ppButton_IGNORE.Text = "Ignore";
            this.ppButton_IGNORE.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.ppButton_IGNORE.TextDrawMode = PPSkin.Enums.DrawMode.Anti;
            this.ppButton_IGNORE.TipAutoCloseTime = 5000;
            this.ppButton_IGNORE.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppButton_IGNORE.TipCloseOnLeave = true;
            this.ppButton_IGNORE.TipDeviation = new System.Drawing.Size(0, 0);
            this.ppButton_IGNORE.TipFontSize = 10;
            this.ppButton_IGNORE.TipFontText = null;
            this.ppButton_IGNORE.TipForeColor = System.Drawing.Color.White;
            this.ppButton_IGNORE.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.ppButton_IGNORE.TipText = null;
            this.ppButton_IGNORE.TipTopMoust = true;
            this.ppButton_IGNORE.Enabled = false;
            this.ppButton_IGNORE.Xoffset = 0;
            this.ppButton_IGNORE.XoffsetIco = 0;
            this.ppButton_IGNORE.Yoffset = 0;
            this.ppButton_IGNORE.YoffsetIco = 0;
            this.ppButton_IGNORE.Click += new System.EventHandler(this.ppButton_IGNORE_Click);
            // 
            // ppButton_ABORT
            // 
            this.ppButton_ABORT.BackColor = System.Drawing.Color.White;
            this.ppButton_ABORT.DialogResult = System.Windows.Forms.DialogResult.None;
            this.ppButton_ABORT.IcoDown = null;
            this.ppButton_ABORT.IcoIn = null;
            this.ppButton_ABORT.IcoRegular = null;
            this.ppButton_ABORT.IcoSize = new System.Drawing.Size(5, 5);
            this.ppButton_ABORT.LineWidth = 1;
            this.ppButton_ABORT.Location = new System.Drawing.Point(21, 121);
            this.ppButton_ABORT.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.ppButton_ABORT.MouseDownBaseColor = System.Drawing.Color.LightGray;
            this.ppButton_ABORT.MouseDownLineColor = System.Drawing.Color.Silver;
            this.ppButton_ABORT.MouseDownTextColor = System.Drawing.Color.Black;
            this.ppButton_ABORT.MouseInBaseColor = System.Drawing.Color.Tomato;
            this.ppButton_ABORT.MouseInLineColor = System.Drawing.Color.Silver;
            this.ppButton_ABORT.MouseInTextColor = System.Drawing.Color.Black;
            this.ppButton_ABORT.Name = "ppButton_ABORT";
            this.ppButton_ABORT.Radius = 3;
            this.ppButton_ABORT.RegularBaseColor = System.Drawing.Color.White;
            this.ppButton_ABORT.RegularLineColor = System.Drawing.Color.LightGray;
            this.ppButton_ABORT.RegularTextColor = System.Drawing.Color.Black;
            this.ppButton_ABORT.Size = new System.Drawing.Size(70, 30);
            this.ppButton_ABORT.TabIndex = 9;
            this.ppButton_ABORT.Text = "Abort";
            this.ppButton_ABORT.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.ppButton_ABORT.TextDrawMode = PPSkin.Enums.DrawMode.Anti;
            this.ppButton_ABORT.TipAutoCloseTime = 5000;
            this.ppButton_ABORT.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppButton_ABORT.TipCloseOnLeave = true;
            this.ppButton_ABORT.TipDeviation = new System.Drawing.Size(0, 0);
            this.ppButton_ABORT.TipFontSize = 10;
            this.ppButton_ABORT.TipFontText = null;
            this.ppButton_ABORT.TipForeColor = System.Drawing.Color.White;
            this.ppButton_ABORT.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.ppButton_ABORT.TipText = null;
            this.ppButton_ABORT.TipTopMoust = true;
            this.ppButton_ABORT.Enabled = false;
            this.ppButton_ABORT.Xoffset = 0;
            this.ppButton_ABORT.XoffsetIco = 0;
            this.ppButton_ABORT.Yoffset = 0;
            this.ppButton_ABORT.YoffsetIco = 0;
            this.ppButton_ABORT.Click += new System.EventHandler(this.ppButton_ABORT_Click);
            // 
            // timer
            // 
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // MessageForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(320, 180);
            this.Controls.Add(this.ppButton_ABORT);
            this.Controls.Add(this.ppButton_IGNORE);
            this.Controls.Add(this.ppButton_CANCLE);
            this.Controls.Add(this.ppButton_RETRY);
            this.Controls.Add(this.ppButton_NO);
            this.Controls.Add(this.ppButton_YES);
            this.Controls.Add(this.ppButton_OK);
            this.EnableDoubleCilckSizeChange = false;
            this.EnableSizeChange = false;
            this.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.Location = new System.Drawing.Point(0, 0);
            this.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MouseMovePosition = PPSkin.PPForm.MovePosition.TITLE;
            this.Name = "MessageForm";
            this.Radius = 0;
            this.ShadowColor = System.Drawing.Color.DimGray;
            this.ShowIcon = false;
            this.ResumeLayout(false);

        }

        #endregion

        private PPButton ppButton_OK;
        private PPButton ppButton_YES;
        private PPButton ppButton_NO;
        private PPButton ppButton_RETRY;
        private PPButton ppButton_CANCLE;
        private PPButton ppButton_IGNORE;
        private PPButton ppButton_ABORT;
        private System.Windows.Forms.Timer timer;
    }
}