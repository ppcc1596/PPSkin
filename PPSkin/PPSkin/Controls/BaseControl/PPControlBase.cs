﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace PPSkin
{
    public partial class PPControlBase : UserControl,IContainer
    {
        private int radius = 5;
        private float borderWidth = 1;
        private Color baseColor = Control.DefaultBackColor;
        private Color borderColor = Color.LightGray;
        private bool showBorder = true;
        private Enums.RoundStyle roundStyle = Enums.RoundStyle.All;

        /// <summary>
        /// 圆角半径
        /// </summary>
        [Description("圆角半径"), Category("PPSkin")]
        public int Radius
        {
            get { return radius; }
            set
            {
                if (radius < 0)
                    return;
                int copy = radius;
                radius = value;
                this.Invalidate();
                this.Region = new Region(PaintHelper.CreatePath(new Rectangle(0, 0, this.Width, this.Height), radius, roundStyle));
                if (copy != radius)
                {
                    RadiusChanged?.Invoke(this, new EventArgs());
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Description("圆角类型"), Category("PPSkin")]
        public Enums.RoundStyle RoundStyle
        {
            get { return roundStyle; }
            set
            {
                roundStyle = value;
                this.Invalidate();
            }
        }

        /// <summary>
        /// 边框宽度
        /// </summary>
        [Description("边框宽度"), Category("PPSkin")]
        public float BorderWidth
        {
            get { return borderWidth; }
            set
            {
                if (value <= 0)
                    return;
                borderWidth = value;// % 2 == 0 ? value + 1 : value;
                this.Invalidate();
            }
        }

        /// <summary>
        /// 背景色
        /// </summary>
        [Description("背景色"), Category("PPSkin")]
        public Color BaseColor
        {
            get { return baseColor; }
            set
            {
                baseColor = value;
                this.Invalidate();
                if (BaseColorChanged != null)
                {
                    BaseColorChanged(this, new EventArgs());
                }
            }
        }

        /// <summary>
        /// 边框色
        /// </summary>
        [Description("边框色"), Category("PPSkin")]
        public Color BorderColor
        {
            get { return borderColor; }
            set
            {
                borderColor = value;
                this.Invalidate();
                if (LineColorChanged != null)
                {
                    LineColorChanged(this, new EventArgs());
                }
            }
        }

        [Description("是否显示边框"), Category("PPSkin")]
        public bool ShowBorder
        {
            get { return showBorder; }
            set
            {
                showBorder = value;
                this.Invalidate();
            }
        }

        public ComponentCollection Components => throw new NotImplementedException();

        /// <summary>
        /// 尺寸变化事件
        /// </summary>
        [Description("尺寸变化事件")]
        public event EventHandler SizeChangeded;

        /// <summary>
        /// 圆角直径变化事件
        /// </summary>
        [Description("圆角直径变化事件")]
        public event EventHandler RadiusChanged;

        /// <summary>
        /// 内部颜色变化事件
        /// </summary>
        [Description("内部颜色变化事件")]
        public event EventHandler BaseColorChanged;

        /// <summary>
        /// 边框颜色变化事件
        /// </summary>
        [Description("边框颜色变化事件")]
        public event EventHandler LineColorChanged;

        public PPControlBase()
        {
            InitializeComponent();
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.DoubleBuffer, true);
            this.SetStyle(ControlStyles.ResizeRedraw, true);
            this.SetStyle(ControlStyles.Selectable, true);
            this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            this.SetStyle(ControlStyles.UserPaint, true);
            this.Region = new Region(PaintHelper.CreatePath(new Rectangle(0, 0, this.Width, this.Height), radius,roundStyle));
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            int width = this.Width;
            int height = this.Height;
            //int halflinewidth = borderWidth % 2 == 0 ? borderWidth / 2 : borderWidth / 2 + 1;
            Graphics g = e.Graphics;
            g.Clear(BackColor);
            PaintHelper.SetGraphicsHighQuality(g);
            Pen pen = new Pen(borderColor, borderWidth);
            SolidBrush brush = new SolidBrush(baseColor);

            GraphicsPath path = PaintHelper.CreatePath(new Rectangle(0, 0, this.Width, this.Height), radius,roundStyle);
            GraphicsPath path2 = PaintHelper.CreatePath(new RectangleF(borderWidth, borderWidth, this.Width - 2f * borderWidth, this.Height - 2f * borderWidth), radius,roundStyle);
            g.FillPath(brush, path2);
            if (showBorder)
            {
                g.DrawPath(pen, path2);
            }

            g.DrawPath(new Pen(this.BackColor), path);
        }

        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);
            this.Region = new Region(PaintHelper.CreatePath(new Rectangle(0, 0, this.Width, this.Height), radius, roundStyle));
            if (SizeChangeded != null)
            {
                SizeChangeded(this, e);
            }
        }

        public void Add(IComponent component)
        {
            components.Add(component);
        }

        public void Add(IComponent component, string name)
        {
            components.Add(component, name);
        }

        public void Remove(IComponent component)
        {
            components.Remove(component);
        }
    }
}