﻿
namespace PPSkin
{
    partial class PPDateTimeDropForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ppDateYearPanel1 = new PPSkin.PPDateYearPanel();
            this.ppDateMonthPanel1 = new PPSkin.PPDateMonthPanel();
            this.ppDateDayPanel1 = new PPSkin.PPDateDayPanel();
            this.Button_today = new PPSkin.PPButton();
            this.Button_forward = new PPSkin.PPButton();
            this.Button_backward = new PPSkin.PPButton();
            this.Button_Top = new PPSkin.PPButton();
            this.ppTimePicker1 = new PPSkin.PPTimePicker();
            this.btn_confirm = new PPSkin.PPButton();
            this.SuspendLayout();
            // 
            // ppDateYearPanel1
            // 
            this.ppDateYearPanel1.BackColor = System.Drawing.Color.White;
            this.ppDateYearPanel1.DateTime = new System.DateTime(2021, 3, 30, 15, 48, 54, 710);
            this.ppDateYearPanel1.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.ppDateYearPanel1.Location = new System.Drawing.Point(5, 40);
            this.ppDateYearPanel1.Name = "ppDateYearPanel1";
            this.ppDateYearPanel1.Size = new System.Drawing.Size(350, 220);
            this.ppDateYearPanel1.TabIndex = 6;
            this.ppDateYearPanel1.YearClick += new System.EventHandler<PPSkin.YearClickEventArgs>(this.ppDateYearPanel1_YearClick);
            // 
            // ppDateMonthPanel1
            // 
            this.ppDateMonthPanel1.BackColor = System.Drawing.Color.White;
            this.ppDateMonthPanel1.DateTime = new System.DateTime(2021, 3, 30, 15, 48, 54, 716);
            this.ppDateMonthPanel1.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.ppDateMonthPanel1.Location = new System.Drawing.Point(5, 40);
            this.ppDateMonthPanel1.Name = "ppDateMonthPanel1";
            this.ppDateMonthPanel1.Size = new System.Drawing.Size(350, 220);
            this.ppDateMonthPanel1.TabIndex = 5;
            this.ppDateMonthPanel1.Visible = false;
            this.ppDateMonthPanel1.MonthClick += new System.EventHandler<PPSkin.MonthClickEventArgs>(this.ppDateMonthPanel1_MonthClick);
            // 
            // ppDateDayPanel1
            // 
            this.ppDateDayPanel1.BackColor = System.Drawing.Color.White;
            this.ppDateDayPanel1.DateTime = new System.DateTime(((long)(0)));
            this.ppDateDayPanel1.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.ppDateDayPanel1.Location = new System.Drawing.Point(5, 40);
            this.ppDateDayPanel1.Name = "ppDateDayPanel1";
            this.ppDateDayPanel1.Size = new System.Drawing.Size(350, 219);
            this.ppDateDayPanel1.TabIndex = 4;
            this.ppDateDayPanel1.DayClick += new System.EventHandler<PPSkin.DayClickEventArgs>(this.ppDateDayPanel1_DayClick);
            // 
            // Button_today
            // 
            this.Button_today.DialogResult = System.Windows.Forms.DialogResult.None;
            this.Button_today.IcoDown = null;
            this.Button_today.IcoIn = null;
            this.Button_today.IcoRegular = null;
            this.Button_today.IcoSize = new System.Drawing.Size(15, 15);
            this.Button_today.LineWidth = 1;
            this.Button_today.Location = new System.Drawing.Point(15, 266);
            this.Button_today.MouseDownBaseColor = System.Drawing.Color.LightGray;
            this.Button_today.MouseDownLineColor = System.Drawing.Color.Silver;
            this.Button_today.MouseDownTextColor = System.Drawing.Color.Black;
            this.Button_today.MouseInBaseColor = System.Drawing.Color.LightGray;
            this.Button_today.MouseInLineColor = System.Drawing.Color.Silver;
            this.Button_today.MouseInTextColor = System.Drawing.Color.Black;
            this.Button_today.Name = "Button_today";
            this.Button_today.Radius = 3;
            this.Button_today.RegularBaseColor = System.Drawing.Color.White;
            this.Button_today.RegularLineColor = System.Drawing.Color.LightGray;
            this.Button_today.RegularTextColor = System.Drawing.Color.Black;
            this.Button_today.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.Button_today.Size = new System.Drawing.Size(114, 30);
            this.Button_today.TabIndex = 3;
            this.Button_today.Text = "现在:2021-03-30";
            this.Button_today.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.Button_today.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.Button_today.TipAutoCloseTime = 5000;
            this.Button_today.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.Button_today.TipCloseOnLeave = true;
            this.Button_today.TipDeviation = new System.Drawing.Size(0, 0);
            this.Button_today.TipFontSize = 10;
            this.Button_today.TipFontText = null;
            this.Button_today.TipForeColor = System.Drawing.Color.White;
            this.Button_today.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.Button_today.TipText = null;
            this.Button_today.TipTopMoust = true;
            this.Button_today.Xoffset = 0;
            this.Button_today.XoffsetIco = 0;
            this.Button_today.Yoffset = 0;
            this.Button_today.YoffsetIco = 0;
            this.Button_today.Click += new System.EventHandler(this.Button_today_Click);
            // 
            // Button_forward
            // 
            this.Button_forward.DialogResult = System.Windows.Forms.DialogResult.None;
            this.Button_forward.IcoDown = global::PPSkin.PPSkinResources.rightBlue;
            this.Button_forward.IcoIn = global::PPSkin.PPSkinResources.rightBlue;
            this.Button_forward.IcoRegular = global::PPSkin.PPSkinResources.right;
            this.Button_forward.IcoSize = new System.Drawing.Size(12, 12);
            this.Button_forward.LineWidth = 1;
            this.Button_forward.Location = new System.Drawing.Point(261, 4);
            this.Button_forward.MouseDownBaseColor = System.Drawing.Color.LightGray;
            this.Button_forward.MouseDownLineColor = System.Drawing.Color.Silver;
            this.Button_forward.MouseDownTextColor = System.Drawing.Color.Black;
            this.Button_forward.MouseInBaseColor = System.Drawing.Color.LightGray;
            this.Button_forward.MouseInLineColor = System.Drawing.Color.Silver;
            this.Button_forward.MouseInTextColor = System.Drawing.Color.Black;
            this.Button_forward.Name = "Button_forward";
            this.Button_forward.Radius = 3;
            this.Button_forward.RegularBaseColor = System.Drawing.Color.White;
            this.Button_forward.RegularLineColor = System.Drawing.Color.LightGray;
            this.Button_forward.RegularTextColor = System.Drawing.Color.Black;
            this.Button_forward.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.Button_forward.Size = new System.Drawing.Size(30, 30);
            this.Button_forward.TabIndex = 2;
            this.Button_forward.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.Button_forward.TextDrawMode = PPSkin.Enums.DrawMode.Anti;
            this.Button_forward.TipAutoCloseTime = 5000;
            this.Button_forward.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.Button_forward.TipCloseOnLeave = true;
            this.Button_forward.TipDeviation = new System.Drawing.Size(0, 0);
            this.Button_forward.TipFontSize = 10;
            this.Button_forward.TipFontText = null;
            this.Button_forward.TipForeColor = System.Drawing.Color.White;
            this.Button_forward.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.Button_forward.TipText = null;
            this.Button_forward.TipTopMoust = true;
            this.Button_forward.Xoffset = 0;
            this.Button_forward.XoffsetIco = 9;
            this.Button_forward.Yoffset = 0;
            this.Button_forward.YoffsetIco = 9;
            this.Button_forward.Click += new System.EventHandler(this.Button_forward_Click);
            // 
            // Button_backward
            // 
            this.Button_backward.DialogResult = System.Windows.Forms.DialogResult.None;
            this.Button_backward.IcoDown = global::PPSkin.PPSkinResources.leftBlue;
            this.Button_backward.IcoIn = global::PPSkin.PPSkinResources.leftBlue;
            this.Button_backward.IcoRegular = global::PPSkin.PPSkinResources.left;
            this.Button_backward.IcoSize = new System.Drawing.Size(12, 12);
            this.Button_backward.LineWidth = 1;
            this.Button_backward.Location = new System.Drawing.Point(69, 4);
            this.Button_backward.MouseDownBaseColor = System.Drawing.Color.LightGray;
            this.Button_backward.MouseDownLineColor = System.Drawing.Color.Silver;
            this.Button_backward.MouseDownTextColor = System.Drawing.Color.Black;
            this.Button_backward.MouseInBaseColor = System.Drawing.Color.LightGray;
            this.Button_backward.MouseInLineColor = System.Drawing.Color.Silver;
            this.Button_backward.MouseInTextColor = System.Drawing.Color.Black;
            this.Button_backward.Name = "Button_backward";
            this.Button_backward.Radius = 3;
            this.Button_backward.RegularBaseColor = System.Drawing.Color.White;
            this.Button_backward.RegularLineColor = System.Drawing.Color.LightGray;
            this.Button_backward.RegularTextColor = System.Drawing.Color.Black;
            this.Button_backward.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.Button_backward.Size = new System.Drawing.Size(30, 30);
            this.Button_backward.TabIndex = 1;
            this.Button_backward.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.Button_backward.TextDrawMode = PPSkin.Enums.DrawMode.Anti;
            this.Button_backward.TipAutoCloseTime = 5000;
            this.Button_backward.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.Button_backward.TipCloseOnLeave = true;
            this.Button_backward.TipDeviation = new System.Drawing.Size(0, 0);
            this.Button_backward.TipFontSize = 10;
            this.Button_backward.TipFontText = null;
            this.Button_backward.TipForeColor = System.Drawing.Color.White;
            this.Button_backward.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.Button_backward.TipText = null;
            this.Button_backward.TipTopMoust = true;
            this.Button_backward.Xoffset = 0;
            this.Button_backward.XoffsetIco = 9;
            this.Button_backward.Yoffset = 0;
            this.Button_backward.YoffsetIco = 9;
            this.Button_backward.Click += new System.EventHandler(this.Button_backward_Click);
            // 
            // Button_Top
            // 
            this.Button_Top.DialogResult = System.Windows.Forms.DialogResult.None;
            this.Button_Top.IcoDown = null;
            this.Button_Top.IcoIn = null;
            this.Button_Top.IcoRegular = null;
            this.Button_Top.IcoSize = new System.Drawing.Size(15, 15);
            this.Button_Top.LineWidth = 1;
            this.Button_Top.Location = new System.Drawing.Point(105, 4);
            this.Button_Top.MouseDownBaseColor = System.Drawing.Color.LightGray;
            this.Button_Top.MouseDownLineColor = System.Drawing.Color.Silver;
            this.Button_Top.MouseDownTextColor = System.Drawing.Color.Black;
            this.Button_Top.MouseInBaseColor = System.Drawing.Color.LightGray;
            this.Button_Top.MouseInLineColor = System.Drawing.Color.Silver;
            this.Button_Top.MouseInTextColor = System.Drawing.Color.Black;
            this.Button_Top.Name = "Button_Top";
            this.Button_Top.Radius = 3;
            this.Button_Top.RegularBaseColor = System.Drawing.Color.White;
            this.Button_Top.RegularLineColor = System.Drawing.Color.LightGray;
            this.Button_Top.RegularTextColor = System.Drawing.Color.Black;
            this.Button_Top.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.Button_Top.Size = new System.Drawing.Size(150, 30);
            this.Button_Top.TabIndex = 0;
            this.Button_Top.Text = "2021-03";
            this.Button_Top.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.Button_Top.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.Button_Top.TipAutoCloseTime = 5000;
            this.Button_Top.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.Button_Top.TipCloseOnLeave = true;
            this.Button_Top.TipDeviation = new System.Drawing.Size(0, 0);
            this.Button_Top.TipFontSize = 10;
            this.Button_Top.TipFontText = null;
            this.Button_Top.TipForeColor = System.Drawing.Color.White;
            this.Button_Top.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.Button_Top.TipText = null;
            this.Button_Top.TipTopMoust = true;
            this.Button_Top.Xoffset = 0;
            this.Button_Top.XoffsetIco = 0;
            this.Button_Top.Yoffset = 0;
            this.Button_Top.YoffsetIco = 0;
            this.Button_Top.Click += new System.EventHandler(this.Button_Top_Click);
            // 
            // ppTimePicker1
            // 
            this.ppTimePicker1.BackColor = System.Drawing.Color.White;
            this.ppTimePicker1.Location = new System.Drawing.Point(137, 267);
            this.ppTimePicker1.Name = "ppTimePicker1";
            this.ppTimePicker1.Size = new System.Drawing.Size(118, 29);
            this.ppTimePicker1.TabIndex = 7;
            this.ppTimePicker1.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.ppTimePicker1.Time = new System.DateTime(((long)(0)));
            this.ppTimePicker1.UseAnime = true;
            // 
            // btn_confirm
            // 
            this.btn_confirm.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btn_confirm.IcoDown = null;
            this.btn_confirm.IcoIn = null;
            this.btn_confirm.IcoRegular = null;
            this.btn_confirm.IcoSize = new System.Drawing.Size(15, 15);
            this.btn_confirm.LineWidth = 1;
            this.btn_confirm.Location = new System.Drawing.Point(263, 265);
            this.btn_confirm.MouseDownBaseColor = System.Drawing.Color.LightGray;
            this.btn_confirm.MouseDownLineColor = System.Drawing.Color.Silver;
            this.btn_confirm.MouseDownTextColor = System.Drawing.Color.Black;
            this.btn_confirm.MouseInBaseColor = System.Drawing.Color.LightGray;
            this.btn_confirm.MouseInLineColor = System.Drawing.Color.Silver;
            this.btn_confirm.MouseInTextColor = System.Drawing.Color.Black;
            this.btn_confirm.Name = "btn_confirm";
            this.btn_confirm.Radius = 3;
            this.btn_confirm.RegularBaseColor = System.Drawing.Color.White;
            this.btn_confirm.RegularLineColor = System.Drawing.Color.LightGray;
            this.btn_confirm.RegularTextColor = System.Drawing.Color.Black;
            this.btn_confirm.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.btn_confirm.Size = new System.Drawing.Size(81, 30);
            this.btn_confirm.TabIndex = 8;
            this.btn_confirm.Text = "确定";
            this.btn_confirm.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.btn_confirm.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.btn_confirm.TipAutoCloseTime = 5000;
            this.btn_confirm.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.btn_confirm.TipCloseOnLeave = true;
            this.btn_confirm.TipDeviation = new System.Drawing.Size(0, 0);
            this.btn_confirm.TipFontSize = 10;
            this.btn_confirm.TipFontText = null;
            this.btn_confirm.TipForeColor = System.Drawing.Color.White;
            this.btn_confirm.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.btn_confirm.TipText = null;
            this.btn_confirm.TipTopMoust = true;
            this.btn_confirm.Xoffset = 0;
            this.btn_confirm.XoffsetIco = 0;
            this.btn_confirm.Yoffset = 0;
            this.btn_confirm.YoffsetIco = 0;
            this.btn_confirm.Click += new System.EventHandler(this.btn_confirm_Click);
            // 
            // PPDateTimeDropForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(360, 301);
            this.Controls.Add(this.btn_confirm);
            this.Controls.Add(this.ppTimePicker1);
            this.Controls.Add(this.ppDateYearPanel1);
            this.Controls.Add(this.ppDateMonthPanel1);
            this.Controls.Add(this.ppDateDayPanel1);
            this.Controls.Add(this.Button_today);
            this.Controls.Add(this.Button_forward);
            this.Controls.Add(this.Button_backward);
            this.Controls.Add(this.Button_Top);
            this.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.Location = new System.Drawing.Point(0, 0);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PPDateTimeDropForm";
            this.Padding = new System.Windows.Forms.Padding(3);
            this.Radius = 5;
            this.ShowBorder = true;
            this.Text = "PPComboboxDropForm";
            this.ResumeLayout(false);

        }

        #endregion

        private PPButton Button_Top;
        private PPButton Button_backward;
        private PPButton Button_forward;
        private PPButton Button_today;
        private PPDateDayPanel ppDateDayPanel1;
        private PPDateMonthPanel ppDateMonthPanel1;
        private PPDateYearPanel ppDateYearPanel1;
        private PPTimePicker ppTimePicker1;
        private PPButton btn_confirm;
    }
}