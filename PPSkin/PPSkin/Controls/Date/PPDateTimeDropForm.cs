﻿using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace PPSkin
{
    public partial class PPDateTimeDropForm : FrmAnchorBase
    {
       
        public PPDateTimePicker MainDateTimePicker;

        private DateTime dateTime = DateTime.Now;

        private DateTime DateTime
        {
            get { return dateTime; }
            set
            {
                dateTime = value;
            }
        }

        private TopStatus topStatus = TopStatus.Day;

        private enum TopStatus
        {
            Day,
            Month,
            Year
        }

        private Lang language = Lang.CH;

        public Lang Language
        {
            get { return language; }
            set
            {
                language = value;
                ppDateDayPanel1.SetLang(language);
                ppDateMonthPanel1.SetLang(language);
                ppDateYearPanel1.SetLang(language);
            }
        }

        public enum Lang
        {
            CH,
            EN
        }

        

        //protected override bool ShowWithoutActivation => true;//确保此窗体不会被激活

        public PPDateTimeDropForm(PPDateTimePicker pPDateTimePicker)
        {
            InitializeComponent();

            if(!DesignMode)
            {
                base.BaseControl = pPDateTimePicker;
                MainDateTimePicker = base.BaseControl as PPDateTimePicker;
                if (!MainDateTimePicker.PickTime)
                {
                    Button_today.Location = new System.Drawing.Point(122, 266);
                    ppTimePicker1.Visible = false;
                    btn_confirm.Visible = false;
                }

                this.DateTime = MainDateTimePicker.Value;
                this.Language = MainDateTimePicker.Language;
                Button_today.Text = language == Lang.CH ? "现在:" + DateTime.Now.ToString("yyyy-MM-dd") : "Now:" + DateTime.Now.ToString("yyyy-MM-dd");
                btn_confirm.Text = language == Lang.CH ? "确认" : "Confirm";
                LoadTopStatus(TopStatus.Day);
            }

        }

        private void Button_backward_Click(object sender, EventArgs e)
        {
            if (topStatus == TopStatus.Day)
            {
                ppDateDayPanel1.LoadMonth(-1);
                Button_Top.Text = ppDateDayPanel1.DateTime.ToString("yyyy-MM");
            }
            else if (topStatus == TopStatus.Year)
            {
                Button_Top.Text = ppDateYearPanel1.LoadYear(-1);
            }
        }

        private void Button_forward_Click(object sender, EventArgs e)
        {
            if (topStatus == TopStatus.Day)
            {
                ppDateDayPanel1.LoadMonth(1);
                Button_Top.Text = ppDateDayPanel1.DateTime.ToString("yyyy-MM");
            }
            else if (topStatus == TopStatus.Year)
            {
                Button_Top.Text = ppDateYearPanel1.LoadYear(1);
            }
        }

        private void ppDateDayPanel1_DayClick(object sender, DayClickEventArgs e)
        {
            if(!MainDateTimePicker.PickTime)
            {
                MainDateTimePicker.Value = e.DateTime;
                this.Close();
            }
            else
            {
                this.DateTime = new DateTime(dateTime.Year, dateTime.Month, e.DateTime.Day, dateTime.Hour, dateTime.Minute, dateTime.Second);
            }
            
        }

        private void ppDateMonthPanel1_MonthClick(object sender, MonthClickEventArgs e)
        {
            this.DateTime = new DateTime(e.DateTime.Year, e.DateTime.Month, 1, dateTime.Hour, dateTime.Minute, dateTime.Second);
            LoadTopStatus(TopStatus.Day);
            ppDateDayPanel1.LoadMonth(0);
        }

        private void ppDateYearPanel1_YearClick(object sender, YearClickEventArgs e)
        {
            this.DateTime = new DateTime(e.DateTime.Year, dateTime.Month, 1, dateTime.Hour, dateTime.Minute, dateTime.Second);
            LoadTopStatus(TopStatus.Month);
            ppDateMonthPanel1.LoadMonth();
        }

        private void LoadTopStatus(TopStatus status)
        {
            switch (status)
            {
                case TopStatus.Day:
                    ppDateDayPanel1.DateTime = this.dateTime;
                    ppDateDayPanel1.Visible = true;
                    ppDateMonthPanel1.Visible = false;
                    ppDateYearPanel1.Visible = false;
                    this.topStatus = status;
                    Button_Top.Text = this.DateTime.ToString("yyyy-MM");
                    break;

                case TopStatus.Month:
                    ppDateMonthPanel1.DateTime = this.dateTime;
                    ppDateDayPanel1.Visible = false;
                    ppDateMonthPanel1.Visible = true;
                    ppDateYearPanel1.Visible = false;
                    this.topStatus = status;
                    Button_Top.Text = this.DateTime.ToString("yyyy");
                    break;

                case TopStatus.Year:
                    ppDateYearPanel1.DateTime = this.dateTime;
                    ppDateDayPanel1.Visible = false;
                    ppDateMonthPanel1.Visible = false;
                    ppDateYearPanel1.Visible = true;
                    ppDateYearPanel1.LoadYear(0);
                    this.topStatus = status;
                    Button_Top.Text = this.DateTime.ToString("yyyy");
                    break;
            }

            ppTimePicker1.Time = this.DateTime;
        }

        private void Button_Top_Click(object sender, EventArgs e)
        {
            switch (this.topStatus)
            {
                case TopStatus.Day:
                    LoadTopStatus(TopStatus.Month); break;
                case TopStatus.Month:
                    LoadTopStatus(TopStatus.Year); break;
            }
        }

        private void Button_today_Click(object sender, EventArgs e)
        {
            MainDateTimePicker.Value = DateTime.Now;
            this.Close();
        }

        private void btn_confirm_Click(object sender, EventArgs e)
        {
            var time = ppTimePicker1.Time;
            MainDateTimePicker.Value = new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, time.Hour, time.Minute, time.Second);
            this.Close();
        }
    }
}