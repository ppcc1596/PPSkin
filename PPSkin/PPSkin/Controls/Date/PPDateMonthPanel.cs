﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace PPSkin
{
    [DefaultEvent("DayClick")]
    internal partial class PPDateMonthPanel : UserControl
    {
        private List<PPButton> Buttons = new List<PPButton>();

        private DateTime dateTime = DateTime.Now;

        public DateTime DateTime
        {
            get { return dateTime; }
            set
            {
                dateTime = value;
            }
        }

        public PPDateMonthPanel()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            //if(!DesignMode)
            //{
                for (int i = 1; i <= 12; i++)
                {
                    PPButton button = new PPButton();
                    button.Size = new Size(50, 50);
                    var padding = new Padding(18, 9, 19, 9);
                    if (i == 1 || i == 5 || i == 9 || i == 4 || i == 8 || i == 12)
                    {
                        padding = new Padding(19, 9, 19, 9);
                    }
                    button.Margin = padding;
                    button.MouseDownBaseColor = System.Drawing.Color.DodgerBlue;
                    button.MouseDownLineColor = System.Drawing.Color.DodgerBlue;
                    button.MouseInBaseColor = System.Drawing.Color.White;
                    button.MouseInLineColor = System.Drawing.Color.DodgerBlue;
                    button.Cursor = Cursors.Hand;
                    button.Text = i.ToString();
                    Buttons.Add(button);
                    flowLayoutPanel1.Controls.Add(button);
                    button.Click += Button_Click;
                }
            //}

        }

        public event EventHandler<MonthClickEventArgs> MonthClick;

        private void Button_Click(object sender, EventArgs e)
        {
            int month = Convert.ToInt32((sender as PPButton).Text);
            if (MonthClick != null)
            {
                this.Invoke(new MethodInvoker(() =>
                {
                    MonthClickEventArgs ee = new MonthClickEventArgs(new DateTime(DateTime.Year, month, 1, DateTime.Hour, DateTime.Minute, DateTime.Second));
                    MonthClick(this, ee);
                }));
            }
        }

        public void LoadMonth()
        {
            PPDateTimePicker pPDateTimePicker = (this.Parent as PPDateTimeDropForm).MainDateTimePicker;
            foreach (PPButton pPButton in Buttons)
            {
                pPButton.Enabled = true;
                pPButton.RegularBaseColor = Color.White;
            }

            if (this.dateTime.Year == pPDateTimePicker.MinDate.Year)
            {
                foreach (PPButton pPButton in Buttons)
                {
                    if (Convert.ToInt32(pPButton.Text) < pPDateTimePicker.MinDate.Month)
                    {
                        pPButton.Enabled = false;
                        pPButton.RegularBaseColor = Color.LightGray;
                    }
                }
            }

            if (this.dateTime.Year == pPDateTimePicker.MaxDate.Year)
            {
                foreach (PPButton pPButton in Buttons)
                {
                    if (Convert.ToInt32(pPButton.Text) > pPDateTimePicker.MinDate.Month)
                    {
                        pPButton.Enabled = false;
                        pPButton.RegularBaseColor = Color.LightGray;
                    }
                }
            }
        }

        public void SetLang(PPDateTimeDropForm.Lang language)
        {
            switch (language)
            {
                case PPDateTimeDropForm.Lang.CH:
                    ppLabel1.Text = "月份";

                    break;

                case PPDateTimeDropForm.Lang.EN:
                    ppLabel1.Text = "Month";
                    break;
            }
        }
    }

    public class MonthClickEventArgs : EventArgs
    {
        public MonthClickEventArgs(DateTime dateTime)
        {
            DateTime = dateTime;
        }

        public DateTime DateTime;
    }
}