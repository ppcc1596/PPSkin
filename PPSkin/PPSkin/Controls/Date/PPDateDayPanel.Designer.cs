﻿
namespace PPSkin
{
    partial class PPDateDayPanel
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.ppLabel7 = new PPSkin.PPLabel();
            this.ppLabel6 = new PPSkin.PPLabel();
            this.ppLabel5 = new PPSkin.PPLabel();
            this.ppLabel4 = new PPSkin.PPLabel();
            this.ppLabel3 = new PPSkin.PPLabel();
            this.ppLabel2 = new PPSkin.PPLabel();
            this.ppLabel1 = new PPSkin.PPLabel();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 15);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(350, 205);
            this.flowLayoutPanel1.TabIndex = 7;
            // 
            // ppLabel7
            // 
            this.ppLabel7.AutoEllipsis = true;
            this.ppLabel7.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ppLabel7.Location = new System.Drawing.Point(300, 0);
            this.ppLabel7.Margin = new System.Windows.Forms.Padding(0);
            this.ppLabel7.Name = "ppLabel7";
            this.ppLabel7.Size = new System.Drawing.Size(50, 15);
            this.ppLabel7.TabIndex = 6;
            this.ppLabel7.Text = "星期六";
            this.ppLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ppLabel7.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            // 
            // ppLabel6
            // 
            this.ppLabel6.AutoEllipsis = true;
            this.ppLabel6.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ppLabel6.Location = new System.Drawing.Point(250, 0);
            this.ppLabel6.Margin = new System.Windows.Forms.Padding(0);
            this.ppLabel6.Name = "ppLabel6";
            this.ppLabel6.Size = new System.Drawing.Size(50, 15);
            this.ppLabel6.TabIndex = 5;
            this.ppLabel6.Text = "星期五";
            this.ppLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ppLabel6.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            // 
            // ppLabel5
            // 
            this.ppLabel5.AutoEllipsis = true;
            this.ppLabel5.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ppLabel5.Location = new System.Drawing.Point(200, 0);
            this.ppLabel5.Margin = new System.Windows.Forms.Padding(0);
            this.ppLabel5.Name = "ppLabel5";
            this.ppLabel5.Size = new System.Drawing.Size(50, 15);
            this.ppLabel5.TabIndex = 4;
            this.ppLabel5.Text = "星期四";
            this.ppLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ppLabel5.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            // 
            // ppLabel4
            // 
            this.ppLabel4.AutoEllipsis = true;
            this.ppLabel4.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ppLabel4.Location = new System.Drawing.Point(150, 0);
            this.ppLabel4.Margin = new System.Windows.Forms.Padding(0);
            this.ppLabel4.Name = "ppLabel4";
            this.ppLabel4.Size = new System.Drawing.Size(50, 15);
            this.ppLabel4.TabIndex = 3;
            this.ppLabel4.Text = "星期三";
            this.ppLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ppLabel4.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            // 
            // ppLabel3
            // 
            this.ppLabel3.AutoEllipsis = true;
            this.ppLabel3.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ppLabel3.Location = new System.Drawing.Point(100, 0);
            this.ppLabel3.Margin = new System.Windows.Forms.Padding(0);
            this.ppLabel3.Name = "ppLabel3";
            this.ppLabel3.Size = new System.Drawing.Size(50, 15);
            this.ppLabel3.TabIndex = 2;
            this.ppLabel3.Text = "星期二";
            this.ppLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ppLabel3.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            // 
            // ppLabel2
            // 
            this.ppLabel2.AutoEllipsis = true;
            this.ppLabel2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ppLabel2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ppLabel2.Location = new System.Drawing.Point(50, 0);
            this.ppLabel2.Margin = new System.Windows.Forms.Padding(0);
            this.ppLabel2.Name = "ppLabel2";
            this.ppLabel2.Size = new System.Drawing.Size(50, 15);
            this.ppLabel2.TabIndex = 1;
            this.ppLabel2.Text = "星期一";
            this.ppLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ppLabel2.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            // 
            // ppLabel1
            // 
            this.ppLabel1.AutoEllipsis = true;
            this.ppLabel1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ppLabel1.Location = new System.Drawing.Point(0, 0);
            this.ppLabel1.Margin = new System.Windows.Forms.Padding(0);
            this.ppLabel1.Name = "ppLabel1";
            this.ppLabel1.Size = new System.Drawing.Size(50, 15);
            this.ppLabel1.TabIndex = 0;
            this.ppLabel1.Text = "星期日";
            this.ppLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ppLabel1.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            // 
            // PPDateDayPanel
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.ppLabel7);
            this.Controls.Add(this.ppLabel6);
            this.Controls.Add(this.ppLabel5);
            this.Controls.Add(this.ppLabel4);
            this.Controls.Add(this.ppLabel3);
            this.Controls.Add(this.ppLabel2);
            this.Controls.Add(this.ppLabel1);
            this.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "PPDateDayPanel";
            this.Size = new System.Drawing.Size(350, 220);
            this.ResumeLayout(false);

        }

        #endregion

        private PPLabel ppLabel1;
        private PPLabel ppLabel2;
        private PPLabel ppLabel3;
        private PPLabel ppLabel4;
        private PPLabel ppLabel5;
        private PPLabel ppLabel6;
        private PPLabel ppLabel7;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
    }
}
