﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace PPSkin
{
    public partial class PPDateTimePicker : PPControlBase
    {
        #region 私有变量

        private DateTime value = DateTime.Now;
        private DateTime minDate = new DateTime(1753, 1, 1);
        private DateTime maxDate = DateTime.MaxValue;
        private string customFormat = "";
        private int BTN_SIZE = 26;
        private Bitmap buttonImage = PPSkinResources.Date;

        #endregion 私有变量

        #region 公共变量
        [Description("日期图标"), Category("PPSkin.样式")]
        public Bitmap ButtonImage
        {
            get { return buttonImage; }
            set
            {
                if (value != null)
                    buttonImage = value;
                this.Invalidate();
            }
        }

        [Description("日期图标大小"), Category("PPSkin.样式")]
        public int ButtonSize
        {
            get { return BTN_SIZE; }
            set
            {
                if (value > 0 && value <= this.Height)
                {
                    BTN_SIZE = value;
                    this.Invalidate();
                }
            }
        }

        [Description("当前选择日期"), Category("PPSkin.数据")]
        public DateTime Value
        {
            get { return this.value; }
            set
            {
                int v1 = value.CompareTo(minDate);
                int v2 = value.CompareTo(maxDate);
                if (v1 >= 0 && v2 <= 0)
                {
                    this.value = value;
                    this.Invalidate();
                }
            }
        }

        [Description("可选择的最大日期"), Category("PPSkin.数据")]
        public DateTime MaxDate
        {
            get { return maxDate; }
            set
            {
                if (value.CompareTo(minDate) >= 0)
                {
                    maxDate = value;
                    if (this.Value.CompareTo(maxDate) > 0)
                    {
                        this.Value = maxDate;
                    }
                }
            }
        }

        [Description("可选择的最小日期"), Category("PPSkin.数据")]
        public DateTime MinDate
        {
            get { return minDate; }
            set
            {
                if (value.CompareTo(maxDate) <= 0)
                {
                    minDate = value;
                    if (this.Value.CompareTo(minDate) < 0)
                    {
                        this.Value = minDate;
                    }
                }
            }
        }

        [Description("选择时间"), Category("PPSkin.数据")]
        public bool PickTime { get; set; } = false;

        [Description("自定义格式"), Category("PPSkin.数据")]
        public string CustomFormat
        {
            get { return customFormat; }
            set
            {
                try
                {
                    string f = this.Value.ToString(value);
                    customFormat = value;
                    this.Invalidate();
                }
                catch
                {
                }
            }
        }

        [Description("弹出窗体圆角"), Category("PPSkin.弹出")]
        public int DropformRadius { get; set; } = 5;

        [Description("弹出窗体背景色"), Category("PPSkin.弹出")]
        public Color DropbaseColor { get; set; } = Color.White;

        [Description("弹出窗体边框色"), Category("PPSkin.弹出")]
        public Color DropborderColor { get; set; } = Color.LightGray;

        [Description("语言"), Category("PPSkin.弹出")]
        public PPDateTimeDropForm.Lang Language { get; set; } = PPDateTimeDropForm.Lang.CH;


        #endregion 公共变量

        public PPDateTimePicker()
        {
            InitializeComponent();
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.DoubleBuffer, true);
            this.SetStyle(ControlStyles.ResizeRedraw, true);
            this.SetStyle(ControlStyles.Selectable, true);
            this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            this.SetStyle(ControlStyles.UserPaint, true);
            this.UpdateStyles();
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;//用双缓冲绘制窗口的所有子控件
                return cp;
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            
            //dropForm.Opacity = 0;
            //dropForm.Show();
            //dropForm.Hide();
            //dropForm.Opacity = 1;
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            Graphics g = e.Graphics;
            //g.Clear(this.BackColor);

            g.SmoothingMode = SmoothingMode.HighQuality;
            g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
            string format = "yyyy-MM-dd";
            if (customFormat != "")
                format = customFormat;
            string text = this.Value.ToString(format);

            Brush Textbrush = new SolidBrush(this.ForeColor);
            Rectangle textrect = new Rectangle(this.Radius / 2, (int)this.BorderWidth, this.Width - Radius - BTN_SIZE, (int)(this.Height - 2 * this.BorderWidth));
            StringFormat stringFormat = new StringFormat();
            stringFormat.Alignment = StringAlignment.Center;
            stringFormat.LineAlignment = StringAlignment.Center;
            stringFormat.Trimming = StringTrimming.EllipsisCharacter;
            g.DrawString(text, this.Font, Textbrush, textrect, stringFormat);

            Rectangle imgrect = new Rectangle(textrect.Right, (this.Height - BTN_SIZE) / 2, BTN_SIZE, BTN_SIZE);
            g.DrawImage(buttonImage, imgrect);

            Textbrush.Dispose();
            stringFormat.Dispose();
        }

        protected override void OnClick(EventArgs e)
        {
            base.OnClick(e);
            PPDateTimeDropForm dropForm = new PPDateTimeDropForm(this);

            dropForm.Show();
        }

    }
}