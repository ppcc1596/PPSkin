﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Linq;

namespace PPSkin
{
    [DefaultEvent("DayClick")]
    internal partial class PPDateDayPanel : UserControl
    {
        private List<PPButton> Buttons = new List<PPButton>();

        private DateTime datetime = DateTime.Now;

        private int selectIndex = 0;

        public DateTime DateTime
        {
            get { return datetime; }
            set
            {
                datetime = value;
                selectIndex = 0;
                Buttons?.ForEach(button => { button.RegularLineColor = Color.LightGray; button.RegularTextColor = Color.Black; });
                var btn=Buttons?.Where(b => b.RegularBaseColor != Color.LightGray && b.Text == datetime.Day.ToString()).FirstOrDefault();
                if(btn!=null)
                {
                    btn.RegularLineColor = Color.DodgerBlue;
                    btn.RegularTextColor = Color.DodgerBlue;
                }
                
            }
        }

        public PPDateDayPanel()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            //if(!DesignMode)
            //{
                for (int i = 0; i <= 42; i++)
                {
                    PPButton button = new PPButton();
                    button.Size = new Size(30, 30);
                    button.Margin = new System.Windows.Forms.Padding(10, 2, 10, 2);
                    button.MouseDownBaseColor = System.Drawing.Color.DodgerBlue;
                    button.MouseDownLineColor = System.Drawing.Color.DodgerBlue;
                    button.MouseInBaseColor = System.Drawing.Color.White;
                    button.MouseInLineColor = System.Drawing.Color.DodgerBlue;
                    button.Cursor = Cursors.Hand;
                    button.TextDrawMode = Enums.DrawMode.Clear;
                    button.Text = "0";
                    Buttons.Add(button);
                    flowLayoutPanel1.Controls.Add(button);
                    button.Click += Button_Click;
                }
                LoadMonth(0);
            //}


            
        }

        public event EventHandler<DayClickEventArgs> DayClick;

        private void Button_Click(object sender, EventArgs e)
        {
            Buttons.ForEach(button => { button.RegularLineColor = Color.LightGray; button.RegularTextColor = Color.Black; });
            var btn=sender as PPButton;
            btn.RegularLineColor = Color.DodgerBlue;
            btn.RegularTextColor = Color.DodgerBlue;
            int day = Convert.ToInt32(btn.Text);
            if (DayClick != null)
            {
                this.Invoke(new MethodInvoker(() =>
                {
                    DayClickEventArgs ee = new DayClickEventArgs(new DateTime(DateTime.Year, DateTime.Month, day));
                    DayClick(this, ee);
                }));
            }
        }

        public void LoadMonth(int index)
        {
            selectIndex += index;

            DateTime dateTime = this.DateTime.AddMonths(selectIndex);
            DateTime lastMonth = dateTime.Year==1&&dateTime.Month==1?dateTime:dateTime.AddMonths(-1);
            DateTime nextMonth = dateTime.AddMonths(1);

            int lastDayCount = DateTime.DaysInMonth(lastMonth.Year, lastMonth.Month);
            int nextDayCount = DateTime.DaysInMonth(nextMonth.Year, nextMonth.Month);
            int DayCount = DateTime.DaysInMonth(dateTime.Year, dateTime.Month);

            DateTime firstDay = new DateTime(dateTime.Year, dateTime.Month, 1);
            int lastCount = 0;
            switch (firstDay.DayOfWeek)
            {
                case DayOfWeek.Sunday: lastCount = 7; break;
                case DayOfWeek.Monday: lastCount = 1; break;
                case DayOfWeek.Tuesday: lastCount = 2; break;
                case DayOfWeek.Wednesday: lastCount = 3; break;
                case DayOfWeek.Thursday: lastCount = 4; break;
                case DayOfWeek.Friday: lastCount = 5; break;
                case DayOfWeek.Saturday: lastCount = 6; break;
            }

            for (int i = 0; i < lastCount; i++)
            {
                Buttons[i].Enabled = false;
                Buttons[i].RegularBaseColor = Color.LightGray;
                Buttons[i].Text = dateTime.Year == 1 && dateTime.Month == 1 ? "":(lastDayCount - lastCount + 1 + i).ToString();

                Buttons[i].RegularTextColor = Color.Black;
                Buttons[i].MouseInTextColor = Color.Black;
            }

            for (int i = lastCount; i < DayCount + lastCount; i++)
            {
                Buttons[i].Enabled = true;
                Buttons[i].RegularBaseColor = Color.White;
                Buttons[i].Text = (i - lastCount + 1).ToString();

                if (i - lastCount + 1 == DateTime.Day)
                {
                    Buttons[i].RegularTextColor = Color.DodgerBlue;
                    Buttons[i].MouseInTextColor = Color.DodgerBlue;
                }
                else
                {
                    Buttons[i].RegularTextColor = Color.Black;
                    Buttons[i].MouseInTextColor = Color.Black;
                }
            }

            for (int i = lastCount + DayCount; i < Buttons.Count; i++)
            {
                Buttons[i].Enabled = false;
                Buttons[i].RegularBaseColor = Color.LightGray;
                Buttons[i].Text = (i - lastCount - DayCount + 1).ToString();
                Buttons[i].RegularTextColor = Color.Black;
                Buttons[i].MouseInTextColor = Color.Black;
            }
            DateTime = dateTime;
        }

        public void SetLang(PPDateTimeDropForm.Lang language)
        {
            switch (language)
            {
                case PPDateTimeDropForm.Lang.CH:
                    ppLabel1.Text = "星期日";
                    ppLabel2.Text = "星期一";
                    ppLabel3.Text = "星期二";
                    ppLabel4.Text = "星期三";
                    ppLabel5.Text = "星期四";
                    ppLabel6.Text = "星期五";
                    ppLabel7.Text = "星期六";
                    break;

                case PPDateTimeDropForm.Lang.EN:
                    ppLabel1.Text = "Sun";
                    ppLabel2.Text = "Mon";
                    ppLabel3.Text = "Tues";
                    ppLabel4.Text = "Wed";
                    ppLabel5.Text = "Thu";
                    ppLabel6.Text = "Fri";
                    ppLabel7.Text = "Sat";
                    break;
            }
        }
    }

    public class DayClickEventArgs : EventArgs
    {
        public DayClickEventArgs(DateTime dateTime)
        {
            DateTime = dateTime;
        }

        public DateTime DateTime;
    }
}