﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace PPSkin
{
    [DefaultEvent("DayClick")]
    internal partial class PPDateYearPanel : UserControl
    {
        private List<PPButton> Buttons = new List<PPButton>();

        private DateTime dateTime = DateTime.Now;

        private int selectIndex = 0;

        public DateTime DateTime
        {
            get { return dateTime; }
            set
            {
                dateTime = value;
                selectIndex = 0;
            }
        }

        public PPDateYearPanel()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            //if(!DesignMode)
            //{
                for (int i = 1; i <= 24; i++)
                {
                    PPButton button = new PPButton();
                    button.Size = new Size(40, 40);
                    button.Margin = new System.Windows.Forms.Padding(9, 6, 9, 5);
                    button.MouseDownBaseColor = System.Drawing.Color.DodgerBlue;
                    button.MouseDownLineColor = System.Drawing.Color.DodgerBlue;
                    button.MouseInBaseColor = System.Drawing.Color.White;
                    button.MouseInLineColor = System.Drawing.Color.DodgerBlue;
                    button.Cursor = Cursors.Hand;
                    button.Text = i.ToString();
                    Buttons.Add(button);
                    flowLayoutPanel1.Controls.Add(button);
                    button.Click += Button_Click;
                }
            //}

        }

        public event EventHandler<YearClickEventArgs> YearClick;

        private void Button_Click(object sender, EventArgs e)
        {
            int year = Convert.ToInt32((sender as PPButton).Text);
            if (YearClick != null)
            {
                this.Invoke(new MethodInvoker(() =>
                {
                    YearClickEventArgs ee = new YearClickEventArgs(new DateTime(year, 1, 1, 0, 0, 0));
                    YearClick(this, ee);
                }));
            }
        }

        public string LoadYear(int index)
        {
            //foreach()
            selectIndex += index;

            PPDateTimePicker mainDateTimePicker = (this.Parent as PPDateTimeDropForm).MainDateTimePicker;
            int year = DateTime.Year;

            int a = (year + selectIndex * 24) / 24;

            int startYear = a * 24;
            int endYear = startYear + 24 - 1;

            for (int i = startYear; i <= endYear; i++)
            {
                Buttons[i - startYear].Text = i.ToString();
                if (i < mainDateTimePicker.MinDate.Year || i > mainDateTimePicker.MaxDate.Year)
                {
                    Buttons[i - startYear].Enabled = false;
                    Buttons[i - startYear].RegularBaseColor = Color.LightGray;
                }
                else
                {
                    Buttons[i - startYear].Enabled = true;
                    Buttons[i - startYear].RegularBaseColor = Color.White;
                }
            }

            return startYear.ToString() + "-" + endYear.ToString();
        }

        public void SetLang(PPDateTimeDropForm.Lang language)
        {
            switch (language)
            {
                case PPDateTimeDropForm.Lang.CH:
                    ppLabel1.Text = "年份";

                    break;

                case PPDateTimeDropForm.Lang.EN:
                    ppLabel1.Text = "Year";
                    break;
            }
        }
    }

    public class YearClickEventArgs : EventArgs
    {
        public YearClickEventArgs(DateTime dateTime)
        {
            DateTime = dateTime;
        }

        public DateTime DateTime;
    }
}