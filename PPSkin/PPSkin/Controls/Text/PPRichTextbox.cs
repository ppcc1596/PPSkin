﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace PPSkin
{
    public partial class PPRichTextbox : PPControlBase
    {
        #region 属性定义

        [Category("自定义")]
        public bool AcceptsTab
        {
            get { return richTextBox1.AcceptsTab; }
            set { richTextBox1.AcceptsTab = value; }
        }

        [Category("自定义")]
        public bool AutoWordSelection
        {
            get { return richTextBox1.AutoWordSelection; }
            set { richTextBox1.AutoWordSelection = value; }
        }

        [Category("自定义")]
        public int BulletIndent
        {
            get { return richTextBox1.BulletIndent; }
            set { richTextBox1.BulletIndent = value; }
        }

        [Category("自定义")]
        public bool DetectUrls
        {
            get { return richTextBox1.DetectUrls; }
            set { richTextBox1.DetectUrls = value; }
        }

        [Category("自定义")]
        public bool EnableAutoDrogDrop
        {
            get { return richTextBox1.EnableAutoDragDrop; }
            set
            {
                richTextBox1.EnableAutoDragDrop = value;
            }
        }

        [Category("自定义")]
        public bool HideSelection
        {
            get { return richTextBox1.HideSelection; }
            set
            {
                richTextBox1.HideSelection = value;
            }
        }

        [Category("自定义")]
        public int MaxLength
        {
            get { return richTextBox1.MaxLength; }
            set
            {
                richTextBox1.MaxLength = value;
            }
        }

        [Category("自定义")]
        public bool Multiline
        {
            get { return richTextBox1.Multiline; }
            set { richTextBox1.Multiline = value; }
        }

        [Category("自定义")]
        public bool ReadOnly
        {
            get { return richTextBox1.ReadOnly; }
            set
            {
                richTextBox1.ReadOnly = value;
            }
        }

        [Category("自定义")]
        public bool ShortcutsEnabled
        {
            get { return richTextBox1.ShortcutsEnabled; }
            set { richTextBox1.ShortcutsEnabled = value; }
        }

        [Category("自定义")]
        public bool ShowSelectionMargin
        {
            get { return richTextBox1.ShowSelectionMargin; }
            set { richTextBox1.ShowSelectionMargin = value; }
        }

        [Category("自定义")]
        public bool WordWrap
        {
            get { return richTextBox1.WordWrap; }
            set
            {
                richTextBox1.WordWrap = value;
            }
        }

        [Category("自定义")]
        public float ZoomFactor
        {
            get { return richTextBox1.ZoomFactor; }
            set { richTextBox1.ZoomFactor = value; }
        }

        [Category("自定义"), Browsable(true)]
        public override string Text
        {
            get
            {
                return richTextBox1.Text;
            }

            set
            {
                richTextBox1.Text = value;
            }
        }

        [Category("自定义")]
        public override Color ForeColor
        {
            get
            {
                return richTextBox1.ForeColor;
            }

            set
            {
                richTextBox1.ForeColor = value;
            }
        }

        [Browsable(false), Category("自定义")]
        public int SelectionStart
        {
            get { return richTextBox1.SelectionStart; }
            set
            {
                richTextBox1.SelectionStart = value;
            }
        }

        [Browsable(false), Category("自定义")]
        public Color SelectionColor
        {
            get { return richTextBox1.SelectionColor; }
            set
            {
                richTextBox1.SelectionColor = value;
            }
        }

        [Category("自定义")]
        public RichTextBoxScrollBars ScrollBars
        {
            get
            {
                return richTextBox1.ScrollBars;
            }
            set
            {
                richTextBox1.ScrollBars = value;
            }
        }

        [Category("滚动条")]
        public Color VScrollBar_BaseColor
        {
            get { return ppvScrollBarExt1.BaseColor; }
            set { ppvScrollBarExt1.BaseColor = value; }
        }

        [Category("滚动条")]
        public Color VScrollBar_ButtonColor
        {
            get { return ppvScrollBarExt1.ButtonColor; }
            set { ppvScrollBarExt1.ButtonColor = value; }
        }

        [Category("滚动条")]
        public Color VScrollBar_ButtonHoverColor
        {
            get { return ppvScrollBarExt1.ButtonHoverColor; }
            set { ppvScrollBarExt1.ButtonHoverColor = value; }
        }

        [Category("滚动条")]
        public int VScrollBar_BaseRadius
        {
            get { return ppvScrollBarExt1.BaseRadius; }
            set { ppvScrollBarExt1.BaseRadius = value; }
        }

        [Category("滚动条")]
        public int VScrollBar_ButtonRadius
        {
            get { return ppvScrollBarExt1.ButtonRadius; }
            set { ppvScrollBarExt1.ButtonRadius = value; }
        }

        [Category("滚动条")]
        public int VScrollBar_BaseSize
        {
            get { return ppvScrollBarExt1.BaseSize; }
            set { ppvScrollBarExt1.BaseSize = value; }
        }

        [Category("滚动条")]
        public int VScrollBar_ButtonSize
        {
            get { return ppvScrollBarExt1.ButtonSize; }
            set { ppvScrollBarExt1.ButtonSize = value; }
        }

        #endregion 属性定义

        public PPRichTextbox()
        {
            InitializeComponent();
            base.SetStyle(
            ControlStyles.UserPaint |
            ControlStyles.DoubleBuffer |
            ControlStyles.OptimizedDoubleBuffer |
            ControlStyles.AllPaintingInWmPaint |
            ControlStyles.ResizeRedraw |
            ControlStyles.SupportsTransparentBackColor, true);
            base.UpdateStyles();
            this.RadiusChanged += PPRichTextbox_RadiusChanged;
            ppvScrollBarExt1.BindingControl(richTextBox1);
            ppvScrollBarExt1.BringToFront();
        }

        private void PPRichTextbox_RadiusChanged(object sender, EventArgs e)
        {
            ResetSize();
        }

        public void Clear()
        {
            richTextBox1.Invoke(new MethodInvoker(() => { richTextBox1.Clear(); }));
        }

        public void AppendText(string text)
        {
            richTextBox1.Invoke(new MethodInvoker(() => { richTextBox1.AppendText(text); }));
        }

        public void ScrollToCaret()
        {
            richTextBox1.Invoke(new MethodInvoker(() => { richTextBox1.ScrollToCaret(); }));
        }

        private void ResetSize()
        {
            richTextBox1.Size = new Size((int)(this.Width - 0.3 * Radius - 2 * BorderWidth) - 2, (int)(this.Height - 0.3 * Radius - 2 * BorderWidth) - 2);
            richTextBox1.Location = new Point((int)(BorderWidth + 0.15 * Radius + 1), (int)(BorderWidth + 0.15 * Radius + 1));
            ppvScrollBarExt1.Location = new Point(richTextBox1.Right - ppvScrollBarExt1.Width, richTextBox1.Top);
            ppvScrollBarExt1.Size = new Size(ppvScrollBarExt1.Width, richTextBox1.Height);
        }

        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);
            ResetSize();
        }

        protected override void OnVisibleChanged(EventArgs e)
        {
            base.OnVisibleChanged(e);
            ResetSize();
        }

        private void PPRichTextbox_BaseColorChanged(object sender, EventArgs e)
        {
            richTextBox1.BackColor = this.BaseColor;
        }

        [Browsable(true), Bindable(true)]
        public new event EventHandler TextChanged;

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
            if (TextChanged != null)
            {
                TextChanged(this, new EventArgs());
            }
        }
    }
}