﻿using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace PPSkin
{
    public partial class PPLabel : Label
    {
        [Description("字符绘制方式")]
        public Enums.DrawMode TextDrawMode { get; set; } = Enums.DrawMode.Anti;

        public PPLabel()
        {
            InitializeComponent();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            Graphics g = e.Graphics;
            g.Clear(this.BackColor);
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            switch(TextDrawMode)
            {
                case Enums.DrawMode.Anti: g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;break;
                case Enums.DrawMode.Clear:g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;break;
                case Enums.DrawMode.Default:g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;break;
            }

            using (var brush=new SolidBrush(this.ForeColor))
            {
                if (this.AutoSize)
                {
                    g.DrawString(this.Text, this.Font, brush, new Point(0, 0));
                }
                else
                {
                    System.Drawing.StringFormat format = new System.Drawing.StringFormat();
                    format.LineAlignment = StringAlignment.Center;
                    format.Trimming = StringTrimming.EllipsisCharacter;

                    switch (this.TextAlign)
                    {
                        case ContentAlignment.TopLeft:
                        case ContentAlignment.MiddleLeft:
                        case ContentAlignment.BottomLeft:
                            format.Alignment = StringAlignment.Near;
                            break;

                        case ContentAlignment.TopRight:
                        case ContentAlignment.MiddleRight:
                        case ContentAlignment.BottomRight:
                            format.Alignment = StringAlignment.Far;
                            break;

                        case ContentAlignment.TopCenter:
                        case ContentAlignment.MiddleCenter:
                        case ContentAlignment.BottomCenter:
                            format.Alignment = StringAlignment.Center;
                            break;
                    }

                    switch (this.TextAlign)
                    {
                        case ContentAlignment.TopLeft:
                        case ContentAlignment.TopCenter:
                        case ContentAlignment.TopRight:
                            format.LineAlignment = StringAlignment.Near;
                            break;

                        case ContentAlignment.MiddleLeft:
                        case ContentAlignment.MiddleRight:
                        case ContentAlignment.MiddleCenter:
                            format.LineAlignment = StringAlignment.Center;
                            break;

                        case ContentAlignment.BottomLeft:
                        case ContentAlignment.BottomRight:
                        case ContentAlignment.BottomCenter:
                            format.LineAlignment = StringAlignment.Far;
                            break;
                    }
                    g.DrawString(this.Text, this.Font, brush, new Rectangle(0, 0, this.Width, this.Height), format);
                }
            }
                
        }
    }
}