﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace PPSkin
{
    public partial class LedNum : UserControl
    {
        private Color numColor = Color.DodgerBlue;
        private char numChar = '-';
        private int spaceWidth = 4;

        [Description("led文字的颜色"), Category("PPSkin")]
        public Color NumColor
        {
            get { return numColor; }
            set
            {
                numColor = value;
                this.Invalidate();
            }
        }

        [Description("led文字的字符"), Category("PPSkin")]
        public char NumChar
        {
            get { return numChar; }
            set
            {
                if (UseableCharList.Contains(value))
                {
                    numChar = value;
                    this.Invalidate();
                }
            }
        }

        [Description("led文字的笔划间隔"), Category("PPSkin")]
        public int SpaceWidth
        {
            get { return spaceWidth; }
            set
            {
                spaceWidth = value;
                this.Invalidate();
            }
        }

        private List<char> UseableCharList = new List<char>() { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.', '-', ':', '_', ' ' };

        private Dictionary<char, int[]> dic = new Dictionary<char, int[]>();

        public LedNum()
        {
            InitializeComponent();
            int[] list = new int[] { 1, 2, 3, 4, 5, 6 };
            dic.Add('0', list);
            list = new int[] { 2, 3 };
            dic.Add('1', list);
            list = new int[] { 1, 2, 4, 5, 7 };
            dic.Add('2', list);
            list = new int[] { 1, 2, 3, 4, 7 };
            dic.Add('3', list);
            list = new int[] { 2, 3, 6, 7 };
            dic.Add('4', list);
            list = new int[] { 1, 3, 4, 6, 7 };
            dic.Add('5', list);
            list = new int[] { 1, 3, 4, 5, 6, 7 };
            dic.Add('6', list);
            list = new int[] { 1, 2, 3 };
            dic.Add('7', list);
            list = new int[] { 1, 2, 3, 4, 5, 6, 7 };
            dic.Add('8', list);
            list = new int[] { 1, 2, 3, 4, 6, 7 };
            dic.Add('9', list);
            list = new int[] { 9 };
            dic.Add('.', list);
            list = new int[] { 7 };
            dic.Add('-', list);
            list = new int[] { 8, 9 };
            dic.Add(':', list);
            list = new int[] { 4 };
            dic.Add('_', list);
            list = new int[] { };
            dic.Add(' ', list);
        }

        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);
            this.Width = this.Height / 2;
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            Graphics g = e.Graphics;
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            int[] val;
            if (dic.TryGetValue(this.NumChar, out val))
            {
                if (val == null)
                    return;
                foreach (int num in val)
                {
                    PaintLed(g, num);
                }
            }
        }

        private void PaintLed(Graphics g, int index)
        {
            Brush brush = new SolidBrush(NumColor);
            int spaceWidth = SpaceWidth;
            int ledWith = this.Width / 5;
            switch (index)
            {
                case 1:
                    Point[] points = new Point[6];
                    points[0] = new Point(spaceWidth + ledWith / 3, ledWith / 3);
                    points[1] = new Point(spaceWidth + 2 * ledWith / 3, 0);
                    points[2] = new Point(this.Width - spaceWidth - 2 * ledWith / 3, 0);
                    points[3] = new Point(this.Width - spaceWidth - ledWith / 3, ledWith / 3);
                    points[4] = new Point(this.Width - spaceWidth - ledWith, ledWith);
                    points[5] = new Point(spaceWidth + ledWith, ledWith);
                    g.FillPolygon(brush, points);
                    break;

                case 2:
                    points = new Point[6];
                    points[0] = new Point(this.Width - ledWith / 3, spaceWidth + ledWith / 3);
                    points[1] = new Point(this.Width, spaceWidth + 2 * ledWith / 3);
                    points[2] = new Point(this.Width, this.Height / 2 - spaceWidth - ledWith / 3);
                    points[3] = new Point(this.Width - ledWith / 3, this.Height / 2 - spaceWidth);
                    points[4] = new Point(this.Width - ledWith, this.Height / 2 - 2 * ledWith / 3 - spaceWidth);
                    points[5] = new Point(this.Width - ledWith, spaceWidth + ledWith);
                    g.FillPolygon(brush, points);
                    break;

                case 3:
                    points = new Point[6];
                    points[0] = new Point(this.Width - ledWith / 3, this.Height / 2 + spaceWidth);
                    points[1] = new Point(this.Width, this.Height / 2 + spaceWidth + ledWith / 3);
                    points[2] = new Point(this.Width, this.Height - spaceWidth - 2 * ledWith / 3);
                    points[3] = new Point(this.Width - ledWith / 3, this.Height - spaceWidth - ledWith / 3);
                    points[4] = new Point(this.Width - ledWith, this.Height - ledWith - spaceWidth);
                    points[5] = new Point(this.Width - ledWith, this.Height / 2 + spaceWidth + 2 * ledWith / 3);
                    g.FillPolygon(brush, points);
                    break;

                case 4:
                    points = new Point[6];
                    points[0] = new Point(spaceWidth + ledWith / 3, this.Height - ledWith / 3);
                    points[1] = new Point(spaceWidth + 2 * ledWith / 3, this.Height);
                    points[2] = new Point(this.Width - spaceWidth - 2 * ledWith / 3, this.Height);
                    points[3] = new Point(this.Width - spaceWidth - ledWith / 3, this.Height - ledWith / 3);
                    points[4] = new Point(this.Width - ledWith - spaceWidth, this.Height - ledWith);
                    points[5] = new Point(spaceWidth + ledWith, this.Height - ledWith);
                    g.FillPolygon(brush, points);
                    break;

                case 5:
                    points = new Point[6];
                    points[0] = new Point(ledWith / 3, this.Height / 2 + spaceWidth);
                    points[1] = new Point(0, this.Height / 2 + spaceWidth + ledWith / 3);
                    points[2] = new Point(0, this.Height - spaceWidth - 2 * ledWith / 3);
                    points[3] = new Point(ledWith / 3, this.Height - spaceWidth - ledWith / 3);
                    points[4] = new Point(ledWith, this.Height - ledWith - spaceWidth);
                    points[5] = new Point(ledWith, this.Height / 2 + spaceWidth + 2 * ledWith / 3);
                    g.FillPolygon(brush, points);
                    break;

                case 6:
                    points = new Point[6];
                    points[0] = new Point(ledWith / 3, spaceWidth + ledWith / 3);
                    points[1] = new Point(0, spaceWidth + 2 * ledWith / 3);
                    points[2] = new Point(0, this.Height / 2 - spaceWidth - ledWith / 3);
                    points[3] = new Point(ledWith / 3, this.Height / 2 - spaceWidth);
                    points[4] = new Point(ledWith, this.Height / 2 - 2 * ledWith / 3 - spaceWidth);
                    points[5] = new Point(ledWith, spaceWidth + ledWith);
                    g.FillPolygon(brush, points);
                    break;

                case 7:
                    points = new Point[6];
                    points[0] = new Point(spaceWidth + ledWith / 3, this.Height / 2);
                    points[1] = new Point(spaceWidth + ledWith, this.Height / 2 - 2 * ledWith / 3);
                    points[2] = new Point(this.Width - spaceWidth - ledWith, this.Height / 2 - 2 * ledWith / 3);
                    points[3] = new Point(this.Width - spaceWidth - ledWith / 3, this.Height / 2);
                    points[4] = new Point(this.Width - spaceWidth - ledWith, this.Height / 2 + 2 * ledWith / 3);
                    points[5] = new Point(spaceWidth + ledWith, this.Height / 2 + 2 * ledWith / 3);
                    g.FillPolygon(brush, points);
                    break;

                case 8:
                    g.FillEllipse(brush, new RectangleF((this.Width - ledWith) / 2, (this.Height / 2 - ledWith) / 2, ledWith, ledWith));
                    break;

                case 9:
                    g.FillEllipse(brush, new RectangleF((this.Width - ledWith) / 2, this.Height / 2 + (this.Height / 2 - ledWith) / 2, ledWith, ledWith));
                    break;
            }
            brush.Dispose();
        }
    }
}