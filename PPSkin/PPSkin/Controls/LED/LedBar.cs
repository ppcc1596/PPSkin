﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace PPSkin
{
    public partial class LedBar : FlowLayoutPanel
    {
        private int charNum = 4;
        private string text = "";
        private int spaceWidth = 4;
        private Color numColor = Color.DodgerBlue;
        private int charInterval = 5;

        [Description("led文字的个数"), Category("PPSkin")]
        public int CharNum
        {
            get { return charNum; }
            set
            {
                if (value > 0)
                {
                    charNum = value;
                    loadLedNum(charNum);
                    SetText(text);
                }
            }
        }

        [Description("led文字的颜色"), Category("PPSkin")]
        public Color NumColor
        {
            get { return numColor; }
            set
            {
                numColor = value;
                foreach (LedNum ledNum in this.Controls)
                {
                    ledNum.NumColor = numColor; ;
                }
            }
        }

        [Description("led文字的笔划间隔"), Category("PPSkin")]
        public int SpaceWidth
        {
            get { return spaceWidth; }
            set
            {
                spaceWidth = value;
                foreach (LedNum ledNum in this.Controls)
                {
                    ledNum.SpaceWidth = spaceWidth;
                }
            }
        }

        [Description("led文字的间隔"), Category("PPSkin")]
        public int CharInterval
        {
            get { return charInterval; }
            set
            {
                charInterval = value;
                foreach (LedNum ledNum in this.Controls)
                {
                    ledNum.Margin = new Padding(charInterval, 0, charInterval, 0);
                }
            }
        }

        [Browsable(true), Description("led文字"), Category("PPSkin")]
        public new string Text
        {
            get { return text; }
            set
            {
                if (value != "")
                {
                    foreach (char c in value)
                    {
                        if (!UseableCharList.Contains(c))
                        {
                            return;
                        }
                    }
                }
                text = value;
                SetText(text);
            }
        }

        private List<char> UseableCharList = new List<char>() { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.', '-', ':', '_', ' ' };

        public LedBar()
        {
            InitializeComponent();
        }

        private void loadLedNum(int num)
        {
            this.Controls.Clear();
            for (int i = 0; i < num; i++)
            {
                LedNum ledNum = new LedNum();
                this.Controls.Add(ledNum);
                ledNum.Margin = new Padding(charInterval, 0, charInterval, 0);
                ledNum.Height = this.Height;
                ledNum.SpaceWidth = spaceWidth;
                ledNum.NumColor = numColor;
                //ledNum.Dock = DockStyle.Left;
            }
        }

        private void SetText(string text)
        {
            if (text.Length >= this.Controls.Count)
            {
                for (int i = 0; i < this.Controls.Count; i++)
                {
                    (this.Controls[i] as LedNum).NumChar = text[i];
                }
            }
            else
            {
                for (int i = 0; i < text.Length; i++)
                {
                    (this.Controls[i] as LedNum).NumChar = text[i];
                }
            }
        }

        protected override void OnHandleCreated(EventArgs e)
        {
            base.OnHandleCreated(e);
            loadLedNum(charNum);
            SetText(text);
        }

        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);
            foreach (Control c in this.Controls)
            {
                c.Height = this.Height;
                //this.BeginInvoke(new MethodInvoker(delegate () { c.Height = this.Height; }));
            }
        }
    }
}