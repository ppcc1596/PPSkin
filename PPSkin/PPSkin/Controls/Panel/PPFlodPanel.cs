﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PPSkin
{
    public partial class PPFlodPanel : Panel
    {
        private PPAnimation animation;
        private PPButton _button = new PPButton();
        private Color titleBaseColor = Color.LightGray;
        private Size arrowSize = new Size(13, 13);
        private string _text = "PPFoldPanel";
        private int BorderWidth { get; set; } = 1;

        [Description("标题高度")]
        public int TitleHeight { get; set; } = 30;

        [Description("圆角半径")]
        public int Radius { get; set; } = 5;
        [Description("边框颜色")]
        public Color BorderColor { get; set; } = Color.DodgerBlue;
        [Description("标题边框颜色")]
        public Color TitleBorderColor { get; set; } =Color.DodgerBlue;

        [Description("边框背景色")]
        public Color TitleBaseColor 
        {
            get { return titleBaseColor; }
            set { 
                titleBaseColor = value;
                _button.BackColor = TitleBaseColor;
                _button.RegularLineColor = TitleBaseColor;
                _button.RegularBaseColor = TitleBaseColor;
                _button.MouseInLineColor = TitleBaseColor;
                _button.MouseInBaseColor = TitleBaseColor;
                _button.MouseDownLineColor = TitleBaseColor;
                _button.MouseDownBaseColor = TitleBaseColor;
            }
        }

        [Description("箭头图标")]
        public Image ArrowImg { get; set; } = PPSkinResources.down;

        [Description("箭头Hover图标")]
        public Image ArrowHoverImg { get; set; } = PPSkinResources.downBlue;

        [Description("控件折叠高度")]
        public int FlodHeight { get; set; } = 100;

        [Description("是否折叠")]
        public bool IsFold {
            get { return this.Height<=TitleHeight; }
            set
            {
                if (!value)
                {
                    if(UseAnime)
                    {
                        Dictionary<string, float> dic = new Dictionary<string, float>();
                        dic.Add("Height", FlodHeight);
                        animation.AnimationControl(dic, AnimeTime);
                    }
                    else
                    {
                        this.Height = FlodHeight;
                    }
                    
                }
                else
                {
                    if(UseAnime)
                    {
                        Dictionary<string, float> dic = new Dictionary<string, float>();
                        dic.Add("Height", TitleHeight);
                        animation.AnimationControl(dic, AnimeTime);
                    }
                    else
                    {
                        this.Height = TitleHeight;
                    }
                    
                }
                    
            }
        }

        public new int Height
        {
            get { return base.Height; }
            set { 
                if(value<TitleHeight)
                    base.Height = TitleHeight;
                else
                    base.Height = value; 
            }
        }

        [Browsable(true)]
        public new string Text 
        {
            get { return _text; }
            set
            {
                _text = value;
            }
        }

        [Description("是否显示箭头图标")]
        public bool ShowArrow
        {
            get { return _button.Visible; }
            set
            {
                this._button.Visible = value;
            }
        }

        [Description("箭头图标大小")]
        public Size ArrowSize
        {
            get { return arrowSize; }
            set
            {
                arrowSize = value;
                _button.IcoSize = arrowSize;
                _button.Location = new Point(this.Width - BorderWidth - Radius - _button.Width + ArrowOffset.X, (TitleHeight - _button.Height) / 2 + ArrowOffset.Y);
            }
        }

        [Description("箭头图标位置偏移")]
        public Point ArrowOffset { get; set; }=new Point(0,0);

        [Description("使用动画")]
        public bool UseAnime { get; set; }=true;

        [Description("动画时间")]
        public int AnimeTime { get; set; } = 100;

        public PPFlodPanel()
        {
            InitializeComponent();

            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.DoubleBuffer, true);
            this.SetStyle(ControlStyles.ResizeRedraw, true);
            this.SetStyle(ControlStyles.Selectable, true);
            this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            this.SetStyle(ControlStyles.UserPaint, true);

            _button.Text = "";
            _button.Size = arrowSize;
            _button.IcoSize= arrowSize;
            _button.Cursor= Cursors.Hand;
            _button.BackColor = TitleBaseColor;
            _button.RegularLineColor = TitleBaseColor;
            _button.RegularBaseColor = TitleBaseColor;
            _button.MouseInLineColor = TitleBaseColor;
            _button.MouseInBaseColor = TitleBaseColor;
            _button.MouseDownLineColor = TitleBaseColor;
            _button.MouseDownBaseColor= TitleBaseColor;

            _button.IcoRegular = ArrowImg;
            _button.IcoDown = ArrowHoverImg;
            _button.IcoIn = ArrowHoverImg;

            _button.Click += _button_Click;
            this.Controls.Add(_button);
            _button.Location = new Point(this.Width - BorderWidth - Radius - _button.Width + ArrowOffset.X, (TitleHeight - _button.Height) / 2 + ArrowOffset.Y);

            animation = new PPAnimation(this);
            animation.AnimationType = AnimationType.EaseOut;

            this.ControlAdded += PPFlodPanel_ControlAdded;
            this.ControlRemoved += PPFlodPanel_ControlRemoved;
        }

        private void _button_Click(object sender, EventArgs e)
        {
            this.IsFold=!this.IsFold;
        }

        private void PPFlodPanel_ControlRemoved(object sender, ControlEventArgs e)
        {
            e.Control.LocationChanged -= Control_LocationChanged;
        }

        private void PPFlodPanel_ControlAdded(object sender, ControlEventArgs e)
        {
            e.Control.LocationChanged += Control_LocationChanged;
        }

        private void Control_LocationChanged(object sender, EventArgs e)
        {
            var c = sender as Control;
            if (c.Top < TitleHeight)
                c.Top = TitleHeight;
            //if(c.Left<=BorderWidth)
            //    c.Left = BorderWidth+1;
            //if(c.Right>this.Width-BorderWidth)
            //    c.Left=this.Width-BorderWidth-c.Width-1;
            //if(c.Bottom>this.Height-BorderWidth)
            //    c.Top=this.Height-BorderWidth-c.Height-1;
        }

        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);

            if(this.Height<this.TitleHeight)
                this.Height = this.TitleHeight;

            _button.Location=new Point(this.Width- BorderWidth - Radius-_button.Width+ArrowOffset.X,(TitleHeight-_button.Height)/2+ArrowOffset.Y);

            if(IsFold)
            {
                _button.IcoRegular = ArrowImg;
                _button.IcoIn = ArrowHoverImg;
                _button.IcoDown = ArrowHoverImg;
            }
            else
            {
                var aimg = (ArrowImg.Clone() as Bitmap);
                var ahimg = (ArrowHoverImg.Clone() as Bitmap);
                aimg.RotateFlip(RotateFlipType.RotateNoneFlipY);
                ahimg.RotateFlip(RotateFlipType.RotateNoneFlipY);

                _button.IcoRegular = aimg;
                _button.IcoIn = ahimg;
                _button.IcoDown = ahimg;
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            var g=e.Graphics;
            g.Clear(BackColor);
            PaintHelper.SetGraphicsHighQuality(g);
            g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;

            if (!IsFold)
            {
                var Borderpath = PaintHelper.CreatePath(new Rectangle(0, TitleHeight - 1, base.Width, base.Height - TitleHeight+1), Radius, Enums.RoundStyle.Bottom);
                using (var pen = new Pen(BorderColor, 1f))
                {
                    g.DrawPath(pen, Borderpath);
                }

            }

            var TitlePath=PaintHelper.CreatePath(new Rectangle(0,0,base.Width,TitleHeight), Radius, IsFold?Enums.RoundStyle.All:Enums.RoundStyle.Top);

            using (var brush=new SolidBrush(TitleBaseColor))
            {
                g.FillPath(brush, TitlePath);
            }
            using (var pen=new Pen(TitleBorderColor,1f))
            {
                g.DrawPath(pen, TitlePath);
            }



            using (var fontBrush=new SolidBrush(ForeColor))
            {
                g.DrawString(this.Text, this.Font, fontBrush, new Rectangle(0, 0, this.Width, TitleHeight),new StringFormat() { LineAlignment=StringAlignment.Center,Alignment=StringAlignment.Center});
            }
                

        }

        protected override void OnClick(EventArgs e)
        {
            base.OnClick(e);
            this.IsFold = !this.IsFold;
        }
    }
}
