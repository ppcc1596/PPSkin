﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace PPSkin
{
    public partial class PPPanel : System.Windows.Forms.Panel
    {
        private int radius = 5;
        private int borderWidth = 1;
        private Color baseColor = Control.DefaultBackColor;
        private Color borderColor = Color.LightGray;
        private bool showBorder = true;
        private Enums.RoundStyle roundStyle=Enums.RoundStyle.All;

        /// <summary>
        /// 圆角半径
        /// </summary>
        [Description("圆角半径"), Category("自定义")]
        public int Radius
        {
            get { return radius; }
            set
            {
                if (radius < 0)
                    return;
                int copy = radius;
                radius = value;
                this.Invalidate();
                this.Region = new Region(PaintHelper.CreatePath(new Rectangle(0, 0, this.Width, this.Height), radius));
                if (copy != radius)
                {
                    if (RadiusChanged != null)
                    {
                        RadiusChanged(this, new EventArgs());
                    }
                }
            }
        }

        /// <summary>
        /// 边框宽度
        /// </summary>
        [Description("边框宽度"), Category("自定义")]
        public int BorderWidth
        {
            get { return borderWidth; }
            set
            {
                if (value <= 0)
                    return;
                borderWidth = value;// % 2 == 0 ? value + 1 : value;
                this.Invalidate();
            }
        }

        /// <summary>
        /// 背景色
        /// </summary>
        [Description("背景色"), Category("自定义")]
        public Color BaseColor
        {
            get { return baseColor; }
            set
            {
                baseColor = value;
                this.Invalidate();
                if (BaseColorChanged != null)
                {
                    BaseColorChanged(this, new EventArgs());
                }
            }
        }

        /// <summary>
        /// 边框色
        /// </summary>
        [Description("边框色"), Category("自定义")]
        public Color BorderColor
        {
            get { return borderColor; }
            set
            {
                borderColor = value;
                this.Invalidate();
                if (LineColorChanged != null)
                {
                    LineColorChanged(this, new EventArgs());
                }
            }
        }

        [Description("是否显示边框"), Category("自定义")]
        public bool ShowBorder
        {
            get { return showBorder; }
            set
            {
                showBorder = value;
                this.Invalidate();
            }
        }

        [Description("圆角样式"), Category("自定义")]
        public Enums.RoundStyle RoundStyle
        {
            get { return roundStyle; }
            set
            {
                roundStyle = value;
                this.Invalidate(true);
                SetReion(radius);
            }
        }

        /// <summary>
        /// 尺寸变化事件
        /// </summary>
        [Description("尺寸变化事件")]
        public event EventHandler SizeChangeded;

        /// <summary>
        /// 圆角直径变化事件
        /// </summary>
        [Description("圆角直径变化事件")]
        public event EventHandler RadiusChanged;

        /// <summary>
        /// 内部颜色变化事件
        /// </summary>
        [Description("内部颜色变化事件")]
        public event EventHandler BaseColorChanged;

        /// <summary>
        /// 边框颜色变化事件
        /// </summary>
        [Description("边框颜色变化事件")]
        public event EventHandler LineColorChanged;

        public PPPanel()
        {
            InitializeComponent();
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.DoubleBuffer, true);
            this.SetStyle(ControlStyles.ResizeRedraw, true);
            this.SetStyle(ControlStyles.Selectable, true);
            this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            this.SetStyle(ControlStyles.UserPaint, true);
            SetReion(radius);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            int width = this.Width;
            int height = this.Height;
            int halflinewidth = borderWidth % 2 == 0 ? borderWidth / 2 : borderWidth / 2 + 1;
            Graphics g = e.Graphics;
            g.Clear(BackColor);
            g.SmoothingMode = SmoothingMode.AntiAlias;
            g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
            Pen pen = new Pen(borderColor, (float)borderWidth);
            SolidBrush brush = new SolidBrush(baseColor);

            GraphicsPath path = PaintHelper.CreatePath(new Rectangle(0, 0, this.Width, this.Height), radius,roundStyle);
            GraphicsPath path2 = PaintHelper.CreatePath(new RectangleF(borderWidth, borderWidth, this.Width - 2f * borderWidth, this.Height - 2f * borderWidth), radius,roundStyle);
            g.FillPath(brush, path2);
            if (showBorder)
            {
                g.DrawPath(pen, path2);
            }
            g.DrawPath(new Pen(this.BackColor), path);

            pen.Dispose();
            brush.Dispose();
            path.Dispose();
            path2.Dispose();

            base.OnPaint(e);
        }

        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);
            SetReion(radius);
            if (SizeChangeded != null)
            {
                SizeChangeded(this, e);
            }
        }

        private void SetReion(int radius)
        {
            using (GraphicsPath path = PaintHelper.CreatePath(new Rectangle(Point.Empty, base.Size), radius,roundStyle))
            {
                Region region = new Region(path);
                path.Widen(Pens.White);
                region.Union(path);
                this.Region = region;
            }
        }
    }
}