﻿namespace PPSkin
{
    partial class PPCombobox
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PPCombobox));
            this.ppNoBoraderCombobox1 = new PPSkin.PPNoBoraderCombobox();
            this.SuspendLayout();
            // 
            // ppNoBoraderCombobox1
            // 
            this.ppNoBoraderCombobox1.ArrowImage = ((System.Drawing.Image)(resources.GetObject("ppNoBoraderCombobox1.ArrowImage")));
            this.ppNoBoraderCombobox1.BackColor = System.Drawing.Color.White;
            this.ppNoBoraderCombobox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ppNoBoraderCombobox1.FormattingEnabled = true;
            this.ppNoBoraderCombobox1.ImageOffset = new System.Drawing.Point(0, 0);
            this.ppNoBoraderCombobox1.ImageSize = new System.Drawing.Size(13, 13);
            this.ppNoBoraderCombobox1.Location = new System.Drawing.Point(8, 9);
            this.ppNoBoraderCombobox1.Name = "ppNoBoraderCombobox1";
            this.ppNoBoraderCombobox1.Size = new System.Drawing.Size(118, 20);
            this.ppNoBoraderCombobox1.TabIndex = 0;
            this.ppNoBoraderCombobox1.SelectedIndexChanged += new System.EventHandler(this.ppNoBoraderCombobox1_SelectedIndexChanged);
            this.ppNoBoraderCombobox1.SelectedValueChanged += new System.EventHandler(this.ppNoBoraderCombobox1_SelectedValueChanged);
            this.ppNoBoraderCombobox1.SizeChanged += new System.EventHandler(this.ppNoBoraderCombobox1_SizeChanged);
            this.ppNoBoraderCombobox1.SelectionChangeCommitted += new System.EventHandler(this.ppNoBoraderCombobox1_SelectionChangeCommitted);
            // 
            // PPCombobox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.ppNoBoraderCombobox1);
            this.Name = "PPCombobox";
            this.Size = new System.Drawing.Size(136, 34);
            this.ResumeLayout(false);

        }

        #endregion

        private PPNoBoraderCombobox ppNoBoraderCombobox1;
    }
}
