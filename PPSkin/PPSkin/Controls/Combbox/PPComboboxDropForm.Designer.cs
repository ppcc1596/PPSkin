﻿
namespace PPSkin
{
    partial class PPComboboxDropForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.ppvScrollBarExt1 = new PPSkin.PPVScrollBarExt();
            this.SuspendLayout();
            // 
            // flowLayoutPanel
            // 
            this.flowLayoutPanel.BackColor = System.Drawing.Color.Transparent;
            this.flowLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel.Location = new System.Drawing.Point(3, 3);
            this.flowLayoutPanel.Name = "flowLayoutPanel";
            this.flowLayoutPanel.Size = new System.Drawing.Size(155, 210);
            this.flowLayoutPanel.TabIndex = 0;
            // 
            // ppvScrollBarExt1
            // 
            this.ppvScrollBarExt1.BackColor = System.Drawing.Color.Transparent;
            this.ppvScrollBarExt1.BaseColor = System.Drawing.Color.Gray;
            this.ppvScrollBarExt1.BaseRadius = 0;
            this.ppvScrollBarExt1.BaseSize = 1;
            this.ppvScrollBarExt1.ButtonColor = System.Drawing.Color.DodgerBlue;
            this.ppvScrollBarExt1.ButtonHoverColor = System.Drawing.Color.DeepSkyBlue;
            this.ppvScrollBarExt1.ButtonRadius = 5;
            this.ppvScrollBarExt1.ButtonSize = 5;
            this.ppvScrollBarExt1.Dock = System.Windows.Forms.DockStyle.Right;
            this.ppvScrollBarExt1.LargeChange = 10;
            this.ppvScrollBarExt1.Location = new System.Drawing.Point(141, 3);
            this.ppvScrollBarExt1.Maximum = 100;
            this.ppvScrollBarExt1.Minimum = 0;
            this.ppvScrollBarExt1.Name = "ppvScrollBarExt1";
            this.ppvScrollBarExt1.Size = new System.Drawing.Size(17, 210);
            this.ppvScrollBarExt1.SmallChange = 1;
            this.ppvScrollBarExt1.TabIndex = 1;
            this.ppvScrollBarExt1.Value = 0;
            this.ppvScrollBarExt1.VisibleValue = 10;
            // 
            // PPComboboxDropForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BorderColor = System.Drawing.Color.Red;
            this.ClientSize = new System.Drawing.Size(161, 216);
            this.Controls.Add(this.ppvScrollBarExt1);
            this.Controls.Add(this.flowLayoutPanel);
            this.Location = new System.Drawing.Point(0, 0);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PPComboboxDropForm";
            this.Padding = new System.Windows.Forms.Padding(3);
            this.Radius = 5;
            this.ShowBorder = true;
            this.Text = "PPComboboxDropForm";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel;
        private PPVScrollBarExt ppvScrollBarExt1;
    }
}