﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace PPSkin
{
    [DefaultEvent("SelectedIndexChanged")]
    public partial class PPCombobox : PPControlBase
    {
        [Category("自定义")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [Editor("System.Windows.Forms.Design.StringCollectionEditor, System.Design", "System.Drawing.Design.UITypeEditor, System.Drawing")]
        public ComboBox.ObjectCollection Items
        {
            get
            {
                return ppNoBoraderCombobox1.Items;
            }
            set
            {
                ppNoBoraderCombobox1.Items.Clear();
                ppNoBoraderCombobox1.Items.Add(value);
            }
        }

        [Browsable(false)]
        public int SelectedIndex
        {
            get { return ppNoBoraderCombobox1.SelectedIndex; }
            set
            {
                ppNoBoraderCombobox1.SelectedIndex = value;
            }
        }

        [Browsable(false)]
        public string SelectedText
        {
            get { return ppNoBoraderCombobox1.SelectedText; }
            set
            {
                ppNoBoraderCombobox1.SelectedText = value;
            }
        }

        [Browsable(false)]
        public object SelectedValue
        {
            get { return ppNoBoraderCombobox1.SelectedValue; }
            set
            {
                ppNoBoraderCombobox1.SelectedValue = value;
            }
        }

        [Browsable(false)]
        public object SelectedItem
        {
            get { return ppNoBoraderCombobox1.SelectedItem; }
            set
            {
                ppNoBoraderCombobox1.SelectedItem = value;
            }
        }

        [Description("箭头图片"), Category("自定义")]
        public Image ArrowImage
        {
            get { return ppNoBoraderCombobox1.ArrowImage; }
            set
            {
                ppNoBoraderCombobox1.ArrowImage = value;
                this.Invalidate();
            }
        }

        [Description("图片大小"), Category("自定义")]
        public Size ImageSize
        {
            get { return ppNoBoraderCombobox1.ImageSize; }
            set
            {
                ppNoBoraderCombobox1.ImageSize = value;
                this.Invalidate();
            }
        }

        [Description("图片位置"), Category("自定义")]
        public Point ImageOffset
        {
            get { return ppNoBoraderCombobox1.ImageOffset; }
            set
            {
                ppNoBoraderCombobox1.ImageOffset = value;
                this.Invalidate();
            }
        }

        public PPCombobox()
        {
            InitializeComponent();
            //this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            //this.SetStyle(ControlStyles.DoubleBuffer, true);
            //this.SetStyle(ControlStyles.ResizeRedraw, true);
            //this.SetStyle(ControlStyles.Selectable, true);
            //this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            //this.SetStyle(ControlStyles.UserPaint, true);
            //this.UpdateStyles();
            this.SizeChangeded += new EventHandler(This_SizeChangeded);
            this.RadiusChanged += new EventHandler(This_RadiusChanged);
            this.BaseColorChanged += new EventHandler(This_BaseColorChanged);
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;//用双缓冲绘制窗口的所有子控件
                return cp;
            }
        }

        private void This_SizeChangeded(object sender, EventArgs e)
        {
            this.Height = ppNoBoraderCombobox1.Height + (int)(2 * BorderWidth) + 2;
            ppNoBoraderCombobox1.Width = this.Width - Radius - 3;
            ppNoBoraderCombobox1.Location = new Point(Radius / 2 + 2, (int)BorderWidth + 1);
        }

        private void This_RadiusChanged(object sender, EventArgs e)
        {
            this.Height = ppNoBoraderCombobox1.Height + (int)(2 * BorderWidth) + 2;
            ppNoBoraderCombobox1.Width = this.Width - Radius - 3;
            ppNoBoraderCombobox1.Location = new Point(Radius / 2 + 2, (int)BorderWidth + 1);
        }

        private void This_BaseColorChanged(object sender, EventArgs e)
        {
            ppNoBoraderCombobox1.BackColor = BaseColor;
        }

        protected override void OnBackColorChanged(EventArgs e)
        {
            this.Invalidate();
            base.OnBackColorChanged(e);
            ppNoBoraderCombobox1.BackColor = BaseColor;
        }

        protected override void OnFontChanged(EventArgs e)
        {
            this.ppNoBoraderCombobox1.Font = base.Font;
            this.Invalidate();
            base.OnFontChanged(e);
        }

        protected override void OnForeColorChanged(EventArgs e)
        {
            ppNoBoraderCombobox1.ForeColor = base.ForeColor;
            this.Invalidate();
            base.OnForeColorChanged(e);
        }

        private void ppNoBoraderCombobox1_SizeChanged(object sender, EventArgs e)
        {
            this.Height = ppNoBoraderCombobox1.Height + (int)(2 * BorderWidth) + 2;
            ppNoBoraderCombobox1.Width = this.Width - Radius - 3;
            ppNoBoraderCombobox1.Location = new Point(Radius / 2 + 2, (int)BorderWidth + 1);
        }

        public event EventHandler SelectedIndexChanged;

        private void ppNoBoraderCombobox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (SelectedIndexChanged != null)
            {
                SelectedIndexChanged(this, new EventArgs());
            }
        }

        public event EventHandler SelectedValueChanged;

        private void ppNoBoraderCombobox1_SelectedValueChanged(object sender, EventArgs e)
        {
            if (SelectedValueChanged != null)
            {
                SelectedValueChanged(this, new EventArgs());
            }
        }

        public event EventHandler SelectionChangeCommitted;

        private void ppNoBoraderCombobox1_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (SelectionChangeCommitted != null)
            {
                SelectionChangeCommitted(this, new EventArgs());
            }
        }
    }
}