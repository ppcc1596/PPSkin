﻿
namespace PPSkin
{
    partial class PPScreenShotForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Panel_control = new PPSkin.PPPanel();
            this.Button_COPY = new PPSkin.PPButton();
            this.Button_SAVE = new PPSkin.PPButton();
            this.Panel_control.SuspendLayout();
            this.SuspendLayout();
            // 
            // Panel_control
            // 
            this.Panel_control.BackColor = System.Drawing.Color.White;
            this.Panel_control.BaseColor = System.Drawing.Color.White;
            this.Panel_control.BorderColor = System.Drawing.Color.LightGray;
            this.Panel_control.BorderWidth = 1;
            this.Panel_control.Controls.Add(this.Button_COPY);
            this.Panel_control.Controls.Add(this.Button_SAVE);
            this.Panel_control.Location = new System.Drawing.Point(513, 420);
            this.Panel_control.Name = "Panel_control";
            this.Panel_control.Radius = 0;
            this.Panel_control.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.Panel_control.ShowBorder = false;
            this.Panel_control.Size = new System.Drawing.Size(48, 24);
            this.Panel_control.TabIndex = 0;
            this.Panel_control.Visible = false;
            // 
            // Button_COPY
            // 
            this.Button_COPY.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Button_COPY.DialogResult = System.Windows.Forms.DialogResult.None;
            this.Button_COPY.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Button_COPY.IcoDown = global::PPSkin.PPSkinResources.ok;
            this.Button_COPY.IcoIn = global::PPSkin.PPSkinResources.ok;
            this.Button_COPY.IcoRegular = global::PPSkin.PPSkinResources.ok;
            this.Button_COPY.IcoSize = new System.Drawing.Size(25, 25);
            this.Button_COPY.LineWidth = 1;
            this.Button_COPY.Location = new System.Drawing.Point(-1, -1);
            this.Button_COPY.MouseDownBaseColor = System.Drawing.Color.White;
            this.Button_COPY.MouseDownLineColor = System.Drawing.Color.White;
            this.Button_COPY.MouseDownTextColor = System.Drawing.Color.Black;
            this.Button_COPY.MouseInBaseColor = System.Drawing.Color.White;
            this.Button_COPY.MouseInLineColor = System.Drawing.Color.White;
            this.Button_COPY.MouseInTextColor = System.Drawing.Color.Black;
            this.Button_COPY.Name = "Button_COPY";
            this.Button_COPY.Radius = 0;
            this.Button_COPY.RegularBaseColor = System.Drawing.Color.White;
            this.Button_COPY.RegularLineColor = System.Drawing.Color.White;
            this.Button_COPY.RegularTextColor = System.Drawing.Color.Black;
            this.Button_COPY.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.Button_COPY.Size = new System.Drawing.Size(25, 25);
            this.Button_COPY.TabIndex = 1;
            this.Button_COPY.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.Button_COPY.TextDrawMode = PPSkin.Enums.DrawMode.Anti;
            this.Button_COPY.TipAutoCloseTime = 5000;
            this.Button_COPY.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.Button_COPY.TipCloseOnLeave = true;
            this.Button_COPY.TipDeviation = new System.Drawing.Size(0, 0);
            this.Button_COPY.TipFontSize = 10;
            this.Button_COPY.TipFontText = null;
            this.Button_COPY.TipForeColor = System.Drawing.Color.White;
            this.Button_COPY.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.Button_COPY.TipText = "";
            this.Button_COPY.TipTopMoust = true;
            this.Button_COPY.Xoffset = 0;
            this.Button_COPY.XoffsetIco = 0;
            this.Button_COPY.Yoffset = 0;
            this.Button_COPY.YoffsetIco = 0;
            this.Button_COPY.Click += new System.EventHandler(this.Button_COPY_Click);
            // 
            // Button_SAVE
            // 
            this.Button_SAVE.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Button_SAVE.DialogResult = System.Windows.Forms.DialogResult.None;
            this.Button_SAVE.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Button_SAVE.IcoDown = global::PPSkin.PPSkinResources.save;
            this.Button_SAVE.IcoIn = global::PPSkin.PPSkinResources.save;
            this.Button_SAVE.IcoRegular = global::PPSkin.PPSkinResources.save;
            this.Button_SAVE.IcoSize = new System.Drawing.Size(25, 25);
            this.Button_SAVE.LineWidth = 1;
            this.Button_SAVE.Location = new System.Drawing.Point(24, -1);
            this.Button_SAVE.MouseDownBaseColor = System.Drawing.Color.White;
            this.Button_SAVE.MouseDownLineColor = System.Drawing.Color.White;
            this.Button_SAVE.MouseDownTextColor = System.Drawing.Color.Black;
            this.Button_SAVE.MouseInBaseColor = System.Drawing.Color.White;
            this.Button_SAVE.MouseInLineColor = System.Drawing.Color.White;
            this.Button_SAVE.MouseInTextColor = System.Drawing.Color.Black;
            this.Button_SAVE.Name = "Button_SAVE";
            this.Button_SAVE.Radius = 0;
            this.Button_SAVE.RegularBaseColor = System.Drawing.Color.White;
            this.Button_SAVE.RegularLineColor = System.Drawing.Color.White;
            this.Button_SAVE.RegularTextColor = System.Drawing.Color.Black;
            this.Button_SAVE.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.Button_SAVE.Size = new System.Drawing.Size(25, 25);
            this.Button_SAVE.TabIndex = 0;
            this.Button_SAVE.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.Button_SAVE.TextDrawMode = PPSkin.Enums.DrawMode.Anti;
            this.Button_SAVE.TipAutoCloseTime = 5000;
            this.Button_SAVE.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.Button_SAVE.TipCloseOnLeave = true;
            this.Button_SAVE.TipDeviation = new System.Drawing.Size(0, 0);
            this.Button_SAVE.TipFontSize = 10;
            this.Button_SAVE.TipFontText = null;
            this.Button_SAVE.TipForeColor = System.Drawing.Color.White;
            this.Button_SAVE.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.Button_SAVE.TipText = "";
            this.Button_SAVE.TipTopMoust = true;
            this.Button_SAVE.Xoffset = 0;
            this.Button_SAVE.XoffsetIco = 0;
            this.Button_SAVE.Yoffset = 0;
            this.Button_SAVE.YoffsetIco = 0;
            this.Button_SAVE.Click += new System.EventHandler(this.Button_SAVE_Click);
            // 
            // PPScreenShotForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(800, 480);
            this.Controls.Add(this.Panel_control);
            this.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "PPScreenShotForm";
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PPScreenShotForm";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.PPScreenShotForm_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.PPScreenShotForm_Paint);
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.PPScreenShotForm_MouseClick);
            this.Panel_control.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private PPPanel Panel_control;
        private PPButton Button_SAVE;
        private PPButton Button_COPY;
    }
}