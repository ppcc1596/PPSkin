﻿using System;

namespace PPSkin
{
    /// <summary>
    /// 用于DataGridViewButtonColumnEx的按钮点击事件
    /// </summary>
    public class DataGridViewButtonClickEventArgs : EventArgs
    {
        public int ColumnIndex { get; set; }

        public int RowIndex { get; set; }

        public object Value { get; set; }

        public System.Windows.Forms.MouseButtons Buttons { get; set; }

        public int Clicks { get; set; }

        public DataGridViewButtonClickEventArgs(int columnIndex, int rowIndex, object value, System.Windows.Forms.MouseButtons buttons, int clicks)
        {
            this.ColumnIndex = columnIndex;
            this.RowIndex = rowIndex;
            this.Value = value;
            this.Buttons = buttons;
            this.Clicks = clicks;
        }
    }
}