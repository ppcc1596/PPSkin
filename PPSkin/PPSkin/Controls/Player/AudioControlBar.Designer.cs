﻿
namespace PPSkin
{
    partial class AudioControlBar
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.label_totalTime = new System.Windows.Forms.Label();
            this.label_currentTime = new System.Windows.Forms.Label();
            this.TrackBar_volume = new PPSkin.PPTrackBar();
            this.TrackBar_time = new PPSkin.PPTrackBar();
            this.elementHost = new System.Windows.Forms.Integration.ElementHost();
            this.label_musicTitle = new System.Windows.Forms.Label();
            this.label_musicAuther = new System.Windows.Forms.Label();
            this.CheckBox_random = new PPSkin.PPCheckBox();
            this.pictureBox_musicImage = new System.Windows.Forms.PictureBox();
            this.Button_NextSong = new PPSkin.PPButton();
            this.Button_play_pause = new PPSkin.PPButton();
            this.Button_LastSong = new PPSkin.PPButton();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_musicImage)).BeginInit();
            this.SuspendLayout();
            // 
            // label_totalTime
            // 
            this.label_totalTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label_totalTime.AutoSize = true;
            this.label_totalTime.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.label_totalTime.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label_totalTime.ForeColor = System.Drawing.Color.White;
            this.label_totalTime.Location = new System.Drawing.Point(606, 37);
            this.label_totalTime.Name = "label_totalTime";
            this.label_totalTime.Size = new System.Drawing.Size(50, 16);
            this.label_totalTime.TabIndex = 18;
            this.label_totalTime.Text = "00:00:00";
            // 
            // label_currentTime
            // 
            this.label_currentTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label_currentTime.AutoSize = true;
            this.label_currentTime.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.label_currentTime.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label_currentTime.ForeColor = System.Drawing.Color.White;
            this.label_currentTime.Location = new System.Drawing.Point(550, 37);
            this.label_currentTime.Name = "label_currentTime";
            this.label_currentTime.Size = new System.Drawing.Size(50, 16);
            this.label_currentTime.TabIndex = 17;
            this.label_currentTime.Text = "00:00:00";
            // 
            // TrackBar_volume
            // 
            this.TrackBar_volume.BackColor = System.Drawing.Color.Transparent;
            this.TrackBar_volume.Corners = false;
            this.TrackBar_volume.Cursor = System.Windows.Forms.Cursors.Hand;
            this.TrackBar_volume.DcimalDigits = 0;
            this.TrackBar_volume.EllipseColor = System.Drawing.Color.LightGray;
            this.TrackBar_volume.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.TrackBar_volume.ForeColor = System.Drawing.Color.White;
            this.TrackBar_volume.IsShowTips = true;
            this.TrackBar_volume.LineColor = System.Drawing.Color.White;
            this.TrackBar_volume.LineWidth = 2;
            this.TrackBar_volume.Location = new System.Drawing.Point(673, 56);
            this.TrackBar_volume.MaxValue = ((long)(100));
            this.TrackBar_volume.MinValue = ((long)(0));
            this.TrackBar_volume.Name = "TrackBar_volume";
            this.TrackBar_volume.RadioButtonSize = 10;
            this.TrackBar_volume.Size = new System.Drawing.Size(100, 25);
            this.TrackBar_volume.TabIndex = 16;
            this.TrackBar_volume.Text = "ppTrackBar2";
            this.TrackBar_volume.TipBackColor = System.Drawing.Color.DeepSkyBlue;
            this.TrackBar_volume.TipForeColor = System.Drawing.Color.White;
            this.TrackBar_volume.TipsFormat = null;
            this.TrackBar_volume.Value = ((long)(0));
            this.TrackBar_volume.ValueColor = System.Drawing.Color.Aqua;
            this.TrackBar_volume.ValueIsSecond = false;
            this.TrackBar_volume.ValueChanged += new System.EventHandler(this.TrackBar_volume_ValueChanged);
            // 
            // TrackBar_time
            // 
            this.TrackBar_time.BackColor = System.Drawing.Color.Transparent;
            this.TrackBar_time.Corners = false;
            this.TrackBar_time.Cursor = System.Windows.Forms.Cursors.Hand;
            this.TrackBar_time.DcimalDigits = 0;
            this.TrackBar_time.EllipseColor = System.Drawing.Color.LightGray;
            this.TrackBar_time.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.TrackBar_time.ForeColor = System.Drawing.Color.White;
            this.TrackBar_time.IsShowTips = true;
            this.TrackBar_time.LineColor = System.Drawing.Color.White;
            this.TrackBar_time.LineWidth = 2;
            this.TrackBar_time.Location = new System.Drawing.Point(125, 56);
            this.TrackBar_time.MaxValue = ((long)(100));
            this.TrackBar_time.MinValue = ((long)(0));
            this.TrackBar_time.Name = "TrackBar_time";
            this.TrackBar_time.RadioButtonSize = 15;
            this.TrackBar_time.Size = new System.Drawing.Size(527, 25);
            this.TrackBar_time.TabIndex = 1;
            this.TrackBar_time.Text = "TrackBar_time";
            this.TrackBar_time.TipBackColor = System.Drawing.Color.DeepSkyBlue;
            this.TrackBar_time.TipForeColor = System.Drawing.Color.White;
            this.TrackBar_time.TipsFormat = null;
            this.TrackBar_time.Value = ((long)(0));
            this.TrackBar_time.ValueColor = System.Drawing.Color.Aqua;
            this.TrackBar_time.ValueIsSecond = true;
            this.TrackBar_time.ValueChanged += new System.EventHandler(this.TrackBar_time_ValueChanged);
            this.TrackBar_time.MouseDown += new System.Windows.Forms.MouseEventHandler(this.TrackBar_time_MouseDown);
            this.TrackBar_time.MouseUp += new System.Windows.Forms.MouseEventHandler(this.TrackBar_time_MouseUp);
            // 
            // elementHost
            // 
            this.elementHost.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.elementHost.BackColor = System.Drawing.Color.DodgerBlue;
            this.elementHost.Location = new System.Drawing.Point(788, 10);
            this.elementHost.Name = "elementHost";
            this.elementHost.Size = new System.Drawing.Size(33, 28);
            this.elementHost.TabIndex = 19;
            this.elementHost.Text = "elementHost1";
            this.elementHost.Child = null;
            // 
            // label_musicTitle
            // 
            this.label_musicTitle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label_musicTitle.AutoSize = true;
            this.label_musicTitle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.label_musicTitle.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label_musicTitle.ForeColor = System.Drawing.Color.White;
            this.label_musicTitle.Location = new System.Drawing.Point(81, 20);
            this.label_musicTitle.Name = "label_musicTitle";
            this.label_musicTitle.Size = new System.Drawing.Size(76, 19);
            this.label_musicTitle.TabIndex = 21;
            this.label_musicTitle.Text = "MusicTitle";
            // 
            // label_musicAuther
            // 
            this.label_musicAuther.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label_musicAuther.AutoSize = true;
            this.label_musicAuther.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.label_musicAuther.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label_musicAuther.ForeColor = System.Drawing.Color.White;
            this.label_musicAuther.Location = new System.Drawing.Point(81, 39);
            this.label_musicAuther.Name = "label_musicAuther";
            this.label_musicAuther.Size = new System.Drawing.Size(51, 19);
            this.label_musicAuther.TabIndex = 22;
            this.label_musicAuther.Text = "Auther";
            // 
            // CheckBox_random
            // 
            this.CheckBox_random.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CheckBox_random.BackColor = System.Drawing.Color.DodgerBlue;
            this.CheckBox_random.ButtonColor = System.Drawing.Color.White;
            this.CheckBox_random.Checked = false;
            this.CheckBox_random.CheckedBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(200)))), ((int)(((byte)(255)))));
            this.CheckBox_random.CheckedColor = System.Drawing.Color.Green;
            this.CheckBox_random.CheckedImage = global::PPSkin.PPSkinResources.random;
            this.CheckBox_random.CheckedText = "";
            this.CheckBox_random.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CheckBox_random.ImageSize = new System.Drawing.Size(30, 30);
            this.CheckBox_random.Location = new System.Drawing.Point(743, 9);
            this.CheckBox_random.Name = "CheckBox_random";
            this.CheckBox_random.Radius = 30;
            this.CheckBox_random.Size = new System.Drawing.Size(30, 30);
            this.CheckBox_random.TabIndex = 23;
            this.CheckBox_random.UnCheckedBaseColor = System.Drawing.Color.DodgerBlue;
            this.CheckBox_random.UnCheckedColor = System.Drawing.Color.Red;
            this.CheckBox_random.UnCheckedImage = global::PPSkin.PPSkinResources.random;
            this.CheckBox_random.UnCheckedText = "";
            this.CheckBox_random.Xoffset = 0;
            this.CheckBox_random.Yoffset = 0;
            this.CheckBox_random.CheckedChanged += new System.EventHandler(this.CheckBox_random_CheckedChanged);
            // 
            // pictureBox_musicImage
            // 
            this.pictureBox_musicImage.BackColor = System.Drawing.Color.DodgerBlue;
            this.pictureBox_musicImage.Location = new System.Drawing.Point(10, 10);
            this.pictureBox_musicImage.Name = "pictureBox_musicImage";
            this.pictureBox_musicImage.Size = new System.Drawing.Size(65, 65);
            this.pictureBox_musicImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_musicImage.TabIndex = 20;
            this.pictureBox_musicImage.TabStop = false;
            // 
            // Button_NextSong
            // 
            this.Button_NextSong.BackColor = System.Drawing.Color.DodgerBlue;
            this.Button_NextSong.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Button_NextSong.IcoDown = global::PPSkin.PPSkinResources.forward;
            this.Button_NextSong.IcoIn = global::PPSkin.PPSkinResources.forward;
            this.Button_NextSong.IcoRegular = global::PPSkin.PPSkinResources.forward;
            this.Button_NextSong.IcoSize = new System.Drawing.Size(30, 30);
            this.Button_NextSong.LineWidth = 3;
            this.Button_NextSong.Location = new System.Drawing.Point(418, 10);
            this.Button_NextSong.MouseDownBaseColor = System.Drawing.Color.LightGray;
            this.Button_NextSong.MouseDownLineColor = System.Drawing.Color.Silver;
            this.Button_NextSong.MouseDownTextColor = System.Drawing.Color.Black;
            this.Button_NextSong.MouseInBaseColor = System.Drawing.Color.LightGray;
            this.Button_NextSong.MouseInLineColor = System.Drawing.Color.White;
            this.Button_NextSong.MouseInTextColor = System.Drawing.Color.Black;
            this.Button_NextSong.Name = "Button_NextSong";
            this.Button_NextSong.Radius = 35;
            this.Button_NextSong.RegularBaseColor = System.Drawing.Color.DodgerBlue;
            this.Button_NextSong.RegularLineColor = System.Drawing.Color.White;
            this.Button_NextSong.RegularTextColor = System.Drawing.Color.Black;
            this.Button_NextSong.Size = new System.Drawing.Size(35, 35);
            this.Button_NextSong.TabIndex = 14;
            //this.Button_NextSong.Enabled = false;
            this.Button_NextSong.Xoffset = 0;
            this.Button_NextSong.XoffsetIco = 4;
            this.Button_NextSong.Yoffset = 0;
            this.Button_NextSong.YoffsetIco = 4;
            this.Button_NextSong.Click += new System.EventHandler(this.Button_NextSong_Click);
            // 
            // Button_play_pause
            // 
            this.Button_play_pause.BackColor = System.Drawing.Color.DodgerBlue;
            this.Button_play_pause.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Button_play_pause.IcoDown = global::PPSkin.PPSkinResources.play;
            this.Button_play_pause.IcoIn = global::PPSkin.PPSkinResources.play;
            this.Button_play_pause.IcoRegular = global::PPSkin.PPSkinResources.play;
            this.Button_play_pause.IcoSize = new System.Drawing.Size(43, 43);
            this.Button_play_pause.LineWidth = 3;
            this.Button_play_pause.Location = new System.Drawing.Point(367, 5);
            this.Button_play_pause.MouseDownBaseColor = System.Drawing.Color.LightGray;
            this.Button_play_pause.MouseDownLineColor = System.Drawing.Color.Silver;
            this.Button_play_pause.MouseDownTextColor = System.Drawing.Color.Black;
            this.Button_play_pause.MouseInBaseColor = System.Drawing.Color.LightGray;
            this.Button_play_pause.MouseInLineColor = System.Drawing.Color.White;
            this.Button_play_pause.MouseInTextColor = System.Drawing.Color.Black;
            this.Button_play_pause.Name = "Button_play_pause";
            this.Button_play_pause.Radius = 45;
            this.Button_play_pause.RegularBaseColor = System.Drawing.Color.DodgerBlue;
            this.Button_play_pause.RegularLineColor = System.Drawing.Color.White;
            this.Button_play_pause.RegularTextColor = System.Drawing.Color.Black;
            this.Button_play_pause.Size = new System.Drawing.Size(45, 45);
            this.Button_play_pause.TabIndex = 13;
            //this.Button_play_pause.Enabled = false;
            this.Button_play_pause.Xoffset = 0;
            this.Button_play_pause.XoffsetIco = 1;
            this.Button_play_pause.Yoffset = 0;
            this.Button_play_pause.YoffsetIco = 1;
            this.Button_play_pause.Click += new System.EventHandler(this.Button_play_pause_Click);
            // 
            // Button_LastSong
            // 
            this.Button_LastSong.BackColor = System.Drawing.Color.DodgerBlue;
            this.Button_LastSong.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Button_LastSong.IcoDown = global::PPSkin.PPSkinResources.backward;
            this.Button_LastSong.IcoIn = global::PPSkin.PPSkinResources.backward;
            this.Button_LastSong.IcoRegular = global::PPSkin.PPSkinResources.backward;
            this.Button_LastSong.IcoSize = new System.Drawing.Size(30, 30);
            this.Button_LastSong.LineWidth = 3;
            this.Button_LastSong.Location = new System.Drawing.Point(326, 10);
            this.Button_LastSong.MouseDownBaseColor = System.Drawing.Color.LightGray;
            this.Button_LastSong.MouseDownLineColor = System.Drawing.Color.Silver;
            this.Button_LastSong.MouseDownTextColor = System.Drawing.Color.Black;
            this.Button_LastSong.MouseInBaseColor = System.Drawing.Color.LightGray;
            this.Button_LastSong.MouseInLineColor = System.Drawing.Color.White;
            this.Button_LastSong.MouseInTextColor = System.Drawing.Color.Black;
            this.Button_LastSong.Name = "Button_LastSong";
            this.Button_LastSong.Radius = 35;
            this.Button_LastSong.RegularBaseColor = System.Drawing.Color.DodgerBlue;
            this.Button_LastSong.RegularLineColor = System.Drawing.Color.White;
            this.Button_LastSong.RegularTextColor = System.Drawing.Color.Black;
            this.Button_LastSong.Size = new System.Drawing.Size(35, 35);
            this.Button_LastSong.TabIndex = 12;
            //this.Button_LastSong.Enabled = false;
            this.Button_LastSong.Xoffset = 0;
            this.Button_LastSong.XoffsetIco = 2;
            this.Button_LastSong.Yoffset = 0;
            this.Button_LastSong.YoffsetIco = 4;
            this.Button_LastSong.Click += new System.EventHandler(this.Button_LastSong_Click);
            // 
            // AudioControlBar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.White;
            this.BaseColor = System.Drawing.Color.DodgerBlue;
            this.BorderColor = System.Drawing.Color.White;
            this.Controls.Add(this.CheckBox_random);
            this.Controls.Add(this.label_musicAuther);
            this.Controls.Add(this.label_musicTitle);
            this.Controls.Add(this.pictureBox_musicImage);
            this.Controls.Add(this.elementHost);
            this.Controls.Add(this.label_totalTime);
            this.Controls.Add(this.label_currentTime);
            this.Controls.Add(this.TrackBar_volume);
            this.Controls.Add(this.TrackBar_time);
            this.Controls.Add(this.Button_NextSong);
            this.Controls.Add(this.Button_play_pause);
            this.Controls.Add(this.Button_LastSong);
            this.Name = "AudioControlBar";
            this.Radius = 15;
            this.Size = new System.Drawing.Size(780, 84);
            this.Load += new System.EventHandler(this.AudioControlBar_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_musicImage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_totalTime;
        private System.Windows.Forms.Label label_currentTime;
        private PPSkin.PPTrackBar TrackBar_volume;
        private PPSkin.PPTrackBar TrackBar_time;
        private PPSkin.PPButton Button_NextSong;
        private PPSkin.PPButton Button_play_pause;
        private PPSkin.PPButton Button_LastSong;
        private System.Windows.Forms.Integration.ElementHost elementHost;
        private System.Windows.Forms.PictureBox pictureBox_musicImage;
        private System.Windows.Forms.Label label_musicTitle;
        private System.Windows.Forms.Label label_musicAuther;
        private PPSkin.PPCheckBox CheckBox_random;
    }
}
