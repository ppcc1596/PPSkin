﻿namespace PPSkin
{
    partial class PPMediaPlayer
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.elementHost1 = new System.Windows.Forms.Integration.ElementHost();
            this.panel_controls = new System.Windows.Forms.Panel();
            this.label_totalTime = new System.Windows.Forms.Label();
            this.label_currentTime = new System.Windows.Forms.Label();
            this.Button_openfile = new PPSkin.PPButton();
            this.Button_fullscreen = new PPSkin.PPButton();
            this.Button_forward = new PPSkin.PPButton();
            this.Button_backward = new PPSkin.PPButton();
            this.Button_stop = new PPSkin.PPButton();
            this.Button_play_pause = new PPSkin.PPButton();
            this.ppTrackBar_volume = new PPSkin.PPTrackBar();
            this.ppTrackBar1 = new PPSkin.PPTrackBar();
            this.panel_controls.SuspendLayout();
            this.SuspendLayout();
            // 
            // elementHost1
            // 
            this.elementHost1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.elementHost1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.elementHost1.Location = new System.Drawing.Point(0, 0);
            this.elementHost1.Name = "elementHost1";
            this.elementHost1.Size = new System.Drawing.Size(752, 423);
            this.elementHost1.TabIndex = 0;
            this.elementHost1.Text = "elementHost1";
            this.elementHost1.Child = null;
            // 
            // panel_controls
            // 
            this.panel_controls.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.panel_controls.Controls.Add(this.Button_openfile);
            this.panel_controls.Controls.Add(this.Button_fullscreen);
            this.panel_controls.Controls.Add(this.Button_forward);
            this.panel_controls.Controls.Add(this.Button_backward);
            this.panel_controls.Controls.Add(this.Button_stop);
            this.panel_controls.Controls.Add(this.Button_play_pause);
            this.panel_controls.Controls.Add(this.ppTrackBar_volume);
            this.panel_controls.Controls.Add(this.label_totalTime);
            this.panel_controls.Controls.Add(this.label_currentTime);
            this.panel_controls.Controls.Add(this.ppTrackBar1);
            this.panel_controls.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel_controls.Location = new System.Drawing.Point(0, 423);
            this.panel_controls.Name = "panel_controls";
            this.panel_controls.Size = new System.Drawing.Size(752, 48);
            this.panel_controls.TabIndex = 1;
            // 
            // label_totalTime
            // 
            this.label_totalTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label_totalTime.AutoSize = true;
            this.label_totalTime.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.label_totalTime.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label_totalTime.ForeColor = System.Drawing.Color.White;
            this.label_totalTime.Location = new System.Drawing.Point(696, 1);
            this.label_totalTime.Name = "label_totalTime";
            this.label_totalTime.Size = new System.Drawing.Size(49, 16);
            this.label_totalTime.TabIndex = 5;
            this.label_totalTime.Text = "00:00:00";
            // 
            // label_currentTime
            // 
            this.label_currentTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label_currentTime.AutoSize = true;
            this.label_currentTime.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.label_currentTime.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label_currentTime.ForeColor = System.Drawing.Color.White;
            this.label_currentTime.Location = new System.Drawing.Point(637, 1);
            this.label_currentTime.Name = "label_currentTime";
            this.label_currentTime.Size = new System.Drawing.Size(49, 16);
            this.label_currentTime.TabIndex = 2;
            this.label_currentTime.Text = "00:00:00";
            // 
            // Button_openfile
            // 
            this.Button_openfile.AllowDrop = true;
            this.Button_openfile.BackColor = System.Drawing.Color.DimGray;
            this.Button_openfile.DialogResult = System.Windows.Forms.DialogResult.None;
            this.Button_openfile.IcoDown = global::PPSkin.PPSkinResources.Open;
            this.Button_openfile.IcoIn = global::PPSkin.PPSkinResources.Open;
            this.Button_openfile.IcoRegular = global::PPSkin.PPSkinResources.Open;
            this.Button_openfile.IcoSize = new System.Drawing.Size(30, 30);
            this.Button_openfile.LineWidth = 1;
            this.Button_openfile.Location = new System.Drawing.Point(719, 15);
            this.Button_openfile.MouseDownBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.Button_openfile.MouseDownLineColor = System.Drawing.Color.Silver;
            this.Button_openfile.MouseDownTextColor = System.Drawing.Color.Black;
            this.Button_openfile.MouseInBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.Button_openfile.MouseInLineColor = System.Drawing.Color.Silver;
            this.Button_openfile.MouseInTextColor = System.Drawing.Color.Black;
            this.Button_openfile.Name = "Button_openfile";
            this.Button_openfile.Radius = 0;
            this.Button_openfile.RegularBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.Button_openfile.RegularLineColor = System.Drawing.Color.LightGray;
            this.Button_openfile.RegularTextColor = System.Drawing.Color.Black;
            this.Button_openfile.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.Button_openfile.Size = new System.Drawing.Size(30, 30);
            this.Button_openfile.TabIndex = 15;
            this.Button_openfile.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.Button_openfile.TextDrawMode = PPSkin.Enums.DrawMode.Anti;
            this.Button_openfile.TipAutoCloseTime = 5000;
            this.Button_openfile.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.Button_openfile.TipCloseOnLeave = true;
            this.Button_openfile.TipDeviation = new System.Drawing.Size(0, 0);
            this.Button_openfile.TipFontSize = 10;
            this.Button_openfile.TipFontText = null;
            this.Button_openfile.TipForeColor = System.Drawing.Color.White;
            this.Button_openfile.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.Button_openfile.TipText = null;
            this.Button_openfile.TipTopMoust = true;
            this.Button_openfile.UseVisualStyleBackColor = false;
            this.Button_openfile.Xoffset = 0;
            this.Button_openfile.XoffsetIco = 0;
            this.Button_openfile.Yoffset = 0;
            this.Button_openfile.YoffsetIco = 0;
            this.Button_openfile.Click += new System.EventHandler(this.Button_openfile_Click);
            this.Button_openfile.DragDrop += new System.Windows.Forms.DragEventHandler(this.Button_openfile_DragDrop);
            this.Button_openfile.DragEnter += new System.Windows.Forms.DragEventHandler(this.Button_openfile_DragEnter);
            // 
            // Button_fullscreen
            // 
            this.Button_fullscreen.BackColor = System.Drawing.Color.DimGray;
            this.Button_fullscreen.DialogResult = System.Windows.Forms.DialogResult.None;
            this.Button_fullscreen.IcoDown = global::PPSkin.PPSkinResources.Max;
            this.Button_fullscreen.IcoIn = global::PPSkin.PPSkinResources.Max;
            this.Button_fullscreen.IcoRegular = global::PPSkin.PPSkinResources.Max;
            this.Button_fullscreen.IcoSize = new System.Drawing.Size(30, 30);
            this.Button_fullscreen.LineWidth = 1;
            this.Button_fullscreen.Location = new System.Drawing.Point(689, 15);
            this.Button_fullscreen.MouseDownBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.Button_fullscreen.MouseDownLineColor = System.Drawing.Color.Silver;
            this.Button_fullscreen.MouseDownTextColor = System.Drawing.Color.Black;
            this.Button_fullscreen.MouseInBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.Button_fullscreen.MouseInLineColor = System.Drawing.Color.Silver;
            this.Button_fullscreen.MouseInTextColor = System.Drawing.Color.Black;
            this.Button_fullscreen.Name = "Button_fullscreen";
            this.Button_fullscreen.Radius = 0;
            this.Button_fullscreen.RegularBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.Button_fullscreen.RegularLineColor = System.Drawing.Color.LightGray;
            this.Button_fullscreen.RegularTextColor = System.Drawing.Color.Black;
            this.Button_fullscreen.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.Button_fullscreen.Size = new System.Drawing.Size(30, 30);
            this.Button_fullscreen.TabIndex = 14;
            this.Button_fullscreen.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.Button_fullscreen.TextDrawMode = PPSkin.Enums.DrawMode.Anti;
            this.Button_fullscreen.TipAutoCloseTime = 5000;
            this.Button_fullscreen.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.Button_fullscreen.TipCloseOnLeave = true;
            this.Button_fullscreen.TipDeviation = new System.Drawing.Size(0, 0);
            this.Button_fullscreen.TipFontSize = 10;
            this.Button_fullscreen.TipFontText = null;
            this.Button_fullscreen.TipForeColor = System.Drawing.Color.White;
            this.Button_fullscreen.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.Button_fullscreen.TipText = null;
            this.Button_fullscreen.TipTopMoust = true;
            this.Button_fullscreen.UseVisualStyleBackColor = false;
            this.Button_fullscreen.Xoffset = 0;
            this.Button_fullscreen.XoffsetIco = 0;
            this.Button_fullscreen.Yoffset = 0;
            this.Button_fullscreen.YoffsetIco = 0;
            this.Button_fullscreen.Click += new System.EventHandler(this.Button_fullscreen_Click);
            // 
            // Button_forward
            // 
            this.Button_forward.BackColor = System.Drawing.Color.DimGray;
            this.Button_forward.DialogResult = System.Windows.Forms.DialogResult.None;
            this.Button_forward.IcoDown = global::PPSkin.PPSkinResources.forward;
            this.Button_forward.IcoIn = global::PPSkin.PPSkinResources.forward;
            this.Button_forward.IcoRegular = global::PPSkin.PPSkinResources.forward;
            this.Button_forward.IcoSize = new System.Drawing.Size(30, 30);
            this.Button_forward.LineWidth = 1;
            this.Button_forward.Location = new System.Drawing.Point(93, 15);
            this.Button_forward.MouseDownBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.Button_forward.MouseDownLineColor = System.Drawing.Color.Silver;
            this.Button_forward.MouseDownTextColor = System.Drawing.Color.Black;
            this.Button_forward.MouseInBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.Button_forward.MouseInLineColor = System.Drawing.Color.Silver;
            this.Button_forward.MouseInTextColor = System.Drawing.Color.Black;
            this.Button_forward.Name = "Button_forward";
            this.Button_forward.Radius = 0;
            this.Button_forward.RegularBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.Button_forward.RegularLineColor = System.Drawing.Color.LightGray;
            this.Button_forward.RegularTextColor = System.Drawing.Color.Black;
            this.Button_forward.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.Button_forward.Size = new System.Drawing.Size(30, 30);
            this.Button_forward.TabIndex = 13;
            this.Button_forward.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.Button_forward.TextDrawMode = PPSkin.Enums.DrawMode.Anti;
            this.Button_forward.TipAutoCloseTime = 5000;
            this.Button_forward.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.Button_forward.TipCloseOnLeave = true;
            this.Button_forward.TipDeviation = new System.Drawing.Size(0, 0);
            this.Button_forward.TipFontSize = 10;
            this.Button_forward.TipFontText = null;
            this.Button_forward.TipForeColor = System.Drawing.Color.White;
            this.Button_forward.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.Button_forward.TipText = null;
            this.Button_forward.TipTopMoust = true;
            this.Button_forward.UseVisualStyleBackColor = false;
            this.Button_forward.Xoffset = 0;
            this.Button_forward.XoffsetIco = 0;
            this.Button_forward.Yoffset = 0;
            this.Button_forward.YoffsetIco = 0;
            this.Button_forward.Click += new System.EventHandler(this.Button_forward_Click);
            // 
            // Button_backward
            // 
            this.Button_backward.BackColor = System.Drawing.Color.DimGray;
            this.Button_backward.DialogResult = System.Windows.Forms.DialogResult.None;
            this.Button_backward.IcoDown = global::PPSkin.PPSkinResources.backward;
            this.Button_backward.IcoIn = global::PPSkin.PPSkinResources.backward;
            this.Button_backward.IcoRegular = global::PPSkin.PPSkinResources.backward;
            this.Button_backward.IcoSize = new System.Drawing.Size(30, 30);
            this.Button_backward.LineWidth = 1;
            this.Button_backward.Location = new System.Drawing.Point(63, 15);
            this.Button_backward.MouseDownBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.Button_backward.MouseDownLineColor = System.Drawing.Color.Silver;
            this.Button_backward.MouseDownTextColor = System.Drawing.Color.Black;
            this.Button_backward.MouseInBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.Button_backward.MouseInLineColor = System.Drawing.Color.Silver;
            this.Button_backward.MouseInTextColor = System.Drawing.Color.Black;
            this.Button_backward.Name = "Button_backward";
            this.Button_backward.Radius = 0;
            this.Button_backward.RegularBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.Button_backward.RegularLineColor = System.Drawing.Color.LightGray;
            this.Button_backward.RegularTextColor = System.Drawing.Color.Black;
            this.Button_backward.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.Button_backward.Size = new System.Drawing.Size(30, 30);
            this.Button_backward.TabIndex = 12;
            this.Button_backward.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.Button_backward.TextDrawMode = PPSkin.Enums.DrawMode.Anti;
            this.Button_backward.TipAutoCloseTime = 5000;
            this.Button_backward.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.Button_backward.TipCloseOnLeave = true;
            this.Button_backward.TipDeviation = new System.Drawing.Size(0, 0);
            this.Button_backward.TipFontSize = 10;
            this.Button_backward.TipFontText = null;
            this.Button_backward.TipForeColor = System.Drawing.Color.White;
            this.Button_backward.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.Button_backward.TipText = null;
            this.Button_backward.TipTopMoust = true;
            this.Button_backward.UseVisualStyleBackColor = false;
            this.Button_backward.Xoffset = 0;
            this.Button_backward.XoffsetIco = 0;
            this.Button_backward.Yoffset = 0;
            this.Button_backward.YoffsetIco = 0;
            this.Button_backward.Click += new System.EventHandler(this.Button_backward_Click);
            // 
            // Button_stop
            // 
            this.Button_stop.BackColor = System.Drawing.Color.DimGray;
            this.Button_stop.DialogResult = System.Windows.Forms.DialogResult.None;
            this.Button_stop.IcoDown = global::PPSkin.PPSkinResources.stop;
            this.Button_stop.IcoIn = global::PPSkin.PPSkinResources.stop;
            this.Button_stop.IcoRegular = global::PPSkin.PPSkinResources.stop;
            this.Button_stop.IcoSize = new System.Drawing.Size(30, 30);
            this.Button_stop.LineWidth = 1;
            this.Button_stop.Location = new System.Drawing.Point(33, 15);
            this.Button_stop.MouseDownBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.Button_stop.MouseDownLineColor = System.Drawing.Color.Silver;
            this.Button_stop.MouseDownTextColor = System.Drawing.Color.Black;
            this.Button_stop.MouseInBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.Button_stop.MouseInLineColor = System.Drawing.Color.Silver;
            this.Button_stop.MouseInTextColor = System.Drawing.Color.Black;
            this.Button_stop.Name = "Button_stop";
            this.Button_stop.Radius = 0;
            this.Button_stop.RegularBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.Button_stop.RegularLineColor = System.Drawing.Color.LightGray;
            this.Button_stop.RegularTextColor = System.Drawing.Color.Black;
            this.Button_stop.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.Button_stop.Size = new System.Drawing.Size(30, 30);
            this.Button_stop.TabIndex = 11;
            this.Button_stop.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.Button_stop.TextDrawMode = PPSkin.Enums.DrawMode.Anti;
            this.Button_stop.TipAutoCloseTime = 5000;
            this.Button_stop.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.Button_stop.TipCloseOnLeave = true;
            this.Button_stop.TipDeviation = new System.Drawing.Size(0, 0);
            this.Button_stop.TipFontSize = 10;
            this.Button_stop.TipFontText = null;
            this.Button_stop.TipForeColor = System.Drawing.Color.White;
            this.Button_stop.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.Button_stop.TipText = null;
            this.Button_stop.TipTopMoust = true;
            this.Button_stop.UseVisualStyleBackColor = false;
            this.Button_stop.Xoffset = 0;
            this.Button_stop.XoffsetIco = 0;
            this.Button_stop.Yoffset = 0;
            this.Button_stop.YoffsetIco = 0;
            this.Button_stop.Click += new System.EventHandler(this.Button_stop_Click);
            // 
            // Button_play_pause
            // 
            this.Button_play_pause.BackColor = System.Drawing.Color.DimGray;
            this.Button_play_pause.DialogResult = System.Windows.Forms.DialogResult.None;
            this.Button_play_pause.IcoDown = global::PPSkin.PPSkinResources.play;
            this.Button_play_pause.IcoIn = global::PPSkin.PPSkinResources.play;
            this.Button_play_pause.IcoRegular = global::PPSkin.PPSkinResources.play;
            this.Button_play_pause.IcoSize = new System.Drawing.Size(30, 30);
            this.Button_play_pause.LineWidth = 1;
            this.Button_play_pause.Location = new System.Drawing.Point(3, 15);
            this.Button_play_pause.MouseDownBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.Button_play_pause.MouseDownLineColor = System.Drawing.Color.Silver;
            this.Button_play_pause.MouseDownTextColor = System.Drawing.Color.Black;
            this.Button_play_pause.MouseInBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.Button_play_pause.MouseInLineColor = System.Drawing.Color.Silver;
            this.Button_play_pause.MouseInTextColor = System.Drawing.Color.Black;
            this.Button_play_pause.Name = "Button_play_pause";
            this.Button_play_pause.Radius = 0;
            this.Button_play_pause.RegularBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.Button_play_pause.RegularLineColor = System.Drawing.Color.LightGray;
            this.Button_play_pause.RegularTextColor = System.Drawing.Color.Black;
            this.Button_play_pause.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.Button_play_pause.Size = new System.Drawing.Size(30, 30);
            this.Button_play_pause.TabIndex = 10;
            this.Button_play_pause.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.Button_play_pause.TextDrawMode = PPSkin.Enums.DrawMode.Anti;
            this.Button_play_pause.TipAutoCloseTime = 5000;
            this.Button_play_pause.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.Button_play_pause.TipCloseOnLeave = true;
            this.Button_play_pause.TipDeviation = new System.Drawing.Size(0, 0);
            this.Button_play_pause.TipFontSize = 10;
            this.Button_play_pause.TipFontText = null;
            this.Button_play_pause.TipForeColor = System.Drawing.Color.White;
            this.Button_play_pause.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.Button_play_pause.TipText = null;
            this.Button_play_pause.TipTopMoust = true;
            this.Button_play_pause.UseVisualStyleBackColor = false;
            this.Button_play_pause.Xoffset = 0;
            this.Button_play_pause.XoffsetIco = 0;
            this.Button_play_pause.Yoffset = 0;
            this.Button_play_pause.YoffsetIco = 0;
            this.Button_play_pause.Click += new System.EventHandler(this.Button_play_pause_Click);
            // 
            // ppTrackBar_volume
            // 
            this.ppTrackBar_volume.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ppTrackBar_volume.BackColor = System.Drawing.Color.Transparent;
            this.ppTrackBar_volume.Corners = false;
            this.ppTrackBar_volume.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ppTrackBar_volume.DcimalDigits = 0;
            this.ppTrackBar_volume.EllipseBorderColor = System.Drawing.Color.White;
            this.ppTrackBar_volume.EllipseColor = System.Drawing.Color.White;
            this.ppTrackBar_volume.IsShowTips = true;
            this.ppTrackBar_volume.LineColor = System.Drawing.Color.LightGray;
            this.ppTrackBar_volume.LineWidth = 2;
            this.ppTrackBar_volume.Location = new System.Drawing.Point(583, 15);
            this.ppTrackBar_volume.MaxValue = ((long)(100));
            this.ppTrackBar_volume.MinValue = ((long)(0));
            this.ppTrackBar_volume.Name = "ppTrackBar_volume";
            this.ppTrackBar_volume.RadioButtonSize = 10;
            this.ppTrackBar_volume.Size = new System.Drawing.Size(100, 30);
            this.ppTrackBar_volume.TabIndex = 8;
            this.ppTrackBar_volume.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppTrackBar_volume.TipForeColor = System.Drawing.Color.White;
            this.ppTrackBar_volume.TipsFormat = null;
            this.ppTrackBar_volume.Value = ((long)(0));
            this.ppTrackBar_volume.ValueColor = System.Drawing.Color.Yellow;
            this.ppTrackBar_volume.ValueIsSecond = false;
            this.ppTrackBar_volume.ValueChanged += new System.EventHandler(this.ppTrackBar_volume_ValueChanged);
            // 
            // ppTrackBar1
            // 
            this.ppTrackBar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ppTrackBar1.BackColor = System.Drawing.Color.Transparent;
            this.ppTrackBar1.Corners = false;
            this.ppTrackBar1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ppTrackBar1.DcimalDigits = 0;
            this.ppTrackBar1.EllipseBorderColor = System.Drawing.Color.White;
            this.ppTrackBar1.EllipseColor = System.Drawing.Color.White;
            this.ppTrackBar1.ForeColor = System.Drawing.Color.White;
            this.ppTrackBar1.IsShowTips = true;
            this.ppTrackBar1.LineColor = System.Drawing.Color.LightGray;
            this.ppTrackBar1.LineWidth = 2;
            this.ppTrackBar1.Location = new System.Drawing.Point(3, 0);
            this.ppTrackBar1.MaxValue = ((long)(100));
            this.ppTrackBar1.MinValue = ((long)(0));
            this.ppTrackBar1.Name = "ppTrackBar1";
            this.ppTrackBar1.RadioButtonSize = 10;
            this.ppTrackBar1.Size = new System.Drawing.Size(628, 16);
            this.ppTrackBar1.TabIndex = 7;
            this.ppTrackBar1.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppTrackBar1.TipForeColor = System.Drawing.Color.White;
            this.ppTrackBar1.TipsFormat = null;
            this.ppTrackBar1.Value = ((long)(98));
            this.ppTrackBar1.ValueColor = System.Drawing.Color.DodgerBlue;
            this.ppTrackBar1.ValueIsSecond = true;
            this.ppTrackBar1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.trackBar1_MouseDown);
            this.ppTrackBar1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.trackBar1_MouseUp);
            // 
            // PPMediaPlayer
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Controls.Add(this.panel_controls);
            this.Controls.Add(this.elementHost1);
            this.Name = "PPMediaPlayer";
            this.Size = new System.Drawing.Size(752, 471);
            this.panel_controls.ResumeLayout(false);
            this.panel_controls.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Integration.ElementHost elementHost1;
        private System.Windows.Forms.Panel panel_controls;
        private System.Windows.Forms.Label label_totalTime;
        private System.Windows.Forms.Label label_currentTime;
        private PPTrackBar ppTrackBar1;
        private PPTrackBar ppTrackBar_volume;
        private PPButton Button_play_pause;
        private PPButton Button_forward;
        private PPButton Button_backward;
        private PPButton Button_stop;
        private PPButton Button_openfile;
        private PPButton Button_fullscreen;
    }
}
