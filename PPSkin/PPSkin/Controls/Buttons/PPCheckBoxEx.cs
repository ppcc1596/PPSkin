﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace PPSkin
{
    public partial class PPCheckBoxEx : CheckBox
    {
        #region 属性定义

        private Image checkedPic = PPSkinResources.CheckSelect.Clone() as Bitmap;
        private Image uncheckedPic = PPSkinResources.CheckNormal.Clone() as Bitmap;
        private Size picSize = new Size(15, 15);
        private Point picOffset = new Point(0, 0);
        private Point textOffset = new Point(0, 0);
        private Color checkedForeColor = Color.DodgerBlue;
        private Align checkAlign = Align.Left;
        private Color uncheckedForeColor = ColorTranslator.FromHtml("#333333");
        private string checkedText = "";
        private string uncheckedText = "";

        [Description("选中时的图片"), Category("PPSkin")]
        public Image CheckedPic
        {
            set { checkedPic = value; base.Invalidate(); }
            get { return checkedPic; }
        }

        [Description("未选中时的图片"), Category("PPSkin")]
        public Image unCheckedPic
        {
            set { uncheckedPic = value; base.Invalidate(); }
            get { return uncheckedPic; }
        }

        [Description("图标的大小"), Category("PPSkin")]
        public Size PicSize
        {
            set { picSize = value; base.Invalidate(); }
            get { return picSize; }
        }

        [Description("图标的位置偏移量"), Category("PPSkin")]
        public Point PicOffset
        {
            get { return picOffset; }
            set
            {
                picOffset = value;
                base.Invalidate();
            }
        }

        [Description("文字的位置偏移量"), Category("PPSkin")]
        public Point TextOffset
        {
            get { return textOffset; }
            set
            {
                textOffset = value;
                base.Invalidate();
            }
        }

        [Description("选中时的字体颜色"), Category("PPSkin")]
        public Color CheckedForeColor
        {
            set { checkedForeColor = value; base.Invalidate(); }
            get { return checkedForeColor; }
        }

        [Description("未选中时的字体颜色"), Category("PPSkin")]
        public Color UnCheckedForeColor
        {
            set { uncheckedForeColor = value; base.Invalidate(); }
            get { return uncheckedForeColor; }
        }

        [Description("复选框的位置"), Category("PPSkin")]
        public new Align CheckAlign
        {
            get { return checkAlign; }
            set { checkAlign = value; base.Invalidate(); }
        }

        [Description("选中时的文字"), Category("PPSkin")]
        public string CheckedText
        {
            get { return checkedText; }
            set { checkedText = value; }
        }

        [Description("未选中时的文字"), Category("PPSkin")]
        public string unCheckedText
        {
            get { return uncheckedText; }
            set { uncheckedText = value; }
        }

        public enum Align
        {
            Left,
            Right
        }

        public Enums.DrawMode TextDrawMode { get; set; } = Enums.DrawMode.Anti;


        #endregion 属性定义

        /// <summary>
        /// 基于原生ChecxBox的重绘版本
        /// </summary>
        public PPCheckBoxEx()
        {
            InitializeComponent();
        }

        protected override void OnPaint(PaintEventArgs pevent)
        {
            base.OnPaint(pevent);

            Graphics g = pevent.Graphics;
            g.Clear(base.BackColor);
            Rectangle radioButtonrect = new Rectangle(new Point(0, 0), picSize);
            RectangleF textrect = PaintHelper.GetTextCenterInRect(Text, base.Font, new RectangleF(18, 0, Width - 10, Height));
            Point TextPoint = new Point(18, (int)textrect.Y);
            if (checkAlign == Align.Left)
            {
                radioButtonrect = new Rectangle(new Point(picOffset.X, (this.Height - picSize.Height) / 2 + picOffset.Y), picSize);
                TextPoint = new Point(18, (int)textrect.Y);
            }
            else
            {
                radioButtonrect = new Rectangle(new Point(base.Width - 18 + picOffset.X, (this.Height - picSize.Height) / 2 + picOffset.Y), picSize);
                TextPoint = new Point(0, (int)textrect.Y);
            }
            g.SmoothingMode = SmoothingMode.AntiAlias;
            g.TextRenderingHint = TextDrawMode == Enums.DrawMode.Anti ? System.Drawing.Text.TextRenderingHint.AntiAlias : System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
            //g.FillRectangle(new SolidBrush(base.BackColor), -1, 0, 18, base.Height);
            string text = this.Text;
            if (this.Checked)
            {
                if (checkedText != "")
                    text = checkedText;
                g.DrawImage(checkedPic, radioButtonrect);
                Brush brushCheck = new SolidBrush(checkedForeColor);
                g.DrawString(text, base.Font, brushCheck, new Point(TextPoint.X + textOffset.X, TextPoint.Y + textOffset.Y));
                brushCheck.Dispose();
            }
            else
            {
                if (uncheckedText != "")
                    text = uncheckedText;
                g.DrawImage(uncheckedPic, radioButtonrect);
                Brush brushUncheck = new SolidBrush(uncheckedForeColor);
                g.DrawString(text, base.Font, brushUncheck, new Point(TextPoint.X + textOffset.X, TextPoint.Y + textOffset.Y));
                brushUncheck.Dispose();
            }
        }
    }
}