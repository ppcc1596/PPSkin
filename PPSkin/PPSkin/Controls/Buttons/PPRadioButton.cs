﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace PPSkin
{
    public partial class PPRadioButton : RadioButton
    {
        #region 属性定义

        private Image checkedPic = PPSkinResources.RadioChecked.Clone() as Bitmap;
        private Image uncheckedPic = PPSkinResources.RadioUnCheck.Clone() as Bitmap;
        private Size picSize = new Size(15, 15);
        private Point picOffset = new Point(0, 0);
        private Color checkedForeColor = Color.DodgerBlue;
        private Align checkAlign = Align.Left;
        private Color uncheckedForeColor = ColorTranslator.FromHtml("#333333");

        [Description("选中时的图片"), Category("PPSkin")]
        public Image CheckedPic
        {
            set { checkedPic = value; base.Invalidate(); }
            get { return checkedPic; }
        }

        [Description("未选中时的图片"), Category("PPSkin")]
        public Image unCheckedPic
        {
            set { uncheckedPic = value; base.Invalidate(); }
            get { return uncheckedPic; }
        }

        [Description("图标的大小"), Category("PPSkin")]
        public Size PicSize
        {
            set { picSize = value; base.Invalidate(); }
            get { return picSize; }
        }

        [Description("图标的位置偏移量"), Category("PPSkin")]
        public Point PicOffset
        {
            get { return picOffset; }
            set
            {
                picOffset = value;
                base.Invalidate();
            }
        }

        [Description("选中时的字体颜色"), Category("PPSkin")]
        public Color CheckedForeColor
        {
            set { checkedForeColor = value; base.Invalidate(); }
            get { return checkedForeColor; }
        }

        [Description("未选中时的字体颜色"), Category("PPSkin")]
        public Color UnCheckedForeColor
        {
            set { uncheckedForeColor = value; base.Invalidate(); }
            get { return uncheckedForeColor; }
        }

        [Description("复选框的位置"), Category("PPSkin")]
        public new Align CheckAlign
        {
            get { return checkAlign; }
            set { checkAlign = value; base.Invalidate(); }
        }

        public enum Align
        {
            Left,
            Right
        }

        #endregion 属性定义

        public Enums.DrawMode TextDrawMode { get; set; } = Enums.DrawMode.Anti;

        public PPRadioButton()
        {
            InitializeComponent();
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.DoubleBuffer, true);
            this.SetStyle(ControlStyles.ResizeRedraw, true);
            this.SetStyle(ControlStyles.Selectable, true);
            this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            this.SetStyle(ControlStyles.UserPaint, true);
            //this.Region = new Region(CreateRound(new Rectangle(0, 0, this.Width, this.Height), radius));
        }

        protected override void OnPaint(PaintEventArgs pevent)
        {
            //base.OnPaint(pevent);

            Graphics g = pevent.Graphics;
            g.SmoothingMode = SmoothingMode.AntiAlias;
            g.TextRenderingHint = TextDrawMode == Enums.DrawMode.Anti ? System.Drawing.Text.TextRenderingHint.AntiAlias : System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
            g.Clear(base.BackColor);
            Rectangle radioButtonrect = new Rectangle(new Point(0, 0), picSize);

            Rectangle textRect;
            StringFormat stringFormat = new StringFormat();

            if (checkAlign == Align.Left)
            {
                radioButtonrect = new Rectangle(new Point(picOffset.X, (this.Height - picSize.Height) / 2 + picOffset.Y), picSize);
                textRect = new Rectangle(radioButtonrect.Right + 3, 0, this.Width - radioButtonrect.Left - radioButtonrect.Width - 3, this.Height);
                stringFormat.LineAlignment = StringAlignment.Center;
                stringFormat.Alignment = StringAlignment.Near;
                stringFormat.Trimming = StringTrimming.EllipsisCharacter;
            }
            else
            {
                radioButtonrect = new Rectangle(new Point(this.Width - picSize.Width + picOffset.X, (this.Height - picSize.Height) / 2 + picOffset.Y), picSize);
                textRect = new Rectangle(0, 0, radioButtonrect.Left - 3, this.Height);
                stringFormat.LineAlignment = StringAlignment.Center;
                stringFormat.Alignment = StringAlignment.Far;
                stringFormat.Trimming = StringTrimming.EllipsisCharacter;
            }

            //g.FillRectangle(new SolidBrush(base.BackColor), -1, 0, 18, base.Height);
            if (this.Checked)
            {
                g.DrawImage(checkedPic, radioButtonrect);
                Brush brushCheck = new SolidBrush(checkedForeColor);
                g.DrawString(base.Text, base.Font, brushCheck, textRect, stringFormat);
                brushCheck.Dispose();
            }
            else
            {
                g.DrawImage(uncheckedPic, radioButtonrect);
                Brush brushUncheck = new SolidBrush(uncheckedForeColor);
                g.DrawString(base.Text, base.Font, brushUncheck, textRect, stringFormat);
                brushUncheck.Dispose();
            }
        }

        private GraphicsPath CreateRound(Rectangle rect, int radius)
        {
            GraphicsPath roundRect = new GraphicsPath();
            int halfradius = radius % 2 == 0 ? radius / 2 : radius / 2 + 1;
            if (radius != 0)
            {
                //顶端
                //roundRect.AddLine(rect.Left + halfradius-1, rect.Top, rect.Right - halfradius, rect.Top);
                //右上角
                roundRect.AddArc(rect.Right - radius, rect.Top, radius, radius, 270f, 90f);
                //右边
                //roundRect.AddLine(rect.Right, rect.Top + halfradius, rect.Right, rect.Bottom - halfradius);
                //右下角

                roundRect.AddArc(rect.Right - radius, rect.Bottom - radius, radius, radius, 0f, 90f);
                //底边
                //roundRect.AddLine(rect.Right - halfradius, rect.Bottom, rect.Left + halfradius, rect.Bottom);
                //左下角
                roundRect.AddArc(rect.Left, rect.Bottom - radius, radius, radius, 90f, 90f);
                //左边
                //roundRect.AddLine(rect.Left, rect.Top + halfradius, rect.Left, rect.Bottom - halfradius);
                //左上角
                roundRect.AddArc(rect.Left, rect.Top, radius, radius, 180f, 90f);

                roundRect.CloseFigure();
            }
            else
            {
                //顶端
                roundRect.AddLine(rect.Left, rect.Top, rect.Right, rect.Top);
                //右边
                roundRect.AddLine(rect.Right, rect.Top, rect.Right, rect.Bottom);
                //底边
                roundRect.AddLine(rect.Right, rect.Bottom, rect.Left, rect.Bottom);
                //左边
                roundRect.AddLine(rect.Left, rect.Top, rect.Left, rect.Bottom);
            }

            return roundRect;
        }
    }
}