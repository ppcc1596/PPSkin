﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using PPSkin;

namespace PPSkinDemo
{
    public partial class Demo : PPForm
    {
        System.Windows.Forms.Timer timer_bar =new System.Windows.Forms.Timer();
        public Demo()
        {
            InitializeComponent();
            this.ppTabButtonBar1.BindingTabControl(ppTabControl1);
            this.ppTabControl1.ShowTab = false;

            timer_bar.Tick += Timer_bar_Tick;
            timer_bar.Interval = 50;
            timer_bar.Enabled = true;
        }

        private void Timer_bar_Tick(object sender, EventArgs e)
        {
            ledBar1.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
            Random random = new Random();
            ledBar1.NumColor = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));

            foreach (Control c in tabPage3.Controls)
            {
                if (c is PPVScrollBarExt)
                {
                    (c as PPVScrollBarExt).Value += 1;
                    if ((c as PPVScrollBarExt).Value == ((c as PPVScrollBarExt).Maximum - (c as PPVScrollBarExt).VisibleValue))
                    {
                        (c as PPVScrollBarExt).Value = (c as PPVScrollBarExt).Minimum;
                    }
                }
                if (c is PPHScrollBarExt)
                {
                    (c as PPHScrollBarExt).Value += 1;
                    if ((c as PPHScrollBarExt).Value == ((c as PPHScrollBarExt).Maximum - (c as PPHScrollBarExt).VisibleValue))
                    {
                        (c as PPHScrollBarExt).Value = (c as PPHScrollBarExt).Minimum;
                    }
                }
                if (c is PPRoundProgressBar)
                {
                    (c as PPRoundProgressBar).Value += 1;
                    if ((c as PPRoundProgressBar).Value == (c as PPRoundProgressBar).Maximum)
                    {
                        (c as PPRoundProgressBar).Value = (c as PPRoundProgressBar).Minimum;
                    }
                }

                if (c is PPProgressBar)
                {
                    (c as PPProgressBar).Value += 1;
                    if ((c as PPProgressBar).Value == (c as PPProgressBar).Maximum)
                    {
                        (c as PPProgressBar).Value = (c as PPProgressBar).Minimum;
                    }
                }

                if (c is PPTrackBar)
                {
                    (c as PPTrackBar).Value += 1;
                    if ((c as PPTrackBar).Value == (c as PPTrackBar).MaxValue)
                    {
                        (c as PPTrackBar).Value = (c as PPTrackBar).MinValue;
                    }
                }

                if (c is PPWaveProgressBar)
                {
                    (c as PPWaveProgressBar).Value += 1;
                    if ((c as PPWaveProgressBar).Value == (c as PPWaveProgressBar).Maximum)
                    {
                        (c as PPWaveProgressBar).Value = (c as PPWaveProgressBar).Minimum;
                    }
                }
            }
        }

        private void Demo_Load(object sender, EventArgs e)
        {
            panel1Animation = new PPAnimation(panel1);
            progressBarAnimation = new PPAnimation(ppRoundProgressBar9);
            panel3Anime= new PPAnimation(panel3);
            ppvScrollBarExt8.BindingControl(ppDataGridView1);
            pphScrollBarExt7.BindingControl(ppDataGridView1);

            ppvScrollBarExt9.BindingControl(ppDataGridView2);
            pphScrollBarExt8.BindingControl(ppDataGridView2);

            ppvScrollBarExt10.BindingControl(ppDataGridView3);
            pphScrollBarExt9.BindingControl(ppDataGridView3);

            ppvScrollBarExt11.BindingControl(ppDataGridView4);
            pphScrollBarExt10.BindingControl(ppDataGridView4);

            ppDataGridView2.LoadStyle(PPDataGridView.Style.Default);

            string[] fname = new string[] { "赵", "钱", "孙", "李", "周", "吴", "郑", "王", "潘", "邹" };

            string[] lname = new string[] { "华", "花", "丽", "芳", "美美", "刚", "明", "超", "婷", "林" };

            string[] sex = new string[] { "男", "女" };

            DataTable dataTable = new DataTable();
            dataTable.Columns.Add("姓名");
            dataTable.Columns.Add("年龄");
            dataTable.Columns.Add("性别");


            Random random = new Random();

            for (int i = 0; i < 100; i++)
            {
                DataRow dataRow = dataTable.NewRow();

                dataRow.ItemArray = new object[] { fname[random.Next(0, 9)] + lname[random.Next(0, 9)], random.Next(5, 18).ToString(), sex[random.Next(0, 1)] };

                dataTable.Rows.Add(dataRow);

            }
            ppDataGridView1.DataSource = dataTable;
            ppDataGridView2.DataSource = dataTable;
            ppDataGridView3.DataSource = dataTable;
            ppDataGridView4.DataSource = dataTable;

            ppDataGridView1.Columns.Add(new PPDataGridViewButtonColumn("按钮1"));
            ppDataGridView2.Columns.Add(new PPDataGridViewButtonColumn("按钮2"));
            ppDataGridView3.Columns.Add(new PPDataGridViewButtonColumn("按钮3"));
            ppDataGridView4.Columns.Add(new PPDataGridViewButtonColumn("按钮4"));


            ppTabButtonBar1.Images = new List<Bitmap>() { Properties.Resources.newBlue };

            PPToolTip pPToolTip = new PPToolTip();
            //ToolTip pPToolTip = new ToolTip();
            //pPToolTip.BaseColor = Color.Red;
            pPToolTip.BorderColor = Color.Red;
            //pPToolTip.ToolTipTitle = "asdadad";
            pPToolTip.ToolTipIcon=ToolTipIcon.Info;
            //pPToolTip.IconSize = new System.Drawing.Size(32,32);
            //pPToolTip.Icon = Properties.Resources.newGray;

            pPToolTip.SetToolTip(ppButton1, "asdadadadadasdasd");

            var iconFont= MemoryFontHelper.FontFromMemorey(Properties.Resources.iconfont, 15f);
            ppButton3.Font = iconFont;
            ppButton3.Text = "\ue68b";

            ppCheckBox20.Font = iconFont;
            ppCheckBox20.CheckedText = "\ue691";
            ppCheckBox20.UnCheckedText = "\ue693";


        }

        PPAnimation panel1Animation;
        private void ppButton34_Click(object sender, EventArgs e)
        {
            var index = ppComboboxEx6.SelectedIndex;
            panel1.Bounds = new Rectangle(200, 200, 100, 100);
            panel1Animation.AnimationType = (AnimationType)index;
            Rectangle rectangle = new Rectangle(400, 200, 100, 100);
            if (ppRadioButton10.Checked)
            {
                rectangle = new Rectangle(400, 150, 200, 200);
            }
            else if (ppRadioButton11.Checked)
            {
                rectangle = new Rectangle(150, 150, 200, 200);
            }
            //panel1Animation.isBlock = true;
            //var stopwatch = new Stopwatch();
            //stopwatch.Start();
            panel1Animation.AnimationRectangle(rectangle, ppNumericUpDown4.Value);
            //stopwatch.Stop();
            //MessageBox.Show(stopwatch.Elapsed.TotalSeconds.ToString());
        }

        private void ppButton39_Click(object sender, EventArgs e)
        {
            PPSkin.MaskForm mf = new PPSkin.MaskForm(this, Properties.Resources.loading1, 100, 100);
            mf.Opacity = 0.5;
            mf.BackColor = Color.Black;
            Thread t = new Thread(delegate () { maskTest(mf); });
            t.Start();
            this.BeginInvoke(new MethodInvoker(() => { mf.ShowDialog(); }));
        }

        private void ppButton38_Click(object sender, EventArgs e)
        {
            PPSkin.MaskForm mf = new PPSkin.MaskForm(this, Properties.Resources.loading1, 50, 50);
            mf.Opacity = 0.2;
            mf.BackColor = Color.Green;
            Thread t = new Thread(delegate () { maskTest(mf); });
            t.Start();
            this.BeginInvoke(new MethodInvoker(() => { mf.ShowDialog(); }));
        }

        private void ppButton37_Click(object sender, EventArgs e)
        {

            PPSkin.MaskForm mf = new PPSkin.MaskForm(ppPanel8, Properties.Resources.loading1, 50, 50);
            mf.Opacity = 0.2;
            mf.BackColor = Color.Green;
            Thread t = new Thread(delegate () { maskTest(mf); });
            t.Start();
            this.BeginInvoke(new MethodInvoker(() => { mf.Show(); }));
        }

        private void ppButton36_Click(object sender, EventArgs e)
        {
            PPSkin.MaskForm mf = new PPSkin.MaskForm(ppRichTextbox5, Properties.Resources.loading1, 100, 100);
            mf.Opacity = 0.5;
            mf.BackColor = Color.Black;
            Thread t = new Thread(delegate () { maskTest(mf); });
            t.Start();
            this.BeginInvoke(new MethodInvoker(() => { mf.Show(); }));
        }

        private void ppButton32_Click(object sender, EventArgs e)
        {
            PPSkin.MaskForm mf = new PPSkin.MaskForm(ppButton40, Properties.Resources.loading1, 20, 20);
            mf.Opacity = 0.2;
            mf.BackColor = Color.Green;
            Thread t = new Thread(delegate () { maskTest(mf); });
            t.Start();
            this.BeginInvoke(new MethodInvoker(() => { mf.Show(); }));
        }

        private void ppButton33_Click(object sender, EventArgs e)
        {
            PPSkin.MaskForm mf = new PPSkin.MaskForm(ppComboboxEx9, Properties.Resources.loading1, 20, 20);
            mf.Opacity = 0.2;
            mf.BackColor = Color.Green;
            Thread t = new Thread(delegate () { maskTest(mf); });
            t.Start();
            this.BeginInvoke(new MethodInvoker(() => { mf.Show(); }));
        }

        private void maskTest(PPSkin.MaskForm mf)
        {
            int i = 0;
            while (i < 5)
            {
                Thread.Sleep(1000);
                i++;
            }
            this.BeginInvoke(new MethodInvoker(() => { mf.Close(); }));
        }

        private void ppCheckBox21_CheckedChanged(object sender, EventArgs e)
        {
            ppMovePanel1.MoveLock = ppCheckBox21.Checked;
        }

        private void ppButton35_Click(object sender, EventArgs e)
        {
            PPScreenShot.ShowScreenShot();
        }

        private void ppButton41_Click(object sender, EventArgs e)
        {
           new FormExt().Show();
        }

        private void ppButton43_Click(object sender, EventArgs e)
        {
            Dictionary<string, float> dic = new Dictionary<string, float>();
            dic.Add("BackColor", panel2.BackColor.ToArgb());
            panel1Animation.AnimationControl(dic, ppNumericUpDown4.Value);

        }

        private void panel2_Click(object sender, EventArgs e)
        {
            colorDialog1.ShowDialog();
            panel2.BackColor = colorDialog1.Color;
        }

        PPAnimation progressBarAnimation;
        private void ppButton53_Click(object sender, EventArgs e)
        {
            var index = ppComboboxEx6.SelectedIndex;
            progressBarAnimation.AnimationType = (AnimationType)index;
            progressBarAnimation.isBlock = false;
            Dictionary<string, float> dic = new Dictionary<string, float>();
            dic.Add("Value", ppNumericUpDown5.Value);
            progressBarAnimation.AnimationControl(dic, ppNumericUpDown4.Value);
        }

        PPAnimation panel3Anime;

        private void panel3_MouseEnter(object sender, EventArgs e)
        {
            Dictionary<string, float> dic = new Dictionary<string, float>();
            dic.Add("BackColor", Color.Red.ToArgb());
            panel3Anime.AnimationRunType = AnimationRunType.Interrupt;
            panel3Anime.AnimationControl(dic, 100);
        }

        private void panel3_MouseLeave(object sender, EventArgs e)
        {
            Dictionary<string, float> dic = new Dictionary<string, float>();
            dic.Add("BackColor", Color.White.ToArgb());
            panel3Anime.AnimationRunType = AnimationRunType.Interrupt;
            panel3Anime.AnimationControl(dic, 100);
        }

        private void SetFontFamily(Control c,FontFamily fontFamily)
        {
            c.Font=new Font(fontFamily,c.Font.Size,c.Font.Style);
            if(c.Controls.Count>0)
            {
                foreach(Control ch in c.Controls)
                {
                    SetFontFamily(ch, fontFamily);
                }
            }
        }

        private void ppButton54_Click(object sender, EventArgs e)
        {
            if(fontDialog1.ShowDialog()==DialogResult.OK)
            {
                SetFontFamily(this, fontDialog1.Font.FontFamily);
            }
        }

        private void ppRoundProgressBar9_ValueChanged(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(ppRoundProgressBar9.Value);
        }
    }
}
