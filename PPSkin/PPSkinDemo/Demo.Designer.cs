﻿namespace PPSkinDemo
{
    partial class Demo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Demo));
            System.Windows.Forms.TreeNode treeNode64 = new System.Windows.Forms.TreeNode("节点19");
            System.Windows.Forms.TreeNode treeNode65 = new System.Windows.Forms.TreeNode("节点20");
            System.Windows.Forms.TreeNode treeNode66 = new System.Windows.Forms.TreeNode("节点5", new System.Windows.Forms.TreeNode[] {
            treeNode64,
            treeNode65});
            System.Windows.Forms.TreeNode treeNode67 = new System.Windows.Forms.TreeNode("节点6");
            System.Windows.Forms.TreeNode treeNode68 = new System.Windows.Forms.TreeNode("节点0", new System.Windows.Forms.TreeNode[] {
            treeNode66,
            treeNode67});
            System.Windows.Forms.TreeNode treeNode69 = new System.Windows.Forms.TreeNode("节点7");
            System.Windows.Forms.TreeNode treeNode70 = new System.Windows.Forms.TreeNode("节点8");
            System.Windows.Forms.TreeNode treeNode71 = new System.Windows.Forms.TreeNode("节点9");
            System.Windows.Forms.TreeNode treeNode72 = new System.Windows.Forms.TreeNode("节点1", new System.Windows.Forms.TreeNode[] {
            treeNode69,
            treeNode70,
            treeNode71});
            System.Windows.Forms.TreeNode treeNode73 = new System.Windows.Forms.TreeNode("节点13");
            System.Windows.Forms.TreeNode treeNode74 = new System.Windows.Forms.TreeNode("节点2", new System.Windows.Forms.TreeNode[] {
            treeNode73});
            System.Windows.Forms.TreeNode treeNode75 = new System.Windows.Forms.TreeNode("节点10");
            System.Windows.Forms.TreeNode treeNode76 = new System.Windows.Forms.TreeNode("节点16");
            System.Windows.Forms.TreeNode treeNode77 = new System.Windows.Forms.TreeNode("节点17");
            System.Windows.Forms.TreeNode treeNode78 = new System.Windows.Forms.TreeNode("节点18");
            System.Windows.Forms.TreeNode treeNode79 = new System.Windows.Forms.TreeNode("节点11", new System.Windows.Forms.TreeNode[] {
            treeNode76,
            treeNode77,
            treeNode78});
            System.Windows.Forms.TreeNode treeNode80 = new System.Windows.Forms.TreeNode("节点12");
            System.Windows.Forms.TreeNode treeNode81 = new System.Windows.Forms.TreeNode("节点3", new System.Windows.Forms.TreeNode[] {
            treeNode75,
            treeNode79,
            treeNode80});
            System.Windows.Forms.TreeNode treeNode82 = new System.Windows.Forms.TreeNode("节点14");
            System.Windows.Forms.TreeNode treeNode83 = new System.Windows.Forms.TreeNode("节点15");
            System.Windows.Forms.TreeNode treeNode84 = new System.Windows.Forms.TreeNode("节点4", new System.Windows.Forms.TreeNode[] {
            treeNode82,
            treeNode83});
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("节点19");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("节点20");
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("节点5", new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode2});
            System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("节点6");
            System.Windows.Forms.TreeNode treeNode5 = new System.Windows.Forms.TreeNode("节点0", new System.Windows.Forms.TreeNode[] {
            treeNode3,
            treeNode4});
            System.Windows.Forms.TreeNode treeNode6 = new System.Windows.Forms.TreeNode("节点7");
            System.Windows.Forms.TreeNode treeNode7 = new System.Windows.Forms.TreeNode("节点8");
            System.Windows.Forms.TreeNode treeNode8 = new System.Windows.Forms.TreeNode("节点9");
            System.Windows.Forms.TreeNode treeNode9 = new System.Windows.Forms.TreeNode("节点1", new System.Windows.Forms.TreeNode[] {
            treeNode6,
            treeNode7,
            treeNode8});
            System.Windows.Forms.TreeNode treeNode10 = new System.Windows.Forms.TreeNode("节点13");
            System.Windows.Forms.TreeNode treeNode11 = new System.Windows.Forms.TreeNode("节点2", new System.Windows.Forms.TreeNode[] {
            treeNode10});
            System.Windows.Forms.TreeNode treeNode12 = new System.Windows.Forms.TreeNode("节点10");
            System.Windows.Forms.TreeNode treeNode13 = new System.Windows.Forms.TreeNode("节点16");
            System.Windows.Forms.TreeNode treeNode14 = new System.Windows.Forms.TreeNode("节点17");
            System.Windows.Forms.TreeNode treeNode15 = new System.Windows.Forms.TreeNode("节点18");
            System.Windows.Forms.TreeNode treeNode16 = new System.Windows.Forms.TreeNode("节点11", new System.Windows.Forms.TreeNode[] {
            treeNode13,
            treeNode14,
            treeNode15});
            System.Windows.Forms.TreeNode treeNode17 = new System.Windows.Forms.TreeNode("节点12");
            System.Windows.Forms.TreeNode treeNode18 = new System.Windows.Forms.TreeNode("节点3", new System.Windows.Forms.TreeNode[] {
            treeNode12,
            treeNode16,
            treeNode17});
            System.Windows.Forms.TreeNode treeNode19 = new System.Windows.Forms.TreeNode("节点14");
            System.Windows.Forms.TreeNode treeNode20 = new System.Windows.Forms.TreeNode("节点15");
            System.Windows.Forms.TreeNode treeNode21 = new System.Windows.Forms.TreeNode("节点4", new System.Windows.Forms.TreeNode[] {
            treeNode19,
            treeNode20});
            System.Windows.Forms.TreeNode treeNode22 = new System.Windows.Forms.TreeNode("节点19");
            System.Windows.Forms.TreeNode treeNode23 = new System.Windows.Forms.TreeNode("节点20");
            System.Windows.Forms.TreeNode treeNode24 = new System.Windows.Forms.TreeNode("节点5", new System.Windows.Forms.TreeNode[] {
            treeNode22,
            treeNode23});
            System.Windows.Forms.TreeNode treeNode25 = new System.Windows.Forms.TreeNode("节点6");
            System.Windows.Forms.TreeNode treeNode26 = new System.Windows.Forms.TreeNode("节点0", new System.Windows.Forms.TreeNode[] {
            treeNode24,
            treeNode25});
            System.Windows.Forms.TreeNode treeNode27 = new System.Windows.Forms.TreeNode("节点7");
            System.Windows.Forms.TreeNode treeNode28 = new System.Windows.Forms.TreeNode("节点8");
            System.Windows.Forms.TreeNode treeNode29 = new System.Windows.Forms.TreeNode("节点9");
            System.Windows.Forms.TreeNode treeNode30 = new System.Windows.Forms.TreeNode("节点1", new System.Windows.Forms.TreeNode[] {
            treeNode27,
            treeNode28,
            treeNode29});
            System.Windows.Forms.TreeNode treeNode31 = new System.Windows.Forms.TreeNode("节点13");
            System.Windows.Forms.TreeNode treeNode32 = new System.Windows.Forms.TreeNode("节点2", new System.Windows.Forms.TreeNode[] {
            treeNode31});
            System.Windows.Forms.TreeNode treeNode33 = new System.Windows.Forms.TreeNode("节点10");
            System.Windows.Forms.TreeNode treeNode34 = new System.Windows.Forms.TreeNode("节点16");
            System.Windows.Forms.TreeNode treeNode35 = new System.Windows.Forms.TreeNode("节点17");
            System.Windows.Forms.TreeNode treeNode36 = new System.Windows.Forms.TreeNode("节点18");
            System.Windows.Forms.TreeNode treeNode37 = new System.Windows.Forms.TreeNode("节点11", new System.Windows.Forms.TreeNode[] {
            treeNode34,
            treeNode35,
            treeNode36});
            System.Windows.Forms.TreeNode treeNode38 = new System.Windows.Forms.TreeNode("节点12");
            System.Windows.Forms.TreeNode treeNode39 = new System.Windows.Forms.TreeNode("节点3", new System.Windows.Forms.TreeNode[] {
            treeNode33,
            treeNode37,
            treeNode38});
            System.Windows.Forms.TreeNode treeNode40 = new System.Windows.Forms.TreeNode("节点14");
            System.Windows.Forms.TreeNode treeNode41 = new System.Windows.Forms.TreeNode("节点15");
            System.Windows.Forms.TreeNode treeNode42 = new System.Windows.Forms.TreeNode("节点4", new System.Windows.Forms.TreeNode[] {
            treeNode40,
            treeNode41});
            this.ppTabButtonBar1 = new PPSkin.PPTabButtonBar();
            this.ppTabControl1 = new PPSkin.PPTabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.ppGroupBox10 = new PPSkin.PPGroupBox();
            this.ppTextbox2 = new PPSkin.PPTextbox();
            this.ppRichTextbox1 = new PPSkin.PPRichTextbox();
            this.Textbox = new PPSkin.PPTextbox();
            this.ppNumericUpDown3 = new PPSkin.PPNumericUpDown();
            this.ppTextbox1 = new PPSkin.PPTextbox();
            this.ppNumericUpDown2 = new PPSkin.PPNumericUpDown();
            this.ppTextbox3 = new PPSkin.PPTextbox();
            this.ppNumericUpDown1 = new PPSkin.PPNumericUpDown();
            this.ppTextbox4 = new PPSkin.PPTextbox();
            this.ppTextbox5 = new PPSkin.PPTextbox();
            this.ppGroupBox2 = new PPSkin.PPGroupBox();
            this.ppRadioButton4 = new PPSkin.PPRadioButton();
            this.ppRadioButton3 = new PPSkin.PPRadioButton();
            this.ppRadioButton2 = new PPSkin.PPRadioButton();
            this.ppRadioButton1 = new PPSkin.PPRadioButton();
            this.ppCheckBoxEx3 = new PPSkin.PPCheckBoxEx();
            this.ppCheckBoxEx4 = new PPSkin.PPCheckBoxEx();
            this.ppCheckBoxEx2 = new PPSkin.PPCheckBoxEx();
            this.ppCheckBoxEx1 = new PPSkin.PPCheckBoxEx();
            this.ppCheckBox16 = new PPSkin.PPCheckBox();
            this.ppCheckBox17 = new PPSkin.PPCheckBox();
            this.ppCheckBox18 = new PPSkin.PPCheckBox();
            this.ppCheckBox19 = new PPSkin.PPCheckBox();
            this.ppCheckBox20 = new PPSkin.PPCheckBox();
            this.ppCheckBox11 = new PPSkin.PPCheckBox();
            this.ppCheckBox12 = new PPSkin.PPCheckBox();
            this.ppCheckBox13 = new PPSkin.PPCheckBox();
            this.ppCheckBox14 = new PPSkin.PPCheckBox();
            this.ppCheckBox15 = new PPSkin.PPCheckBox();
            this.ppCheckBox6 = new PPSkin.PPCheckBox();
            this.ppCheckBox7 = new PPSkin.PPCheckBox();
            this.ppCheckBox8 = new PPSkin.PPCheckBox();
            this.ppCheckBox9 = new PPSkin.PPCheckBox();
            this.ppCheckBox10 = new PPSkin.PPCheckBox();
            this.ppCheckBox5 = new PPSkin.PPCheckBox();
            this.ppCheckBox4 = new PPSkin.PPCheckBox();
            this.ppCheckBox3 = new PPSkin.PPCheckBox();
            this.ppCheckBox2 = new PPSkin.PPCheckBox();
            this.ppCheckBox1 = new PPSkin.PPCheckBox();
            this.ppGroupBox1 = new PPSkin.PPGroupBox();
            this.ppButton26 = new PPSkin.PPButton();
            this.ppButton27 = new PPSkin.PPButton();
            this.ppButton28 = new PPSkin.PPButton();
            this.ppButton29 = new PPSkin.PPButton();
            this.ppButton30 = new PPSkin.PPButton();
            this.ppButton21 = new PPSkin.PPButton();
            this.ppButton22 = new PPSkin.PPButton();
            this.ppButton23 = new PPSkin.PPButton();
            this.ppButton24 = new PPSkin.PPButton();
            this.ppButton25 = new PPSkin.PPButton();
            this.ppButton16 = new PPSkin.PPButton();
            this.ppButton17 = new PPSkin.PPButton();
            this.ppButton18 = new PPSkin.PPButton();
            this.ppButton19 = new PPSkin.PPButton();
            this.ppButton20 = new PPSkin.PPButton();
            this.ppButton11 = new PPSkin.PPButton();
            this.ppButton12 = new PPSkin.PPButton();
            this.ppButton13 = new PPSkin.PPButton();
            this.ppButton14 = new PPSkin.PPButton();
            this.ppButton15 = new PPSkin.PPButton();
            this.ppButton6 = new PPSkin.PPButton();
            this.ppButton7 = new PPSkin.PPButton();
            this.ppButton8 = new PPSkin.PPButton();
            this.ppButton9 = new PPSkin.PPButton();
            this.ppButton10 = new PPSkin.PPButton();
            this.ppButton5 = new PPSkin.PPButton();
            this.ppButton4 = new PPSkin.PPButton();
            this.ppButton3 = new PPSkin.PPButton();
            this.ppButton2 = new PPSkin.PPButton();
            this.ppButton1 = new PPSkin.PPButton();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.ppDateTimePicker13 = new PPSkin.PPDateTimePicker();
            this.ppDateTimePicker14 = new PPSkin.PPDateTimePicker();
            this.ppDateTimePicker15 = new PPSkin.PPDateTimePicker();
            this.ppDateTimePicker16 = new PPSkin.PPDateTimePicker();
            this.ppDateTimePicker9 = new PPSkin.PPDateTimePicker();
            this.ppDateTimePicker10 = new PPSkin.PPDateTimePicker();
            this.ppDateTimePicker11 = new PPSkin.PPDateTimePicker();
            this.ppDateTimePicker12 = new PPSkin.PPDateTimePicker();
            this.ppDateTimePicker1 = new PPSkin.PPDateTimePicker();
            this.ppDateTimePicker2 = new PPSkin.PPDateTimePicker();
            this.ppDateTimePicker3 = new PPSkin.PPDateTimePicker();
            this.ppDateTimePicker4 = new PPSkin.PPDateTimePicker();
            this.ppDateTimePicker5 = new PPSkin.PPDateTimePicker();
            this.ppDateTimePicker6 = new PPSkin.PPDateTimePicker();
            this.ppDateTimePicker7 = new PPSkin.PPDateTimePicker();
            this.ppDateTimePicker8 = new PPSkin.PPDateTimePicker();
            this.ppCombobox4 = new PPSkin.PPCombobox();
            this.ppCombobox3 = new PPSkin.PPCombobox();
            this.ppCombobox2 = new PPSkin.PPCombobox();
            this.ppCombobox1 = new PPSkin.PPCombobox();
            this.ppComboboxEx4 = new PPSkin.PPComboboxEx();
            this.ppComboboxEx3 = new PPSkin.PPComboboxEx();
            this.ppComboboxEx2 = new PPSkin.PPComboboxEx();
            this.ppComboboxEx1 = new PPSkin.PPComboboxEx();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.pphScrollBarExt5 = new PPSkin.PPHScrollBarExt();
            this.pphScrollBarExt4 = new PPSkin.PPHScrollBarExt();
            this.ppWaveProgressBar7 = new PPSkin.PPWaveProgressBar();
            this.ppWaveProgressBar6 = new PPSkin.PPWaveProgressBar();
            this.ppWaveProgressBar5 = new PPSkin.PPWaveProgressBar();
            this.ppWaveProgressBar4 = new PPSkin.PPWaveProgressBar();
            this.ppWaveProgressBar3 = new PPSkin.PPWaveProgressBar();
            this.ppWaveProgressBar2 = new PPSkin.PPWaveProgressBar();
            this.ppWaveProgressBar1 = new PPSkin.PPWaveProgressBar();
            this.ppTrackBar3 = new PPSkin.PPTrackBar();
            this.ppTrackBar2 = new PPSkin.PPTrackBar();
            this.ppTrackBar1 = new PPSkin.PPTrackBar();
            this.ppRoundProgressBar8 = new PPSkin.PPRoundProgressBar();
            this.ppRoundProgressBar7 = new PPSkin.PPRoundProgressBar();
            this.ppRoundProgressBar6 = new PPSkin.PPRoundProgressBar();
            this.ppRoundProgressBar5 = new PPSkin.PPRoundProgressBar();
            this.ppRoundProgressBar4 = new PPSkin.PPRoundProgressBar();
            this.ppRoundProgressBar3 = new PPSkin.PPRoundProgressBar();
            this.ppRoundProgressBar2 = new PPSkin.PPRoundProgressBar();
            this.ppRoundProgressBar1 = new PPSkin.PPRoundProgressBar();
            this.ppProgressBar3 = new PPSkin.PPProgressBar();
            this.ppProgressBar2 = new PPSkin.PPProgressBar();
            this.ppProgressBar1 = new PPSkin.PPProgressBar();
            this.pphScrollBarExt3 = new PPSkin.PPHScrollBarExt();
            this.pphScrollBarExt2 = new PPSkin.PPHScrollBarExt();
            this.pphScrollBarExt1 = new PPSkin.PPHScrollBarExt();
            this.ppvScrollBarExt6 = new PPSkin.PPVScrollBarExt();
            this.ppvScrollBarExt5 = new PPSkin.PPVScrollBarExt();
            this.ppvScrollBarExt4 = new PPSkin.PPVScrollBarExt();
            this.ppvScrollBarExt3 = new PPSkin.PPVScrollBarExt();
            this.ppvScrollBarExt2 = new PPSkin.PPVScrollBarExt();
            this.ppvScrollBarExt1 = new PPSkin.PPVScrollBarExt();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.ppFlodPanel1 = new PPSkin.PPFlodPanel();
            this.ppButton42 = new PPSkin.PPButton();
            this.ppFlodPanel2 = new PPSkin.PPFlodPanel();
            this.ppButton44 = new PPSkin.PPButton();
            this.ppFlodPanel3 = new PPSkin.PPFlodPanel();
            this.ppButton45 = new PPSkin.PPButton();
            this.ppFlodPanel4 = new PPSkin.PPFlodPanel();
            this.ppButton46 = new PPSkin.PPButton();
            this.ppFlodPanel5 = new PPSkin.PPFlodPanel();
            this.ppButton47 = new PPSkin.PPButton();
            this.ppFlodPanel6 = new PPSkin.PPFlodPanel();
            this.ppButton48 = new PPSkin.PPButton();
            this.ppFlodPanel7 = new PPSkin.PPFlodPanel();
            this.ppButton49 = new PPSkin.PPButton();
            this.ppFlodPanel8 = new PPSkin.PPFlodPanel();
            this.ppButton50 = new PPSkin.PPButton();
            this.ppFlodPanel9 = new PPSkin.PPFlodPanel();
            this.ppButton51 = new PPSkin.PPButton();
            this.ppFlodPanel10 = new PPSkin.PPFlodPanel();
            this.ppButton52 = new PPSkin.PPButton();
            this.ppGroupBox5 = new PPSkin.PPGroupBox();
            this.ppMovePanel1 = new PPSkin.PPMovePanel();
            this.ppCheckBox21 = new PPSkin.PPCheckBox();
            this.ppComboboxEx5 = new PPSkin.PPComboboxEx();
            this.ppPanel7 = new PPSkin.PPPanel();
            this.ppButton31 = new PPSkin.PPButton();
            this.ppGroupBox4 = new PPSkin.PPGroupBox();
            this.ppPanel4 = new PPSkin.PPPanel();
            this.ppPanel3 = new PPSkin.PPPanel();
            this.ppPanel5 = new PPSkin.PPPanel();
            this.ppPanel6 = new PPSkin.PPPanel();
            this.ppGroupBox3 = new PPSkin.PPGroupBox();
            this.ppGroupBox7 = new PPSkin.PPGroupBox();
            this.ppGroupBox6 = new PPSkin.PPGroupBox();
            this.ppGroupBox8 = new PPSkin.PPGroupBox();
            this.ppGroupBox9 = new PPSkin.PPGroupBox();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.ppTreeView3 = new PPSkin.PPTreeView();
            this.ppTreeView1 = new PPSkin.PPTreeView();
            this.ppTreeView2 = new PPSkin.PPTreeView();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.ppTabControl2 = new PPSkin.PPTabControl();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.ppTabControl6 = new PPSkin.PPTabControl();
            this.tabPage16 = new System.Windows.Forms.TabPage();
            this.tabPage17 = new System.Windows.Forms.TabPage();
            this.ppTabControl5 = new PPSkin.PPTabControl();
            this.tabPage14 = new System.Windows.Forms.TabPage();
            this.tabPage15 = new System.Windows.Forms.TabPage();
            this.ppTabControl3 = new PPSkin.PPTabControl();
            this.tabPage10 = new System.Windows.Forms.TabPage();
            this.tabPage11 = new System.Windows.Forms.TabPage();
            this.ppTabControl4 = new PPSkin.PPTabControl();
            this.tabPage12 = new System.Windows.Forms.TabPage();
            this.tabPage13 = new System.Windows.Forms.TabPage();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this.ppTabControl7 = new PPSkin.PPTabControl();
            this.tabPage18 = new System.Windows.Forms.TabPage();
            this.tabPage19 = new System.Windows.Forms.TabPage();
            this.ppTabControl8 = new PPSkin.PPTabControl();
            this.tabPage20 = new System.Windows.Forms.TabPage();
            this.tabPage21 = new System.Windows.Forms.TabPage();
            this.ppTabControl9 = new PPSkin.PPTabControl();
            this.tabPage22 = new System.Windows.Forms.TabPage();
            this.tabPage23 = new System.Windows.Forms.TabPage();
            this.ppTabControl10 = new PPSkin.PPTabControl();
            this.tabPage24 = new System.Windows.Forms.TabPage();
            this.tabPage25 = new System.Windows.Forms.TabPage();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.pphScrollBarExt10 = new PPSkin.PPHScrollBarExt();
            this.ppvScrollBarExt11 = new PPSkin.PPVScrollBarExt();
            this.pphScrollBarExt9 = new PPSkin.PPHScrollBarExt();
            this.ppvScrollBarExt10 = new PPSkin.PPVScrollBarExt();
            this.pphScrollBarExt8 = new PPSkin.PPHScrollBarExt();
            this.ppvScrollBarExt9 = new PPSkin.PPVScrollBarExt();
            this.pphScrollBarExt7 = new PPSkin.PPHScrollBarExt();
            this.ppvScrollBarExt8 = new PPSkin.PPVScrollBarExt();
            this.ppDataGridView4 = new PPSkin.PPDataGridView();
            this.ppDataGridView3 = new PPSkin.PPDataGridView();
            this.ppDataGridView2 = new PPSkin.PPDataGridView();
            this.ppDataGridView1 = new PPSkin.PPDataGridView();
            this.tabPage26 = new System.Windows.Forms.TabPage();
            this.ppButton35 = new PPSkin.PPButton();
            this.unlockerPanel3 = new PPSkin.UnlockerPanel();
            this.unlockerPanel2 = new PPSkin.UnlockerPanel();
            this.unlockerPanel1 = new PPSkin.UnlockerPanel();
            this.ledBar1 = new PPSkin.LedBar();
            this.ledNum14 = new PPSkin.LedNum();
            this.ledNum13 = new PPSkin.LedNum();
            this.ledNum12 = new PPSkin.LedNum();
            this.ledNum11 = new PPSkin.LedNum();
            this.ledNum10 = new PPSkin.LedNum();
            this.ledNum9 = new PPSkin.LedNum();
            this.ledNum8 = new PPSkin.LedNum();
            this.ledNum7 = new PPSkin.LedNum();
            this.ledNum6 = new PPSkin.LedNum();
            this.ledNum5 = new PPSkin.LedNum();
            this.ledNum4 = new PPSkin.LedNum();
            this.ledNum3 = new PPSkin.LedNum();
            this.ledNum2 = new PPSkin.LedNum();
            this.ledNum1 = new PPSkin.LedNum();
            this.tabPage27 = new System.Windows.Forms.TabPage();
            this.ppButton54 = new PPSkin.PPButton();
            this.ppButton53 = new PPSkin.PPButton();
            this.ppNumericUpDown5 = new PPSkin.PPNumericUpDown();
            this.ppRoundProgressBar9 = new PPSkin.PPRoundProgressBar();
            this.ppComboboxEx6 = new PPSkin.PPComboboxEx();
            this.panel2 = new System.Windows.Forms.Panel();
            this.ppButton43 = new PPSkin.PPButton();
            this.ppLabel1 = new PPSkin.PPLabel();
            this.ppNumericUpDown4 = new PPSkin.PPNumericUpDown();
            this.ppRadioButton11 = new PPSkin.PPRadioButton();
            this.ppRadioButton10 = new PPSkin.PPRadioButton();
            this.ppRadioButton9 = new PPSkin.PPRadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.ppButton34 = new PPSkin.PPButton();
            this.tabPage28 = new System.Windows.Forms.TabPage();
            this.panel3 = new System.Windows.Forms.Panel();
            this.ppButton41 = new PPSkin.PPButton();
            this.ppComboboxEx9 = new PPSkin.PPComboboxEx();
            this.ppButton40 = new PPSkin.PPButton();
            this.ppRichTextbox5 = new PPSkin.PPRichTextbox();
            this.ppPanel8 = new PPSkin.PPPanel();
            this.ppButton33 = new PPSkin.PPButton();
            this.ppButton32 = new PPSkin.PPButton();
            this.ppButton36 = new PPSkin.PPButton();
            this.ppButton37 = new PPSkin.PPButton();
            this.ppButton38 = new PPSkin.PPButton();
            this.ppButton39 = new PPSkin.PPButton();
            this.ppPanel2 = new PPSkin.PPPanel();
            this.ppPanel1 = new PPSkin.PPPanel();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.fontDialog1 = new System.Windows.Forms.FontDialog();
            this.ppTabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.ppGroupBox10.SuspendLayout();
            this.ppGroupBox2.SuspendLayout();
            this.ppGroupBox1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.ppFlodPanel1.SuspendLayout();
            this.ppFlodPanel2.SuspendLayout();
            this.ppFlodPanel3.SuspendLayout();
            this.ppFlodPanel4.SuspendLayout();
            this.ppFlodPanel5.SuspendLayout();
            this.ppFlodPanel6.SuspendLayout();
            this.ppFlodPanel7.SuspendLayout();
            this.ppFlodPanel8.SuspendLayout();
            this.ppFlodPanel9.SuspendLayout();
            this.ppFlodPanel10.SuspendLayout();
            this.ppGroupBox5.SuspendLayout();
            this.ppMovePanel1.SuspendLayout();
            this.ppGroupBox4.SuspendLayout();
            this.ppGroupBox3.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.ppTabControl2.SuspendLayout();
            this.tabPage8.SuspendLayout();
            this.ppTabControl6.SuspendLayout();
            this.ppTabControl5.SuspendLayout();
            this.ppTabControl3.SuspendLayout();
            this.ppTabControl4.SuspendLayout();
            this.tabPage9.SuspendLayout();
            this.ppTabControl7.SuspendLayout();
            this.ppTabControl8.SuspendLayout();
            this.ppTabControl9.SuspendLayout();
            this.ppTabControl10.SuspendLayout();
            this.tabPage7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ppDataGridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ppDataGridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ppDataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ppDataGridView1)).BeginInit();
            this.tabPage26.SuspendLayout();
            this.tabPage27.SuspendLayout();
            this.tabPage28.SuspendLayout();
            this.ppPanel2.SuspendLayout();
            this.ppPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ppTabButtonBar1
            // 
            this.ppTabButtonBar1.AnimationTime = 500;
            this.ppTabButtonBar1.AnimationType = PPSkin.AnimationType.BounceOut;
            this.ppTabButtonBar1.BarSize = 3;
            this.ppTabButtonBar1.BindTabControl = null;
            this.ppTabButtonBar1.ButtonWidth = 40;
            this.ppTabButtonBar1.Direction = PPSkin.PPTabButtonBar.direction.Vertical;
            this.ppTabButtonBar1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ppTabButtonBar1.EnableAnime = true;
            this.ppTabButtonBar1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ppTabButtonBar1.ImageOffset = new System.Drawing.Point(2, 10);
            this.ppTabButtonBar1.Images = ((System.Collections.Generic.List<System.Drawing.Bitmap>)(resources.GetObject("ppTabButtonBar1.Images")));
            this.ppTabButtonBar1.ImageSize = new System.Drawing.Size(20, 20);
            this.ppTabButtonBar1.ItemAutoSize = false;
            this.ppTabButtonBar1.Location = new System.Drawing.Point(0, 0);
            this.ppTabButtonBar1.Margin = new System.Windows.Forms.Padding(48, 24, 48, 24);
            this.ppTabButtonBar1.MouseSelectColor = System.Drawing.Color.Silver;
            this.ppTabButtonBar1.Name = "ppTabButtonBar1";
            this.ppTabButtonBar1.NormalBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.ppTabButtonBar1.NormalForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(150)))), ((int)(((byte)(150)))));
            this.ppTabButtonBar1.SelectBarColor = System.Drawing.Color.DeepSkyBlue;
            this.ppTabButtonBar1.SelectBarPosition = PPSkin.PPTabButtonBar.selectBarPosition.Left;
            this.ppTabButtonBar1.SelectBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.ppTabButtonBar1.SelectForeColor = System.Drawing.Color.White;
            this.ppTabButtonBar1.SelectRectH = 40;
            this.ppTabButtonBar1.SelectRectW = 3;
            this.ppTabButtonBar1.SelectRectX = 0;
            this.ppTabButtonBar1.SelectRectY = 0;
            this.ppTabButtonBar1.ShowImages = true;
            this.ppTabButtonBar1.Size = new System.Drawing.Size(150, 705);
            this.ppTabButtonBar1.TabIndex = 0;
            this.ppTabButtonBar1.TextAlignment = System.Drawing.StringAlignment.Near;
            this.ppTabButtonBar1.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.ppTabButtonBar1.TextOffset = new System.Drawing.Point(18, 0);
            this.ppTabButtonBar1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("ppTabButtonBar1.Texts")));
            // 
            // ppTabControl1
            // 
            this.ppTabControl1.BaseColor = System.Drawing.Color.White;
            this.ppTabControl1.Controls.Add(this.tabPage1);
            this.ppTabControl1.Controls.Add(this.tabPage2);
            this.ppTabControl1.Controls.Add(this.tabPage3);
            this.ppTabControl1.Controls.Add(this.tabPage4);
            this.ppTabControl1.Controls.Add(this.tabPage5);
            this.ppTabControl1.Controls.Add(this.tabPage6);
            this.ppTabControl1.Controls.Add(this.tabPage7);
            this.ppTabControl1.Controls.Add(this.tabPage26);
            this.ppTabControl1.Controls.Add(this.tabPage27);
            this.ppTabControl1.Controls.Add(this.tabPage28);
            this.ppTabControl1.Cursor = System.Windows.Forms.Cursors.Default;
            this.ppTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ppTabControl1.ItemSize = new System.Drawing.Size(50, 30);
            this.ppTabControl1.Location = new System.Drawing.Point(0, 0);
            this.ppTabControl1.Name = "ppTabControl1";
            this.ppTabControl1.SelectedIndex = 0;
            this.ppTabControl1.SelectLineHeight = 1;
            this.ppTabControl1.ShowCloseButton = false;
            this.ppTabControl1.ShowSelectLine = false;
            this.ppTabControl1.ShowTab = true;
            this.ppTabControl1.ShowTabBorder = false;
            this.ppTabControl1.Size = new System.Drawing.Size(928, 705);
            this.ppTabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.ppTabControl1.TabBackColor = System.Drawing.Color.WhiteSmoke;
            this.ppTabControl1.TabBorderColor = System.Drawing.Color.LightGray;
            this.ppTabControl1.TabCloseButtonColor = System.Drawing.Color.DimGray;
            this.ppTabControl1.TabFont = new System.Drawing.Font("微软雅黑", 10F);
            this.ppTabControl1.TabInCloseButtonColor = System.Drawing.Color.DodgerBlue;
            this.ppTabControl1.TabIndex = 1;
            this.ppTabControl1.TabSelectBackColor = System.Drawing.Color.White;
            this.ppTabControl1.TabSelectBorderColor = System.Drawing.Color.DarkGray;
            this.ppTabControl1.TabSelectLineColor = System.Drawing.Color.DodgerBlue;
            this.ppTabControl1.TabSelectTextColor = System.Drawing.Color.DodgerBlue;
            this.ppTabControl1.TabTextAlign = PPSkin.PPTabControl.TextAlign.CENTER;
            this.ppTabControl1.TabTextColor = System.Drawing.Color.Black;
            this.ppTabControl1.TabTextOffset = new System.Drawing.Point(0, 0);
            this.ppTabControl1.TextDrawMode = PPSkin.PPTabControl.drawMode.Clear;
            this.ppTabControl1.TitleBaseColor = System.Drawing.Color.WhiteSmoke;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.White;
            this.tabPage1.Controls.Add(this.ppGroupBox10);
            this.tabPage1.Controls.Add(this.ppGroupBox2);
            this.tabPage1.Controls.Add(this.ppGroupBox1);
            this.tabPage1.Location = new System.Drawing.Point(0, 30);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(928, 675);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "按钮";
            // 
            // ppGroupBox10
            // 
            this.ppGroupBox10.BackColor = System.Drawing.Color.White;
            this.ppGroupBox10.BaseColor = System.Drawing.Color.White;
            this.ppGroupBox10.BorderColor = System.Drawing.Color.Gray;
            this.ppGroupBox10.BorderWidth = 1;
            this.ppGroupBox10.Controls.Add(this.ppTextbox2);
            this.ppGroupBox10.Controls.Add(this.ppRichTextbox1);
            this.ppGroupBox10.Controls.Add(this.Textbox);
            this.ppGroupBox10.Controls.Add(this.ppNumericUpDown3);
            this.ppGroupBox10.Controls.Add(this.ppTextbox1);
            this.ppGroupBox10.Controls.Add(this.ppNumericUpDown2);
            this.ppGroupBox10.Controls.Add(this.ppTextbox3);
            this.ppGroupBox10.Controls.Add(this.ppNumericUpDown1);
            this.ppGroupBox10.Controls.Add(this.ppTextbox4);
            this.ppGroupBox10.Controls.Add(this.ppTextbox5);
            this.ppGroupBox10.Location = new System.Drawing.Point(20, 387);
            this.ppGroupBox10.Name = "ppGroupBox10";
            this.ppGroupBox10.Radius = 10;
            this.ppGroupBox10.Size = new System.Drawing.Size(898, 282);
            this.ppGroupBox10.TabIndex = 20;
            this.ppGroupBox10.TabStop = false;
            this.ppGroupBox10.Text = "TextBox";
            this.ppGroupBox10.TextAlignment = System.Drawing.StringAlignment.Center;
            this.ppGroupBox10.TextDrawMode = PPSkin.PPGroupBox.DrawMode.Clear;
            this.ppGroupBox10.TilteOffset = 0;
            this.ppGroupBox10.TitleBaseColor = System.Drawing.Color.White;
            this.ppGroupBox10.TitleBorderColor = System.Drawing.Color.Gray;
            this.ppGroupBox10.TitleRadius = 10;
            // 
            // ppTextbox2
            // 
            this.ppTextbox2.BackColor = System.Drawing.Color.White;
            this.ppTextbox2.BaseColor = System.Drawing.Color.White;
            this.ppTextbox2.BorderColor = System.Drawing.Color.Tomato;
            this.ppTextbox2.BorderWidth = 1F;
            this.ppTextbox2.Location = new System.Drawing.Point(438, 53);
            this.ppTextbox2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ppTextbox2.MaxLength = 32767;
            this.ppTextbox2.Name = "ppTextbox2";
            this.ppTextbox2.PasswordChar = '\0';
            this.ppTextbox2.Radius = 5;
            this.ppTextbox2.ReadOnly = false;
            this.ppTextbox2.Regex = "";
            this.ppTextbox2.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppTextbox2.ShowBorder = true;
            this.ppTextbox2.ShowTip = false;
            this.ppTextbox2.Size = new System.Drawing.Size(180, 35);
            this.ppTextbox2.TabIndex = 12;
            this.ppTextbox2.Text = "ppTextbox2";
            this.ppTextbox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.ppTextbox2.WatermarkText = null;
            // 
            // ppRichTextbox1
            // 
            this.ppRichTextbox1.AcceptsTab = false;
            this.ppRichTextbox1.AutoWordSelection = false;
            this.ppRichTextbox1.BackColor = System.Drawing.SystemColors.Control;
            this.ppRichTextbox1.BaseColor = System.Drawing.Color.White;
            this.ppRichTextbox1.BorderColor = System.Drawing.Color.Black;
            this.ppRichTextbox1.BorderWidth = 1F;
            this.ppRichTextbox1.BulletIndent = 0;
            this.ppRichTextbox1.DetectUrls = true;
            this.ppRichTextbox1.EnableAutoDrogDrop = false;
            this.ppRichTextbox1.Font = new System.Drawing.Font("思源黑体 CN Normal", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ppRichTextbox1.HideSelection = true;
            this.ppRichTextbox1.Location = new System.Drawing.Point(626, 63);
            this.ppRichTextbox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ppRichTextbox1.MaxLength = 2147483647;
            this.ppRichTextbox1.Multiline = true;
            this.ppRichTextbox1.Name = "ppRichTextbox1";
            this.ppRichTextbox1.Radius = 10;
            this.ppRichTextbox1.ReadOnly = false;
            this.ppRichTextbox1.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppRichTextbox1.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.ppRichTextbox1.SelectionColor = System.Drawing.Color.Black;
            this.ppRichTextbox1.SelectionStart = 0;
            this.ppRichTextbox1.ShortcutsEnabled = true;
            this.ppRichTextbox1.ShowBorder = true;
            this.ppRichTextbox1.ShowSelectionMargin = false;
            this.ppRichTextbox1.Size = new System.Drawing.Size(269, 189);
            this.ppRichTextbox1.TabIndex = 19;
            this.ppRichTextbox1.VScrollBar_BaseColor = System.Drawing.Color.Gainsboro;
            this.ppRichTextbox1.VScrollBar_BaseRadius = 0;
            this.ppRichTextbox1.VScrollBar_BaseSize = 2;
            this.ppRichTextbox1.VScrollBar_ButtonColor = System.Drawing.Color.DodgerBlue;
            this.ppRichTextbox1.VScrollBar_ButtonHoverColor = System.Drawing.Color.DeepSkyBlue;
            this.ppRichTextbox1.VScrollBar_ButtonRadius = 10;
            this.ppRichTextbox1.VScrollBar_ButtonSize = 10;
            this.ppRichTextbox1.WordWrap = true;
            this.ppRichTextbox1.ZoomFactor = 1F;
            // 
            // Textbox
            // 
            this.Textbox.BackColor = System.Drawing.Color.White;
            this.Textbox.BaseColor = System.Drawing.Color.White;
            this.Textbox.BorderColor = System.Drawing.Color.LightGray;
            this.Textbox.BorderWidth = 1F;
            this.Textbox.Location = new System.Drawing.Point(9, 53);
            this.Textbox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Textbox.MaxLength = 32767;
            this.Textbox.Name = "Textbox";
            this.Textbox.PasswordChar = '\0';
            this.Textbox.Radius = 5;
            this.Textbox.ReadOnly = false;
            this.Textbox.Regex = "";
            this.Textbox.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.Textbox.ShowBorder = true;
            this.Textbox.ShowTip = false;
            this.Textbox.Size = new System.Drawing.Size(180, 35);
            this.Textbox.TabIndex = 10;
            this.Textbox.Text = "Textbox";
            this.Textbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.Textbox.WatermarkText = null;
            // 
            // ppNumericUpDown3
            // 
            this.ppNumericUpDown3.BackColor = System.Drawing.Color.White;
            this.ppNumericUpDown3.BaseColor = System.Drawing.Color.Moccasin;
            this.ppNumericUpDown3.BorderColor = System.Drawing.Color.Red;
            this.ppNumericUpDown3.BorderWidth = 1F;
            this.ppNumericUpDown3.ButtonPicDown = ((System.Drawing.Bitmap)(resources.GetObject("ppNumericUpDown3.ButtonPicDown")));
            this.ppNumericUpDown3.ButtonPicDownHover = ((System.Drawing.Bitmap)(resources.GetObject("ppNumericUpDown3.ButtonPicDownHover")));
            this.ppNumericUpDown3.ButtonPicUP = ((System.Drawing.Bitmap)(resources.GetObject("ppNumericUpDown3.ButtonPicUP")));
            this.ppNumericUpDown3.ButtonPicUPHover = ((System.Drawing.Bitmap)(resources.GetObject("ppNumericUpDown3.ButtonPicUPHover")));
            this.ppNumericUpDown3.ButtonSize = new System.Drawing.Size(13, 13);
            this.ppNumericUpDown3.Location = new System.Drawing.Point(438, 234);
            this.ppNumericUpDown3.Maxinum = 100;
            this.ppNumericUpDown3.Mininum = 0;
            this.ppNumericUpDown3.Name = "ppNumericUpDown3";
            this.ppNumericUpDown3.PadLeftCount = 0;
            this.ppNumericUpDown3.Radius = 0;
            this.ppNumericUpDown3.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppNumericUpDown3.ShowBorder = true;
            this.ppNumericUpDown3.Size = new System.Drawing.Size(180, 32);
            this.ppNumericUpDown3.TabIndex = 18;
            this.ppNumericUpDown3.Value = 0;
            // 
            // ppTextbox1
            // 
            this.ppTextbox1.BackColor = System.Drawing.Color.White;
            this.ppTextbox1.BaseColor = System.Drawing.Color.White;
            this.ppTextbox1.BorderColor = System.Drawing.Color.LightGray;
            this.ppTextbox1.BorderWidth = 1F;
            this.ppTextbox1.Location = new System.Drawing.Point(215, 53);
            this.ppTextbox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ppTextbox1.MaxLength = 32767;
            this.ppTextbox1.Name = "ppTextbox1";
            this.ppTextbox1.PasswordChar = '\0';
            this.ppTextbox1.Radius = 32;
            this.ppTextbox1.ReadOnly = false;
            this.ppTextbox1.Regex = "";
            this.ppTextbox1.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppTextbox1.ShowBorder = true;
            this.ppTextbox1.ShowTip = false;
            this.ppTextbox1.Size = new System.Drawing.Size(180, 35);
            this.ppTextbox1.TabIndex = 11;
            this.ppTextbox1.Text = "ppTextbox1";
            this.ppTextbox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.ppTextbox1.WatermarkText = null;
            // 
            // ppNumericUpDown2
            // 
            this.ppNumericUpDown2.BackColor = System.Drawing.Color.White;
            this.ppNumericUpDown2.BaseColor = System.Drawing.Color.White;
            this.ppNumericUpDown2.BorderColor = System.Drawing.Color.OrangeRed;
            this.ppNumericUpDown2.BorderWidth = 1F;
            this.ppNumericUpDown2.ButtonPicDown = ((System.Drawing.Bitmap)(resources.GetObject("ppNumericUpDown2.ButtonPicDown")));
            this.ppNumericUpDown2.ButtonPicDownHover = ((System.Drawing.Bitmap)(resources.GetObject("ppNumericUpDown2.ButtonPicDownHover")));
            this.ppNumericUpDown2.ButtonPicUP = ((System.Drawing.Bitmap)(resources.GetObject("ppNumericUpDown2.ButtonPicUP")));
            this.ppNumericUpDown2.ButtonPicUPHover = ((System.Drawing.Bitmap)(resources.GetObject("ppNumericUpDown2.ButtonPicUPHover")));
            this.ppNumericUpDown2.ButtonSize = new System.Drawing.Size(13, 13);
            this.ppNumericUpDown2.Location = new System.Drawing.Point(215, 234);
            this.ppNumericUpDown2.Maxinum = 100;
            this.ppNumericUpDown2.Mininum = 0;
            this.ppNumericUpDown2.Name = "ppNumericUpDown2";
            this.ppNumericUpDown2.PadLeftCount = 0;
            this.ppNumericUpDown2.Radius = 30;
            this.ppNumericUpDown2.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppNumericUpDown2.ShowBorder = true;
            this.ppNumericUpDown2.Size = new System.Drawing.Size(180, 32);
            this.ppNumericUpDown2.TabIndex = 17;
            this.ppNumericUpDown2.Value = 0;
            // 
            // ppTextbox3
            // 
            this.ppTextbox3.BackColor = System.Drawing.Color.White;
            this.ppTextbox3.BaseColor = System.Drawing.Color.White;
            this.ppTextbox3.BorderColor = System.Drawing.Color.LightGray;
            this.ppTextbox3.BorderWidth = 1F;
            this.ppTextbox3.Location = new System.Drawing.Point(9, 150);
            this.ppTextbox3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ppTextbox3.MaxLength = 32767;
            this.ppTextbox3.Name = "ppTextbox3";
            this.ppTextbox3.PasswordChar = '\0';
            this.ppTextbox3.Radius = 0;
            this.ppTextbox3.ReadOnly = false;
            this.ppTextbox3.Regex = "";
            this.ppTextbox3.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppTextbox3.ShowBorder = true;
            this.ppTextbox3.ShowTip = false;
            this.ppTextbox3.Size = new System.Drawing.Size(180, 35);
            this.ppTextbox3.TabIndex = 13;
            this.ppTextbox3.Text = "ppTextbox3";
            this.ppTextbox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.ppTextbox3.WatermarkText = null;
            // 
            // ppNumericUpDown1
            // 
            this.ppNumericUpDown1.BackColor = System.Drawing.Color.White;
            this.ppNumericUpDown1.BaseColor = System.Drawing.Color.White;
            this.ppNumericUpDown1.BorderColor = System.Drawing.Color.LightGray;
            this.ppNumericUpDown1.BorderWidth = 1F;
            this.ppNumericUpDown1.ButtonPicDown = ((System.Drawing.Bitmap)(resources.GetObject("ppNumericUpDown1.ButtonPicDown")));
            this.ppNumericUpDown1.ButtonPicDownHover = ((System.Drawing.Bitmap)(resources.GetObject("ppNumericUpDown1.ButtonPicDownHover")));
            this.ppNumericUpDown1.ButtonPicUP = ((System.Drawing.Bitmap)(resources.GetObject("ppNumericUpDown1.ButtonPicUP")));
            this.ppNumericUpDown1.ButtonPicUPHover = ((System.Drawing.Bitmap)(resources.GetObject("ppNumericUpDown1.ButtonPicUPHover")));
            this.ppNumericUpDown1.ButtonSize = new System.Drawing.Size(13, 13);
            this.ppNumericUpDown1.Location = new System.Drawing.Point(9, 234);
            this.ppNumericUpDown1.Maxinum = 100;
            this.ppNumericUpDown1.Mininum = 0;
            this.ppNumericUpDown1.Name = "ppNumericUpDown1";
            this.ppNumericUpDown1.PadLeftCount = 0;
            this.ppNumericUpDown1.Radius = 5;
            this.ppNumericUpDown1.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppNumericUpDown1.ShowBorder = true;
            this.ppNumericUpDown1.Size = new System.Drawing.Size(180, 32);
            this.ppNumericUpDown1.TabIndex = 16;
            this.ppNumericUpDown1.Value = 0;
            // 
            // ppTextbox4
            // 
            this.ppTextbox4.BackColor = System.Drawing.Color.White;
            this.ppTextbox4.BaseColor = System.Drawing.Color.Aquamarine;
            this.ppTextbox4.BorderColor = System.Drawing.Color.Tomato;
            this.ppTextbox4.BorderWidth = 1F;
            this.ppTextbox4.Location = new System.Drawing.Point(215, 150);
            this.ppTextbox4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ppTextbox4.MaxLength = 32767;
            this.ppTextbox4.Name = "ppTextbox4";
            this.ppTextbox4.PasswordChar = '\0';
            this.ppTextbox4.Radius = 5;
            this.ppTextbox4.ReadOnly = false;
            this.ppTextbox4.Regex = "";
            this.ppTextbox4.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppTextbox4.ShowBorder = true;
            this.ppTextbox4.ShowTip = false;
            this.ppTextbox4.Size = new System.Drawing.Size(180, 35);
            this.ppTextbox4.TabIndex = 14;
            this.ppTextbox4.Text = "ppTextbox4";
            this.ppTextbox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.ppTextbox4.WatermarkText = null;
            // 
            // ppTextbox5
            // 
            this.ppTextbox5.BackColor = System.Drawing.Color.White;
            this.ppTextbox5.BaseColor = System.Drawing.Color.White;
            this.ppTextbox5.BorderColor = System.Drawing.Color.LightGray;
            this.ppTextbox5.BorderWidth = 1F;
            this.ppTextbox5.Location = new System.Drawing.Point(438, 150);
            this.ppTextbox5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ppTextbox5.MaxLength = 32767;
            this.ppTextbox5.Name = "ppTextbox5";
            this.ppTextbox5.PasswordChar = '\0';
            this.ppTextbox5.Radius = 0;
            this.ppTextbox5.ReadOnly = false;
            this.ppTextbox5.Regex = "";
            this.ppTextbox5.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppTextbox5.ShowBorder = true;
            this.ppTextbox5.ShowTip = false;
            this.ppTextbox5.Size = new System.Drawing.Size(180, 35);
            this.ppTextbox5.TabIndex = 15;
            this.ppTextbox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.ppTextbox5.WatermarkText = "waterText";
            // 
            // ppGroupBox2
            // 
            this.ppGroupBox2.BackColor = System.Drawing.Color.White;
            this.ppGroupBox2.BaseColor = System.Drawing.Color.White;
            this.ppGroupBox2.BorderColor = System.Drawing.Color.Gray;
            this.ppGroupBox2.BorderWidth = 1;
            this.ppGroupBox2.Controls.Add(this.ppRadioButton4);
            this.ppGroupBox2.Controls.Add(this.ppRadioButton3);
            this.ppGroupBox2.Controls.Add(this.ppRadioButton2);
            this.ppGroupBox2.Controls.Add(this.ppRadioButton1);
            this.ppGroupBox2.Controls.Add(this.ppCheckBoxEx3);
            this.ppGroupBox2.Controls.Add(this.ppCheckBoxEx4);
            this.ppGroupBox2.Controls.Add(this.ppCheckBoxEx2);
            this.ppGroupBox2.Controls.Add(this.ppCheckBoxEx1);
            this.ppGroupBox2.Controls.Add(this.ppCheckBox16);
            this.ppGroupBox2.Controls.Add(this.ppCheckBox17);
            this.ppGroupBox2.Controls.Add(this.ppCheckBox18);
            this.ppGroupBox2.Controls.Add(this.ppCheckBox19);
            this.ppGroupBox2.Controls.Add(this.ppCheckBox20);
            this.ppGroupBox2.Controls.Add(this.ppCheckBox11);
            this.ppGroupBox2.Controls.Add(this.ppCheckBox12);
            this.ppGroupBox2.Controls.Add(this.ppCheckBox13);
            this.ppGroupBox2.Controls.Add(this.ppCheckBox14);
            this.ppGroupBox2.Controls.Add(this.ppCheckBox15);
            this.ppGroupBox2.Controls.Add(this.ppCheckBox6);
            this.ppGroupBox2.Controls.Add(this.ppCheckBox7);
            this.ppGroupBox2.Controls.Add(this.ppCheckBox8);
            this.ppGroupBox2.Controls.Add(this.ppCheckBox9);
            this.ppGroupBox2.Controls.Add(this.ppCheckBox10);
            this.ppGroupBox2.Controls.Add(this.ppCheckBox5);
            this.ppGroupBox2.Controls.Add(this.ppCheckBox4);
            this.ppGroupBox2.Controls.Add(this.ppCheckBox3);
            this.ppGroupBox2.Controls.Add(this.ppCheckBox2);
            this.ppGroupBox2.Controls.Add(this.ppCheckBox1);
            this.ppGroupBox2.Location = new System.Drawing.Point(496, 24);
            this.ppGroupBox2.Name = "ppGroupBox2";
            this.ppGroupBox2.Radius = 10;
            this.ppGroupBox2.Size = new System.Drawing.Size(422, 348);
            this.ppGroupBox2.TabIndex = 1;
            this.ppGroupBox2.TabStop = false;
            this.ppGroupBox2.Text = "CheckBox";
            this.ppGroupBox2.TextAlignment = System.Drawing.StringAlignment.Center;
            this.ppGroupBox2.TextDrawMode = PPSkin.PPGroupBox.DrawMode.Clear;
            this.ppGroupBox2.TilteOffset = 0;
            this.ppGroupBox2.TitleBaseColor = System.Drawing.Color.White;
            this.ppGroupBox2.TitleBorderColor = System.Drawing.Color.Gray;
            this.ppGroupBox2.TitleRadius = 10;
            // 
            // ppRadioButton4
            // 
            this.ppRadioButton4.AutoSize = true;
            this.ppRadioButton4.CheckAlign = PPSkin.PPRadioButton.Align.Right;
            this.ppRadioButton4.CheckedForeColor = System.Drawing.Color.DodgerBlue;
            this.ppRadioButton4.CheckedPic = ((System.Drawing.Image)(resources.GetObject("ppRadioButton4.CheckedPic")));
            this.ppRadioButton4.Location = new System.Drawing.Point(148, 294);
            this.ppRadioButton4.Name = "ppRadioButton4";
            this.ppRadioButton4.PicOffset = new System.Drawing.Point(0, 0);
            this.ppRadioButton4.PicSize = new System.Drawing.Size(15, 15);
            this.ppRadioButton4.Size = new System.Drawing.Size(98, 21);
            this.ppRadioButton4.TabIndex = 27;
            this.ppRadioButton4.TabStop = true;
            this.ppRadioButton4.Text = "RadioButton";
            this.ppRadioButton4.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.ppRadioButton4.UnCheckedForeColor = System.Drawing.Color.Red;
            this.ppRadioButton4.unCheckedPic = ((System.Drawing.Image)(resources.GetObject("ppRadioButton4.unCheckedPic")));
            // 
            // ppRadioButton3
            // 
            this.ppRadioButton3.AutoSize = true;
            this.ppRadioButton3.CheckAlign = PPSkin.PPRadioButton.Align.Left;
            this.ppRadioButton3.CheckedForeColor = System.Drawing.Color.DodgerBlue;
            this.ppRadioButton3.CheckedPic = ((System.Drawing.Image)(resources.GetObject("ppRadioButton3.CheckedPic")));
            this.ppRadioButton3.Location = new System.Drawing.Point(19, 294);
            this.ppRadioButton3.Name = "ppRadioButton3";
            this.ppRadioButton3.PicOffset = new System.Drawing.Point(0, 0);
            this.ppRadioButton3.PicSize = new System.Drawing.Size(15, 15);
            this.ppRadioButton3.Size = new System.Drawing.Size(98, 21);
            this.ppRadioButton3.TabIndex = 26;
            this.ppRadioButton3.TabStop = true;
            this.ppRadioButton3.Text = "RadioButton";
            this.ppRadioButton3.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.ppRadioButton3.UnCheckedForeColor = System.Drawing.Color.Red;
            this.ppRadioButton3.unCheckedPic = ((System.Drawing.Image)(resources.GetObject("ppRadioButton3.unCheckedPic")));
            // 
            // ppRadioButton2
            // 
            this.ppRadioButton2.AutoSize = true;
            this.ppRadioButton2.CheckAlign = PPSkin.PPRadioButton.Align.Right;
            this.ppRadioButton2.CheckedForeColor = System.Drawing.Color.DodgerBlue;
            this.ppRadioButton2.CheckedPic = ((System.Drawing.Image)(resources.GetObject("ppRadioButton2.CheckedPic")));
            this.ppRadioButton2.Location = new System.Drawing.Point(148, 252);
            this.ppRadioButton2.Name = "ppRadioButton2";
            this.ppRadioButton2.PicOffset = new System.Drawing.Point(0, 0);
            this.ppRadioButton2.PicSize = new System.Drawing.Size(15, 15);
            this.ppRadioButton2.Size = new System.Drawing.Size(98, 21);
            this.ppRadioButton2.TabIndex = 25;
            this.ppRadioButton2.TabStop = true;
            this.ppRadioButton2.Text = "RadioButton";
            this.ppRadioButton2.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.ppRadioButton2.UnCheckedForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.ppRadioButton2.unCheckedPic = ((System.Drawing.Image)(resources.GetObject("ppRadioButton2.unCheckedPic")));
            // 
            // ppRadioButton1
            // 
            this.ppRadioButton1.AutoSize = true;
            this.ppRadioButton1.CheckAlign = PPSkin.PPRadioButton.Align.Left;
            this.ppRadioButton1.CheckedForeColor = System.Drawing.Color.DodgerBlue;
            this.ppRadioButton1.CheckedPic = ((System.Drawing.Image)(resources.GetObject("ppRadioButton1.CheckedPic")));
            this.ppRadioButton1.Location = new System.Drawing.Point(19, 252);
            this.ppRadioButton1.Name = "ppRadioButton1";
            this.ppRadioButton1.PicOffset = new System.Drawing.Point(0, 0);
            this.ppRadioButton1.PicSize = new System.Drawing.Size(15, 15);
            this.ppRadioButton1.Size = new System.Drawing.Size(98, 21);
            this.ppRadioButton1.TabIndex = 24;
            this.ppRadioButton1.TabStop = true;
            this.ppRadioButton1.Text = "RadioButton";
            this.ppRadioButton1.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.ppRadioButton1.UnCheckedForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.ppRadioButton1.unCheckedPic = ((System.Drawing.Image)(resources.GetObject("ppRadioButton1.unCheckedPic")));
            // 
            // ppCheckBoxEx3
            // 
            this.ppCheckBoxEx3.CheckAlign = PPSkin.PPCheckBoxEx.Align.Left;
            this.ppCheckBoxEx3.CheckedForeColor = System.Drawing.Color.DodgerBlue;
            this.ppCheckBoxEx3.CheckedPic = global::PPSkinDemo.Properties.Resources.newBlue;
            this.ppCheckBoxEx3.CheckedText = "Check";
            this.ppCheckBoxEx3.Location = new System.Drawing.Point(326, 209);
            this.ppCheckBoxEx3.Name = "ppCheckBoxEx3";
            this.ppCheckBoxEx3.PicOffset = new System.Drawing.Point(0, 0);
            this.ppCheckBoxEx3.PicSize = new System.Drawing.Size(15, 15);
            this.ppCheckBoxEx3.Size = new System.Drawing.Size(82, 16);
            this.ppCheckBoxEx3.TabIndex = 23;
            this.ppCheckBoxEx3.Text = "ppCheckBoxEx3";
            this.ppCheckBoxEx3.TextDrawMode = PPSkin.Enums.DrawMode.Anti;
            this.ppCheckBoxEx3.TextOffset = new System.Drawing.Point(0, 0);
            this.ppCheckBoxEx3.UnCheckedForeColor = System.Drawing.Color.Red;
            this.ppCheckBoxEx3.unCheckedPic = global::PPSkinDemo.Properties.Resources.newGray;
            this.ppCheckBoxEx3.unCheckedText = "UnCheck";
            // 
            // ppCheckBoxEx4
            // 
            this.ppCheckBoxEx4.CheckAlign = PPSkin.PPCheckBoxEx.Align.Left;
            this.ppCheckBoxEx4.CheckedForeColor = System.Drawing.Color.DodgerBlue;
            this.ppCheckBoxEx4.CheckedPic = global::PPSkinDemo.Properties.Resources.newBlue;
            this.ppCheckBoxEx4.CheckedText = "Check";
            this.ppCheckBoxEx4.Location = new System.Drawing.Point(222, 209);
            this.ppCheckBoxEx4.Name = "ppCheckBoxEx4";
            this.ppCheckBoxEx4.PicOffset = new System.Drawing.Point(0, 0);
            this.ppCheckBoxEx4.PicSize = new System.Drawing.Size(15, 15);
            this.ppCheckBoxEx4.Size = new System.Drawing.Size(82, 16);
            this.ppCheckBoxEx4.TabIndex = 22;
            this.ppCheckBoxEx4.Text = "ppCheckBoxEx4";
            this.ppCheckBoxEx4.TextDrawMode = PPSkin.Enums.DrawMode.Anti;
            this.ppCheckBoxEx4.TextOffset = new System.Drawing.Point(0, 0);
            this.ppCheckBoxEx4.UnCheckedForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.ppCheckBoxEx4.unCheckedPic = global::PPSkinDemo.Properties.Resources.newGray;
            this.ppCheckBoxEx4.unCheckedText = "UnCheck";
            // 
            // ppCheckBoxEx2
            // 
            this.ppCheckBoxEx2.CheckAlign = PPSkin.PPCheckBoxEx.Align.Left;
            this.ppCheckBoxEx2.CheckedForeColor = System.Drawing.Color.DodgerBlue;
            this.ppCheckBoxEx2.CheckedPic = ((System.Drawing.Image)(resources.GetObject("ppCheckBoxEx2.CheckedPic")));
            this.ppCheckBoxEx2.CheckedText = "Check";
            this.ppCheckBoxEx2.Location = new System.Drawing.Point(122, 209);
            this.ppCheckBoxEx2.Name = "ppCheckBoxEx2";
            this.ppCheckBoxEx2.PicOffset = new System.Drawing.Point(0, 0);
            this.ppCheckBoxEx2.PicSize = new System.Drawing.Size(15, 15);
            this.ppCheckBoxEx2.Size = new System.Drawing.Size(82, 16);
            this.ppCheckBoxEx2.TabIndex = 21;
            this.ppCheckBoxEx2.Text = "ppCheckBoxEx2";
            this.ppCheckBoxEx2.TextDrawMode = PPSkin.Enums.DrawMode.Anti;
            this.ppCheckBoxEx2.TextOffset = new System.Drawing.Point(0, 0);
            this.ppCheckBoxEx2.UnCheckedForeColor = System.Drawing.Color.Red;
            this.ppCheckBoxEx2.unCheckedPic = ((System.Drawing.Image)(resources.GetObject("ppCheckBoxEx2.unCheckedPic")));
            this.ppCheckBoxEx2.unCheckedText = "UnCheck";
            // 
            // ppCheckBoxEx1
            // 
            this.ppCheckBoxEx1.CheckAlign = PPSkin.PPCheckBoxEx.Align.Left;
            this.ppCheckBoxEx1.CheckedForeColor = System.Drawing.Color.DodgerBlue;
            this.ppCheckBoxEx1.CheckedPic = ((System.Drawing.Image)(resources.GetObject("ppCheckBoxEx1.CheckedPic")));
            this.ppCheckBoxEx1.CheckedText = "Check";
            this.ppCheckBoxEx1.Location = new System.Drawing.Point(19, 209);
            this.ppCheckBoxEx1.Name = "ppCheckBoxEx1";
            this.ppCheckBoxEx1.PicOffset = new System.Drawing.Point(0, 0);
            this.ppCheckBoxEx1.PicSize = new System.Drawing.Size(15, 15);
            this.ppCheckBoxEx1.Size = new System.Drawing.Size(82, 16);
            this.ppCheckBoxEx1.TabIndex = 20;
            this.ppCheckBoxEx1.Text = "ppCheckBoxEx1";
            this.ppCheckBoxEx1.TextDrawMode = PPSkin.Enums.DrawMode.Anti;
            this.ppCheckBoxEx1.TextOffset = new System.Drawing.Point(0, 0);
            this.ppCheckBoxEx1.UnCheckedForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.ppCheckBoxEx1.unCheckedPic = ((System.Drawing.Image)(resources.GetObject("ppCheckBoxEx1.unCheckedPic")));
            this.ppCheckBoxEx1.unCheckedText = "UnCheck";
            // 
            // ppCheckBox16
            // 
            this.ppCheckBox16.BaseColor = System.Drawing.Color.DodgerBlue;
            this.ppCheckBox16.ButtonColor = System.Drawing.Color.White;
            this.ppCheckBox16.ButtonRadius = 22;
            this.ppCheckBox16.buttonRectHeight = 22;
            this.ppCheckBox16.buttonRectWidth = 22;
            this.ppCheckBox16.buttonRectX = 34;
            this.ppCheckBox16.buttonRectY = 4;
            this.ppCheckBox16.Checked = true;
            this.ppCheckBox16.CheckedBaseColor = System.Drawing.Color.DodgerBlue;
            this.ppCheckBox16.CheckedColor = System.Drawing.Color.OrangeRed;
            this.ppCheckBox16.CheckedImage = null;
            this.ppCheckBox16.CheckedText = "On";
            this.ppCheckBox16.ImageSize = new System.Drawing.Size(15, 15);
            this.ppCheckBox16.Location = new System.Drawing.Point(348, 146);
            this.ppCheckBox16.Name = "ppCheckBox16";
            this.ppCheckBox16.Radius = 30;
            this.ppCheckBox16.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppCheckBox16.Size = new System.Drawing.Size(60, 30);
            this.ppCheckBox16.TabIndex = 19;
            this.ppCheckBox16.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.ppCheckBox16.UnCheckedBaseColor = System.Drawing.Color.Red;
            this.ppCheckBox16.UnCheckedColor = System.Drawing.Color.Black;
            this.ppCheckBox16.UnCheckedImage = null;
            this.ppCheckBox16.UnCheckedText = "Off";
            this.ppCheckBox16.UseAnimation = true;
            this.ppCheckBox16.Xoffset = 0;
            this.ppCheckBox16.Yoffset = 0;
            // 
            // ppCheckBox17
            // 
            this.ppCheckBox17.BaseColor = System.Drawing.Color.Lime;
            this.ppCheckBox17.ButtonColor = System.Drawing.Color.Yellow;
            this.ppCheckBox17.ButtonRadius = 22;
            this.ppCheckBox17.buttonRectHeight = 22;
            this.ppCheckBox17.buttonRectWidth = 22;
            this.ppCheckBox17.buttonRectX = 34;
            this.ppCheckBox17.buttonRectY = 4;
            this.ppCheckBox17.Checked = true;
            this.ppCheckBox17.CheckedBaseColor = System.Drawing.Color.Lime;
            this.ppCheckBox17.CheckedColor = System.Drawing.Color.Red;
            this.ppCheckBox17.CheckedImage = null;
            this.ppCheckBox17.CheckedText = "On";
            this.ppCheckBox17.ImageSize = new System.Drawing.Size(15, 15);
            this.ppCheckBox17.Location = new System.Drawing.Point(270, 146);
            this.ppCheckBox17.Name = "ppCheckBox17";
            this.ppCheckBox17.Radius = 30;
            this.ppCheckBox17.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppCheckBox17.Size = new System.Drawing.Size(60, 30);
            this.ppCheckBox17.TabIndex = 18;
            this.ppCheckBox17.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.ppCheckBox17.UnCheckedBaseColor = System.Drawing.Color.DarkOrange;
            this.ppCheckBox17.UnCheckedColor = System.Drawing.Color.Red;
            this.ppCheckBox17.UnCheckedImage = null;
            this.ppCheckBox17.UnCheckedText = "Off";
            this.ppCheckBox17.UseAnimation = true;
            this.ppCheckBox17.Xoffset = 0;
            this.ppCheckBox17.Yoffset = 0;
            // 
            // ppCheckBox18
            // 
            this.ppCheckBox18.BaseColor = System.Drawing.Color.Orange;
            this.ppCheckBox18.ButtonColor = System.Drawing.Color.Red;
            this.ppCheckBox18.ButtonRadius = 22;
            this.ppCheckBox18.buttonRectHeight = 22;
            this.ppCheckBox18.buttonRectWidth = 22;
            this.ppCheckBox18.buttonRectX = 34;
            this.ppCheckBox18.buttonRectY = 4;
            this.ppCheckBox18.Checked = true;
            this.ppCheckBox18.CheckedBaseColor = System.Drawing.Color.Orange;
            this.ppCheckBox18.CheckedColor = System.Drawing.Color.Green;
            this.ppCheckBox18.CheckedImage = null;
            this.ppCheckBox18.CheckedText = "On";
            this.ppCheckBox18.ImageSize = new System.Drawing.Size(15, 15);
            this.ppCheckBox18.Location = new System.Drawing.Point(186, 146);
            this.ppCheckBox18.Name = "ppCheckBox18";
            this.ppCheckBox18.Radius = 30;
            this.ppCheckBox18.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppCheckBox18.Size = new System.Drawing.Size(60, 30);
            this.ppCheckBox18.TabIndex = 17;
            this.ppCheckBox18.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.ppCheckBox18.UnCheckedBaseColor = System.Drawing.Color.NavajoWhite;
            this.ppCheckBox18.UnCheckedColor = System.Drawing.Color.Red;
            this.ppCheckBox18.UnCheckedImage = null;
            this.ppCheckBox18.UnCheckedText = "Off";
            this.ppCheckBox18.UseAnimation = true;
            this.ppCheckBox18.Xoffset = 0;
            this.ppCheckBox18.Yoffset = 0;
            // 
            // ppCheckBox19
            // 
            this.ppCheckBox19.BaseColor = System.Drawing.Color.LightGray;
            this.ppCheckBox19.ButtonColor = System.Drawing.Color.Black;
            this.ppCheckBox19.ButtonRadius = 22;
            this.ppCheckBox19.buttonRectHeight = 22;
            this.ppCheckBox19.buttonRectWidth = 22;
            this.ppCheckBox19.buttonRectX = 34;
            this.ppCheckBox19.buttonRectY = 4;
            this.ppCheckBox19.Checked = true;
            this.ppCheckBox19.CheckedBaseColor = System.Drawing.Color.LightGray;
            this.ppCheckBox19.CheckedColor = System.Drawing.Color.Green;
            this.ppCheckBox19.CheckedImage = null;
            this.ppCheckBox19.CheckedText = "On";
            this.ppCheckBox19.ImageSize = new System.Drawing.Size(15, 15);
            this.ppCheckBox19.Location = new System.Drawing.Point(99, 146);
            this.ppCheckBox19.Name = "ppCheckBox19";
            this.ppCheckBox19.Radius = 30;
            this.ppCheckBox19.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppCheckBox19.Size = new System.Drawing.Size(60, 30);
            this.ppCheckBox19.TabIndex = 16;
            this.ppCheckBox19.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.ppCheckBox19.UnCheckedBaseColor = System.Drawing.Color.LightGray;
            this.ppCheckBox19.UnCheckedColor = System.Drawing.Color.Red;
            this.ppCheckBox19.UnCheckedImage = null;
            this.ppCheckBox19.UnCheckedText = "Off";
            this.ppCheckBox19.UseAnimation = true;
            this.ppCheckBox19.Xoffset = 0;
            this.ppCheckBox19.Yoffset = 0;
            // 
            // ppCheckBox20
            // 
            this.ppCheckBox20.BaseColor = System.Drawing.Color.DodgerBlue;
            this.ppCheckBox20.ButtonColor = System.Drawing.Color.White;
            this.ppCheckBox20.ButtonRadius = 22;
            this.ppCheckBox20.buttonRectHeight = 22;
            this.ppCheckBox20.buttonRectWidth = 22;
            this.ppCheckBox20.buttonRectX = 34;
            this.ppCheckBox20.buttonRectY = 4;
            this.ppCheckBox20.Checked = true;
            this.ppCheckBox20.CheckedBaseColor = System.Drawing.Color.DodgerBlue;
            this.ppCheckBox20.CheckedColor = System.Drawing.Color.Chartreuse;
            this.ppCheckBox20.CheckedImage = null;
            this.ppCheckBox20.CheckedText = "On";
            this.ppCheckBox20.ImageSize = new System.Drawing.Size(15, 15);
            this.ppCheckBox20.Location = new System.Drawing.Point(19, 146);
            this.ppCheckBox20.Name = "ppCheckBox20";
            this.ppCheckBox20.Radius = 30;
            this.ppCheckBox20.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppCheckBox20.Size = new System.Drawing.Size(60, 30);
            this.ppCheckBox20.TabIndex = 15;
            this.ppCheckBox20.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.ppCheckBox20.UnCheckedBaseColor = System.Drawing.Color.Gray;
            this.ppCheckBox20.UnCheckedColor = System.Drawing.Color.Red;
            this.ppCheckBox20.UnCheckedImage = null;
            this.ppCheckBox20.UnCheckedText = "Off";
            this.ppCheckBox20.UseAnimation = true;
            this.ppCheckBox20.Xoffset = 0;
            this.ppCheckBox20.Yoffset = 0;
            // 
            // ppCheckBox11
            // 
            this.ppCheckBox11.BaseColor = System.Drawing.Color.Red;
            this.ppCheckBox11.ButtonColor = System.Drawing.Color.White;
            this.ppCheckBox11.ButtonRadius = 22;
            this.ppCheckBox11.buttonRectHeight = 22;
            this.ppCheckBox11.buttonRectWidth = 22;
            this.ppCheckBox11.buttonRectX = 4;
            this.ppCheckBox11.buttonRectY = 4;
            this.ppCheckBox11.Checked = false;
            this.ppCheckBox11.CheckedBaseColor = System.Drawing.Color.DodgerBlue;
            this.ppCheckBox11.CheckedColor = System.Drawing.Color.Green;
            this.ppCheckBox11.CheckedImage = null;
            this.ppCheckBox11.CheckedText = "";
            this.ppCheckBox11.ImageSize = new System.Drawing.Size(15, 15);
            this.ppCheckBox11.Location = new System.Drawing.Point(348, 110);
            this.ppCheckBox11.Name = "ppCheckBox11";
            this.ppCheckBox11.Radius = 0;
            this.ppCheckBox11.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppCheckBox11.Size = new System.Drawing.Size(60, 30);
            this.ppCheckBox11.TabIndex = 14;
            this.ppCheckBox11.TextDrawMode = PPSkin.Enums.DrawMode.Anti;
            this.ppCheckBox11.UnCheckedBaseColor = System.Drawing.Color.Red;
            this.ppCheckBox11.UnCheckedColor = System.Drawing.Color.Red;
            this.ppCheckBox11.UnCheckedImage = null;
            this.ppCheckBox11.UnCheckedText = "";
            this.ppCheckBox11.UseAnimation = true;
            this.ppCheckBox11.Xoffset = 0;
            this.ppCheckBox11.Yoffset = 0;
            // 
            // ppCheckBox12
            // 
            this.ppCheckBox12.BaseColor = System.Drawing.Color.DarkOrange;
            this.ppCheckBox12.ButtonColor = System.Drawing.Color.Yellow;
            this.ppCheckBox12.ButtonRadius = 22;
            this.ppCheckBox12.buttonRectHeight = 22;
            this.ppCheckBox12.buttonRectWidth = 22;
            this.ppCheckBox12.buttonRectX = 4;
            this.ppCheckBox12.buttonRectY = 4;
            this.ppCheckBox12.Checked = false;
            this.ppCheckBox12.CheckedBaseColor = System.Drawing.Color.Lime;
            this.ppCheckBox12.CheckedColor = System.Drawing.Color.Green;
            this.ppCheckBox12.CheckedImage = null;
            this.ppCheckBox12.CheckedText = "";
            this.ppCheckBox12.ImageSize = new System.Drawing.Size(15, 15);
            this.ppCheckBox12.Location = new System.Drawing.Point(270, 110);
            this.ppCheckBox12.Name = "ppCheckBox12";
            this.ppCheckBox12.Radius = 0;
            this.ppCheckBox12.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppCheckBox12.Size = new System.Drawing.Size(60, 30);
            this.ppCheckBox12.TabIndex = 13;
            this.ppCheckBox12.TextDrawMode = PPSkin.Enums.DrawMode.Anti;
            this.ppCheckBox12.UnCheckedBaseColor = System.Drawing.Color.DarkOrange;
            this.ppCheckBox12.UnCheckedColor = System.Drawing.Color.Red;
            this.ppCheckBox12.UnCheckedImage = null;
            this.ppCheckBox12.UnCheckedText = "";
            this.ppCheckBox12.UseAnimation = true;
            this.ppCheckBox12.Xoffset = 0;
            this.ppCheckBox12.Yoffset = 0;
            // 
            // ppCheckBox13
            // 
            this.ppCheckBox13.BaseColor = System.Drawing.Color.NavajoWhite;
            this.ppCheckBox13.ButtonColor = System.Drawing.Color.Red;
            this.ppCheckBox13.ButtonRadius = 22;
            this.ppCheckBox13.buttonRectHeight = 22;
            this.ppCheckBox13.buttonRectWidth = 22;
            this.ppCheckBox13.buttonRectX = 4;
            this.ppCheckBox13.buttonRectY = 4;
            this.ppCheckBox13.Checked = false;
            this.ppCheckBox13.CheckedBaseColor = System.Drawing.Color.Orange;
            this.ppCheckBox13.CheckedColor = System.Drawing.Color.Green;
            this.ppCheckBox13.CheckedImage = null;
            this.ppCheckBox13.CheckedText = "";
            this.ppCheckBox13.ImageSize = new System.Drawing.Size(15, 15);
            this.ppCheckBox13.Location = new System.Drawing.Point(186, 110);
            this.ppCheckBox13.Name = "ppCheckBox13";
            this.ppCheckBox13.Radius = 0;
            this.ppCheckBox13.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppCheckBox13.Size = new System.Drawing.Size(60, 30);
            this.ppCheckBox13.TabIndex = 12;
            this.ppCheckBox13.TextDrawMode = PPSkin.Enums.DrawMode.Anti;
            this.ppCheckBox13.UnCheckedBaseColor = System.Drawing.Color.NavajoWhite;
            this.ppCheckBox13.UnCheckedColor = System.Drawing.Color.Red;
            this.ppCheckBox13.UnCheckedImage = null;
            this.ppCheckBox13.UnCheckedText = "";
            this.ppCheckBox13.UseAnimation = true;
            this.ppCheckBox13.Xoffset = 0;
            this.ppCheckBox13.Yoffset = 0;
            // 
            // ppCheckBox14
            // 
            this.ppCheckBox14.BaseColor = System.Drawing.Color.LightGray;
            this.ppCheckBox14.ButtonColor = System.Drawing.Color.Black;
            this.ppCheckBox14.ButtonRadius = 22;
            this.ppCheckBox14.buttonRectHeight = 22;
            this.ppCheckBox14.buttonRectWidth = 22;
            this.ppCheckBox14.buttonRectX = 4;
            this.ppCheckBox14.buttonRectY = 4;
            this.ppCheckBox14.Checked = false;
            this.ppCheckBox14.CheckedBaseColor = System.Drawing.Color.LightGray;
            this.ppCheckBox14.CheckedColor = System.Drawing.Color.Green;
            this.ppCheckBox14.CheckedImage = null;
            this.ppCheckBox14.CheckedText = "";
            this.ppCheckBox14.ImageSize = new System.Drawing.Size(15, 15);
            this.ppCheckBox14.Location = new System.Drawing.Point(99, 110);
            this.ppCheckBox14.Name = "ppCheckBox14";
            this.ppCheckBox14.Radius = 0;
            this.ppCheckBox14.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppCheckBox14.Size = new System.Drawing.Size(60, 30);
            this.ppCheckBox14.TabIndex = 11;
            this.ppCheckBox14.TextDrawMode = PPSkin.Enums.DrawMode.Anti;
            this.ppCheckBox14.UnCheckedBaseColor = System.Drawing.Color.LightGray;
            this.ppCheckBox14.UnCheckedColor = System.Drawing.Color.Red;
            this.ppCheckBox14.UnCheckedImage = null;
            this.ppCheckBox14.UnCheckedText = "";
            this.ppCheckBox14.UseAnimation = true;
            this.ppCheckBox14.Xoffset = 0;
            this.ppCheckBox14.Yoffset = 0;
            // 
            // ppCheckBox15
            // 
            this.ppCheckBox15.BaseColor = System.Drawing.Color.Gray;
            this.ppCheckBox15.ButtonColor = System.Drawing.Color.White;
            this.ppCheckBox15.ButtonRadius = 0;
            this.ppCheckBox15.buttonRectHeight = 22;
            this.ppCheckBox15.buttonRectWidth = 22;
            this.ppCheckBox15.buttonRectX = 4;
            this.ppCheckBox15.buttonRectY = 4;
            this.ppCheckBox15.Checked = false;
            this.ppCheckBox15.CheckedBaseColor = System.Drawing.Color.DodgerBlue;
            this.ppCheckBox15.CheckedColor = System.Drawing.Color.Green;
            this.ppCheckBox15.CheckedImage = null;
            this.ppCheckBox15.CheckedText = "";
            this.ppCheckBox15.ImageSize = new System.Drawing.Size(15, 15);
            this.ppCheckBox15.Location = new System.Drawing.Point(19, 110);
            this.ppCheckBox15.Name = "ppCheckBox15";
            this.ppCheckBox15.Radius = 0;
            this.ppCheckBox15.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppCheckBox15.Size = new System.Drawing.Size(60, 30);
            this.ppCheckBox15.TabIndex = 10;
            this.ppCheckBox15.TextDrawMode = PPSkin.Enums.DrawMode.Anti;
            this.ppCheckBox15.UnCheckedBaseColor = System.Drawing.Color.Gray;
            this.ppCheckBox15.UnCheckedColor = System.Drawing.Color.Red;
            this.ppCheckBox15.UnCheckedImage = null;
            this.ppCheckBox15.UnCheckedText = "";
            this.ppCheckBox15.UseAnimation = true;
            this.ppCheckBox15.Xoffset = 0;
            this.ppCheckBox15.Yoffset = 0;
            // 
            // ppCheckBox6
            // 
            this.ppCheckBox6.BaseColor = System.Drawing.Color.Red;
            this.ppCheckBox6.ButtonColor = System.Drawing.Color.White;
            this.ppCheckBox6.ButtonRadius = 22;
            this.ppCheckBox6.buttonRectHeight = 22;
            this.ppCheckBox6.buttonRectWidth = 22;
            this.ppCheckBox6.buttonRectX = 4;
            this.ppCheckBox6.buttonRectY = 4;
            this.ppCheckBox6.Checked = false;
            this.ppCheckBox6.CheckedBaseColor = System.Drawing.Color.DodgerBlue;
            this.ppCheckBox6.CheckedColor = System.Drawing.Color.Green;
            this.ppCheckBox6.CheckedImage = null;
            this.ppCheckBox6.CheckedText = "";
            this.ppCheckBox6.ImageSize = new System.Drawing.Size(15, 15);
            this.ppCheckBox6.Location = new System.Drawing.Point(348, 74);
            this.ppCheckBox6.Name = "ppCheckBox6";
            this.ppCheckBox6.Radius = 20;
            this.ppCheckBox6.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppCheckBox6.Size = new System.Drawing.Size(60, 30);
            this.ppCheckBox6.TabIndex = 9;
            this.ppCheckBox6.TextDrawMode = PPSkin.Enums.DrawMode.Anti;
            this.ppCheckBox6.UnCheckedBaseColor = System.Drawing.Color.Red;
            this.ppCheckBox6.UnCheckedColor = System.Drawing.Color.Red;
            this.ppCheckBox6.UnCheckedImage = null;
            this.ppCheckBox6.UnCheckedText = "";
            this.ppCheckBox6.UseAnimation = true;
            this.ppCheckBox6.Xoffset = 0;
            this.ppCheckBox6.Yoffset = 0;
            // 
            // ppCheckBox7
            // 
            this.ppCheckBox7.BaseColor = System.Drawing.Color.DarkOrange;
            this.ppCheckBox7.ButtonColor = System.Drawing.Color.Yellow;
            this.ppCheckBox7.ButtonRadius = 22;
            this.ppCheckBox7.buttonRectHeight = 22;
            this.ppCheckBox7.buttonRectWidth = 22;
            this.ppCheckBox7.buttonRectX = 4;
            this.ppCheckBox7.buttonRectY = 4;
            this.ppCheckBox7.Checked = false;
            this.ppCheckBox7.CheckedBaseColor = System.Drawing.Color.Lime;
            this.ppCheckBox7.CheckedColor = System.Drawing.Color.Green;
            this.ppCheckBox7.CheckedImage = null;
            this.ppCheckBox7.CheckedText = "";
            this.ppCheckBox7.ImageSize = new System.Drawing.Size(15, 15);
            this.ppCheckBox7.Location = new System.Drawing.Point(270, 74);
            this.ppCheckBox7.Name = "ppCheckBox7";
            this.ppCheckBox7.Radius = 20;
            this.ppCheckBox7.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppCheckBox7.Size = new System.Drawing.Size(60, 30);
            this.ppCheckBox7.TabIndex = 8;
            this.ppCheckBox7.TextDrawMode = PPSkin.Enums.DrawMode.Anti;
            this.ppCheckBox7.UnCheckedBaseColor = System.Drawing.Color.DarkOrange;
            this.ppCheckBox7.UnCheckedColor = System.Drawing.Color.Red;
            this.ppCheckBox7.UnCheckedImage = null;
            this.ppCheckBox7.UnCheckedText = "";
            this.ppCheckBox7.UseAnimation = true;
            this.ppCheckBox7.Xoffset = 0;
            this.ppCheckBox7.Yoffset = 0;
            // 
            // ppCheckBox8
            // 
            this.ppCheckBox8.BaseColor = System.Drawing.Color.NavajoWhite;
            this.ppCheckBox8.ButtonColor = System.Drawing.Color.Red;
            this.ppCheckBox8.ButtonRadius = 22;
            this.ppCheckBox8.buttonRectHeight = 22;
            this.ppCheckBox8.buttonRectWidth = 22;
            this.ppCheckBox8.buttonRectX = 4;
            this.ppCheckBox8.buttonRectY = 4;
            this.ppCheckBox8.Checked = false;
            this.ppCheckBox8.CheckedBaseColor = System.Drawing.Color.Orange;
            this.ppCheckBox8.CheckedColor = System.Drawing.Color.Green;
            this.ppCheckBox8.CheckedImage = null;
            this.ppCheckBox8.CheckedText = "";
            this.ppCheckBox8.ImageSize = new System.Drawing.Size(15, 15);
            this.ppCheckBox8.Location = new System.Drawing.Point(186, 74);
            this.ppCheckBox8.Name = "ppCheckBox8";
            this.ppCheckBox8.Radius = 20;
            this.ppCheckBox8.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppCheckBox8.Size = new System.Drawing.Size(60, 30);
            this.ppCheckBox8.TabIndex = 7;
            this.ppCheckBox8.TextDrawMode = PPSkin.Enums.DrawMode.Anti;
            this.ppCheckBox8.UnCheckedBaseColor = System.Drawing.Color.NavajoWhite;
            this.ppCheckBox8.UnCheckedColor = System.Drawing.Color.Red;
            this.ppCheckBox8.UnCheckedImage = null;
            this.ppCheckBox8.UnCheckedText = "";
            this.ppCheckBox8.UseAnimation = true;
            this.ppCheckBox8.Xoffset = 0;
            this.ppCheckBox8.Yoffset = 0;
            // 
            // ppCheckBox9
            // 
            this.ppCheckBox9.BaseColor = System.Drawing.Color.LightGray;
            this.ppCheckBox9.ButtonColor = System.Drawing.Color.Black;
            this.ppCheckBox9.ButtonRadius = 22;
            this.ppCheckBox9.buttonRectHeight = 22;
            this.ppCheckBox9.buttonRectWidth = 22;
            this.ppCheckBox9.buttonRectX = 4;
            this.ppCheckBox9.buttonRectY = 4;
            this.ppCheckBox9.Checked = false;
            this.ppCheckBox9.CheckedBaseColor = System.Drawing.Color.LightGray;
            this.ppCheckBox9.CheckedColor = System.Drawing.Color.Green;
            this.ppCheckBox9.CheckedImage = null;
            this.ppCheckBox9.CheckedText = "";
            this.ppCheckBox9.ImageSize = new System.Drawing.Size(15, 15);
            this.ppCheckBox9.Location = new System.Drawing.Point(99, 74);
            this.ppCheckBox9.Name = "ppCheckBox9";
            this.ppCheckBox9.Radius = 20;
            this.ppCheckBox9.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppCheckBox9.Size = new System.Drawing.Size(60, 30);
            this.ppCheckBox9.TabIndex = 6;
            this.ppCheckBox9.TextDrawMode = PPSkin.Enums.DrawMode.Anti;
            this.ppCheckBox9.UnCheckedBaseColor = System.Drawing.Color.LightGray;
            this.ppCheckBox9.UnCheckedColor = System.Drawing.Color.Red;
            this.ppCheckBox9.UnCheckedImage = null;
            this.ppCheckBox9.UnCheckedText = "";
            this.ppCheckBox9.UseAnimation = true;
            this.ppCheckBox9.Xoffset = 0;
            this.ppCheckBox9.Yoffset = 0;
            // 
            // ppCheckBox10
            // 
            this.ppCheckBox10.BaseColor = System.Drawing.Color.Gray;
            this.ppCheckBox10.ButtonColor = System.Drawing.Color.White;
            this.ppCheckBox10.ButtonRadius = 22;
            this.ppCheckBox10.buttonRectHeight = 22;
            this.ppCheckBox10.buttonRectWidth = 22;
            this.ppCheckBox10.buttonRectX = 4;
            this.ppCheckBox10.buttonRectY = 4;
            this.ppCheckBox10.Checked = false;
            this.ppCheckBox10.CheckedBaseColor = System.Drawing.Color.DodgerBlue;
            this.ppCheckBox10.CheckedColor = System.Drawing.Color.Green;
            this.ppCheckBox10.CheckedImage = null;
            this.ppCheckBox10.CheckedText = "";
            this.ppCheckBox10.ImageSize = new System.Drawing.Size(15, 15);
            this.ppCheckBox10.Location = new System.Drawing.Point(19, 74);
            this.ppCheckBox10.Name = "ppCheckBox10";
            this.ppCheckBox10.Radius = 20;
            this.ppCheckBox10.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppCheckBox10.Size = new System.Drawing.Size(60, 30);
            this.ppCheckBox10.TabIndex = 5;
            this.ppCheckBox10.TextDrawMode = PPSkin.Enums.DrawMode.Anti;
            this.ppCheckBox10.UnCheckedBaseColor = System.Drawing.Color.Gray;
            this.ppCheckBox10.UnCheckedColor = System.Drawing.Color.Red;
            this.ppCheckBox10.UnCheckedImage = null;
            this.ppCheckBox10.UnCheckedText = "";
            this.ppCheckBox10.UseAnimation = true;
            this.ppCheckBox10.Xoffset = 0;
            this.ppCheckBox10.Yoffset = 0;
            // 
            // ppCheckBox5
            // 
            this.ppCheckBox5.BaseColor = System.Drawing.Color.Red;
            this.ppCheckBox5.ButtonColor = System.Drawing.Color.White;
            this.ppCheckBox5.ButtonRadius = 22;
            this.ppCheckBox5.buttonRectHeight = 22;
            this.ppCheckBox5.buttonRectWidth = 22;
            this.ppCheckBox5.buttonRectX = 4;
            this.ppCheckBox5.buttonRectY = 4;
            this.ppCheckBox5.Checked = false;
            this.ppCheckBox5.CheckedBaseColor = System.Drawing.Color.DodgerBlue;
            this.ppCheckBox5.CheckedColor = System.Drawing.Color.Green;
            this.ppCheckBox5.CheckedImage = null;
            this.ppCheckBox5.CheckedText = "";
            this.ppCheckBox5.ImageSize = new System.Drawing.Size(15, 15);
            this.ppCheckBox5.Location = new System.Drawing.Point(348, 38);
            this.ppCheckBox5.Name = "ppCheckBox5";
            this.ppCheckBox5.Radius = 30;
            this.ppCheckBox5.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppCheckBox5.Size = new System.Drawing.Size(60, 30);
            this.ppCheckBox5.TabIndex = 4;
            this.ppCheckBox5.TextDrawMode = PPSkin.Enums.DrawMode.Anti;
            this.ppCheckBox5.UnCheckedBaseColor = System.Drawing.Color.Red;
            this.ppCheckBox5.UnCheckedColor = System.Drawing.Color.Red;
            this.ppCheckBox5.UnCheckedImage = null;
            this.ppCheckBox5.UnCheckedText = "";
            this.ppCheckBox5.UseAnimation = false;
            this.ppCheckBox5.Xoffset = 0;
            this.ppCheckBox5.Yoffset = 0;
            // 
            // ppCheckBox4
            // 
            this.ppCheckBox4.BaseColor = System.Drawing.Color.DarkOrange;
            this.ppCheckBox4.ButtonColor = System.Drawing.Color.Yellow;
            this.ppCheckBox4.ButtonRadius = 22;
            this.ppCheckBox4.buttonRectHeight = 22;
            this.ppCheckBox4.buttonRectWidth = 22;
            this.ppCheckBox4.buttonRectX = 4;
            this.ppCheckBox4.buttonRectY = 4;
            this.ppCheckBox4.Checked = false;
            this.ppCheckBox4.CheckedBaseColor = System.Drawing.Color.Lime;
            this.ppCheckBox4.CheckedColor = System.Drawing.Color.Green;
            this.ppCheckBox4.CheckedImage = null;
            this.ppCheckBox4.CheckedText = "";
            this.ppCheckBox4.ImageSize = new System.Drawing.Size(15, 15);
            this.ppCheckBox4.Location = new System.Drawing.Point(270, 38);
            this.ppCheckBox4.Name = "ppCheckBox4";
            this.ppCheckBox4.Radius = 30;
            this.ppCheckBox4.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppCheckBox4.Size = new System.Drawing.Size(60, 30);
            this.ppCheckBox4.TabIndex = 3;
            this.ppCheckBox4.TextDrawMode = PPSkin.Enums.DrawMode.Anti;
            this.ppCheckBox4.UnCheckedBaseColor = System.Drawing.Color.DarkOrange;
            this.ppCheckBox4.UnCheckedColor = System.Drawing.Color.Red;
            this.ppCheckBox4.UnCheckedImage = null;
            this.ppCheckBox4.UnCheckedText = "";
            this.ppCheckBox4.UseAnimation = false;
            this.ppCheckBox4.Xoffset = 0;
            this.ppCheckBox4.Yoffset = 0;
            // 
            // ppCheckBox3
            // 
            this.ppCheckBox3.BaseColor = System.Drawing.Color.NavajoWhite;
            this.ppCheckBox3.ButtonColor = System.Drawing.Color.Red;
            this.ppCheckBox3.ButtonRadius = 22;
            this.ppCheckBox3.buttonRectHeight = 22;
            this.ppCheckBox3.buttonRectWidth = 22;
            this.ppCheckBox3.buttonRectX = 4;
            this.ppCheckBox3.buttonRectY = 4;
            this.ppCheckBox3.Checked = false;
            this.ppCheckBox3.CheckedBaseColor = System.Drawing.Color.Orange;
            this.ppCheckBox3.CheckedColor = System.Drawing.Color.Green;
            this.ppCheckBox3.CheckedImage = null;
            this.ppCheckBox3.CheckedText = "";
            this.ppCheckBox3.ImageSize = new System.Drawing.Size(15, 15);
            this.ppCheckBox3.Location = new System.Drawing.Point(186, 38);
            this.ppCheckBox3.Name = "ppCheckBox3";
            this.ppCheckBox3.Radius = 30;
            this.ppCheckBox3.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppCheckBox3.Size = new System.Drawing.Size(60, 30);
            this.ppCheckBox3.TabIndex = 2;
            this.ppCheckBox3.TextDrawMode = PPSkin.Enums.DrawMode.Anti;
            this.ppCheckBox3.UnCheckedBaseColor = System.Drawing.Color.NavajoWhite;
            this.ppCheckBox3.UnCheckedColor = System.Drawing.Color.Red;
            this.ppCheckBox3.UnCheckedImage = null;
            this.ppCheckBox3.UnCheckedText = "";
            this.ppCheckBox3.UseAnimation = false;
            this.ppCheckBox3.Xoffset = 0;
            this.ppCheckBox3.Yoffset = 0;
            // 
            // ppCheckBox2
            // 
            this.ppCheckBox2.BaseColor = System.Drawing.Color.LightGray;
            this.ppCheckBox2.ButtonColor = System.Drawing.Color.Black;
            this.ppCheckBox2.ButtonRadius = 22;
            this.ppCheckBox2.buttonRectHeight = 22;
            this.ppCheckBox2.buttonRectWidth = 22;
            this.ppCheckBox2.buttonRectX = 4;
            this.ppCheckBox2.buttonRectY = 4;
            this.ppCheckBox2.Checked = false;
            this.ppCheckBox2.CheckedBaseColor = System.Drawing.Color.LightGray;
            this.ppCheckBox2.CheckedColor = System.Drawing.Color.Green;
            this.ppCheckBox2.CheckedImage = null;
            this.ppCheckBox2.CheckedText = "";
            this.ppCheckBox2.ImageSize = new System.Drawing.Size(15, 15);
            this.ppCheckBox2.Location = new System.Drawing.Point(99, 38);
            this.ppCheckBox2.Name = "ppCheckBox2";
            this.ppCheckBox2.Radius = 28;
            this.ppCheckBox2.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppCheckBox2.Size = new System.Drawing.Size(60, 30);
            this.ppCheckBox2.TabIndex = 1;
            this.ppCheckBox2.TextDrawMode = PPSkin.Enums.DrawMode.Anti;
            this.ppCheckBox2.UnCheckedBaseColor = System.Drawing.Color.LightGray;
            this.ppCheckBox2.UnCheckedColor = System.Drawing.Color.Red;
            this.ppCheckBox2.UnCheckedImage = null;
            this.ppCheckBox2.UnCheckedText = "";
            this.ppCheckBox2.UseAnimation = false;
            this.ppCheckBox2.Xoffset = 0;
            this.ppCheckBox2.Yoffset = 0;
            // 
            // ppCheckBox1
            // 
            this.ppCheckBox1.BaseColor = System.Drawing.Color.Gray;
            this.ppCheckBox1.ButtonColor = System.Drawing.Color.White;
            this.ppCheckBox1.ButtonRadius = 22;
            this.ppCheckBox1.buttonRectHeight = 22;
            this.ppCheckBox1.buttonRectWidth = 22;
            this.ppCheckBox1.buttonRectX = 4;
            this.ppCheckBox1.buttonRectY = 4;
            this.ppCheckBox1.Checked = false;
            this.ppCheckBox1.CheckedBaseColor = System.Drawing.Color.DodgerBlue;
            this.ppCheckBox1.CheckedColor = System.Drawing.Color.Green;
            this.ppCheckBox1.CheckedImage = null;
            this.ppCheckBox1.CheckedText = "";
            this.ppCheckBox1.ImageSize = new System.Drawing.Size(15, 15);
            this.ppCheckBox1.Location = new System.Drawing.Point(19, 38);
            this.ppCheckBox1.Name = "ppCheckBox1";
            this.ppCheckBox1.Radius = 30;
            this.ppCheckBox1.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppCheckBox1.Size = new System.Drawing.Size(60, 30);
            this.ppCheckBox1.TabIndex = 0;
            this.ppCheckBox1.TextDrawMode = PPSkin.Enums.DrawMode.Anti;
            this.ppCheckBox1.UnCheckedBaseColor = System.Drawing.Color.Gray;
            this.ppCheckBox1.UnCheckedColor = System.Drawing.Color.Red;
            this.ppCheckBox1.UnCheckedImage = null;
            this.ppCheckBox1.UnCheckedText = "";
            this.ppCheckBox1.UseAnimation = false;
            this.ppCheckBox1.Xoffset = 0;
            this.ppCheckBox1.Yoffset = 0;
            // 
            // ppGroupBox1
            // 
            this.ppGroupBox1.BackColor = System.Drawing.Color.White;
            this.ppGroupBox1.BaseColor = System.Drawing.Color.White;
            this.ppGroupBox1.BorderColor = System.Drawing.Color.Gray;
            this.ppGroupBox1.BorderWidth = 1;
            this.ppGroupBox1.Controls.Add(this.ppButton26);
            this.ppGroupBox1.Controls.Add(this.ppButton27);
            this.ppGroupBox1.Controls.Add(this.ppButton28);
            this.ppGroupBox1.Controls.Add(this.ppButton29);
            this.ppGroupBox1.Controls.Add(this.ppButton30);
            this.ppGroupBox1.Controls.Add(this.ppButton21);
            this.ppGroupBox1.Controls.Add(this.ppButton22);
            this.ppGroupBox1.Controls.Add(this.ppButton23);
            this.ppGroupBox1.Controls.Add(this.ppButton24);
            this.ppGroupBox1.Controls.Add(this.ppButton25);
            this.ppGroupBox1.Controls.Add(this.ppButton16);
            this.ppGroupBox1.Controls.Add(this.ppButton17);
            this.ppGroupBox1.Controls.Add(this.ppButton18);
            this.ppGroupBox1.Controls.Add(this.ppButton19);
            this.ppGroupBox1.Controls.Add(this.ppButton20);
            this.ppGroupBox1.Controls.Add(this.ppButton11);
            this.ppGroupBox1.Controls.Add(this.ppButton12);
            this.ppGroupBox1.Controls.Add(this.ppButton13);
            this.ppGroupBox1.Controls.Add(this.ppButton14);
            this.ppGroupBox1.Controls.Add(this.ppButton15);
            this.ppGroupBox1.Controls.Add(this.ppButton6);
            this.ppGroupBox1.Controls.Add(this.ppButton7);
            this.ppGroupBox1.Controls.Add(this.ppButton8);
            this.ppGroupBox1.Controls.Add(this.ppButton9);
            this.ppGroupBox1.Controls.Add(this.ppButton10);
            this.ppGroupBox1.Controls.Add(this.ppButton5);
            this.ppGroupBox1.Controls.Add(this.ppButton4);
            this.ppGroupBox1.Controls.Add(this.ppButton3);
            this.ppGroupBox1.Controls.Add(this.ppButton2);
            this.ppGroupBox1.Controls.Add(this.ppButton1);
            this.ppGroupBox1.Location = new System.Drawing.Point(20, 24);
            this.ppGroupBox1.Name = "ppGroupBox1";
            this.ppGroupBox1.Radius = 10;
            this.ppGroupBox1.Size = new System.Drawing.Size(422, 348);
            this.ppGroupBox1.TabIndex = 0;
            this.ppGroupBox1.TabStop = false;
            this.ppGroupBox1.Text = "按钮";
            this.ppGroupBox1.TextAlignment = System.Drawing.StringAlignment.Center;
            this.ppGroupBox1.TextDrawMode = PPSkin.PPGroupBox.DrawMode.Clear;
            this.ppGroupBox1.TilteOffset = 0;
            this.ppGroupBox1.TitleBaseColor = System.Drawing.Color.White;
            this.ppGroupBox1.TitleBorderColor = System.Drawing.Color.Gray;
            this.ppGroupBox1.TitleRadius = 10;
            // 
            // ppButton26
            // 
            this.ppButton26.BackColor = System.Drawing.Color.White;
            this.ppButton26.BaseColor = System.Drawing.Color.Red;
            this.ppButton26.DialogResult = System.Windows.Forms.DialogResult.None;
            this.ppButton26.IcoDown = null;
            this.ppButton26.IcoIn = null;
            this.ppButton26.IcoRegular = null;
            this.ppButton26.IcoSize = new System.Drawing.Size(15, 15);
            this.ppButton26.LineColor = System.Drawing.Color.Red;
            this.ppButton26.LineWidth = 1;
            this.ppButton26.Location = new System.Drawing.Point(333, 74);
            this.ppButton26.MouseDownBaseColor = System.Drawing.Color.LightGray;
            this.ppButton26.MouseDownLineColor = System.Drawing.Color.Red;
            this.ppButton26.MouseDownTextColor = System.Drawing.Color.Red;
            this.ppButton26.MouseInBaseColor = System.Drawing.Color.DodgerBlue;
            this.ppButton26.MouseInLineColor = System.Drawing.Color.DodgerBlue;
            this.ppButton26.MouseInTextColor = System.Drawing.Color.Black;
            this.ppButton26.Name = "ppButton26";
            this.ppButton26.Radius = 10;
            this.ppButton26.RegularBaseColor = System.Drawing.Color.Red;
            this.ppButton26.RegularLineColor = System.Drawing.Color.Red;
            this.ppButton26.RegularTextColor = System.Drawing.Color.Black;
            this.ppButton26.RoundStyle = PPSkin.Enums.RoundStyle.TopRight;
            this.ppButton26.Size = new System.Drawing.Size(75, 30);
            this.ppButton26.TabIndex = 34;
            this.ppButton26.Text = "按钮";
            this.ppButton26.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.ppButton26.TextColor = System.Drawing.Color.Black;
            this.ppButton26.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.ppButton26.TipAutoCloseTime = 5000;
            this.ppButton26.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppButton26.TipCloseOnLeave = true;
            this.ppButton26.TipDeviation = new System.Drawing.Size(0, 0);
            this.ppButton26.TipFontSize = 10;
            this.ppButton26.TipFontText = null;
            this.ppButton26.TipForeColor = System.Drawing.Color.White;
            this.ppButton26.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.ppButton26.TipText = null;
            this.ppButton26.TipTopMoust = true;
            this.ppButton26.UseSpecial = true;
            this.ppButton26.UseVisualStyleBackColor = false;
            this.ppButton26.Xoffset = 0;
            this.ppButton26.XoffsetIco = 0;
            this.ppButton26.Yoffset = 0;
            this.ppButton26.YoffsetIco = 0;
            // 
            // ppButton27
            // 
            this.ppButton27.BackColor = System.Drawing.Color.White;
            this.ppButton27.BaseColor = System.Drawing.Color.DodgerBlue;
            this.ppButton27.DialogResult = System.Windows.Forms.DialogResult.None;
            this.ppButton27.IcoDown = null;
            this.ppButton27.IcoIn = null;
            this.ppButton27.IcoRegular = null;
            this.ppButton27.IcoSize = new System.Drawing.Size(15, 15);
            this.ppButton27.LineColor = System.Drawing.Color.DodgerBlue;
            this.ppButton27.LineWidth = 1;
            this.ppButton27.Location = new System.Drawing.Point(252, 74);
            this.ppButton27.MouseDownBaseColor = System.Drawing.Color.LightGray;
            this.ppButton27.MouseDownLineColor = System.Drawing.Color.Silver;
            this.ppButton27.MouseDownTextColor = System.Drawing.Color.Black;
            this.ppButton27.MouseInBaseColor = System.Drawing.Color.Red;
            this.ppButton27.MouseInLineColor = System.Drawing.Color.Silver;
            this.ppButton27.MouseInTextColor = System.Drawing.Color.Black;
            this.ppButton27.Name = "ppButton27";
            this.ppButton27.Radius = 10;
            this.ppButton27.RegularBaseColor = System.Drawing.Color.DodgerBlue;
            this.ppButton27.RegularLineColor = System.Drawing.Color.DodgerBlue;
            this.ppButton27.RegularTextColor = System.Drawing.Color.Black;
            this.ppButton27.RoundStyle = PPSkin.Enums.RoundStyle.BottomLeft;
            this.ppButton27.Size = new System.Drawing.Size(75, 30);
            this.ppButton27.TabIndex = 33;
            this.ppButton27.Text = "按钮";
            this.ppButton27.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.ppButton27.TextColor = System.Drawing.Color.Black;
            this.ppButton27.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.ppButton27.TipAutoCloseTime = 5000;
            this.ppButton27.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppButton27.TipCloseOnLeave = true;
            this.ppButton27.TipDeviation = new System.Drawing.Size(0, 0);
            this.ppButton27.TipFontSize = 10;
            this.ppButton27.TipFontText = null;
            this.ppButton27.TipForeColor = System.Drawing.Color.White;
            this.ppButton27.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.ppButton27.TipText = null;
            this.ppButton27.TipTopMoust = true;
            this.ppButton27.UseSpecial = true;
            this.ppButton27.UseVisualStyleBackColor = false;
            this.ppButton27.Xoffset = 0;
            this.ppButton27.XoffsetIco = 0;
            this.ppButton27.Yoffset = 0;
            this.ppButton27.YoffsetIco = 0;
            // 
            // ppButton28
            // 
            this.ppButton28.BackColor = System.Drawing.Color.White;
            this.ppButton28.BaseColor = System.Drawing.Color.White;
            this.ppButton28.DialogResult = System.Windows.Forms.DialogResult.None;
            this.ppButton28.IcoDown = null;
            this.ppButton28.IcoIn = null;
            this.ppButton28.IcoRegular = null;
            this.ppButton28.IcoSize = new System.Drawing.Size(15, 15);
            this.ppButton28.LineColor = System.Drawing.Color.DodgerBlue;
            this.ppButton28.LineWidth = 1;
            this.ppButton28.Location = new System.Drawing.Point(171, 74);
            this.ppButton28.MouseDownBaseColor = System.Drawing.Color.White;
            this.ppButton28.MouseDownLineColor = System.Drawing.Color.Cyan;
            this.ppButton28.MouseDownTextColor = System.Drawing.Color.Black;
            this.ppButton28.MouseInBaseColor = System.Drawing.Color.LightGray;
            this.ppButton28.MouseInLineColor = System.Drawing.Color.Red;
            this.ppButton28.MouseInTextColor = System.Drawing.Color.Red;
            this.ppButton28.Name = "ppButton28";
            this.ppButton28.Radius = 10;
            this.ppButton28.RegularBaseColor = System.Drawing.Color.White;
            this.ppButton28.RegularLineColor = System.Drawing.Color.DodgerBlue;
            this.ppButton28.RegularTextColor = System.Drawing.Color.DodgerBlue;
            this.ppButton28.RoundStyle = PPSkin.Enums.RoundStyle.Right;
            this.ppButton28.Size = new System.Drawing.Size(75, 30);
            this.ppButton28.TabIndex = 32;
            this.ppButton28.Text = "按钮";
            this.ppButton28.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.ppButton28.TextColor = System.Drawing.Color.DodgerBlue;
            this.ppButton28.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.ppButton28.TipAutoCloseTime = 5000;
            this.ppButton28.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppButton28.TipCloseOnLeave = true;
            this.ppButton28.TipDeviation = new System.Drawing.Size(0, 0);
            this.ppButton28.TipFontSize = 10;
            this.ppButton28.TipFontText = null;
            this.ppButton28.TipForeColor = System.Drawing.Color.White;
            this.ppButton28.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.ppButton28.TipText = null;
            this.ppButton28.TipTopMoust = true;
            this.ppButton28.UseSpecial = true;
            this.ppButton28.UseVisualStyleBackColor = false;
            this.ppButton28.Xoffset = 0;
            this.ppButton28.XoffsetIco = 0;
            this.ppButton28.Yoffset = 0;
            this.ppButton28.YoffsetIco = 0;
            // 
            // ppButton29
            // 
            this.ppButton29.BackColor = System.Drawing.Color.White;
            this.ppButton29.BaseColor = System.Drawing.Color.White;
            this.ppButton29.DialogResult = System.Windows.Forms.DialogResult.None;
            this.ppButton29.IcoDown = null;
            this.ppButton29.IcoIn = null;
            this.ppButton29.IcoRegular = null;
            this.ppButton29.IcoSize = new System.Drawing.Size(15, 15);
            this.ppButton29.LineColor = System.Drawing.Color.Red;
            this.ppButton29.LineWidth = 1;
            this.ppButton29.Location = new System.Drawing.Point(90, 74);
            this.ppButton29.MouseDownBaseColor = System.Drawing.Color.White;
            this.ppButton29.MouseDownLineColor = System.Drawing.Color.Silver;
            this.ppButton29.MouseDownTextColor = System.Drawing.Color.Black;
            this.ppButton29.MouseInBaseColor = System.Drawing.Color.LightGray;
            this.ppButton29.MouseInLineColor = System.Drawing.Color.Silver;
            this.ppButton29.MouseInTextColor = System.Drawing.Color.Black;
            this.ppButton29.Name = "ppButton29";
            this.ppButton29.Radius = 10;
            this.ppButton29.RegularBaseColor = System.Drawing.Color.White;
            this.ppButton29.RegularLineColor = System.Drawing.Color.Red;
            this.ppButton29.RegularTextColor = System.Drawing.Color.Black;
            this.ppButton29.RoundStyle = PPSkin.Enums.RoundStyle.Left;
            this.ppButton29.Size = new System.Drawing.Size(75, 30);
            this.ppButton29.TabIndex = 31;
            this.ppButton29.Text = "按钮";
            this.ppButton29.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.ppButton29.TextColor = System.Drawing.Color.Black;
            this.ppButton29.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.ppButton29.TipAutoCloseTime = 5000;
            this.ppButton29.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppButton29.TipCloseOnLeave = true;
            this.ppButton29.TipDeviation = new System.Drawing.Size(0, 0);
            this.ppButton29.TipFontSize = 10;
            this.ppButton29.TipFontText = null;
            this.ppButton29.TipForeColor = System.Drawing.Color.White;
            this.ppButton29.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.ppButton29.TipText = null;
            this.ppButton29.TipTopMoust = true;
            this.ppButton29.UseSpecial = true;
            this.ppButton29.UseVisualStyleBackColor = false;
            this.ppButton29.Xoffset = 0;
            this.ppButton29.XoffsetIco = 0;
            this.ppButton29.Yoffset = 0;
            this.ppButton29.YoffsetIco = 0;
            // 
            // ppButton30
            // 
            this.ppButton30.BackColor = System.Drawing.Color.White;
            this.ppButton30.BaseColor = System.Drawing.Color.White;
            this.ppButton30.DialogResult = System.Windows.Forms.DialogResult.None;
            this.ppButton30.IcoDown = null;
            this.ppButton30.IcoIn = null;
            this.ppButton30.IcoRegular = null;
            this.ppButton30.IcoSize = new System.Drawing.Size(15, 15);
            this.ppButton30.LineColor = System.Drawing.Color.LightGray;
            this.ppButton30.LineWidth = 1;
            this.ppButton30.Location = new System.Drawing.Point(9, 74);
            this.ppButton30.MouseDownBaseColor = System.Drawing.Color.LightGray;
            this.ppButton30.MouseDownLineColor = System.Drawing.Color.Silver;
            this.ppButton30.MouseDownTextColor = System.Drawing.Color.Black;
            this.ppButton30.MouseInBaseColor = System.Drawing.Color.LightGray;
            this.ppButton30.MouseInLineColor = System.Drawing.Color.Silver;
            this.ppButton30.MouseInTextColor = System.Drawing.Color.Black;
            this.ppButton30.Name = "ppButton30";
            this.ppButton30.Radius = 10;
            this.ppButton30.RegularBaseColor = System.Drawing.Color.White;
            this.ppButton30.RegularLineColor = System.Drawing.Color.LightGray;
            this.ppButton30.RegularTextColor = System.Drawing.Color.Black;
            this.ppButton30.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppButton30.Size = new System.Drawing.Size(75, 30);
            this.ppButton30.TabIndex = 30;
            this.ppButton30.Text = "按钮";
            this.ppButton30.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.ppButton30.TextColor = System.Drawing.Color.Black;
            this.ppButton30.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.ppButton30.TipAutoCloseTime = 5000;
            this.ppButton30.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppButton30.TipCloseOnLeave = true;
            this.ppButton30.TipDeviation = new System.Drawing.Size(0, 0);
            this.ppButton30.TipFontSize = 10;
            this.ppButton30.TipFontText = null;
            this.ppButton30.TipForeColor = System.Drawing.Color.White;
            this.ppButton30.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.ppButton30.TipText = null;
            this.ppButton30.TipTopMoust = true;
            this.ppButton30.UseSpecial = true;
            this.ppButton30.UseVisualStyleBackColor = false;
            this.ppButton30.Xoffset = 0;
            this.ppButton30.XoffsetIco = 0;
            this.ppButton30.Yoffset = 0;
            this.ppButton30.YoffsetIco = 0;
            // 
            // ppButton21
            // 
            this.ppButton21.BackColor = System.Drawing.Color.White;
            this.ppButton21.BaseColor = System.Drawing.Color.Red;
            this.ppButton21.DialogResult = System.Windows.Forms.DialogResult.None;
            this.ppButton21.IcoDown = null;
            this.ppButton21.IcoIn = null;
            this.ppButton21.IcoRegular = null;
            this.ppButton21.IcoSize = new System.Drawing.Size(15, 15);
            this.ppButton21.LineColor = System.Drawing.Color.Red;
            this.ppButton21.LineWidth = 1;
            this.ppButton21.Location = new System.Drawing.Point(333, 263);
            this.ppButton21.MouseDownBaseColor = System.Drawing.Color.LightGray;
            this.ppButton21.MouseDownLineColor = System.Drawing.Color.Red;
            this.ppButton21.MouseDownTextColor = System.Drawing.Color.Red;
            this.ppButton21.MouseInBaseColor = System.Drawing.Color.DodgerBlue;
            this.ppButton21.MouseInLineColor = System.Drawing.Color.DodgerBlue;
            this.ppButton21.MouseInTextColor = System.Drawing.Color.Black;
            this.ppButton21.Name = "ppButton21";
            this.ppButton21.Radius = 72;
            this.ppButton21.RegularBaseColor = System.Drawing.Color.Red;
            this.ppButton21.RegularLineColor = System.Drawing.Color.Red;
            this.ppButton21.RegularTextColor = System.Drawing.Color.Black;
            this.ppButton21.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppButton21.Size = new System.Drawing.Size(75, 75);
            this.ppButton21.TabIndex = 29;
            this.ppButton21.Text = "按钮";
            this.ppButton21.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.ppButton21.TextColor = System.Drawing.Color.Black;
            this.ppButton21.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.ppButton21.TipAutoCloseTime = 5000;
            this.ppButton21.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppButton21.TipCloseOnLeave = true;
            this.ppButton21.TipDeviation = new System.Drawing.Size(0, 0);
            this.ppButton21.TipFontSize = 10;
            this.ppButton21.TipFontText = null;
            this.ppButton21.TipForeColor = System.Drawing.Color.White;
            this.ppButton21.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.ppButton21.TipText = null;
            this.ppButton21.TipTopMoust = true;
            this.ppButton21.UseSpecial = true;
            this.ppButton21.UseVisualStyleBackColor = false;
            this.ppButton21.Xoffset = 0;
            this.ppButton21.XoffsetIco = 0;
            this.ppButton21.Yoffset = 0;
            this.ppButton21.YoffsetIco = 0;
            // 
            // ppButton22
            // 
            this.ppButton22.BackColor = System.Drawing.Color.White;
            this.ppButton22.BaseColor = System.Drawing.Color.DodgerBlue;
            this.ppButton22.DialogResult = System.Windows.Forms.DialogResult.None;
            this.ppButton22.IcoDown = null;
            this.ppButton22.IcoIn = null;
            this.ppButton22.IcoRegular = null;
            this.ppButton22.IcoSize = new System.Drawing.Size(15, 15);
            this.ppButton22.LineColor = System.Drawing.Color.DodgerBlue;
            this.ppButton22.LineWidth = 1;
            this.ppButton22.Location = new System.Drawing.Point(252, 263);
            this.ppButton22.MouseDownBaseColor = System.Drawing.Color.LightGray;
            this.ppButton22.MouseDownLineColor = System.Drawing.Color.Silver;
            this.ppButton22.MouseDownTextColor = System.Drawing.Color.Black;
            this.ppButton22.MouseInBaseColor = System.Drawing.Color.Red;
            this.ppButton22.MouseInLineColor = System.Drawing.Color.Silver;
            this.ppButton22.MouseInTextColor = System.Drawing.Color.Black;
            this.ppButton22.Name = "ppButton22";
            this.ppButton22.Radius = 72;
            this.ppButton22.RegularBaseColor = System.Drawing.Color.DodgerBlue;
            this.ppButton22.RegularLineColor = System.Drawing.Color.DodgerBlue;
            this.ppButton22.RegularTextColor = System.Drawing.Color.Black;
            this.ppButton22.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppButton22.Size = new System.Drawing.Size(75, 75);
            this.ppButton22.TabIndex = 28;
            this.ppButton22.Text = "按钮";
            this.ppButton22.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.ppButton22.TextColor = System.Drawing.Color.Black;
            this.ppButton22.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.ppButton22.TipAutoCloseTime = 5000;
            this.ppButton22.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppButton22.TipCloseOnLeave = true;
            this.ppButton22.TipDeviation = new System.Drawing.Size(0, 0);
            this.ppButton22.TipFontSize = 10;
            this.ppButton22.TipFontText = null;
            this.ppButton22.TipForeColor = System.Drawing.Color.White;
            this.ppButton22.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.ppButton22.TipText = null;
            this.ppButton22.TipTopMoust = true;
            this.ppButton22.UseSpecial = true;
            this.ppButton22.UseVisualStyleBackColor = false;
            this.ppButton22.Xoffset = 0;
            this.ppButton22.XoffsetIco = 0;
            this.ppButton22.Yoffset = 0;
            this.ppButton22.YoffsetIco = 0;
            // 
            // ppButton23
            // 
            this.ppButton23.BackColor = System.Drawing.Color.White;
            this.ppButton23.BaseColor = System.Drawing.Color.White;
            this.ppButton23.DialogResult = System.Windows.Forms.DialogResult.None;
            this.ppButton23.IcoDown = null;
            this.ppButton23.IcoIn = null;
            this.ppButton23.IcoRegular = null;
            this.ppButton23.IcoSize = new System.Drawing.Size(15, 15);
            this.ppButton23.LineColor = System.Drawing.Color.DodgerBlue;
            this.ppButton23.LineWidth = 1;
            this.ppButton23.Location = new System.Drawing.Point(171, 263);
            this.ppButton23.MouseDownBaseColor = System.Drawing.Color.White;
            this.ppButton23.MouseDownLineColor = System.Drawing.Color.Cyan;
            this.ppButton23.MouseDownTextColor = System.Drawing.Color.Black;
            this.ppButton23.MouseInBaseColor = System.Drawing.Color.LightGray;
            this.ppButton23.MouseInLineColor = System.Drawing.Color.Red;
            this.ppButton23.MouseInTextColor = System.Drawing.Color.Red;
            this.ppButton23.Name = "ppButton23";
            this.ppButton23.Radius = 72;
            this.ppButton23.RegularBaseColor = System.Drawing.Color.White;
            this.ppButton23.RegularLineColor = System.Drawing.Color.DodgerBlue;
            this.ppButton23.RegularTextColor = System.Drawing.Color.DodgerBlue;
            this.ppButton23.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppButton23.Size = new System.Drawing.Size(75, 75);
            this.ppButton23.TabIndex = 27;
            this.ppButton23.Text = "按钮";
            this.ppButton23.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.ppButton23.TextColor = System.Drawing.Color.DodgerBlue;
            this.ppButton23.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.ppButton23.TipAutoCloseTime = 5000;
            this.ppButton23.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppButton23.TipCloseOnLeave = true;
            this.ppButton23.TipDeviation = new System.Drawing.Size(0, 0);
            this.ppButton23.TipFontSize = 10;
            this.ppButton23.TipFontText = null;
            this.ppButton23.TipForeColor = System.Drawing.Color.White;
            this.ppButton23.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.ppButton23.TipText = null;
            this.ppButton23.TipTopMoust = true;
            this.ppButton23.UseSpecial = true;
            this.ppButton23.UseVisualStyleBackColor = false;
            this.ppButton23.Xoffset = 0;
            this.ppButton23.XoffsetIco = 0;
            this.ppButton23.Yoffset = 0;
            this.ppButton23.YoffsetIco = 0;
            // 
            // ppButton24
            // 
            this.ppButton24.BackColor = System.Drawing.Color.White;
            this.ppButton24.BaseColor = System.Drawing.Color.White;
            this.ppButton24.DialogResult = System.Windows.Forms.DialogResult.None;
            this.ppButton24.IcoDown = null;
            this.ppButton24.IcoIn = null;
            this.ppButton24.IcoRegular = null;
            this.ppButton24.IcoSize = new System.Drawing.Size(15, 15);
            this.ppButton24.LineColor = System.Drawing.Color.Red;
            this.ppButton24.LineWidth = 1;
            this.ppButton24.Location = new System.Drawing.Point(90, 263);
            this.ppButton24.MouseDownBaseColor = System.Drawing.Color.White;
            this.ppButton24.MouseDownLineColor = System.Drawing.Color.Silver;
            this.ppButton24.MouseDownTextColor = System.Drawing.Color.Black;
            this.ppButton24.MouseInBaseColor = System.Drawing.Color.LightGray;
            this.ppButton24.MouseInLineColor = System.Drawing.Color.Silver;
            this.ppButton24.MouseInTextColor = System.Drawing.Color.Black;
            this.ppButton24.Name = "ppButton24";
            this.ppButton24.Radius = 72;
            this.ppButton24.RegularBaseColor = System.Drawing.Color.White;
            this.ppButton24.RegularLineColor = System.Drawing.Color.Red;
            this.ppButton24.RegularTextColor = System.Drawing.Color.Black;
            this.ppButton24.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppButton24.Size = new System.Drawing.Size(75, 75);
            this.ppButton24.TabIndex = 26;
            this.ppButton24.Text = "按钮";
            this.ppButton24.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.ppButton24.TextColor = System.Drawing.Color.Black;
            this.ppButton24.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.ppButton24.TipAutoCloseTime = 5000;
            this.ppButton24.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppButton24.TipCloseOnLeave = true;
            this.ppButton24.TipDeviation = new System.Drawing.Size(0, 0);
            this.ppButton24.TipFontSize = 10;
            this.ppButton24.TipFontText = null;
            this.ppButton24.TipForeColor = System.Drawing.Color.White;
            this.ppButton24.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.ppButton24.TipText = null;
            this.ppButton24.TipTopMoust = true;
            this.ppButton24.UseSpecial = true;
            this.ppButton24.UseVisualStyleBackColor = false;
            this.ppButton24.Xoffset = 0;
            this.ppButton24.XoffsetIco = 0;
            this.ppButton24.Yoffset = 0;
            this.ppButton24.YoffsetIco = 0;
            // 
            // ppButton25
            // 
            this.ppButton25.BackColor = System.Drawing.Color.White;
            this.ppButton25.BaseColor = System.Drawing.Color.White;
            this.ppButton25.DialogResult = System.Windows.Forms.DialogResult.None;
            this.ppButton25.IcoDown = null;
            this.ppButton25.IcoIn = null;
            this.ppButton25.IcoRegular = null;
            this.ppButton25.IcoSize = new System.Drawing.Size(15, 15);
            this.ppButton25.LineColor = System.Drawing.Color.LightGray;
            this.ppButton25.LineWidth = 1;
            this.ppButton25.Location = new System.Drawing.Point(9, 263);
            this.ppButton25.MouseDownBaseColor = System.Drawing.Color.LightGray;
            this.ppButton25.MouseDownLineColor = System.Drawing.Color.Silver;
            this.ppButton25.MouseDownTextColor = System.Drawing.Color.Black;
            this.ppButton25.MouseInBaseColor = System.Drawing.Color.LightGray;
            this.ppButton25.MouseInLineColor = System.Drawing.Color.Silver;
            this.ppButton25.MouseInTextColor = System.Drawing.Color.Black;
            this.ppButton25.Name = "ppButton25";
            this.ppButton25.Radius = 72;
            this.ppButton25.RegularBaseColor = System.Drawing.Color.White;
            this.ppButton25.RegularLineColor = System.Drawing.Color.LightGray;
            this.ppButton25.RegularTextColor = System.Drawing.Color.Black;
            this.ppButton25.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppButton25.Size = new System.Drawing.Size(75, 75);
            this.ppButton25.TabIndex = 25;
            this.ppButton25.Text = "按钮";
            this.ppButton25.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.ppButton25.TextColor = System.Drawing.Color.Black;
            this.ppButton25.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.ppButton25.TipAutoCloseTime = 5000;
            this.ppButton25.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppButton25.TipCloseOnLeave = true;
            this.ppButton25.TipDeviation = new System.Drawing.Size(0, 0);
            this.ppButton25.TipFontSize = 10;
            this.ppButton25.TipFontText = null;
            this.ppButton25.TipForeColor = System.Drawing.Color.White;
            this.ppButton25.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.ppButton25.TipText = null;
            this.ppButton25.TipTopMoust = true;
            this.ppButton25.UseSpecial = true;
            this.ppButton25.UseVisualStyleBackColor = false;
            this.ppButton25.Xoffset = 0;
            this.ppButton25.XoffsetIco = 0;
            this.ppButton25.Yoffset = 0;
            this.ppButton25.YoffsetIco = 0;
            // 
            // ppButton16
            // 
            this.ppButton16.BackColor = System.Drawing.Color.White;
            this.ppButton16.BaseColor = System.Drawing.Color.Red;
            this.ppButton16.DialogResult = System.Windows.Forms.DialogResult.None;
            this.ppButton16.IcoDown = null;
            this.ppButton16.IcoIn = null;
            this.ppButton16.IcoRegular = null;
            this.ppButton16.IcoSize = new System.Drawing.Size(15, 15);
            this.ppButton16.LineColor = System.Drawing.Color.Red;
            this.ppButton16.LineWidth = 1;
            this.ppButton16.Location = new System.Drawing.Point(333, 182);
            this.ppButton16.MouseDownBaseColor = System.Drawing.Color.LightGray;
            this.ppButton16.MouseDownLineColor = System.Drawing.Color.Red;
            this.ppButton16.MouseDownTextColor = System.Drawing.Color.Red;
            this.ppButton16.MouseInBaseColor = System.Drawing.Color.DodgerBlue;
            this.ppButton16.MouseInLineColor = System.Drawing.Color.DodgerBlue;
            this.ppButton16.MouseInTextColor = System.Drawing.Color.Black;
            this.ppButton16.Name = "ppButton16";
            this.ppButton16.Radius = 10;
            this.ppButton16.RegularBaseColor = System.Drawing.Color.Red;
            this.ppButton16.RegularLineColor = System.Drawing.Color.Red;
            this.ppButton16.RegularTextColor = System.Drawing.Color.Black;
            this.ppButton16.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppButton16.Size = new System.Drawing.Size(75, 75);
            this.ppButton16.TabIndex = 24;
            this.ppButton16.Text = "按钮";
            this.ppButton16.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.ppButton16.TextColor = System.Drawing.Color.Black;
            this.ppButton16.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.ppButton16.TipAutoCloseTime = 5000;
            this.ppButton16.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppButton16.TipCloseOnLeave = true;
            this.ppButton16.TipDeviation = new System.Drawing.Size(0, 0);
            this.ppButton16.TipFontSize = 10;
            this.ppButton16.TipFontText = null;
            this.ppButton16.TipForeColor = System.Drawing.Color.White;
            this.ppButton16.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.ppButton16.TipText = null;
            this.ppButton16.TipTopMoust = true;
            this.ppButton16.UseSpecial = true;
            this.ppButton16.UseVisualStyleBackColor = false;
            this.ppButton16.Xoffset = 0;
            this.ppButton16.XoffsetIco = 0;
            this.ppButton16.Yoffset = 0;
            this.ppButton16.YoffsetIco = 0;
            // 
            // ppButton17
            // 
            this.ppButton17.BackColor = System.Drawing.Color.White;
            this.ppButton17.BaseColor = System.Drawing.Color.DodgerBlue;
            this.ppButton17.DialogResult = System.Windows.Forms.DialogResult.None;
            this.ppButton17.IcoDown = null;
            this.ppButton17.IcoIn = null;
            this.ppButton17.IcoRegular = null;
            this.ppButton17.IcoSize = new System.Drawing.Size(15, 15);
            this.ppButton17.LineColor = System.Drawing.Color.DodgerBlue;
            this.ppButton17.LineWidth = 1;
            this.ppButton17.Location = new System.Drawing.Point(252, 182);
            this.ppButton17.MouseDownBaseColor = System.Drawing.Color.LightGray;
            this.ppButton17.MouseDownLineColor = System.Drawing.Color.Silver;
            this.ppButton17.MouseDownTextColor = System.Drawing.Color.Black;
            this.ppButton17.MouseInBaseColor = System.Drawing.Color.Red;
            this.ppButton17.MouseInLineColor = System.Drawing.Color.Silver;
            this.ppButton17.MouseInTextColor = System.Drawing.Color.Black;
            this.ppButton17.Name = "ppButton17";
            this.ppButton17.Radius = 10;
            this.ppButton17.RegularBaseColor = System.Drawing.Color.DodgerBlue;
            this.ppButton17.RegularLineColor = System.Drawing.Color.DodgerBlue;
            this.ppButton17.RegularTextColor = System.Drawing.Color.Black;
            this.ppButton17.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppButton17.Size = new System.Drawing.Size(75, 75);
            this.ppButton17.TabIndex = 23;
            this.ppButton17.Text = "按钮";
            this.ppButton17.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.ppButton17.TextColor = System.Drawing.Color.Black;
            this.ppButton17.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.ppButton17.TipAutoCloseTime = 5000;
            this.ppButton17.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppButton17.TipCloseOnLeave = true;
            this.ppButton17.TipDeviation = new System.Drawing.Size(0, 0);
            this.ppButton17.TipFontSize = 10;
            this.ppButton17.TipFontText = null;
            this.ppButton17.TipForeColor = System.Drawing.Color.White;
            this.ppButton17.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.ppButton17.TipText = null;
            this.ppButton17.TipTopMoust = true;
            this.ppButton17.UseSpecial = true;
            this.ppButton17.UseVisualStyleBackColor = false;
            this.ppButton17.Xoffset = 0;
            this.ppButton17.XoffsetIco = 0;
            this.ppButton17.Yoffset = 0;
            this.ppButton17.YoffsetIco = 0;
            // 
            // ppButton18
            // 
            this.ppButton18.BackColor = System.Drawing.Color.White;
            this.ppButton18.BaseColor = System.Drawing.Color.White;
            this.ppButton18.DialogResult = System.Windows.Forms.DialogResult.None;
            this.ppButton18.IcoDown = null;
            this.ppButton18.IcoIn = null;
            this.ppButton18.IcoRegular = null;
            this.ppButton18.IcoSize = new System.Drawing.Size(15, 15);
            this.ppButton18.LineColor = System.Drawing.Color.DodgerBlue;
            this.ppButton18.LineWidth = 1;
            this.ppButton18.Location = new System.Drawing.Point(171, 182);
            this.ppButton18.MouseDownBaseColor = System.Drawing.Color.White;
            this.ppButton18.MouseDownLineColor = System.Drawing.Color.Cyan;
            this.ppButton18.MouseDownTextColor = System.Drawing.Color.Black;
            this.ppButton18.MouseInBaseColor = System.Drawing.Color.LightGray;
            this.ppButton18.MouseInLineColor = System.Drawing.Color.Red;
            this.ppButton18.MouseInTextColor = System.Drawing.Color.Red;
            this.ppButton18.Name = "ppButton18";
            this.ppButton18.Radius = 10;
            this.ppButton18.RegularBaseColor = System.Drawing.Color.White;
            this.ppButton18.RegularLineColor = System.Drawing.Color.DodgerBlue;
            this.ppButton18.RegularTextColor = System.Drawing.Color.DodgerBlue;
            this.ppButton18.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppButton18.Size = new System.Drawing.Size(75, 75);
            this.ppButton18.TabIndex = 22;
            this.ppButton18.Text = "按钮";
            this.ppButton18.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.ppButton18.TextColor = System.Drawing.Color.DodgerBlue;
            this.ppButton18.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.ppButton18.TipAutoCloseTime = 5000;
            this.ppButton18.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppButton18.TipCloseOnLeave = true;
            this.ppButton18.TipDeviation = new System.Drawing.Size(0, 0);
            this.ppButton18.TipFontSize = 10;
            this.ppButton18.TipFontText = null;
            this.ppButton18.TipForeColor = System.Drawing.Color.White;
            this.ppButton18.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.ppButton18.TipText = null;
            this.ppButton18.TipTopMoust = true;
            this.ppButton18.UseSpecial = true;
            this.ppButton18.UseVisualStyleBackColor = false;
            this.ppButton18.Xoffset = 0;
            this.ppButton18.XoffsetIco = 0;
            this.ppButton18.Yoffset = 0;
            this.ppButton18.YoffsetIco = 0;
            // 
            // ppButton19
            // 
            this.ppButton19.BackColor = System.Drawing.Color.White;
            this.ppButton19.BaseColor = System.Drawing.Color.White;
            this.ppButton19.DialogResult = System.Windows.Forms.DialogResult.None;
            this.ppButton19.IcoDown = null;
            this.ppButton19.IcoIn = null;
            this.ppButton19.IcoRegular = null;
            this.ppButton19.IcoSize = new System.Drawing.Size(15, 15);
            this.ppButton19.LineColor = System.Drawing.Color.Red;
            this.ppButton19.LineWidth = 1;
            this.ppButton19.Location = new System.Drawing.Point(90, 182);
            this.ppButton19.MouseDownBaseColor = System.Drawing.Color.White;
            this.ppButton19.MouseDownLineColor = System.Drawing.Color.Silver;
            this.ppButton19.MouseDownTextColor = System.Drawing.Color.Black;
            this.ppButton19.MouseInBaseColor = System.Drawing.Color.LightGray;
            this.ppButton19.MouseInLineColor = System.Drawing.Color.Silver;
            this.ppButton19.MouseInTextColor = System.Drawing.Color.Black;
            this.ppButton19.Name = "ppButton19";
            this.ppButton19.Radius = 10;
            this.ppButton19.RegularBaseColor = System.Drawing.Color.White;
            this.ppButton19.RegularLineColor = System.Drawing.Color.Red;
            this.ppButton19.RegularTextColor = System.Drawing.Color.Black;
            this.ppButton19.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppButton19.Size = new System.Drawing.Size(75, 75);
            this.ppButton19.TabIndex = 21;
            this.ppButton19.Text = "按钮";
            this.ppButton19.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.ppButton19.TextColor = System.Drawing.Color.Black;
            this.ppButton19.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.ppButton19.TipAutoCloseTime = 5000;
            this.ppButton19.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppButton19.TipCloseOnLeave = true;
            this.ppButton19.TipDeviation = new System.Drawing.Size(0, 0);
            this.ppButton19.TipFontSize = 10;
            this.ppButton19.TipFontText = null;
            this.ppButton19.TipForeColor = System.Drawing.Color.White;
            this.ppButton19.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.ppButton19.TipText = null;
            this.ppButton19.TipTopMoust = true;
            this.ppButton19.UseSpecial = true;
            this.ppButton19.UseVisualStyleBackColor = false;
            this.ppButton19.Xoffset = 0;
            this.ppButton19.XoffsetIco = 0;
            this.ppButton19.Yoffset = 0;
            this.ppButton19.YoffsetIco = 0;
            // 
            // ppButton20
            // 
            this.ppButton20.BackColor = System.Drawing.Color.White;
            this.ppButton20.BaseColor = System.Drawing.Color.White;
            this.ppButton20.DialogResult = System.Windows.Forms.DialogResult.None;
            this.ppButton20.IcoDown = null;
            this.ppButton20.IcoIn = null;
            this.ppButton20.IcoRegular = null;
            this.ppButton20.IcoSize = new System.Drawing.Size(15, 15);
            this.ppButton20.LineColor = System.Drawing.Color.LightGray;
            this.ppButton20.LineWidth = 1;
            this.ppButton20.Location = new System.Drawing.Point(9, 182);
            this.ppButton20.MouseDownBaseColor = System.Drawing.Color.LightGray;
            this.ppButton20.MouseDownLineColor = System.Drawing.Color.Silver;
            this.ppButton20.MouseDownTextColor = System.Drawing.Color.Black;
            this.ppButton20.MouseInBaseColor = System.Drawing.Color.LightGray;
            this.ppButton20.MouseInLineColor = System.Drawing.Color.Silver;
            this.ppButton20.MouseInTextColor = System.Drawing.Color.Black;
            this.ppButton20.Name = "ppButton20";
            this.ppButton20.Radius = 10;
            this.ppButton20.RegularBaseColor = System.Drawing.Color.White;
            this.ppButton20.RegularLineColor = System.Drawing.Color.LightGray;
            this.ppButton20.RegularTextColor = System.Drawing.Color.Black;
            this.ppButton20.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppButton20.Size = new System.Drawing.Size(75, 75);
            this.ppButton20.TabIndex = 20;
            this.ppButton20.Text = "按钮";
            this.ppButton20.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.ppButton20.TextColor = System.Drawing.Color.Black;
            this.ppButton20.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.ppButton20.TipAutoCloseTime = 5000;
            this.ppButton20.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppButton20.TipCloseOnLeave = true;
            this.ppButton20.TipDeviation = new System.Drawing.Size(0, 0);
            this.ppButton20.TipFontSize = 10;
            this.ppButton20.TipFontText = null;
            this.ppButton20.TipForeColor = System.Drawing.Color.White;
            this.ppButton20.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.ppButton20.TipText = null;
            this.ppButton20.TipTopMoust = true;
            this.ppButton20.UseSpecial = true;
            this.ppButton20.UseVisualStyleBackColor = false;
            this.ppButton20.Xoffset = 0;
            this.ppButton20.XoffsetIco = 0;
            this.ppButton20.Yoffset = 0;
            this.ppButton20.YoffsetIco = 0;
            // 
            // ppButton11
            // 
            this.ppButton11.BackColor = System.Drawing.Color.White;
            this.ppButton11.BaseColor = System.Drawing.Color.Red;
            this.ppButton11.DialogResult = System.Windows.Forms.DialogResult.None;
            this.ppButton11.IcoDown = null;
            this.ppButton11.IcoIn = null;
            this.ppButton11.IcoRegular = null;
            this.ppButton11.IcoSize = new System.Drawing.Size(15, 15);
            this.ppButton11.LineColor = System.Drawing.Color.Red;
            this.ppButton11.LineWidth = 1;
            this.ppButton11.Location = new System.Drawing.Point(333, 146);
            this.ppButton11.MouseDownBaseColor = System.Drawing.Color.LightGray;
            this.ppButton11.MouseDownLineColor = System.Drawing.Color.Red;
            this.ppButton11.MouseDownTextColor = System.Drawing.Color.Red;
            this.ppButton11.MouseInBaseColor = System.Drawing.Color.DodgerBlue;
            this.ppButton11.MouseInLineColor = System.Drawing.Color.DodgerBlue;
            this.ppButton11.MouseInTextColor = System.Drawing.Color.Black;
            this.ppButton11.Name = "ppButton11";
            this.ppButton11.Radius = 25;
            this.ppButton11.RegularBaseColor = System.Drawing.Color.Red;
            this.ppButton11.RegularLineColor = System.Drawing.Color.Red;
            this.ppButton11.RegularTextColor = System.Drawing.Color.Black;
            this.ppButton11.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppButton11.Size = new System.Drawing.Size(75, 30);
            this.ppButton11.TabIndex = 19;
            this.ppButton11.Text = "按钮";
            this.ppButton11.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.ppButton11.TextColor = System.Drawing.Color.Black;
            this.ppButton11.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.ppButton11.TipAutoCloseTime = 5000;
            this.ppButton11.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppButton11.TipCloseOnLeave = true;
            this.ppButton11.TipDeviation = new System.Drawing.Size(0, 0);
            this.ppButton11.TipFontSize = 10;
            this.ppButton11.TipFontText = null;
            this.ppButton11.TipForeColor = System.Drawing.Color.White;
            this.ppButton11.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.ppButton11.TipText = null;
            this.ppButton11.TipTopMoust = true;
            this.ppButton11.UseSpecial = true;
            this.ppButton11.UseVisualStyleBackColor = false;
            this.ppButton11.Xoffset = 0;
            this.ppButton11.XoffsetIco = 0;
            this.ppButton11.Yoffset = 0;
            this.ppButton11.YoffsetIco = 0;
            // 
            // ppButton12
            // 
            this.ppButton12.BackColor = System.Drawing.Color.White;
            this.ppButton12.BaseColor = System.Drawing.Color.DodgerBlue;
            this.ppButton12.DialogResult = System.Windows.Forms.DialogResult.None;
            this.ppButton12.IcoDown = null;
            this.ppButton12.IcoIn = null;
            this.ppButton12.IcoRegular = null;
            this.ppButton12.IcoSize = new System.Drawing.Size(15, 15);
            this.ppButton12.LineColor = System.Drawing.Color.DodgerBlue;
            this.ppButton12.LineWidth = 1;
            this.ppButton12.Location = new System.Drawing.Point(252, 146);
            this.ppButton12.MouseDownBaseColor = System.Drawing.Color.LightGray;
            this.ppButton12.MouseDownLineColor = System.Drawing.Color.Silver;
            this.ppButton12.MouseDownTextColor = System.Drawing.Color.Black;
            this.ppButton12.MouseInBaseColor = System.Drawing.Color.Red;
            this.ppButton12.MouseInLineColor = System.Drawing.Color.Silver;
            this.ppButton12.MouseInTextColor = System.Drawing.Color.Black;
            this.ppButton12.Name = "ppButton12";
            this.ppButton12.Radius = 25;
            this.ppButton12.RegularBaseColor = System.Drawing.Color.DodgerBlue;
            this.ppButton12.RegularLineColor = System.Drawing.Color.DodgerBlue;
            this.ppButton12.RegularTextColor = System.Drawing.Color.Black;
            this.ppButton12.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppButton12.Size = new System.Drawing.Size(75, 30);
            this.ppButton12.TabIndex = 18;
            this.ppButton12.Text = "按钮";
            this.ppButton12.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.ppButton12.TextColor = System.Drawing.Color.Black;
            this.ppButton12.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.ppButton12.TipAutoCloseTime = 5000;
            this.ppButton12.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppButton12.TipCloseOnLeave = true;
            this.ppButton12.TipDeviation = new System.Drawing.Size(0, 0);
            this.ppButton12.TipFontSize = 10;
            this.ppButton12.TipFontText = null;
            this.ppButton12.TipForeColor = System.Drawing.Color.White;
            this.ppButton12.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.ppButton12.TipText = null;
            this.ppButton12.TipTopMoust = true;
            this.ppButton12.UseSpecial = true;
            this.ppButton12.UseVisualStyleBackColor = false;
            this.ppButton12.Xoffset = 0;
            this.ppButton12.XoffsetIco = 0;
            this.ppButton12.Yoffset = 0;
            this.ppButton12.YoffsetIco = 0;
            // 
            // ppButton13
            // 
            this.ppButton13.BackColor = System.Drawing.Color.White;
            this.ppButton13.BaseColor = System.Drawing.Color.White;
            this.ppButton13.DialogResult = System.Windows.Forms.DialogResult.None;
            this.ppButton13.IcoDown = null;
            this.ppButton13.IcoIn = null;
            this.ppButton13.IcoRegular = null;
            this.ppButton13.IcoSize = new System.Drawing.Size(15, 15);
            this.ppButton13.LineColor = System.Drawing.Color.DodgerBlue;
            this.ppButton13.LineWidth = 1;
            this.ppButton13.Location = new System.Drawing.Point(171, 146);
            this.ppButton13.MouseDownBaseColor = System.Drawing.Color.White;
            this.ppButton13.MouseDownLineColor = System.Drawing.Color.Cyan;
            this.ppButton13.MouseDownTextColor = System.Drawing.Color.Black;
            this.ppButton13.MouseInBaseColor = System.Drawing.Color.LightGray;
            this.ppButton13.MouseInLineColor = System.Drawing.Color.Red;
            this.ppButton13.MouseInTextColor = System.Drawing.Color.Red;
            this.ppButton13.Name = "ppButton13";
            this.ppButton13.Radius = 25;
            this.ppButton13.RegularBaseColor = System.Drawing.Color.White;
            this.ppButton13.RegularLineColor = System.Drawing.Color.DodgerBlue;
            this.ppButton13.RegularTextColor = System.Drawing.Color.DodgerBlue;
            this.ppButton13.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppButton13.Size = new System.Drawing.Size(75, 30);
            this.ppButton13.TabIndex = 17;
            this.ppButton13.Text = "按钮";
            this.ppButton13.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.ppButton13.TextColor = System.Drawing.Color.DodgerBlue;
            this.ppButton13.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.ppButton13.TipAutoCloseTime = 5000;
            this.ppButton13.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppButton13.TipCloseOnLeave = true;
            this.ppButton13.TipDeviation = new System.Drawing.Size(0, 0);
            this.ppButton13.TipFontSize = 10;
            this.ppButton13.TipFontText = null;
            this.ppButton13.TipForeColor = System.Drawing.Color.White;
            this.ppButton13.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.ppButton13.TipText = null;
            this.ppButton13.TipTopMoust = true;
            this.ppButton13.UseSpecial = true;
            this.ppButton13.UseVisualStyleBackColor = false;
            this.ppButton13.Xoffset = 0;
            this.ppButton13.XoffsetIco = 0;
            this.ppButton13.Yoffset = 0;
            this.ppButton13.YoffsetIco = 0;
            // 
            // ppButton14
            // 
            this.ppButton14.BackColor = System.Drawing.Color.White;
            this.ppButton14.BaseColor = System.Drawing.Color.White;
            this.ppButton14.DialogResult = System.Windows.Forms.DialogResult.None;
            this.ppButton14.IcoDown = null;
            this.ppButton14.IcoIn = null;
            this.ppButton14.IcoRegular = null;
            this.ppButton14.IcoSize = new System.Drawing.Size(15, 15);
            this.ppButton14.LineColor = System.Drawing.Color.Red;
            this.ppButton14.LineWidth = 1;
            this.ppButton14.Location = new System.Drawing.Point(90, 146);
            this.ppButton14.MouseDownBaseColor = System.Drawing.Color.White;
            this.ppButton14.MouseDownLineColor = System.Drawing.Color.Silver;
            this.ppButton14.MouseDownTextColor = System.Drawing.Color.Black;
            this.ppButton14.MouseInBaseColor = System.Drawing.Color.LightGray;
            this.ppButton14.MouseInLineColor = System.Drawing.Color.Silver;
            this.ppButton14.MouseInTextColor = System.Drawing.Color.Black;
            this.ppButton14.Name = "ppButton14";
            this.ppButton14.Radius = 25;
            this.ppButton14.RegularBaseColor = System.Drawing.Color.White;
            this.ppButton14.RegularLineColor = System.Drawing.Color.Red;
            this.ppButton14.RegularTextColor = System.Drawing.Color.Black;
            this.ppButton14.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppButton14.Size = new System.Drawing.Size(75, 30);
            this.ppButton14.TabIndex = 16;
            this.ppButton14.Text = "按钮";
            this.ppButton14.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.ppButton14.TextColor = System.Drawing.Color.Black;
            this.ppButton14.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.ppButton14.TipAutoCloseTime = 5000;
            this.ppButton14.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppButton14.TipCloseOnLeave = true;
            this.ppButton14.TipDeviation = new System.Drawing.Size(0, 0);
            this.ppButton14.TipFontSize = 10;
            this.ppButton14.TipFontText = null;
            this.ppButton14.TipForeColor = System.Drawing.Color.White;
            this.ppButton14.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.ppButton14.TipText = null;
            this.ppButton14.TipTopMoust = true;
            this.ppButton14.UseSpecial = true;
            this.ppButton14.UseVisualStyleBackColor = false;
            this.ppButton14.Xoffset = 0;
            this.ppButton14.XoffsetIco = 0;
            this.ppButton14.Yoffset = 0;
            this.ppButton14.YoffsetIco = 0;
            // 
            // ppButton15
            // 
            this.ppButton15.BackColor = System.Drawing.Color.White;
            this.ppButton15.BaseColor = System.Drawing.Color.White;
            this.ppButton15.DialogResult = System.Windows.Forms.DialogResult.None;
            this.ppButton15.IcoDown = null;
            this.ppButton15.IcoIn = null;
            this.ppButton15.IcoRegular = null;
            this.ppButton15.IcoSize = new System.Drawing.Size(15, 15);
            this.ppButton15.LineColor = System.Drawing.Color.LightGray;
            this.ppButton15.LineWidth = 1;
            this.ppButton15.Location = new System.Drawing.Point(9, 146);
            this.ppButton15.MouseDownBaseColor = System.Drawing.Color.LightGray;
            this.ppButton15.MouseDownLineColor = System.Drawing.Color.Silver;
            this.ppButton15.MouseDownTextColor = System.Drawing.Color.Black;
            this.ppButton15.MouseInBaseColor = System.Drawing.Color.LightGray;
            this.ppButton15.MouseInLineColor = System.Drawing.Color.Silver;
            this.ppButton15.MouseInTextColor = System.Drawing.Color.Black;
            this.ppButton15.Name = "ppButton15";
            this.ppButton15.Radius = 25;
            this.ppButton15.RegularBaseColor = System.Drawing.Color.White;
            this.ppButton15.RegularLineColor = System.Drawing.Color.LightGray;
            this.ppButton15.RegularTextColor = System.Drawing.Color.Black;
            this.ppButton15.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppButton15.Size = new System.Drawing.Size(75, 30);
            this.ppButton15.TabIndex = 15;
            this.ppButton15.Text = "按钮";
            this.ppButton15.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.ppButton15.TextColor = System.Drawing.Color.Black;
            this.ppButton15.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.ppButton15.TipAutoCloseTime = 5000;
            this.ppButton15.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppButton15.TipCloseOnLeave = true;
            this.ppButton15.TipDeviation = new System.Drawing.Size(0, 0);
            this.ppButton15.TipFontSize = 10;
            this.ppButton15.TipFontText = null;
            this.ppButton15.TipForeColor = System.Drawing.Color.White;
            this.ppButton15.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.ppButton15.TipText = null;
            this.ppButton15.TipTopMoust = true;
            this.ppButton15.UseSpecial = true;
            this.ppButton15.UseVisualStyleBackColor = false;
            this.ppButton15.Xoffset = 0;
            this.ppButton15.XoffsetIco = 0;
            this.ppButton15.Yoffset = 0;
            this.ppButton15.YoffsetIco = 0;
            // 
            // ppButton6
            // 
            this.ppButton6.BackColor = System.Drawing.Color.White;
            this.ppButton6.BaseColor = System.Drawing.Color.Red;
            this.ppButton6.DialogResult = System.Windows.Forms.DialogResult.None;
            this.ppButton6.IcoDown = null;
            this.ppButton6.IcoIn = null;
            this.ppButton6.IcoRegular = null;
            this.ppButton6.IcoSize = new System.Drawing.Size(15, 15);
            this.ppButton6.LineColor = System.Drawing.Color.Red;
            this.ppButton6.LineWidth = 1;
            this.ppButton6.Location = new System.Drawing.Point(333, 110);
            this.ppButton6.MouseDownBaseColor = System.Drawing.Color.LightGray;
            this.ppButton6.MouseDownLineColor = System.Drawing.Color.Red;
            this.ppButton6.MouseDownTextColor = System.Drawing.Color.Red;
            this.ppButton6.MouseInBaseColor = System.Drawing.Color.DodgerBlue;
            this.ppButton6.MouseInLineColor = System.Drawing.Color.DodgerBlue;
            this.ppButton6.MouseInTextColor = System.Drawing.Color.Black;
            this.ppButton6.Name = "ppButton6";
            this.ppButton6.Radius = 0;
            this.ppButton6.RegularBaseColor = System.Drawing.Color.Red;
            this.ppButton6.RegularLineColor = System.Drawing.Color.Red;
            this.ppButton6.RegularTextColor = System.Drawing.Color.Black;
            this.ppButton6.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppButton6.Size = new System.Drawing.Size(75, 30);
            this.ppButton6.TabIndex = 14;
            this.ppButton6.Text = "按钮";
            this.ppButton6.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.ppButton6.TextColor = System.Drawing.Color.Black;
            this.ppButton6.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.ppButton6.TipAutoCloseTime = 5000;
            this.ppButton6.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppButton6.TipCloseOnLeave = true;
            this.ppButton6.TipDeviation = new System.Drawing.Size(0, 0);
            this.ppButton6.TipFontSize = 10;
            this.ppButton6.TipFontText = null;
            this.ppButton6.TipForeColor = System.Drawing.Color.White;
            this.ppButton6.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.ppButton6.TipText = null;
            this.ppButton6.TipTopMoust = true;
            this.ppButton6.UseSpecial = true;
            this.ppButton6.UseVisualStyleBackColor = false;
            this.ppButton6.Xoffset = 0;
            this.ppButton6.XoffsetIco = 0;
            this.ppButton6.Yoffset = 0;
            this.ppButton6.YoffsetIco = 0;
            // 
            // ppButton7
            // 
            this.ppButton7.BackColor = System.Drawing.Color.White;
            this.ppButton7.BaseColor = System.Drawing.Color.DodgerBlue;
            this.ppButton7.DialogResult = System.Windows.Forms.DialogResult.None;
            this.ppButton7.IcoDown = null;
            this.ppButton7.IcoIn = null;
            this.ppButton7.IcoRegular = null;
            this.ppButton7.IcoSize = new System.Drawing.Size(15, 15);
            this.ppButton7.LineColor = System.Drawing.Color.DodgerBlue;
            this.ppButton7.LineWidth = 1;
            this.ppButton7.Location = new System.Drawing.Point(252, 110);
            this.ppButton7.MouseDownBaseColor = System.Drawing.Color.LightGray;
            this.ppButton7.MouseDownLineColor = System.Drawing.Color.Silver;
            this.ppButton7.MouseDownTextColor = System.Drawing.Color.Black;
            this.ppButton7.MouseInBaseColor = System.Drawing.Color.Red;
            this.ppButton7.MouseInLineColor = System.Drawing.Color.Silver;
            this.ppButton7.MouseInTextColor = System.Drawing.Color.Black;
            this.ppButton7.Name = "ppButton7";
            this.ppButton7.Radius = 0;
            this.ppButton7.RegularBaseColor = System.Drawing.Color.DodgerBlue;
            this.ppButton7.RegularLineColor = System.Drawing.Color.DodgerBlue;
            this.ppButton7.RegularTextColor = System.Drawing.Color.Black;
            this.ppButton7.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppButton7.Size = new System.Drawing.Size(75, 30);
            this.ppButton7.TabIndex = 13;
            this.ppButton7.Text = "按钮";
            this.ppButton7.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.ppButton7.TextColor = System.Drawing.Color.Black;
            this.ppButton7.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.ppButton7.TipAutoCloseTime = 5000;
            this.ppButton7.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppButton7.TipCloseOnLeave = true;
            this.ppButton7.TipDeviation = new System.Drawing.Size(0, 0);
            this.ppButton7.TipFontSize = 10;
            this.ppButton7.TipFontText = null;
            this.ppButton7.TipForeColor = System.Drawing.Color.White;
            this.ppButton7.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.ppButton7.TipText = null;
            this.ppButton7.TipTopMoust = true;
            this.ppButton7.UseSpecial = true;
            this.ppButton7.UseVisualStyleBackColor = false;
            this.ppButton7.Xoffset = 0;
            this.ppButton7.XoffsetIco = 0;
            this.ppButton7.Yoffset = 0;
            this.ppButton7.YoffsetIco = 0;
            // 
            // ppButton8
            // 
            this.ppButton8.BackColor = System.Drawing.Color.White;
            this.ppButton8.BaseColor = System.Drawing.Color.White;
            this.ppButton8.DialogResult = System.Windows.Forms.DialogResult.None;
            this.ppButton8.IcoDown = null;
            this.ppButton8.IcoIn = null;
            this.ppButton8.IcoRegular = null;
            this.ppButton8.IcoSize = new System.Drawing.Size(15, 15);
            this.ppButton8.LineColor = System.Drawing.Color.DodgerBlue;
            this.ppButton8.LineWidth = 1;
            this.ppButton8.Location = new System.Drawing.Point(171, 110);
            this.ppButton8.MouseDownBaseColor = System.Drawing.Color.White;
            this.ppButton8.MouseDownLineColor = System.Drawing.Color.Cyan;
            this.ppButton8.MouseDownTextColor = System.Drawing.Color.Black;
            this.ppButton8.MouseInBaseColor = System.Drawing.Color.LightGray;
            this.ppButton8.MouseInLineColor = System.Drawing.Color.Red;
            this.ppButton8.MouseInTextColor = System.Drawing.Color.Red;
            this.ppButton8.Name = "ppButton8";
            this.ppButton8.Radius = 0;
            this.ppButton8.RegularBaseColor = System.Drawing.Color.White;
            this.ppButton8.RegularLineColor = System.Drawing.Color.DodgerBlue;
            this.ppButton8.RegularTextColor = System.Drawing.Color.DodgerBlue;
            this.ppButton8.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppButton8.Size = new System.Drawing.Size(75, 30);
            this.ppButton8.TabIndex = 12;
            this.ppButton8.Text = "按钮";
            this.ppButton8.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.ppButton8.TextColor = System.Drawing.Color.DodgerBlue;
            this.ppButton8.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.ppButton8.TipAutoCloseTime = 5000;
            this.ppButton8.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppButton8.TipCloseOnLeave = true;
            this.ppButton8.TipDeviation = new System.Drawing.Size(0, 0);
            this.ppButton8.TipFontSize = 10;
            this.ppButton8.TipFontText = null;
            this.ppButton8.TipForeColor = System.Drawing.Color.White;
            this.ppButton8.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.ppButton8.TipText = null;
            this.ppButton8.TipTopMoust = true;
            this.ppButton8.UseSpecial = true;
            this.ppButton8.UseVisualStyleBackColor = false;
            this.ppButton8.Xoffset = 0;
            this.ppButton8.XoffsetIco = 0;
            this.ppButton8.Yoffset = 0;
            this.ppButton8.YoffsetIco = 0;
            // 
            // ppButton9
            // 
            this.ppButton9.BackColor = System.Drawing.Color.White;
            this.ppButton9.BaseColor = System.Drawing.Color.White;
            this.ppButton9.DialogResult = System.Windows.Forms.DialogResult.None;
            this.ppButton9.IcoDown = null;
            this.ppButton9.IcoIn = null;
            this.ppButton9.IcoRegular = null;
            this.ppButton9.IcoSize = new System.Drawing.Size(15, 15);
            this.ppButton9.LineColor = System.Drawing.Color.Red;
            this.ppButton9.LineWidth = 1;
            this.ppButton9.Location = new System.Drawing.Point(90, 110);
            this.ppButton9.MouseDownBaseColor = System.Drawing.Color.White;
            this.ppButton9.MouseDownLineColor = System.Drawing.Color.Silver;
            this.ppButton9.MouseDownTextColor = System.Drawing.Color.Black;
            this.ppButton9.MouseInBaseColor = System.Drawing.Color.LightGray;
            this.ppButton9.MouseInLineColor = System.Drawing.Color.Silver;
            this.ppButton9.MouseInTextColor = System.Drawing.Color.Black;
            this.ppButton9.Name = "ppButton9";
            this.ppButton9.Radius = 0;
            this.ppButton9.RegularBaseColor = System.Drawing.Color.White;
            this.ppButton9.RegularLineColor = System.Drawing.Color.Red;
            this.ppButton9.RegularTextColor = System.Drawing.Color.Black;
            this.ppButton9.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppButton9.Size = new System.Drawing.Size(75, 30);
            this.ppButton9.TabIndex = 11;
            this.ppButton9.Text = "按钮";
            this.ppButton9.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.ppButton9.TextColor = System.Drawing.Color.Black;
            this.ppButton9.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.ppButton9.TipAutoCloseTime = 5000;
            this.ppButton9.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppButton9.TipCloseOnLeave = true;
            this.ppButton9.TipDeviation = new System.Drawing.Size(0, 0);
            this.ppButton9.TipFontSize = 10;
            this.ppButton9.TipFontText = null;
            this.ppButton9.TipForeColor = System.Drawing.Color.White;
            this.ppButton9.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.ppButton9.TipText = null;
            this.ppButton9.TipTopMoust = true;
            this.ppButton9.UseSpecial = true;
            this.ppButton9.UseVisualStyleBackColor = false;
            this.ppButton9.Xoffset = 0;
            this.ppButton9.XoffsetIco = 0;
            this.ppButton9.Yoffset = 0;
            this.ppButton9.YoffsetIco = 0;
            // 
            // ppButton10
            // 
            this.ppButton10.BackColor = System.Drawing.Color.White;
            this.ppButton10.BaseColor = System.Drawing.Color.White;
            this.ppButton10.DialogResult = System.Windows.Forms.DialogResult.None;
            this.ppButton10.IcoDown = null;
            this.ppButton10.IcoIn = null;
            this.ppButton10.IcoRegular = null;
            this.ppButton10.IcoSize = new System.Drawing.Size(15, 15);
            this.ppButton10.LineColor = System.Drawing.Color.LightGray;
            this.ppButton10.LineWidth = 1;
            this.ppButton10.Location = new System.Drawing.Point(9, 110);
            this.ppButton10.MouseDownBaseColor = System.Drawing.Color.LightGray;
            this.ppButton10.MouseDownLineColor = System.Drawing.Color.Silver;
            this.ppButton10.MouseDownTextColor = System.Drawing.Color.Black;
            this.ppButton10.MouseInBaseColor = System.Drawing.Color.LightGray;
            this.ppButton10.MouseInLineColor = System.Drawing.Color.Silver;
            this.ppButton10.MouseInTextColor = System.Drawing.Color.Black;
            this.ppButton10.Name = "ppButton10";
            this.ppButton10.Radius = 0;
            this.ppButton10.RegularBaseColor = System.Drawing.Color.White;
            this.ppButton10.RegularLineColor = System.Drawing.Color.LightGray;
            this.ppButton10.RegularTextColor = System.Drawing.Color.Black;
            this.ppButton10.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppButton10.Size = new System.Drawing.Size(75, 30);
            this.ppButton10.TabIndex = 10;
            this.ppButton10.Text = "按钮";
            this.ppButton10.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.ppButton10.TextColor = System.Drawing.Color.Black;
            this.ppButton10.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.ppButton10.TipAutoCloseTime = 5000;
            this.ppButton10.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppButton10.TipCloseOnLeave = true;
            this.ppButton10.TipDeviation = new System.Drawing.Size(0, 0);
            this.ppButton10.TipFontSize = 10;
            this.ppButton10.TipFontText = null;
            this.ppButton10.TipForeColor = System.Drawing.Color.White;
            this.ppButton10.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.ppButton10.TipText = null;
            this.ppButton10.TipTopMoust = true;
            this.ppButton10.UseSpecial = true;
            this.ppButton10.UseVisualStyleBackColor = false;
            this.ppButton10.Xoffset = 0;
            this.ppButton10.XoffsetIco = 0;
            this.ppButton10.Yoffset = 0;
            this.ppButton10.YoffsetIco = 0;
            // 
            // ppButton5
            // 
            this.ppButton5.BackColor = System.Drawing.Color.White;
            this.ppButton5.BaseColor = System.Drawing.Color.Red;
            this.ppButton5.DialogResult = System.Windows.Forms.DialogResult.None;
            this.ppButton5.IcoDown = null;
            this.ppButton5.IcoIn = null;
            this.ppButton5.IcoRegular = null;
            this.ppButton5.IcoSize = new System.Drawing.Size(15, 15);
            this.ppButton5.LineColor = System.Drawing.Color.Red;
            this.ppButton5.LineWidth = 1;
            this.ppButton5.Location = new System.Drawing.Point(333, 38);
            this.ppButton5.MouseDownBaseColor = System.Drawing.Color.LightGray;
            this.ppButton5.MouseDownLineColor = System.Drawing.Color.Red;
            this.ppButton5.MouseDownTextColor = System.Drawing.Color.Red;
            this.ppButton5.MouseInBaseColor = System.Drawing.Color.DodgerBlue;
            this.ppButton5.MouseInLineColor = System.Drawing.Color.DodgerBlue;
            this.ppButton5.MouseInTextColor = System.Drawing.Color.Black;
            this.ppButton5.Name = "ppButton5";
            this.ppButton5.Radius = 10;
            this.ppButton5.RegularBaseColor = System.Drawing.Color.Red;
            this.ppButton5.RegularLineColor = System.Drawing.Color.Red;
            this.ppButton5.RegularTextColor = System.Drawing.Color.Black;
            this.ppButton5.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppButton5.Size = new System.Drawing.Size(75, 30);
            this.ppButton5.TabIndex = 9;
            this.ppButton5.Text = "按钮";
            this.ppButton5.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.ppButton5.TextColor = System.Drawing.Color.Black;
            this.ppButton5.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.ppButton5.TipAutoCloseTime = 5000;
            this.ppButton5.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppButton5.TipCloseOnLeave = true;
            this.ppButton5.TipDeviation = new System.Drawing.Size(0, 0);
            this.ppButton5.TipFontSize = 10;
            this.ppButton5.TipFontText = null;
            this.ppButton5.TipForeColor = System.Drawing.Color.White;
            this.ppButton5.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.ppButton5.TipText = null;
            this.ppButton5.TipTopMoust = true;
            this.ppButton5.UseSpecial = true;
            this.ppButton5.UseVisualStyleBackColor = false;
            this.ppButton5.Xoffset = 0;
            this.ppButton5.XoffsetIco = 0;
            this.ppButton5.Yoffset = 0;
            this.ppButton5.YoffsetIco = 0;
            // 
            // ppButton4
            // 
            this.ppButton4.BackColor = System.Drawing.Color.White;
            this.ppButton4.BaseColor = System.Drawing.Color.DodgerBlue;
            this.ppButton4.DialogResult = System.Windows.Forms.DialogResult.None;
            this.ppButton4.IcoDown = null;
            this.ppButton4.IcoIn = null;
            this.ppButton4.IcoRegular = null;
            this.ppButton4.IcoSize = new System.Drawing.Size(15, 15);
            this.ppButton4.LineColor = System.Drawing.Color.DodgerBlue;
            this.ppButton4.LineWidth = 1;
            this.ppButton4.Location = new System.Drawing.Point(252, 38);
            this.ppButton4.MouseDownBaseColor = System.Drawing.Color.LightGray;
            this.ppButton4.MouseDownLineColor = System.Drawing.Color.Silver;
            this.ppButton4.MouseDownTextColor = System.Drawing.Color.Black;
            this.ppButton4.MouseInBaseColor = System.Drawing.Color.Red;
            this.ppButton4.MouseInLineColor = System.Drawing.Color.Silver;
            this.ppButton4.MouseInTextColor = System.Drawing.Color.Black;
            this.ppButton4.Name = "ppButton4";
            this.ppButton4.Radius = 10;
            this.ppButton4.RegularBaseColor = System.Drawing.Color.DodgerBlue;
            this.ppButton4.RegularLineColor = System.Drawing.Color.DodgerBlue;
            this.ppButton4.RegularTextColor = System.Drawing.Color.Black;
            this.ppButton4.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppButton4.Size = new System.Drawing.Size(75, 30);
            this.ppButton4.TabIndex = 8;
            this.ppButton4.Text = "按钮";
            this.ppButton4.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.ppButton4.TextColor = System.Drawing.Color.Black;
            this.ppButton4.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.ppButton4.TipAutoCloseTime = 5000;
            this.ppButton4.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppButton4.TipCloseOnLeave = true;
            this.ppButton4.TipDeviation = new System.Drawing.Size(0, 0);
            this.ppButton4.TipFontSize = 10;
            this.ppButton4.TipFontText = null;
            this.ppButton4.TipForeColor = System.Drawing.Color.White;
            this.ppButton4.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.ppButton4.TipText = null;
            this.ppButton4.TipTopMoust = true;
            this.ppButton4.UseSpecial = true;
            this.ppButton4.UseVisualStyleBackColor = false;
            this.ppButton4.Xoffset = 0;
            this.ppButton4.XoffsetIco = 0;
            this.ppButton4.Yoffset = 0;
            this.ppButton4.YoffsetIco = 0;
            // 
            // ppButton3
            // 
            this.ppButton3.BackColor = System.Drawing.Color.White;
            this.ppButton3.BaseColor = System.Drawing.Color.White;
            this.ppButton3.DialogResult = System.Windows.Forms.DialogResult.None;
            this.ppButton3.IcoDown = null;
            this.ppButton3.IcoIn = null;
            this.ppButton3.IcoRegular = null;
            this.ppButton3.IcoSize = new System.Drawing.Size(15, 15);
            this.ppButton3.LineColor = System.Drawing.Color.DodgerBlue;
            this.ppButton3.LineWidth = 1;
            this.ppButton3.Location = new System.Drawing.Point(171, 38);
            this.ppButton3.MouseDownBaseColor = System.Drawing.Color.White;
            this.ppButton3.MouseDownLineColor = System.Drawing.Color.Cyan;
            this.ppButton3.MouseDownTextColor = System.Drawing.Color.Black;
            this.ppButton3.MouseInBaseColor = System.Drawing.Color.LightGray;
            this.ppButton3.MouseInLineColor = System.Drawing.Color.Red;
            this.ppButton3.MouseInTextColor = System.Drawing.Color.Red;
            this.ppButton3.Name = "ppButton3";
            this.ppButton3.Radius = 10;
            this.ppButton3.RegularBaseColor = System.Drawing.Color.White;
            this.ppButton3.RegularLineColor = System.Drawing.Color.DodgerBlue;
            this.ppButton3.RegularTextColor = System.Drawing.Color.DodgerBlue;
            this.ppButton3.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppButton3.Size = new System.Drawing.Size(75, 30);
            this.ppButton3.TabIndex = 7;
            this.ppButton3.Text = "按钮";
            this.ppButton3.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.ppButton3.TextColor = System.Drawing.Color.DodgerBlue;
            this.ppButton3.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.ppButton3.TipAutoCloseTime = 5000;
            this.ppButton3.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppButton3.TipCloseOnLeave = true;
            this.ppButton3.TipDeviation = new System.Drawing.Size(0, 0);
            this.ppButton3.TipFontSize = 10;
            this.ppButton3.TipFontText = null;
            this.ppButton3.TipForeColor = System.Drawing.Color.White;
            this.ppButton3.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.ppButton3.TipText = null;
            this.ppButton3.TipTopMoust = true;
            this.ppButton3.UseSpecial = true;
            this.ppButton3.UseVisualStyleBackColor = false;
            this.ppButton3.Xoffset = 0;
            this.ppButton3.XoffsetIco = 0;
            this.ppButton3.Yoffset = 0;
            this.ppButton3.YoffsetIco = 0;
            // 
            // ppButton2
            // 
            this.ppButton2.BackColor = System.Drawing.Color.White;
            this.ppButton2.BaseColor = System.Drawing.Color.White;
            this.ppButton2.DialogResult = System.Windows.Forms.DialogResult.None;
            this.ppButton2.IcoDown = null;
            this.ppButton2.IcoIn = null;
            this.ppButton2.IcoRegular = null;
            this.ppButton2.IcoSize = new System.Drawing.Size(15, 15);
            this.ppButton2.LineColor = System.Drawing.Color.Red;
            this.ppButton2.LineWidth = 1;
            this.ppButton2.Location = new System.Drawing.Point(90, 38);
            this.ppButton2.MouseDownBaseColor = System.Drawing.Color.White;
            this.ppButton2.MouseDownLineColor = System.Drawing.Color.Silver;
            this.ppButton2.MouseDownTextColor = System.Drawing.Color.Black;
            this.ppButton2.MouseInBaseColor = System.Drawing.Color.LightGray;
            this.ppButton2.MouseInLineColor = System.Drawing.Color.Silver;
            this.ppButton2.MouseInTextColor = System.Drawing.Color.Black;
            this.ppButton2.Name = "ppButton2";
            this.ppButton2.Radius = 10;
            this.ppButton2.RegularBaseColor = System.Drawing.Color.White;
            this.ppButton2.RegularLineColor = System.Drawing.Color.Red;
            this.ppButton2.RegularTextColor = System.Drawing.Color.Black;
            this.ppButton2.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppButton2.Size = new System.Drawing.Size(75, 30);
            this.ppButton2.TabIndex = 6;
            this.ppButton2.Text = "按钮";
            this.ppButton2.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.ppButton2.TextColor = System.Drawing.Color.Black;
            this.ppButton2.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.ppButton2.TipAutoCloseTime = 5000;
            this.ppButton2.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppButton2.TipCloseOnLeave = true;
            this.ppButton2.TipDeviation = new System.Drawing.Size(0, 0);
            this.ppButton2.TipFontSize = 10;
            this.ppButton2.TipFontText = null;
            this.ppButton2.TipForeColor = System.Drawing.Color.White;
            this.ppButton2.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.ppButton2.TipText = null;
            this.ppButton2.TipTopMoust = true;
            this.ppButton2.UseSpecial = true;
            this.ppButton2.UseVisualStyleBackColor = false;
            this.ppButton2.Xoffset = 0;
            this.ppButton2.XoffsetIco = 0;
            this.ppButton2.Yoffset = 0;
            this.ppButton2.YoffsetIco = 0;
            // 
            // ppButton1
            // 
            this.ppButton1.BackColor = System.Drawing.Color.White;
            this.ppButton1.BaseColor = System.Drawing.Color.White;
            this.ppButton1.DialogResult = System.Windows.Forms.DialogResult.None;
            this.ppButton1.IcoDown = null;
            this.ppButton1.IcoIn = null;
            this.ppButton1.IcoRegular = null;
            this.ppButton1.IcoSize = new System.Drawing.Size(15, 15);
            this.ppButton1.LineColor = System.Drawing.Color.LightGray;
            this.ppButton1.LineWidth = 1;
            this.ppButton1.Location = new System.Drawing.Point(9, 38);
            this.ppButton1.MouseDownBaseColor = System.Drawing.Color.LightGray;
            this.ppButton1.MouseDownLineColor = System.Drawing.Color.Silver;
            this.ppButton1.MouseDownTextColor = System.Drawing.Color.Black;
            this.ppButton1.MouseInBaseColor = System.Drawing.Color.LightGray;
            this.ppButton1.MouseInLineColor = System.Drawing.Color.Silver;
            this.ppButton1.MouseInTextColor = System.Drawing.Color.Black;
            this.ppButton1.Name = "ppButton1";
            this.ppButton1.Radius = 10;
            this.ppButton1.RegularBaseColor = System.Drawing.Color.White;
            this.ppButton1.RegularLineColor = System.Drawing.Color.LightGray;
            this.ppButton1.RegularTextColor = System.Drawing.Color.Black;
            this.ppButton1.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppButton1.Size = new System.Drawing.Size(75, 30);
            this.ppButton1.TabIndex = 5;
            this.ppButton1.Text = "按钮";
            this.ppButton1.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.ppButton1.TextColor = System.Drawing.Color.Black;
            this.ppButton1.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.ppButton1.TipAutoCloseTime = 5000;
            this.ppButton1.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppButton1.TipCloseOnLeave = true;
            this.ppButton1.TipDeviation = new System.Drawing.Size(0, 0);
            this.ppButton1.TipFontSize = 10;
            this.ppButton1.TipFontText = null;
            this.ppButton1.TipForeColor = System.Drawing.Color.White;
            this.ppButton1.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.ppButton1.TipText = null;
            this.ppButton1.TipTopMoust = true;
            this.ppButton1.UseSpecial = true;
            this.ppButton1.UseVisualStyleBackColor = false;
            this.ppButton1.Xoffset = 0;
            this.ppButton1.XoffsetIco = 0;
            this.ppButton1.Yoffset = 0;
            this.ppButton1.YoffsetIco = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.White;
            this.tabPage2.Controls.Add(this.ppDateTimePicker13);
            this.tabPage2.Controls.Add(this.ppDateTimePicker14);
            this.tabPage2.Controls.Add(this.ppDateTimePicker15);
            this.tabPage2.Controls.Add(this.ppDateTimePicker16);
            this.tabPage2.Controls.Add(this.ppDateTimePicker9);
            this.tabPage2.Controls.Add(this.ppDateTimePicker10);
            this.tabPage2.Controls.Add(this.ppDateTimePicker11);
            this.tabPage2.Controls.Add(this.ppDateTimePicker12);
            this.tabPage2.Controls.Add(this.ppDateTimePicker1);
            this.tabPage2.Controls.Add(this.ppDateTimePicker2);
            this.tabPage2.Controls.Add(this.ppDateTimePicker3);
            this.tabPage2.Controls.Add(this.ppDateTimePicker4);
            this.tabPage2.Controls.Add(this.ppDateTimePicker5);
            this.tabPage2.Controls.Add(this.ppDateTimePicker6);
            this.tabPage2.Controls.Add(this.ppDateTimePicker7);
            this.tabPage2.Controls.Add(this.ppDateTimePicker8);
            this.tabPage2.Controls.Add(this.ppCombobox4);
            this.tabPage2.Controls.Add(this.ppCombobox3);
            this.tabPage2.Controls.Add(this.ppCombobox2);
            this.tabPage2.Controls.Add(this.ppCombobox1);
            this.tabPage2.Controls.Add(this.ppComboboxEx4);
            this.tabPage2.Controls.Add(this.ppComboboxEx3);
            this.tabPage2.Controls.Add(this.ppComboboxEx2);
            this.tabPage2.Controls.Add(this.ppComboboxEx1);
            this.tabPage2.Location = new System.Drawing.Point(0, 30);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(928, 675);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "下拉框";
            // 
            // ppDateTimePicker13
            // 
            this.ppDateTimePicker13.BackColor = System.Drawing.Color.White;
            this.ppDateTimePicker13.BaseColor = System.Drawing.Color.Moccasin;
            this.ppDateTimePicker13.BorderColor = System.Drawing.Color.Gold;
            this.ppDateTimePicker13.BorderWidth = 1F;
            this.ppDateTimePicker13.ButtonImage = ((System.Drawing.Bitmap)(resources.GetObject("ppDateTimePicker13.ButtonImage")));
            this.ppDateTimePicker13.ButtonSize = 26;
            this.ppDateTimePicker13.CustomFormat = "yyyy年MM月dd日 HH:mm:ss";
            this.ppDateTimePicker13.DropbaseColor = System.Drawing.Color.White;
            this.ppDateTimePicker13.DropborderColor = System.Drawing.Color.LightGray;
            this.ppDateTimePicker13.DropformRadius = 5;
            this.ppDateTimePicker13.Language = PPSkin.PPDateTimeDropForm.Lang.EN;
            this.ppDateTimePicker13.Location = new System.Drawing.Point(676, 503);
            this.ppDateTimePicker13.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ppDateTimePicker13.MaxDate = new System.DateTime(9999, 12, 31, 23, 59, 59, 999);
            this.ppDateTimePicker13.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.ppDateTimePicker13.Name = "ppDateTimePicker13";
            this.ppDateTimePicker13.PickTime = true;
            this.ppDateTimePicker13.Radius = 5;
            this.ppDateTimePicker13.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppDateTimePicker13.ShowBorder = true;
            this.ppDateTimePicker13.Size = new System.Drawing.Size(186, 34);
            this.ppDateTimePicker13.TabIndex = 35;
            this.ppDateTimePicker13.Value = new System.DateTime(2021, 4, 1, 11, 35, 9, 237);
            // 
            // ppDateTimePicker14
            // 
            this.ppDateTimePicker14.BackColor = System.Drawing.Color.White;
            this.ppDateTimePicker14.BaseColor = System.Drawing.Color.White;
            this.ppDateTimePicker14.BorderColor = System.Drawing.Color.Red;
            this.ppDateTimePicker14.BorderWidth = 1F;
            this.ppDateTimePicker14.ButtonImage = ((System.Drawing.Bitmap)(resources.GetObject("ppDateTimePicker14.ButtonImage")));
            this.ppDateTimePicker14.ButtonSize = 26;
            this.ppDateTimePicker14.CustomFormat = "yyyy年MM月dd日 HH:mm:ss";
            this.ppDateTimePicker14.DropbaseColor = System.Drawing.Color.White;
            this.ppDateTimePicker14.DropborderColor = System.Drawing.Color.LightGray;
            this.ppDateTimePicker14.DropformRadius = 5;
            this.ppDateTimePicker14.Language = PPSkin.PPDateTimeDropForm.Lang.EN;
            this.ppDateTimePicker14.Location = new System.Drawing.Point(443, 503);
            this.ppDateTimePicker14.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ppDateTimePicker14.MaxDate = new System.DateTime(9999, 12, 31, 23, 59, 59, 999);
            this.ppDateTimePicker14.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.ppDateTimePicker14.Name = "ppDateTimePicker14";
            this.ppDateTimePicker14.PickTime = true;
            this.ppDateTimePicker14.Radius = 5;
            this.ppDateTimePicker14.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppDateTimePicker14.ShowBorder = true;
            this.ppDateTimePicker14.Size = new System.Drawing.Size(185, 34);
            this.ppDateTimePicker14.TabIndex = 34;
            this.ppDateTimePicker14.Value = new System.DateTime(2021, 4, 1, 11, 35, 9, 237);
            // 
            // ppDateTimePicker15
            // 
            this.ppDateTimePicker15.BackColor = System.Drawing.Color.White;
            this.ppDateTimePicker15.BaseColor = System.Drawing.Color.White;
            this.ppDateTimePicker15.BorderColor = System.Drawing.Color.LightGray;
            this.ppDateTimePicker15.BorderWidth = 1F;
            this.ppDateTimePicker15.ButtonImage = ((System.Drawing.Bitmap)(resources.GetObject("ppDateTimePicker15.ButtonImage")));
            this.ppDateTimePicker15.ButtonSize = 26;
            this.ppDateTimePicker15.CustomFormat = "yyyy年MM月dd日 HH:mm:ss";
            this.ppDateTimePicker15.DropbaseColor = System.Drawing.Color.White;
            this.ppDateTimePicker15.DropborderColor = System.Drawing.Color.LightGray;
            this.ppDateTimePicker15.DropformRadius = 5;
            this.ppDateTimePicker15.Language = PPSkin.PPDateTimeDropForm.Lang.EN;
            this.ppDateTimePicker15.Location = new System.Drawing.Point(229, 503);
            this.ppDateTimePicker15.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ppDateTimePicker15.MaxDate = new System.DateTime(9999, 12, 31, 23, 59, 59, 999);
            this.ppDateTimePicker15.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.ppDateTimePicker15.Name = "ppDateTimePicker15";
            this.ppDateTimePicker15.PickTime = true;
            this.ppDateTimePicker15.Radius = 30;
            this.ppDateTimePicker15.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppDateTimePicker15.ShowBorder = true;
            this.ppDateTimePicker15.Size = new System.Drawing.Size(210, 34);
            this.ppDateTimePicker15.TabIndex = 33;
            this.ppDateTimePicker15.Value = new System.DateTime(2021, 4, 1, 11, 35, 9, 237);
            // 
            // ppDateTimePicker16
            // 
            this.ppDateTimePicker16.BackColor = System.Drawing.Color.White;
            this.ppDateTimePicker16.BaseColor = System.Drawing.Color.White;
            this.ppDateTimePicker16.BorderColor = System.Drawing.Color.LightGray;
            this.ppDateTimePicker16.BorderWidth = 1F;
            this.ppDateTimePicker16.ButtonImage = ((System.Drawing.Bitmap)(resources.GetObject("ppDateTimePicker16.ButtonImage")));
            this.ppDateTimePicker16.ButtonSize = 26;
            this.ppDateTimePicker16.CustomFormat = "yyyy年MM月dd日 HH:mm:ss";
            this.ppDateTimePicker16.DropbaseColor = System.Drawing.Color.White;
            this.ppDateTimePicker16.DropborderColor = System.Drawing.Color.LightGray;
            this.ppDateTimePicker16.DropformRadius = 5;
            this.ppDateTimePicker16.Language = PPSkin.PPDateTimeDropForm.Lang.EN;
            this.ppDateTimePicker16.Location = new System.Drawing.Point(31, 503);
            this.ppDateTimePicker16.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ppDateTimePicker16.MaxDate = new System.DateTime(9999, 12, 31, 23, 59, 59, 999);
            this.ppDateTimePicker16.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.ppDateTimePicker16.Name = "ppDateTimePicker16";
            this.ppDateTimePicker16.PickTime = true;
            this.ppDateTimePicker16.Radius = 5;
            this.ppDateTimePicker16.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppDateTimePicker16.ShowBorder = true;
            this.ppDateTimePicker16.Size = new System.Drawing.Size(192, 34);
            this.ppDateTimePicker16.TabIndex = 32;
            this.ppDateTimePicker16.Value = new System.DateTime(2021, 4, 1, 11, 35, 9, 237);
            // 
            // ppDateTimePicker9
            // 
            this.ppDateTimePicker9.BackColor = System.Drawing.Color.White;
            this.ppDateTimePicker9.BaseColor = System.Drawing.Color.Moccasin;
            this.ppDateTimePicker9.BorderColor = System.Drawing.Color.Gold;
            this.ppDateTimePicker9.BorderWidth = 1F;
            this.ppDateTimePicker9.ButtonImage = ((System.Drawing.Bitmap)(resources.GetObject("ppDateTimePicker9.ButtonImage")));
            this.ppDateTimePicker9.ButtonSize = 26;
            this.ppDateTimePicker9.CustomFormat = "yyyy年MM月dd日";
            this.ppDateTimePicker9.DropbaseColor = System.Drawing.Color.White;
            this.ppDateTimePicker9.DropborderColor = System.Drawing.Color.LightGray;
            this.ppDateTimePicker9.DropformRadius = 5;
            this.ppDateTimePicker9.Language = PPSkin.PPDateTimeDropForm.Lang.EN;
            this.ppDateTimePicker9.Location = new System.Drawing.Point(676, 283);
            this.ppDateTimePicker9.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ppDateTimePicker9.MaxDate = new System.DateTime(9999, 12, 31, 23, 59, 59, 999);
            this.ppDateTimePicker9.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.ppDateTimePicker9.Name = "ppDateTimePicker9";
            this.ppDateTimePicker9.PickTime = false;
            this.ppDateTimePicker9.Radius = 5;
            this.ppDateTimePicker9.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppDateTimePicker9.ShowBorder = true;
            this.ppDateTimePicker9.Size = new System.Drawing.Size(167, 34);
            this.ppDateTimePicker9.TabIndex = 31;
            this.ppDateTimePicker9.Value = new System.DateTime(2021, 4, 1, 11, 35, 9, 237);
            // 
            // ppDateTimePicker10
            // 
            this.ppDateTimePicker10.BackColor = System.Drawing.Color.White;
            this.ppDateTimePicker10.BaseColor = System.Drawing.Color.White;
            this.ppDateTimePicker10.BorderColor = System.Drawing.Color.Red;
            this.ppDateTimePicker10.BorderWidth = 1F;
            this.ppDateTimePicker10.ButtonImage = ((System.Drawing.Bitmap)(resources.GetObject("ppDateTimePicker10.ButtonImage")));
            this.ppDateTimePicker10.ButtonSize = 26;
            this.ppDateTimePicker10.CustomFormat = "yyyy年MM月dd日";
            this.ppDateTimePicker10.DropbaseColor = System.Drawing.Color.White;
            this.ppDateTimePicker10.DropborderColor = System.Drawing.Color.LightGray;
            this.ppDateTimePicker10.DropformRadius = 5;
            this.ppDateTimePicker10.Language = PPSkin.PPDateTimeDropForm.Lang.EN;
            this.ppDateTimePicker10.Location = new System.Drawing.Point(443, 283);
            this.ppDateTimePicker10.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ppDateTimePicker10.MaxDate = new System.DateTime(9999, 12, 31, 23, 59, 59, 999);
            this.ppDateTimePicker10.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.ppDateTimePicker10.Name = "ppDateTimePicker10";
            this.ppDateTimePicker10.PickTime = false;
            this.ppDateTimePicker10.Radius = 5;
            this.ppDateTimePicker10.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppDateTimePicker10.ShowBorder = true;
            this.ppDateTimePicker10.Size = new System.Drawing.Size(167, 34);
            this.ppDateTimePicker10.TabIndex = 30;
            this.ppDateTimePicker10.Value = new System.DateTime(2021, 4, 1, 11, 35, 9, 237);
            // 
            // ppDateTimePicker11
            // 
            this.ppDateTimePicker11.BackColor = System.Drawing.Color.White;
            this.ppDateTimePicker11.BaseColor = System.Drawing.Color.White;
            this.ppDateTimePicker11.BorderColor = System.Drawing.Color.LightGray;
            this.ppDateTimePicker11.BorderWidth = 1F;
            this.ppDateTimePicker11.ButtonImage = ((System.Drawing.Bitmap)(resources.GetObject("ppDateTimePicker11.ButtonImage")));
            this.ppDateTimePicker11.ButtonSize = 26;
            this.ppDateTimePicker11.CustomFormat = "yyyy年MM月dd日";
            this.ppDateTimePicker11.DropbaseColor = System.Drawing.Color.White;
            this.ppDateTimePicker11.DropborderColor = System.Drawing.Color.LightGray;
            this.ppDateTimePicker11.DropformRadius = 5;
            this.ppDateTimePicker11.Language = PPSkin.PPDateTimeDropForm.Lang.EN;
            this.ppDateTimePicker11.Location = new System.Drawing.Point(229, 283);
            this.ppDateTimePicker11.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ppDateTimePicker11.MaxDate = new System.DateTime(9999, 12, 31, 23, 59, 59, 999);
            this.ppDateTimePicker11.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.ppDateTimePicker11.Name = "ppDateTimePicker11";
            this.ppDateTimePicker11.PickTime = false;
            this.ppDateTimePicker11.Radius = 30;
            this.ppDateTimePicker11.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppDateTimePicker11.ShowBorder = true;
            this.ppDateTimePicker11.Size = new System.Drawing.Size(167, 34);
            this.ppDateTimePicker11.TabIndex = 29;
            this.ppDateTimePicker11.Value = new System.DateTime(2021, 4, 1, 11, 35, 9, 237);
            // 
            // ppDateTimePicker12
            // 
            this.ppDateTimePicker12.BackColor = System.Drawing.Color.White;
            this.ppDateTimePicker12.BaseColor = System.Drawing.Color.White;
            this.ppDateTimePicker12.BorderColor = System.Drawing.Color.LightGray;
            this.ppDateTimePicker12.BorderWidth = 1F;
            this.ppDateTimePicker12.ButtonImage = ((System.Drawing.Bitmap)(resources.GetObject("ppDateTimePicker12.ButtonImage")));
            this.ppDateTimePicker12.ButtonSize = 26;
            this.ppDateTimePicker12.CustomFormat = "yyyy年MM月dd日";
            this.ppDateTimePicker12.DropbaseColor = System.Drawing.Color.White;
            this.ppDateTimePicker12.DropborderColor = System.Drawing.Color.LightGray;
            this.ppDateTimePicker12.DropformRadius = 5;
            this.ppDateTimePicker12.Language = PPSkin.PPDateTimeDropForm.Lang.EN;
            this.ppDateTimePicker12.Location = new System.Drawing.Point(31, 283);
            this.ppDateTimePicker12.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ppDateTimePicker12.MaxDate = new System.DateTime(9999, 12, 31, 23, 59, 59, 999);
            this.ppDateTimePicker12.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.ppDateTimePicker12.Name = "ppDateTimePicker12";
            this.ppDateTimePicker12.PickTime = false;
            this.ppDateTimePicker12.Radius = 5;
            this.ppDateTimePicker12.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppDateTimePicker12.ShowBorder = true;
            this.ppDateTimePicker12.Size = new System.Drawing.Size(167, 34);
            this.ppDateTimePicker12.TabIndex = 28;
            this.ppDateTimePicker12.Value = new System.DateTime(2021, 4, 1, 11, 35, 9, 237);
            // 
            // ppDateTimePicker1
            // 
            this.ppDateTimePicker1.BackColor = System.Drawing.Color.White;
            this.ppDateTimePicker1.BaseColor = System.Drawing.Color.Moccasin;
            this.ppDateTimePicker1.BorderColor = System.Drawing.Color.Gold;
            this.ppDateTimePicker1.BorderWidth = 1F;
            this.ppDateTimePicker1.ButtonImage = ((System.Drawing.Bitmap)(resources.GetObject("ppDateTimePicker1.ButtonImage")));
            this.ppDateTimePicker1.ButtonSize = 26;
            this.ppDateTimePicker1.CustomFormat = "yyyy年MM月dd日 HH:mm:ss";
            this.ppDateTimePicker1.DropbaseColor = System.Drawing.Color.White;
            this.ppDateTimePicker1.DropborderColor = System.Drawing.Color.LightGray;
            this.ppDateTimePicker1.DropformRadius = 5;
            this.ppDateTimePicker1.Language = PPSkin.PPDateTimeDropForm.Lang.CH;
            this.ppDateTimePicker1.Location = new System.Drawing.Point(676, 384);
            this.ppDateTimePicker1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ppDateTimePicker1.MaxDate = new System.DateTime(9999, 12, 31, 23, 59, 59, 999);
            this.ppDateTimePicker1.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.ppDateTimePicker1.Name = "ppDateTimePicker1";
            this.ppDateTimePicker1.PickTime = true;
            this.ppDateTimePicker1.Radius = 5;
            this.ppDateTimePicker1.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppDateTimePicker1.ShowBorder = true;
            this.ppDateTimePicker1.Size = new System.Drawing.Size(186, 34);
            this.ppDateTimePicker1.TabIndex = 27;
            this.ppDateTimePicker1.Value = new System.DateTime(2021, 4, 1, 11, 35, 9, 237);
            // 
            // ppDateTimePicker2
            // 
            this.ppDateTimePicker2.BackColor = System.Drawing.Color.White;
            this.ppDateTimePicker2.BaseColor = System.Drawing.Color.White;
            this.ppDateTimePicker2.BorderColor = System.Drawing.Color.Red;
            this.ppDateTimePicker2.BorderWidth = 1F;
            this.ppDateTimePicker2.ButtonImage = ((System.Drawing.Bitmap)(resources.GetObject("ppDateTimePicker2.ButtonImage")));
            this.ppDateTimePicker2.ButtonSize = 26;
            this.ppDateTimePicker2.CustomFormat = "yyyy年MM月dd日 HH:mm:ss";
            this.ppDateTimePicker2.DropbaseColor = System.Drawing.Color.White;
            this.ppDateTimePicker2.DropborderColor = System.Drawing.Color.LightGray;
            this.ppDateTimePicker2.DropformRadius = 5;
            this.ppDateTimePicker2.Language = PPSkin.PPDateTimeDropForm.Lang.CH;
            this.ppDateTimePicker2.Location = new System.Drawing.Point(443, 384);
            this.ppDateTimePicker2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ppDateTimePicker2.MaxDate = new System.DateTime(9999, 12, 31, 23, 59, 59, 999);
            this.ppDateTimePicker2.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.ppDateTimePicker2.Name = "ppDateTimePicker2";
            this.ppDateTimePicker2.PickTime = true;
            this.ppDateTimePicker2.Radius = 5;
            this.ppDateTimePicker2.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppDateTimePicker2.ShowBorder = true;
            this.ppDateTimePicker2.Size = new System.Drawing.Size(185, 34);
            this.ppDateTimePicker2.TabIndex = 26;
            this.ppDateTimePicker2.Value = new System.DateTime(2021, 4, 1, 11, 35, 9, 237);
            // 
            // ppDateTimePicker3
            // 
            this.ppDateTimePicker3.BackColor = System.Drawing.Color.White;
            this.ppDateTimePicker3.BaseColor = System.Drawing.Color.White;
            this.ppDateTimePicker3.BorderColor = System.Drawing.Color.LightGray;
            this.ppDateTimePicker3.BorderWidth = 1F;
            this.ppDateTimePicker3.ButtonImage = ((System.Drawing.Bitmap)(resources.GetObject("ppDateTimePicker3.ButtonImage")));
            this.ppDateTimePicker3.ButtonSize = 26;
            this.ppDateTimePicker3.CustomFormat = "yyyy年MM月dd日 HH:mm:ss";
            this.ppDateTimePicker3.DropbaseColor = System.Drawing.Color.White;
            this.ppDateTimePicker3.DropborderColor = System.Drawing.Color.LightGray;
            this.ppDateTimePicker3.DropformRadius = 5;
            this.ppDateTimePicker3.Language = PPSkin.PPDateTimeDropForm.Lang.CH;
            this.ppDateTimePicker3.Location = new System.Drawing.Point(229, 384);
            this.ppDateTimePicker3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ppDateTimePicker3.MaxDate = new System.DateTime(9999, 12, 31, 23, 59, 59, 999);
            this.ppDateTimePicker3.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.ppDateTimePicker3.Name = "ppDateTimePicker3";
            this.ppDateTimePicker3.PickTime = true;
            this.ppDateTimePicker3.Radius = 30;
            this.ppDateTimePicker3.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppDateTimePicker3.ShowBorder = true;
            this.ppDateTimePicker3.Size = new System.Drawing.Size(210, 34);
            this.ppDateTimePicker3.TabIndex = 25;
            this.ppDateTimePicker3.Value = new System.DateTime(2021, 4, 1, 11, 35, 9, 237);
            // 
            // ppDateTimePicker4
            // 
            this.ppDateTimePicker4.BackColor = System.Drawing.Color.White;
            this.ppDateTimePicker4.BaseColor = System.Drawing.Color.White;
            this.ppDateTimePicker4.BorderColor = System.Drawing.Color.LightGray;
            this.ppDateTimePicker4.BorderWidth = 1F;
            this.ppDateTimePicker4.ButtonImage = ((System.Drawing.Bitmap)(resources.GetObject("ppDateTimePicker4.ButtonImage")));
            this.ppDateTimePicker4.ButtonSize = 26;
            this.ppDateTimePicker4.CustomFormat = "yyyy年MM月dd日 HH:mm:ss";
            this.ppDateTimePicker4.DropbaseColor = System.Drawing.Color.White;
            this.ppDateTimePicker4.DropborderColor = System.Drawing.Color.LightGray;
            this.ppDateTimePicker4.DropformRadius = 5;
            this.ppDateTimePicker4.Language = PPSkin.PPDateTimeDropForm.Lang.CH;
            this.ppDateTimePicker4.Location = new System.Drawing.Point(31, 384);
            this.ppDateTimePicker4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ppDateTimePicker4.MaxDate = new System.DateTime(9999, 12, 31, 23, 59, 59, 999);
            this.ppDateTimePicker4.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.ppDateTimePicker4.Name = "ppDateTimePicker4";
            this.ppDateTimePicker4.PickTime = true;
            this.ppDateTimePicker4.Radius = 5;
            this.ppDateTimePicker4.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppDateTimePicker4.ShowBorder = true;
            this.ppDateTimePicker4.Size = new System.Drawing.Size(192, 34);
            this.ppDateTimePicker4.TabIndex = 24;
            this.ppDateTimePicker4.Value = new System.DateTime(2021, 4, 1, 11, 35, 9, 237);
            // 
            // ppDateTimePicker5
            // 
            this.ppDateTimePicker5.BackColor = System.Drawing.Color.White;
            this.ppDateTimePicker5.BaseColor = System.Drawing.Color.Moccasin;
            this.ppDateTimePicker5.BorderColor = System.Drawing.Color.Gold;
            this.ppDateTimePicker5.BorderWidth = 1F;
            this.ppDateTimePicker5.ButtonImage = ((System.Drawing.Bitmap)(resources.GetObject("ppDateTimePicker5.ButtonImage")));
            this.ppDateTimePicker5.ButtonSize = 26;
            this.ppDateTimePicker5.CustomFormat = "yyyy年MM月dd日";
            this.ppDateTimePicker5.DropbaseColor = System.Drawing.Color.White;
            this.ppDateTimePicker5.DropborderColor = System.Drawing.Color.LightGray;
            this.ppDateTimePicker5.DropformRadius = 5;
            this.ppDateTimePicker5.Language = PPSkin.PPDateTimeDropForm.Lang.CH;
            this.ppDateTimePicker5.Location = new System.Drawing.Point(676, 195);
            this.ppDateTimePicker5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ppDateTimePicker5.MaxDate = new System.DateTime(9999, 12, 31, 23, 59, 59, 999);
            this.ppDateTimePicker5.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.ppDateTimePicker5.Name = "ppDateTimePicker5";
            this.ppDateTimePicker5.PickTime = false;
            this.ppDateTimePicker5.Radius = 5;
            this.ppDateTimePicker5.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppDateTimePicker5.ShowBorder = true;
            this.ppDateTimePicker5.Size = new System.Drawing.Size(167, 34);
            this.ppDateTimePicker5.TabIndex = 23;
            this.ppDateTimePicker5.Value = new System.DateTime(2021, 4, 1, 11, 35, 9, 237);
            // 
            // ppDateTimePicker6
            // 
            this.ppDateTimePicker6.BackColor = System.Drawing.Color.White;
            this.ppDateTimePicker6.BaseColor = System.Drawing.Color.White;
            this.ppDateTimePicker6.BorderColor = System.Drawing.Color.Red;
            this.ppDateTimePicker6.BorderWidth = 1F;
            this.ppDateTimePicker6.ButtonImage = ((System.Drawing.Bitmap)(resources.GetObject("ppDateTimePicker6.ButtonImage")));
            this.ppDateTimePicker6.ButtonSize = 26;
            this.ppDateTimePicker6.CustomFormat = "yyyy年MM月dd日";
            this.ppDateTimePicker6.DropbaseColor = System.Drawing.Color.White;
            this.ppDateTimePicker6.DropborderColor = System.Drawing.Color.LightGray;
            this.ppDateTimePicker6.DropformRadius = 5;
            this.ppDateTimePicker6.Language = PPSkin.PPDateTimeDropForm.Lang.CH;
            this.ppDateTimePicker6.Location = new System.Drawing.Point(443, 195);
            this.ppDateTimePicker6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ppDateTimePicker6.MaxDate = new System.DateTime(9999, 12, 31, 23, 59, 59, 999);
            this.ppDateTimePicker6.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.ppDateTimePicker6.Name = "ppDateTimePicker6";
            this.ppDateTimePicker6.PickTime = false;
            this.ppDateTimePicker6.Radius = 5;
            this.ppDateTimePicker6.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppDateTimePicker6.ShowBorder = true;
            this.ppDateTimePicker6.Size = new System.Drawing.Size(167, 34);
            this.ppDateTimePicker6.TabIndex = 22;
            this.ppDateTimePicker6.Value = new System.DateTime(2021, 4, 1, 11, 35, 9, 237);
            // 
            // ppDateTimePicker7
            // 
            this.ppDateTimePicker7.BackColor = System.Drawing.Color.White;
            this.ppDateTimePicker7.BaseColor = System.Drawing.Color.White;
            this.ppDateTimePicker7.BorderColor = System.Drawing.Color.LightGray;
            this.ppDateTimePicker7.BorderWidth = 1F;
            this.ppDateTimePicker7.ButtonImage = ((System.Drawing.Bitmap)(resources.GetObject("ppDateTimePicker7.ButtonImage")));
            this.ppDateTimePicker7.ButtonSize = 26;
            this.ppDateTimePicker7.CustomFormat = "yyyy年MM月dd日";
            this.ppDateTimePicker7.DropbaseColor = System.Drawing.Color.White;
            this.ppDateTimePicker7.DropborderColor = System.Drawing.Color.LightGray;
            this.ppDateTimePicker7.DropformRadius = 5;
            this.ppDateTimePicker7.Language = PPSkin.PPDateTimeDropForm.Lang.CH;
            this.ppDateTimePicker7.Location = new System.Drawing.Point(229, 195);
            this.ppDateTimePicker7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ppDateTimePicker7.MaxDate = new System.DateTime(9999, 12, 31, 23, 59, 59, 999);
            this.ppDateTimePicker7.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.ppDateTimePicker7.Name = "ppDateTimePicker7";
            this.ppDateTimePicker7.PickTime = false;
            this.ppDateTimePicker7.Radius = 30;
            this.ppDateTimePicker7.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppDateTimePicker7.ShowBorder = true;
            this.ppDateTimePicker7.Size = new System.Drawing.Size(167, 34);
            this.ppDateTimePicker7.TabIndex = 21;
            this.ppDateTimePicker7.Value = new System.DateTime(2021, 4, 1, 11, 35, 9, 237);
            // 
            // ppDateTimePicker8
            // 
            this.ppDateTimePicker8.BackColor = System.Drawing.Color.White;
            this.ppDateTimePicker8.BaseColor = System.Drawing.Color.White;
            this.ppDateTimePicker8.BorderColor = System.Drawing.Color.LightGray;
            this.ppDateTimePicker8.BorderWidth = 1F;
            this.ppDateTimePicker8.ButtonImage = ((System.Drawing.Bitmap)(resources.GetObject("ppDateTimePicker8.ButtonImage")));
            this.ppDateTimePicker8.ButtonSize = 26;
            this.ppDateTimePicker8.CustomFormat = "yyyy年MM月dd日";
            this.ppDateTimePicker8.DropbaseColor = System.Drawing.Color.White;
            this.ppDateTimePicker8.DropborderColor = System.Drawing.Color.LightGray;
            this.ppDateTimePicker8.DropformRadius = 5;
            this.ppDateTimePicker8.Language = PPSkin.PPDateTimeDropForm.Lang.CH;
            this.ppDateTimePicker8.Location = new System.Drawing.Point(31, 195);
            this.ppDateTimePicker8.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ppDateTimePicker8.MaxDate = new System.DateTime(9999, 12, 31, 23, 59, 59, 999);
            this.ppDateTimePicker8.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.ppDateTimePicker8.Name = "ppDateTimePicker8";
            this.ppDateTimePicker8.PickTime = false;
            this.ppDateTimePicker8.Radius = 5;
            this.ppDateTimePicker8.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppDateTimePicker8.ShowBorder = true;
            this.ppDateTimePicker8.Size = new System.Drawing.Size(167, 34);
            this.ppDateTimePicker8.TabIndex = 20;
            this.ppDateTimePicker8.Value = new System.DateTime(2021, 4, 1, 11, 35, 9, 237);
            // 
            // ppCombobox4
            // 
            this.ppCombobox4.ArrowImage = ((System.Drawing.Image)(resources.GetObject("ppCombobox4.ArrowImage")));
            this.ppCombobox4.BackColor = System.Drawing.Color.White;
            this.ppCombobox4.BaseColor = System.Drawing.Color.Moccasin;
            this.ppCombobox4.BorderColor = System.Drawing.Color.Gold;
            this.ppCombobox4.BorderWidth = 1F;
            this.ppCombobox4.ImageOffset = new System.Drawing.Point(0, 0);
            this.ppCombobox4.ImageSize = new System.Drawing.Size(13, 10);
            this.ppCombobox4.Items.AddRange(new object[] {
            "asdasdasd",
            "  a",
            "d",
            "ad",
            "a",
            "d",
            "a",
            "sda",
            "sd",
            "ad",
            "a",
            "d"});
            this.ppCombobox4.Location = new System.Drawing.Point(676, 111);
            this.ppCombobox4.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.ppCombobox4.Name = "ppCombobox4";
            this.ppCombobox4.Radius = 5;
            this.ppCombobox4.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppCombobox4.SelectedIndex = -1;
            this.ppCombobox4.SelectedItem = null;
            this.ppCombobox4.SelectedText = "";
            this.ppCombobox4.SelectedValue = null;
            this.ppCombobox4.ShowBorder = true;
            this.ppCombobox4.Size = new System.Drawing.Size(167, 29);
            this.ppCombobox4.TabIndex = 11;
            // 
            // ppCombobox3
            // 
            this.ppCombobox3.ArrowImage = ((System.Drawing.Image)(resources.GetObject("ppCombobox3.ArrowImage")));
            this.ppCombobox3.BackColor = System.Drawing.Color.White;
            this.ppCombobox3.BaseColor = System.Drawing.Color.White;
            this.ppCombobox3.BorderColor = System.Drawing.Color.Red;
            this.ppCombobox3.BorderWidth = 1F;
            this.ppCombobox3.ImageOffset = new System.Drawing.Point(0, 0);
            this.ppCombobox3.ImageSize = new System.Drawing.Size(13, 10);
            this.ppCombobox3.Location = new System.Drawing.Point(443, 111);
            this.ppCombobox3.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.ppCombobox3.Name = "ppCombobox3";
            this.ppCombobox3.Radius = 5;
            this.ppCombobox3.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppCombobox3.SelectedIndex = -1;
            this.ppCombobox3.SelectedItem = null;
            this.ppCombobox3.SelectedText = "";
            this.ppCombobox3.SelectedValue = null;
            this.ppCombobox3.ShowBorder = true;
            this.ppCombobox3.Size = new System.Drawing.Size(167, 29);
            this.ppCombobox3.TabIndex = 10;
            // 
            // ppCombobox2
            // 
            this.ppCombobox2.ArrowImage = ((System.Drawing.Image)(resources.GetObject("ppCombobox2.ArrowImage")));
            this.ppCombobox2.BackColor = System.Drawing.Color.White;
            this.ppCombobox2.BaseColor = System.Drawing.Color.White;
            this.ppCombobox2.BorderColor = System.Drawing.Color.LightGray;
            this.ppCombobox2.BorderWidth = 1F;
            this.ppCombobox2.ImageOffset = new System.Drawing.Point(0, 0);
            this.ppCombobox2.ImageSize = new System.Drawing.Size(13, 10);
            this.ppCombobox2.Items.AddRange(new object[] {
            "asdadad",
            "asd",
            "asd",
            "a",
            "d",
            "ad",
            "s"});
            this.ppCombobox2.Location = new System.Drawing.Point(229, 111);
            this.ppCombobox2.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.ppCombobox2.Name = "ppCombobox2";
            this.ppCombobox2.Radius = 27;
            this.ppCombobox2.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppCombobox2.SelectedIndex = -1;
            this.ppCombobox2.SelectedItem = null;
            this.ppCombobox2.SelectedText = "";
            this.ppCombobox2.SelectedValue = null;
            this.ppCombobox2.ShowBorder = true;
            this.ppCombobox2.Size = new System.Drawing.Size(167, 29);
            this.ppCombobox2.TabIndex = 9;
            // 
            // ppCombobox1
            // 
            this.ppCombobox1.ArrowImage = ((System.Drawing.Image)(resources.GetObject("ppCombobox1.ArrowImage")));
            this.ppCombobox1.BackColor = System.Drawing.Color.White;
            this.ppCombobox1.BaseColor = System.Drawing.Color.White;
            this.ppCombobox1.BorderColor = System.Drawing.Color.LightGray;
            this.ppCombobox1.BorderWidth = 1F;
            this.ppCombobox1.ImageOffset = new System.Drawing.Point(0, 0);
            this.ppCombobox1.ImageSize = new System.Drawing.Size(13, 10);
            this.ppCombobox1.Items.AddRange(new object[] {
            "asdad",
            "",
            "asda",
            "d",
            "asd",
            "a",
            "d",
            "ads",
            "a",
            "d"});
            this.ppCombobox1.Location = new System.Drawing.Point(31, 111);
            this.ppCombobox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ppCombobox1.Name = "ppCombobox1";
            this.ppCombobox1.Radius = 5;
            this.ppCombobox1.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppCombobox1.SelectedIndex = -1;
            this.ppCombobox1.SelectedItem = null;
            this.ppCombobox1.SelectedText = "";
            this.ppCombobox1.SelectedValue = null;
            this.ppCombobox1.ShowBorder = true;
            this.ppCombobox1.Size = new System.Drawing.Size(167, 29);
            this.ppCombobox1.TabIndex = 8;
            // 
            // ppComboboxEx4
            // 
            this.ppComboboxEx4.BackColor = System.Drawing.Color.White;
            this.ppComboboxEx4.BaseColor = System.Drawing.Color.Moccasin;
            this.ppComboboxEx4.BorderColor = System.Drawing.Color.Gold;
            this.ppComboboxEx4.BorderWidth = 1F;
            this.ppComboboxEx4.DropbaseColor = System.Drawing.Color.White;
            this.ppComboboxEx4.DropborderColor = System.Drawing.Color.LightGray;
            this.ppComboboxEx4.DropbuttonColor = System.Drawing.Color.White;
            this.ppComboboxEx4.DropbuttonMouseDownColor = System.Drawing.Color.DimGray;
            this.ppComboboxEx4.DropbuttonMouseInColor = System.Drawing.Color.LightGray;
            this.ppComboboxEx4.DropbuttonRadius = 5;
            this.ppComboboxEx4.DropbuttonSelectColor = System.Drawing.Color.DimGray;
            this.ppComboboxEx4.DropformRadius = 5;
            this.ppComboboxEx4.DropTextAlign = PPSkin.PPComboboxEx.Align.LEFT;
            this.ppComboboxEx4.Items.AddRange(new object[] {
            "asda",
            "as",
            "das",
            "da",
            "sd",
            "agsaeg",
            "s",
            "beytj",
            "rkju",
            "l59",
            "79p[",
            "9[\'-8-0p;rfuyti"});
            this.ppComboboxEx4.Location = new System.Drawing.Point(676, 38);
            this.ppComboboxEx4.MaxDropDownItems = 8;
            this.ppComboboxEx4.Name = "ppComboboxEx4";
            this.ppComboboxEx4.Radius = 5;
            this.ppComboboxEx4.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppComboboxEx4.SelectedIndex = -1;
            this.ppComboboxEx4.SelectedItem = null;
            this.ppComboboxEx4.SelectedText = "";
            this.ppComboboxEx4.SelectedValue = null;
            this.ppComboboxEx4.Size = new System.Drawing.Size(167, 33);
            this.ppComboboxEx4.TabIndex = 7;
            this.ppComboboxEx4.TextAlign = PPSkin.PPComboboxEx.Align.LEFT;
            this.ppComboboxEx4.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            // 
            // ppComboboxEx3
            // 
            this.ppComboboxEx3.BackColor = System.Drawing.Color.White;
            this.ppComboboxEx3.BaseColor = System.Drawing.Color.White;
            this.ppComboboxEx3.BorderColor = System.Drawing.Color.Red;
            this.ppComboboxEx3.BorderWidth = 1F;
            this.ppComboboxEx3.DropbaseColor = System.Drawing.Color.White;
            this.ppComboboxEx3.DropborderColor = System.Drawing.Color.LightGray;
            this.ppComboboxEx3.DropbuttonColor = System.Drawing.Color.White;
            this.ppComboboxEx3.DropbuttonMouseDownColor = System.Drawing.Color.DimGray;
            this.ppComboboxEx3.DropbuttonMouseInColor = System.Drawing.Color.LightGray;
            this.ppComboboxEx3.DropbuttonRadius = 5;
            this.ppComboboxEx3.DropbuttonSelectColor = System.Drawing.Color.DimGray;
            this.ppComboboxEx3.DropformRadius = 5;
            this.ppComboboxEx3.DropTextAlign = PPSkin.PPComboboxEx.Align.LEFT;
            this.ppComboboxEx3.Items.AddRange(new object[] {
            "asda",
            "as",
            "das"});
            this.ppComboboxEx3.Location = new System.Drawing.Point(443, 38);
            this.ppComboboxEx3.MaxDropDownItems = 8;
            this.ppComboboxEx3.Name = "ppComboboxEx3";
            this.ppComboboxEx3.Radius = 5;
            this.ppComboboxEx3.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppComboboxEx3.SelectedIndex = -1;
            this.ppComboboxEx3.SelectedItem = null;
            this.ppComboboxEx3.SelectedText = "";
            this.ppComboboxEx3.SelectedValue = null;
            this.ppComboboxEx3.Size = new System.Drawing.Size(167, 33);
            this.ppComboboxEx3.TabIndex = 6;
            this.ppComboboxEx3.TextAlign = PPSkin.PPComboboxEx.Align.CENTER;
            this.ppComboboxEx3.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            // 
            // ppComboboxEx2
            // 
            this.ppComboboxEx2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ppComboboxEx2.BaseColor = System.Drawing.Color.White;
            this.ppComboboxEx2.BorderColor = System.Drawing.Color.LightGray;
            this.ppComboboxEx2.BorderWidth = 1F;
            this.ppComboboxEx2.DropbaseColor = System.Drawing.Color.White;
            this.ppComboboxEx2.DropborderColor = System.Drawing.Color.LightGray;
            this.ppComboboxEx2.DropbuttonColor = System.Drawing.Color.White;
            this.ppComboboxEx2.DropbuttonMouseDownColor = System.Drawing.Color.DimGray;
            this.ppComboboxEx2.DropbuttonMouseInColor = System.Drawing.Color.LightGray;
            this.ppComboboxEx2.DropbuttonRadius = 5;
            this.ppComboboxEx2.DropbuttonSelectColor = System.Drawing.Color.DimGray;
            this.ppComboboxEx2.DropformRadius = 10;
            this.ppComboboxEx2.DropTextAlign = PPSkin.PPComboboxEx.Align.LEFT;
            this.ppComboboxEx2.Items.AddRange(new object[] {
            "asda",
            "as",
            "das",
            "da",
            "sd",
            "agsaeg",
            "s",
            "beytj",
            "rkju",
            "l59",
            "79p[",
            "9[\'-8-0p;rfuyti"});
            this.ppComboboxEx2.Location = new System.Drawing.Point(229, 38);
            this.ppComboboxEx2.MaxDropDownItems = 8;
            this.ppComboboxEx2.Name = "ppComboboxEx2";
            this.ppComboboxEx2.Radius = 30;
            this.ppComboboxEx2.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppComboboxEx2.SelectedIndex = -1;
            this.ppComboboxEx2.SelectedItem = null;
            this.ppComboboxEx2.SelectedText = "";
            this.ppComboboxEx2.SelectedValue = null;
            this.ppComboboxEx2.Size = new System.Drawing.Size(167, 33);
            this.ppComboboxEx2.TabIndex = 5;
            this.ppComboboxEx2.TextAlign = PPSkin.PPComboboxEx.Align.LEFT;
            this.ppComboboxEx2.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            // 
            // ppComboboxEx1
            // 
            this.ppComboboxEx1.BackColor = System.Drawing.Color.White;
            this.ppComboboxEx1.BaseColor = System.Drawing.Color.White;
            this.ppComboboxEx1.BorderColor = System.Drawing.Color.LightGray;
            this.ppComboboxEx1.BorderWidth = 1F;
            this.ppComboboxEx1.DropbaseColor = System.Drawing.Color.White;
            this.ppComboboxEx1.DropborderColor = System.Drawing.Color.LightGray;
            this.ppComboboxEx1.DropbuttonColor = System.Drawing.Color.White;
            this.ppComboboxEx1.DropbuttonMouseDownColor = System.Drawing.Color.DimGray;
            this.ppComboboxEx1.DropbuttonMouseInColor = System.Drawing.Color.LightGray;
            this.ppComboboxEx1.DropbuttonRadius = 5;
            this.ppComboboxEx1.DropbuttonSelectColor = System.Drawing.Color.DimGray;
            this.ppComboboxEx1.DropformRadius = 5;
            this.ppComboboxEx1.DropTextAlign = PPSkin.PPComboboxEx.Align.LEFT;
            this.ppComboboxEx1.Items.AddRange(new object[] {
            "asda",
            "as",
            "das",
            "da",
            "sd",
            "agsaeg",
            "s",
            "beytj",
            "rkju",
            "l59",
            "79p[",
            "9[\'-8-0p;rfuyti"});
            this.ppComboboxEx1.Location = new System.Drawing.Point(31, 38);
            this.ppComboboxEx1.MaxDropDownItems = 8;
            this.ppComboboxEx1.Name = "ppComboboxEx1";
            this.ppComboboxEx1.Radius = 5;
            this.ppComboboxEx1.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppComboboxEx1.SelectedIndex = -1;
            this.ppComboboxEx1.SelectedItem = null;
            this.ppComboboxEx1.SelectedText = "";
            this.ppComboboxEx1.SelectedValue = null;
            this.ppComboboxEx1.Size = new System.Drawing.Size(167, 33);
            this.ppComboboxEx1.TabIndex = 4;
            this.ppComboboxEx1.TextAlign = PPSkin.PPComboboxEx.Align.LEFT;
            this.ppComboboxEx1.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.White;
            this.tabPage3.Controls.Add(this.pphScrollBarExt5);
            this.tabPage3.Controls.Add(this.pphScrollBarExt4);
            this.tabPage3.Controls.Add(this.ppWaveProgressBar7);
            this.tabPage3.Controls.Add(this.ppWaveProgressBar6);
            this.tabPage3.Controls.Add(this.ppWaveProgressBar5);
            this.tabPage3.Controls.Add(this.ppWaveProgressBar4);
            this.tabPage3.Controls.Add(this.ppWaveProgressBar3);
            this.tabPage3.Controls.Add(this.ppWaveProgressBar2);
            this.tabPage3.Controls.Add(this.ppWaveProgressBar1);
            this.tabPage3.Controls.Add(this.ppTrackBar3);
            this.tabPage3.Controls.Add(this.ppTrackBar2);
            this.tabPage3.Controls.Add(this.ppTrackBar1);
            this.tabPage3.Controls.Add(this.ppRoundProgressBar8);
            this.tabPage3.Controls.Add(this.ppRoundProgressBar7);
            this.tabPage3.Controls.Add(this.ppRoundProgressBar6);
            this.tabPage3.Controls.Add(this.ppRoundProgressBar5);
            this.tabPage3.Controls.Add(this.ppRoundProgressBar4);
            this.tabPage3.Controls.Add(this.ppRoundProgressBar3);
            this.tabPage3.Controls.Add(this.ppRoundProgressBar2);
            this.tabPage3.Controls.Add(this.ppRoundProgressBar1);
            this.tabPage3.Controls.Add(this.ppProgressBar3);
            this.tabPage3.Controls.Add(this.ppProgressBar2);
            this.tabPage3.Controls.Add(this.ppProgressBar1);
            this.tabPage3.Controls.Add(this.pphScrollBarExt3);
            this.tabPage3.Controls.Add(this.pphScrollBarExt2);
            this.tabPage3.Controls.Add(this.pphScrollBarExt1);
            this.tabPage3.Controls.Add(this.ppvScrollBarExt6);
            this.tabPage3.Controls.Add(this.ppvScrollBarExt5);
            this.tabPage3.Controls.Add(this.ppvScrollBarExt4);
            this.tabPage3.Controls.Add(this.ppvScrollBarExt3);
            this.tabPage3.Controls.Add(this.ppvScrollBarExt2);
            this.tabPage3.Controls.Add(this.ppvScrollBarExt1);
            this.tabPage3.Location = new System.Drawing.Point(0, 30);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(928, 675);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "滚动条";
            // 
            // pphScrollBarExt5
            // 
            this.pphScrollBarExt5.BackColor = System.Drawing.Color.Transparent;
            this.pphScrollBarExt5.BaseColor = System.Drawing.Color.Orange;
            this.pphScrollBarExt5.BaseRadius = 0;
            this.pphScrollBarExt5.BaseSize = 2;
            this.pphScrollBarExt5.ButtonColor = System.Drawing.Color.Red;
            this.pphScrollBarExt5.ButtonHoverColor = System.Drawing.Color.Firebrick;
            this.pphScrollBarExt5.ButtonRadius = 10;
            this.pphScrollBarExt5.ButtonSize = 10;
            this.pphScrollBarExt5.LargeChange = 10;
            this.pphScrollBarExt5.Location = new System.Drawing.Point(15, 617);
            this.pphScrollBarExt5.Maximum = 100;
            this.pphScrollBarExt5.Minimum = 0;
            this.pphScrollBarExt5.Name = "pphScrollBarExt5";
            this.pphScrollBarExt5.Size = new System.Drawing.Size(250, 20);
            this.pphScrollBarExt5.SmallChange = 1;
            this.pphScrollBarExt5.TabIndex = 64;
            this.pphScrollBarExt5.Value = 8;
            this.pphScrollBarExt5.VisibleValue = 10;
            // 
            // pphScrollBarExt4
            // 
            this.pphScrollBarExt4.BackColor = System.Drawing.Color.Transparent;
            this.pphScrollBarExt4.BaseColor = System.Drawing.Color.RosyBrown;
            this.pphScrollBarExt4.BaseRadius = 0;
            this.pphScrollBarExt4.BaseSize = 2;
            this.pphScrollBarExt4.ButtonColor = System.Drawing.Color.DarkOrange;
            this.pphScrollBarExt4.ButtonHoverColor = System.Drawing.Color.DeepSkyBlue;
            this.pphScrollBarExt4.ButtonRadius = 10;
            this.pphScrollBarExt4.ButtonSize = 10;
            this.pphScrollBarExt4.LargeChange = 10;
            this.pphScrollBarExt4.Location = new System.Drawing.Point(15, 561);
            this.pphScrollBarExt4.Maximum = 100;
            this.pphScrollBarExt4.Minimum = 0;
            this.pphScrollBarExt4.Name = "pphScrollBarExt4";
            this.pphScrollBarExt4.Size = new System.Drawing.Size(250, 20);
            this.pphScrollBarExt4.SmallChange = 1;
            this.pphScrollBarExt4.TabIndex = 63;
            this.pphScrollBarExt4.Value = 6;
            this.pphScrollBarExt4.VisibleValue = 10;
            // 
            // ppWaveProgressBar7
            // 
            this.ppWaveProgressBar7.BackColor = System.Drawing.Color.White;
            this.ppWaveProgressBar7.BaseColor = System.Drawing.Color.LightCoral;
            this.ppWaveProgressBar7.Location = new System.Drawing.Point(874, 499);
            this.ppWaveProgressBar7.Maximum = ((long)(100));
            this.ppWaveProgressBar7.Minimum = ((long)(0));
            this.ppWaveProgressBar7.Name = "ppWaveProgressBar7";
            this.ppWaveProgressBar7.Radius = 30;
            this.ppWaveProgressBar7.Size = new System.Drawing.Size(30, 150);
            this.ppWaveProgressBar7.Step = ((long)(1));
            this.ppWaveProgressBar7.TabIndex = 62;
            this.ppWaveProgressBar7.Value = ((long)(50));
            this.ppWaveProgressBar7.ValueType = PPSkin.PPWaveProgressBar.VALUETYPE.PERCENT;
            this.ppWaveProgressBar7.WaveColor = System.Drawing.Color.Aqua;
            this.ppWaveProgressBar7.WaveHeight = 10;
            this.ppWaveProgressBar7.WaveSpeed = 20;
            this.ppWaveProgressBar7.WaveTop = 65;
            this.ppWaveProgressBar7.WaveWidth = 100;
            // 
            // ppWaveProgressBar6
            // 
            this.ppWaveProgressBar6.BackColor = System.Drawing.Color.White;
            this.ppWaveProgressBar6.BaseColor = System.Drawing.Color.WhiteSmoke;
            this.ppWaveProgressBar6.Location = new System.Drawing.Point(838, 499);
            this.ppWaveProgressBar6.Maximum = ((long)(100));
            this.ppWaveProgressBar6.Minimum = ((long)(0));
            this.ppWaveProgressBar6.Name = "ppWaveProgressBar6";
            this.ppWaveProgressBar6.Radius = 0;
            this.ppWaveProgressBar6.Size = new System.Drawing.Size(30, 150);
            this.ppWaveProgressBar6.Step = ((long)(1));
            this.ppWaveProgressBar6.TabIndex = 61;
            this.ppWaveProgressBar6.Value = ((long)(50));
            this.ppWaveProgressBar6.ValueType = PPSkin.PPWaveProgressBar.VALUETYPE.PERCENT;
            this.ppWaveProgressBar6.WaveColor = System.Drawing.Color.DeepSkyBlue;
            this.ppWaveProgressBar6.WaveHeight = 10;
            this.ppWaveProgressBar6.WaveSpeed = 20;
            this.ppWaveProgressBar6.WaveTop = 65;
            this.ppWaveProgressBar6.WaveWidth = 100;
            // 
            // ppWaveProgressBar5
            // 
            this.ppWaveProgressBar5.BackColor = System.Drawing.Color.White;
            this.ppWaveProgressBar5.BaseColor = System.Drawing.Color.WhiteSmoke;
            this.ppWaveProgressBar5.Location = new System.Drawing.Point(802, 499);
            this.ppWaveProgressBar5.Maximum = ((long)(100));
            this.ppWaveProgressBar5.Minimum = ((long)(0));
            this.ppWaveProgressBar5.Name = "ppWaveProgressBar5";
            this.ppWaveProgressBar5.Radius = 20;
            this.ppWaveProgressBar5.Size = new System.Drawing.Size(30, 150);
            this.ppWaveProgressBar5.Step = ((long)(1));
            this.ppWaveProgressBar5.TabIndex = 60;
            this.ppWaveProgressBar5.Value = ((long)(50));
            this.ppWaveProgressBar5.ValueType = PPSkin.PPWaveProgressBar.VALUETYPE.PERCENT;
            this.ppWaveProgressBar5.WaveColor = System.Drawing.Color.Pink;
            this.ppWaveProgressBar5.WaveHeight = 10;
            this.ppWaveProgressBar5.WaveSpeed = 20;
            this.ppWaveProgressBar5.WaveTop = 65;
            this.ppWaveProgressBar5.WaveWidth = 100;
            // 
            // ppWaveProgressBar4
            // 
            this.ppWaveProgressBar4.BackColor = System.Drawing.Color.White;
            this.ppWaveProgressBar4.BaseColor = System.Drawing.Color.WhiteSmoke;
            this.ppWaveProgressBar4.Location = new System.Drawing.Point(766, 499);
            this.ppWaveProgressBar4.Maximum = ((long)(100));
            this.ppWaveProgressBar4.Minimum = ((long)(0));
            this.ppWaveProgressBar4.Name = "ppWaveProgressBar4";
            this.ppWaveProgressBar4.Radius = 30;
            this.ppWaveProgressBar4.Size = new System.Drawing.Size(30, 150);
            this.ppWaveProgressBar4.Step = ((long)(1));
            this.ppWaveProgressBar4.TabIndex = 59;
            this.ppWaveProgressBar4.Value = ((long)(50));
            this.ppWaveProgressBar4.ValueType = PPSkin.PPWaveProgressBar.VALUETYPE.PERCENT;
            this.ppWaveProgressBar4.WaveColor = System.Drawing.Color.DarkTurquoise;
            this.ppWaveProgressBar4.WaveHeight = 10;
            this.ppWaveProgressBar4.WaveSpeed = 20;
            this.ppWaveProgressBar4.WaveTop = 65;
            this.ppWaveProgressBar4.WaveWidth = 100;
            // 
            // ppWaveProgressBar3
            // 
            this.ppWaveProgressBar3.BackColor = System.Drawing.Color.White;
            this.ppWaveProgressBar3.BaseColor = System.Drawing.Color.White;
            this.ppWaveProgressBar3.Location = new System.Drawing.Point(598, 499);
            this.ppWaveProgressBar3.Maximum = ((long)(100));
            this.ppWaveProgressBar3.Minimum = ((long)(0));
            this.ppWaveProgressBar3.Name = "ppWaveProgressBar3";
            this.ppWaveProgressBar3.Radius = 150;
            this.ppWaveProgressBar3.Size = new System.Drawing.Size(150, 150);
            this.ppWaveProgressBar3.Step = ((long)(1));
            this.ppWaveProgressBar3.TabIndex = 58;
            this.ppWaveProgressBar3.Value = ((long)(75));
            this.ppWaveProgressBar3.ValueType = PPSkin.PPWaveProgressBar.VALUETYPE.PERCENT;
            this.ppWaveProgressBar3.WaveColor = System.Drawing.Color.Violet;
            this.ppWaveProgressBar3.WaveHeight = 20;
            this.ppWaveProgressBar3.WaveSpeed = 10;
            this.ppWaveProgressBar3.WaveTop = 17;
            this.ppWaveProgressBar3.WaveWidth = 200;
            // 
            // ppWaveProgressBar2
            // 
            this.ppWaveProgressBar2.BackColor = System.Drawing.Color.White;
            this.ppWaveProgressBar2.BaseColor = System.Drawing.Color.WhiteSmoke;
            this.ppWaveProgressBar2.Location = new System.Drawing.Point(442, 499);
            this.ppWaveProgressBar2.Maximum = ((long)(100));
            this.ppWaveProgressBar2.Minimum = ((long)(0));
            this.ppWaveProgressBar2.Name = "ppWaveProgressBar2";
            this.ppWaveProgressBar2.Radius = 30;
            this.ppWaveProgressBar2.Size = new System.Drawing.Size(150, 150);
            this.ppWaveProgressBar2.Step = ((long)(1));
            this.ppWaveProgressBar2.TabIndex = 57;
            this.ppWaveProgressBar2.Value = ((long)(50));
            this.ppWaveProgressBar2.ValueType = PPSkin.PPWaveProgressBar.VALUETYPE.PERCENT;
            this.ppWaveProgressBar2.WaveColor = System.Drawing.Color.CornflowerBlue;
            this.ppWaveProgressBar2.WaveHeight = 20;
            this.ppWaveProgressBar2.WaveSpeed = 10;
            this.ppWaveProgressBar2.WaveTop = 55;
            this.ppWaveProgressBar2.WaveWidth = 200;
            // 
            // ppWaveProgressBar1
            // 
            this.ppWaveProgressBar1.BackColor = System.Drawing.Color.White;
            this.ppWaveProgressBar1.BaseColor = System.Drawing.Color.WhiteSmoke;
            this.ppWaveProgressBar1.Location = new System.Drawing.Point(286, 499);
            this.ppWaveProgressBar1.Maximum = ((long)(100));
            this.ppWaveProgressBar1.Minimum = ((long)(0));
            this.ppWaveProgressBar1.Name = "ppWaveProgressBar1";
            this.ppWaveProgressBar1.Radius = 0;
            this.ppWaveProgressBar1.Size = new System.Drawing.Size(150, 150);
            this.ppWaveProgressBar1.Step = ((long)(1));
            this.ppWaveProgressBar1.TabIndex = 56;
            this.ppWaveProgressBar1.Value = ((long)(0));
            this.ppWaveProgressBar1.ValueType = PPSkin.PPWaveProgressBar.VALUETYPE.PERCENT;
            this.ppWaveProgressBar1.WaveColor = System.Drawing.Color.DodgerBlue;
            this.ppWaveProgressBar1.WaveHeight = 20;
            this.ppWaveProgressBar1.WaveSpeed = 10;
            this.ppWaveProgressBar1.WaveTop = 130;
            this.ppWaveProgressBar1.WaveWidth = 200;
            // 
            // ppTrackBar3
            // 
            this.ppTrackBar3.Corners = false;
            this.ppTrackBar3.DcimalDigits = 0;
            this.ppTrackBar3.EllipseBorderColor = System.Drawing.Color.LightGray;
            this.ppTrackBar3.EllipseColor = System.Drawing.Color.White;
            this.ppTrackBar3.IsShowTips = true;
            this.ppTrackBar3.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(231)))), ((int)(((byte)(237)))));
            this.ppTrackBar3.LineWidth = 3;
            this.ppTrackBar3.Location = new System.Drawing.Point(612, 103);
            this.ppTrackBar3.MaxValue = ((long)(100));
            this.ppTrackBar3.MinValue = ((long)(0));
            this.ppTrackBar3.Name = "ppTrackBar3";
            this.ppTrackBar3.RadioButtonSize = 18;
            this.ppTrackBar3.ShowButton = true;
            this.ppTrackBar3.Size = new System.Drawing.Size(292, 36);
            this.ppTrackBar3.TabIndex = 55;
            this.ppTrackBar3.TipBackColor = System.Drawing.Color.DeepSkyBlue;
            this.ppTrackBar3.TipForeColor = System.Drawing.Color.White;
            this.ppTrackBar3.TipsFormat = null;
            this.ppTrackBar3.Value = ((long)(10));
            this.ppTrackBar3.ValueColor = System.Drawing.Color.DodgerBlue;
            this.ppTrackBar3.ValueIsSecond = false;
            // 
            // ppTrackBar2
            // 
            this.ppTrackBar2.Corners = true;
            this.ppTrackBar2.DcimalDigits = 0;
            this.ppTrackBar2.EllipseBorderColor = System.Drawing.Color.White;
            this.ppTrackBar2.EllipseColor = System.Drawing.Color.DodgerBlue;
            this.ppTrackBar2.IsShowTips = true;
            this.ppTrackBar2.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(231)))), ((int)(((byte)(237)))));
            this.ppTrackBar2.LineWidth = 10;
            this.ppTrackBar2.Location = new System.Drawing.Point(612, 67);
            this.ppTrackBar2.MaxValue = ((long)(100));
            this.ppTrackBar2.MinValue = ((long)(0));
            this.ppTrackBar2.Name = "ppTrackBar2";
            this.ppTrackBar2.RadioButtonSize = 18;
            this.ppTrackBar2.ShowButton = true;
            this.ppTrackBar2.Size = new System.Drawing.Size(292, 30);
            this.ppTrackBar2.TabIndex = 54;
            this.ppTrackBar2.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppTrackBar2.TipForeColor = System.Drawing.Color.White;
            this.ppTrackBar2.TipsFormat = null;
            this.ppTrackBar2.Value = ((long)(50));
            this.ppTrackBar2.ValueColor = System.Drawing.Color.DarkTurquoise;
            this.ppTrackBar2.ValueIsSecond = false;
            // 
            // ppTrackBar1
            // 
            this.ppTrackBar1.BackColor = System.Drawing.Color.White;
            this.ppTrackBar1.Corners = true;
            this.ppTrackBar1.DcimalDigits = 0;
            this.ppTrackBar1.EllipseBorderColor = System.Drawing.Color.LightGray;
            this.ppTrackBar1.EllipseColor = System.Drawing.Color.White;
            this.ppTrackBar1.IsShowTips = true;
            this.ppTrackBar1.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(231)))), ((int)(((byte)(237)))));
            this.ppTrackBar1.LineWidth = 10;
            this.ppTrackBar1.Location = new System.Drawing.Point(612, 25);
            this.ppTrackBar1.MaxValue = ((long)(100));
            this.ppTrackBar1.MinValue = ((long)(0));
            this.ppTrackBar1.Name = "ppTrackBar1";
            this.ppTrackBar1.RadioButtonSize = 20;
            this.ppTrackBar1.ShowButton = true;
            this.ppTrackBar1.Size = new System.Drawing.Size(292, 30);
            this.ppTrackBar1.TabIndex = 53;
            this.ppTrackBar1.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppTrackBar1.TipForeColor = System.Drawing.Color.White;
            this.ppTrackBar1.TipsFormat = null;
            this.ppTrackBar1.Value = ((long)(100));
            this.ppTrackBar1.ValueColor = System.Drawing.Color.DodgerBlue;
            this.ppTrackBar1.ValueIsSecond = false;
            // 
            // ppRoundProgressBar8
            // 
            this.ppRoundProgressBar8.BaseColor = System.Drawing.Color.LightGray;
            this.ppRoundProgressBar8.CircleWidth = 16;
            this.ppRoundProgressBar8.Direction = PPSkin.PPRoundProgressBar.DIRECTION.ClockWise;
            this.ppRoundProgressBar8.InsideColor = System.Drawing.Color.LightGray;
            this.ppRoundProgressBar8.Location = new System.Drawing.Point(754, 330);
            this.ppRoundProgressBar8.Maximum = ((long)(100));
            this.ppRoundProgressBar8.Minimum = ((long)(0));
            this.ppRoundProgressBar8.Name = "ppRoundProgressBar8";
            this.ppRoundProgressBar8.ShowType = PPSkin.PPRoundProgressBar.SHOWType.RING;
            this.ppRoundProgressBar8.Size = new System.Drawing.Size(150, 150);
            this.ppRoundProgressBar8.StartAngle = 90;
            this.ppRoundProgressBar8.Step = ((long)(1));
            this.ppRoundProgressBar8.TabIndex = 52;
            this.ppRoundProgressBar8.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.ppRoundProgressBar8.Value = ((long)(50));
            this.ppRoundProgressBar8.ValueColor = System.Drawing.Color.DodgerBlue;
            this.ppRoundProgressBar8.ValueType = PPSkin.PPRoundProgressBar.VALUETYPE.PERCENT;
            // 
            // ppRoundProgressBar7
            // 
            this.ppRoundProgressBar7.BaseColor = System.Drawing.Color.WhiteSmoke;
            this.ppRoundProgressBar7.CircleWidth = 16;
            this.ppRoundProgressBar7.Direction = PPSkin.PPRoundProgressBar.DIRECTION.ClockWise;
            this.ppRoundProgressBar7.InsideColor = System.Drawing.Color.Red;
            this.ppRoundProgressBar7.Location = new System.Drawing.Point(598, 330);
            this.ppRoundProgressBar7.Maximum = ((long)(100));
            this.ppRoundProgressBar7.Minimum = ((long)(0));
            this.ppRoundProgressBar7.Name = "ppRoundProgressBar7";
            this.ppRoundProgressBar7.ShowType = PPSkin.PPRoundProgressBar.SHOWType.RING;
            this.ppRoundProgressBar7.Size = new System.Drawing.Size(150, 150);
            this.ppRoundProgressBar7.StartAngle = 0;
            this.ppRoundProgressBar7.Step = ((long)(1));
            this.ppRoundProgressBar7.TabIndex = 51;
            this.ppRoundProgressBar7.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.ppRoundProgressBar7.Value = ((long)(50));
            this.ppRoundProgressBar7.ValueColor = System.Drawing.Color.DodgerBlue;
            this.ppRoundProgressBar7.ValueType = PPSkin.PPRoundProgressBar.VALUETYPE.VALUE;
            // 
            // ppRoundProgressBar6
            // 
            this.ppRoundProgressBar6.BaseColor = System.Drawing.Color.WhiteSmoke;
            this.ppRoundProgressBar6.CircleWidth = 16;
            this.ppRoundProgressBar6.Direction = PPSkin.PPRoundProgressBar.DIRECTION.AntiClockWise;
            this.ppRoundProgressBar6.InsideColor = System.Drawing.Color.LightGray;
            this.ppRoundProgressBar6.Location = new System.Drawing.Point(442, 330);
            this.ppRoundProgressBar6.Maximum = ((long)(100));
            this.ppRoundProgressBar6.Minimum = ((long)(0));
            this.ppRoundProgressBar6.Name = "ppRoundProgressBar6";
            this.ppRoundProgressBar6.ShowType = PPSkin.PPRoundProgressBar.SHOWType.SECTOR;
            this.ppRoundProgressBar6.Size = new System.Drawing.Size(150, 150);
            this.ppRoundProgressBar6.StartAngle = 0;
            this.ppRoundProgressBar6.Step = ((long)(1));
            this.ppRoundProgressBar6.TabIndex = 50;
            this.ppRoundProgressBar6.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.ppRoundProgressBar6.Value = ((long)(75));
            this.ppRoundProgressBar6.ValueColor = System.Drawing.Color.Crimson;
            this.ppRoundProgressBar6.ValueType = PPSkin.PPRoundProgressBar.VALUETYPE.PERCENT;
            // 
            // ppRoundProgressBar5
            // 
            this.ppRoundProgressBar5.BaseColor = System.Drawing.Color.LightGray;
            this.ppRoundProgressBar5.CircleWidth = 16;
            this.ppRoundProgressBar5.Direction = PPSkin.PPRoundProgressBar.DIRECTION.ClockWise;
            this.ppRoundProgressBar5.InsideColor = System.Drawing.Color.LightGray;
            this.ppRoundProgressBar5.Location = new System.Drawing.Point(286, 330);
            this.ppRoundProgressBar5.Maximum = ((long)(100));
            this.ppRoundProgressBar5.Minimum = ((long)(0));
            this.ppRoundProgressBar5.Name = "ppRoundProgressBar5";
            this.ppRoundProgressBar5.ShowType = PPSkin.PPRoundProgressBar.SHOWType.SECTOR;
            this.ppRoundProgressBar5.Size = new System.Drawing.Size(150, 150);
            this.ppRoundProgressBar5.StartAngle = 0;
            this.ppRoundProgressBar5.Step = ((long)(1));
            this.ppRoundProgressBar5.TabIndex = 49;
            this.ppRoundProgressBar5.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.ppRoundProgressBar5.Value = ((long)(75));
            this.ppRoundProgressBar5.ValueColor = System.Drawing.Color.DodgerBlue;
            this.ppRoundProgressBar5.ValueType = PPSkin.PPRoundProgressBar.VALUETYPE.PERCENT;
            // 
            // ppRoundProgressBar4
            // 
            this.ppRoundProgressBar4.BaseColor = System.Drawing.Color.Red;
            this.ppRoundProgressBar4.CircleWidth = 16;
            this.ppRoundProgressBar4.Direction = PPSkin.PPRoundProgressBar.DIRECTION.ClockWise;
            this.ppRoundProgressBar4.InsideColor = System.Drawing.Color.LightGray;
            this.ppRoundProgressBar4.Location = new System.Drawing.Point(754, 155);
            this.ppRoundProgressBar4.Maximum = ((long)(100));
            this.ppRoundProgressBar4.Minimum = ((long)(0));
            this.ppRoundProgressBar4.Name = "ppRoundProgressBar4";
            this.ppRoundProgressBar4.ShowType = PPSkin.PPRoundProgressBar.SHOWType.RING;
            this.ppRoundProgressBar4.Size = new System.Drawing.Size(150, 150);
            this.ppRoundProgressBar4.StartAngle = 0;
            this.ppRoundProgressBar4.Step = ((long)(1));
            this.ppRoundProgressBar4.TabIndex = 48;
            this.ppRoundProgressBar4.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.ppRoundProgressBar4.Value = ((long)(50));
            this.ppRoundProgressBar4.ValueColor = System.Drawing.Color.DodgerBlue;
            this.ppRoundProgressBar4.ValueType = PPSkin.PPRoundProgressBar.VALUETYPE.PERCENT;
            // 
            // ppRoundProgressBar3
            // 
            this.ppRoundProgressBar3.BaseColor = System.Drawing.Color.LightGray;
            this.ppRoundProgressBar3.CircleWidth = 16;
            this.ppRoundProgressBar3.Direction = PPSkin.PPRoundProgressBar.DIRECTION.ClockWise;
            this.ppRoundProgressBar3.InsideColor = System.Drawing.Color.White;
            this.ppRoundProgressBar3.Location = new System.Drawing.Point(598, 155);
            this.ppRoundProgressBar3.Maximum = ((long)(100));
            this.ppRoundProgressBar3.Minimum = ((long)(0));
            this.ppRoundProgressBar3.Name = "ppRoundProgressBar3";
            this.ppRoundProgressBar3.ShowType = PPSkin.PPRoundProgressBar.SHOWType.RING;
            this.ppRoundProgressBar3.Size = new System.Drawing.Size(150, 150);
            this.ppRoundProgressBar3.StartAngle = 0;
            this.ppRoundProgressBar3.Step = ((long)(1));
            this.ppRoundProgressBar3.TabIndex = 47;
            this.ppRoundProgressBar3.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.ppRoundProgressBar3.Value = ((long)(50));
            this.ppRoundProgressBar3.ValueColor = System.Drawing.Color.DodgerBlue;
            this.ppRoundProgressBar3.ValueType = PPSkin.PPRoundProgressBar.VALUETYPE.PERCENT;
            // 
            // ppRoundProgressBar2
            // 
            this.ppRoundProgressBar2.BaseColor = System.Drawing.Color.LightGray;
            this.ppRoundProgressBar2.CircleWidth = 16;
            this.ppRoundProgressBar2.Direction = PPSkin.PPRoundProgressBar.DIRECTION.AntiClockWise;
            this.ppRoundProgressBar2.InsideColor = System.Drawing.Color.LightGray;
            this.ppRoundProgressBar2.Location = new System.Drawing.Point(442, 155);
            this.ppRoundProgressBar2.Maximum = ((long)(100));
            this.ppRoundProgressBar2.Minimum = ((long)(0));
            this.ppRoundProgressBar2.Name = "ppRoundProgressBar2";
            this.ppRoundProgressBar2.ShowType = PPSkin.PPRoundProgressBar.SHOWType.RING;
            this.ppRoundProgressBar2.Size = new System.Drawing.Size(150, 150);
            this.ppRoundProgressBar2.StartAngle = 0;
            this.ppRoundProgressBar2.Step = ((long)(1));
            this.ppRoundProgressBar2.TabIndex = 46;
            this.ppRoundProgressBar2.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.ppRoundProgressBar2.Value = ((long)(50));
            this.ppRoundProgressBar2.ValueColor = System.Drawing.Color.DodgerBlue;
            this.ppRoundProgressBar2.ValueType = PPSkin.PPRoundProgressBar.VALUETYPE.PERCENT;
            // 
            // ppRoundProgressBar1
            // 
            this.ppRoundProgressBar1.BaseColor = System.Drawing.Color.LightGray;
            this.ppRoundProgressBar1.CircleWidth = 16;
            this.ppRoundProgressBar1.Direction = PPSkin.PPRoundProgressBar.DIRECTION.ClockWise;
            this.ppRoundProgressBar1.InsideColor = System.Drawing.Color.LightGray;
            this.ppRoundProgressBar1.Location = new System.Drawing.Point(286, 155);
            this.ppRoundProgressBar1.Maximum = ((long)(100));
            this.ppRoundProgressBar1.Minimum = ((long)(0));
            this.ppRoundProgressBar1.Name = "ppRoundProgressBar1";
            this.ppRoundProgressBar1.ShowType = PPSkin.PPRoundProgressBar.SHOWType.RING;
            this.ppRoundProgressBar1.Size = new System.Drawing.Size(150, 150);
            this.ppRoundProgressBar1.StartAngle = 0;
            this.ppRoundProgressBar1.Step = ((long)(1));
            this.ppRoundProgressBar1.TabIndex = 45;
            this.ppRoundProgressBar1.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.ppRoundProgressBar1.Value = ((long)(50));
            this.ppRoundProgressBar1.ValueColor = System.Drawing.Color.DodgerBlue;
            this.ppRoundProgressBar1.ValueType = PPSkin.PPRoundProgressBar.VALUETYPE.PERCENT;
            // 
            // ppProgressBar3
            // 
            this.ppProgressBar3.BackColor = System.Drawing.Color.White;
            this.ppProgressBar3.BaseColor = System.Drawing.Color.LightGray;
            this.ppProgressBar3.Location = new System.Drawing.Point(286, 107);
            this.ppProgressBar3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ppProgressBar3.Maximum = ((long)(100));
            this.ppProgressBar3.Minimum = ((long)(0));
            this.ppProgressBar3.Name = "ppProgressBar3";
            this.ppProgressBar3.Radius = 32;
            this.ppProgressBar3.ShowTip = false;
            this.ppProgressBar3.ShowValue = true;
            this.ppProgressBar3.Size = new System.Drawing.Size(306, 32);
            this.ppProgressBar3.Step = ((long)(1));
            this.ppProgressBar3.TabIndex = 44;
            this.ppProgressBar3.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.ppProgressBar3.Value = ((long)(100));
            this.ppProgressBar3.ValueColor = System.Drawing.Color.DodgerBlue;
            this.ppProgressBar3.ValueType = PPSkin.PPProgressBar.VALUETYPE.VALUE;
            // 
            // ppProgressBar2
            // 
            this.ppProgressBar2.BackColor = System.Drawing.Color.White;
            this.ppProgressBar2.BaseColor = System.Drawing.Color.LightCoral;
            this.ppProgressBar2.Location = new System.Drawing.Point(286, 67);
            this.ppProgressBar2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ppProgressBar2.Maximum = ((long)(100));
            this.ppProgressBar2.Minimum = ((long)(0));
            this.ppProgressBar2.Name = "ppProgressBar2";
            this.ppProgressBar2.Radius = 10;
            this.ppProgressBar2.ShowTip = false;
            this.ppProgressBar2.ShowValue = true;
            this.ppProgressBar2.Size = new System.Drawing.Size(306, 32);
            this.ppProgressBar2.Step = ((long)(1));
            this.ppProgressBar2.TabIndex = 43;
            this.ppProgressBar2.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.ppProgressBar2.Value = ((long)(3));
            this.ppProgressBar2.ValueColor = System.Drawing.Color.DeepSkyBlue;
            this.ppProgressBar2.ValueType = PPSkin.PPProgressBar.VALUETYPE.PERCENT;
            // 
            // ppProgressBar1
            // 
            this.ppProgressBar1.BaseColor = System.Drawing.Color.LightGray;
            this.ppProgressBar1.Location = new System.Drawing.Point(286, 25);
            this.ppProgressBar1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ppProgressBar1.Maximum = ((long)(100));
            this.ppProgressBar1.Minimum = ((long)(0));
            this.ppProgressBar1.Name = "ppProgressBar1";
            this.ppProgressBar1.Radius = 0;
            this.ppProgressBar1.ShowTip = false;
            this.ppProgressBar1.ShowValue = true;
            this.ppProgressBar1.Size = new System.Drawing.Size(306, 32);
            this.ppProgressBar1.Step = ((long)(1));
            this.ppProgressBar1.TabIndex = 42;
            this.ppProgressBar1.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.ppProgressBar1.Value = ((long)(50));
            this.ppProgressBar1.ValueColor = System.Drawing.Color.DodgerBlue;
            this.ppProgressBar1.ValueType = PPSkin.PPProgressBar.VALUETYPE.PERCENT;
            // 
            // pphScrollBarExt3
            // 
            this.pphScrollBarExt3.BackColor = System.Drawing.Color.Transparent;
            this.pphScrollBarExt3.BaseColor = System.Drawing.Color.Gainsboro;
            this.pphScrollBarExt3.BaseRadius = 0;
            this.pphScrollBarExt3.BaseSize = 2;
            this.pphScrollBarExt3.ButtonColor = System.Drawing.Color.DarkTurquoise;
            this.pphScrollBarExt3.ButtonHoverColor = System.Drawing.Color.DeepSkyBlue;
            this.pphScrollBarExt3.ButtonRadius = 10;
            this.pphScrollBarExt3.ButtonSize = 10;
            this.pphScrollBarExt3.LargeChange = 10;
            this.pphScrollBarExt3.Location = new System.Drawing.Point(15, 516);
            this.pphScrollBarExt3.Maximum = 100;
            this.pphScrollBarExt3.Minimum = 0;
            this.pphScrollBarExt3.Name = "pphScrollBarExt3";
            this.pphScrollBarExt3.Size = new System.Drawing.Size(250, 20);
            this.pphScrollBarExt3.SmallChange = 1;
            this.pphScrollBarExt3.TabIndex = 41;
            this.pphScrollBarExt3.Value = 4;
            this.pphScrollBarExt3.VisibleValue = 10;
            // 
            // pphScrollBarExt2
            // 
            this.pphScrollBarExt2.BackColor = System.Drawing.Color.Transparent;
            this.pphScrollBarExt2.BaseColor = System.Drawing.Color.WhiteSmoke;
            this.pphScrollBarExt2.BaseRadius = 12;
            this.pphScrollBarExt2.BaseSize = 12;
            this.pphScrollBarExt2.ButtonColor = System.Drawing.Color.DodgerBlue;
            this.pphScrollBarExt2.ButtonHoverColor = System.Drawing.Color.DeepSkyBlue;
            this.pphScrollBarExt2.ButtonRadius = 10;
            this.pphScrollBarExt2.ButtonSize = 10;
            this.pphScrollBarExt2.LargeChange = 10;
            this.pphScrollBarExt2.Location = new System.Drawing.Point(15, 473);
            this.pphScrollBarExt2.Maximum = 100;
            this.pphScrollBarExt2.Minimum = 0;
            this.pphScrollBarExt2.Name = "pphScrollBarExt2";
            this.pphScrollBarExt2.Size = new System.Drawing.Size(250, 20);
            this.pphScrollBarExt2.SmallChange = 1;
            this.pphScrollBarExt2.TabIndex = 40;
            this.pphScrollBarExt2.Value = 2;
            this.pphScrollBarExt2.VisibleValue = 10;
            // 
            // pphScrollBarExt1
            // 
            this.pphScrollBarExt1.BackColor = System.Drawing.Color.Transparent;
            this.pphScrollBarExt1.BaseColor = System.Drawing.Color.Gainsboro;
            this.pphScrollBarExt1.BaseRadius = 0;
            this.pphScrollBarExt1.BaseSize = 2;
            this.pphScrollBarExt1.ButtonColor = System.Drawing.Color.DodgerBlue;
            this.pphScrollBarExt1.ButtonHoverColor = System.Drawing.Color.DeepSkyBlue;
            this.pphScrollBarExt1.ButtonRadius = 10;
            this.pphScrollBarExt1.ButtonSize = 10;
            this.pphScrollBarExt1.LargeChange = 10;
            this.pphScrollBarExt1.Location = new System.Drawing.Point(15, 434);
            this.pphScrollBarExt1.Maximum = 100;
            this.pphScrollBarExt1.Minimum = 0;
            this.pphScrollBarExt1.Name = "pphScrollBarExt1";
            this.pphScrollBarExt1.Size = new System.Drawing.Size(250, 20);
            this.pphScrollBarExt1.SmallChange = 1;
            this.pphScrollBarExt1.TabIndex = 39;
            this.pphScrollBarExt1.Value = 0;
            this.pphScrollBarExt1.VisibleValue = 10;
            // 
            // ppvScrollBarExt6
            // 
            this.ppvScrollBarExt6.BackColor = System.Drawing.Color.Transparent;
            this.ppvScrollBarExt6.BaseColor = System.Drawing.Color.Gray;
            this.ppvScrollBarExt6.BaseRadius = 0;
            this.ppvScrollBarExt6.BaseSize = 1;
            this.ppvScrollBarExt6.ButtonColor = System.Drawing.Color.DodgerBlue;
            this.ppvScrollBarExt6.ButtonHoverColor = System.Drawing.Color.DeepSkyBlue;
            this.ppvScrollBarExt6.ButtonRadius = 0;
            this.ppvScrollBarExt6.ButtonSize = 9;
            this.ppvScrollBarExt6.LargeChange = 10;
            this.ppvScrollBarExt6.Location = new System.Drawing.Point(245, 25);
            this.ppvScrollBarExt6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ppvScrollBarExt6.Maximum = 100;
            this.ppvScrollBarExt6.Minimum = 0;
            this.ppvScrollBarExt6.Name = "ppvScrollBarExt6";
            this.ppvScrollBarExt6.Size = new System.Drawing.Size(19, 402);
            this.ppvScrollBarExt6.SmallChange = 1;
            this.ppvScrollBarExt6.TabIndex = 38;
            this.ppvScrollBarExt6.Value = 10;
            this.ppvScrollBarExt6.VisibleValue = 10;
            // 
            // ppvScrollBarExt5
            // 
            this.ppvScrollBarExt5.BackColor = System.Drawing.Color.Transparent;
            this.ppvScrollBarExt5.BaseColor = System.Drawing.Color.Black;
            this.ppvScrollBarExt5.BaseRadius = 0;
            this.ppvScrollBarExt5.BaseSize = 2;
            this.ppvScrollBarExt5.ButtonColor = System.Drawing.Color.DodgerBlue;
            this.ppvScrollBarExt5.ButtonHoverColor = System.Drawing.Color.DeepSkyBlue;
            this.ppvScrollBarExt5.ButtonRadius = 10;
            this.ppvScrollBarExt5.ButtonSize = 10;
            this.ppvScrollBarExt5.LargeChange = 10;
            this.ppvScrollBarExt5.Location = new System.Drawing.Point(199, 25);
            this.ppvScrollBarExt5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ppvScrollBarExt5.Maximum = 100;
            this.ppvScrollBarExt5.Minimum = 0;
            this.ppvScrollBarExt5.Name = "ppvScrollBarExt5";
            this.ppvScrollBarExt5.Size = new System.Drawing.Size(20, 402);
            this.ppvScrollBarExt5.SmallChange = 1;
            this.ppvScrollBarExt5.TabIndex = 37;
            this.ppvScrollBarExt5.Value = 8;
            this.ppvScrollBarExt5.VisibleValue = 10;
            // 
            // ppvScrollBarExt4
            // 
            this.ppvScrollBarExt4.BackColor = System.Drawing.Color.Transparent;
            this.ppvScrollBarExt4.BaseColor = System.Drawing.Color.Gainsboro;
            this.ppvScrollBarExt4.BaseRadius = 0;
            this.ppvScrollBarExt4.BaseSize = 2;
            this.ppvScrollBarExt4.ButtonColor = System.Drawing.Color.Aqua;
            this.ppvScrollBarExt4.ButtonHoverColor = System.Drawing.Color.Magenta;
            this.ppvScrollBarExt4.ButtonRadius = 10;
            this.ppvScrollBarExt4.ButtonSize = 10;
            this.ppvScrollBarExt4.LargeChange = 10;
            this.ppvScrollBarExt4.Location = new System.Drawing.Point(154, 25);
            this.ppvScrollBarExt4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ppvScrollBarExt4.Maximum = 100;
            this.ppvScrollBarExt4.Minimum = 0;
            this.ppvScrollBarExt4.Name = "ppvScrollBarExt4";
            this.ppvScrollBarExt4.Size = new System.Drawing.Size(20, 402);
            this.ppvScrollBarExt4.SmallChange = 1;
            this.ppvScrollBarExt4.TabIndex = 36;
            this.ppvScrollBarExt4.Value = 6;
            this.ppvScrollBarExt4.VisibleValue = 10;
            // 
            // ppvScrollBarExt3
            // 
            this.ppvScrollBarExt3.BackColor = System.Drawing.Color.Transparent;
            this.ppvScrollBarExt3.BaseColor = System.Drawing.Color.Gainsboro;
            this.ppvScrollBarExt3.BaseRadius = 0;
            this.ppvScrollBarExt3.BaseSize = 2;
            this.ppvScrollBarExt3.ButtonColor = System.Drawing.Color.DarkTurquoise;
            this.ppvScrollBarExt3.ButtonHoverColor = System.Drawing.Color.DeepSkyBlue;
            this.ppvScrollBarExt3.ButtonRadius = 10;
            this.ppvScrollBarExt3.ButtonSize = 10;
            this.ppvScrollBarExt3.LargeChange = 10;
            this.ppvScrollBarExt3.Location = new System.Drawing.Point(107, 25);
            this.ppvScrollBarExt3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ppvScrollBarExt3.Maximum = 100;
            this.ppvScrollBarExt3.Minimum = 0;
            this.ppvScrollBarExt3.Name = "ppvScrollBarExt3";
            this.ppvScrollBarExt3.Size = new System.Drawing.Size(20, 402);
            this.ppvScrollBarExt3.SmallChange = 1;
            this.ppvScrollBarExt3.TabIndex = 35;
            this.ppvScrollBarExt3.Value = 4;
            this.ppvScrollBarExt3.VisibleValue = 10;
            // 
            // ppvScrollBarExt2
            // 
            this.ppvScrollBarExt2.BackColor = System.Drawing.Color.Transparent;
            this.ppvScrollBarExt2.BaseColor = System.Drawing.Color.WhiteSmoke;
            this.ppvScrollBarExt2.BaseRadius = 12;
            this.ppvScrollBarExt2.BaseSize = 12;
            this.ppvScrollBarExt2.ButtonColor = System.Drawing.Color.DodgerBlue;
            this.ppvScrollBarExt2.ButtonHoverColor = System.Drawing.Color.DeepSkyBlue;
            this.ppvScrollBarExt2.ButtonRadius = 10;
            this.ppvScrollBarExt2.ButtonSize = 10;
            this.ppvScrollBarExt2.LargeChange = 10;
            this.ppvScrollBarExt2.Location = new System.Drawing.Point(61, 25);
            this.ppvScrollBarExt2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ppvScrollBarExt2.Maximum = 100;
            this.ppvScrollBarExt2.Minimum = 0;
            this.ppvScrollBarExt2.Name = "ppvScrollBarExt2";
            this.ppvScrollBarExt2.Size = new System.Drawing.Size(20, 402);
            this.ppvScrollBarExt2.SmallChange = 1;
            this.ppvScrollBarExt2.TabIndex = 34;
            this.ppvScrollBarExt2.Value = 2;
            this.ppvScrollBarExt2.VisibleValue = 10;
            // 
            // ppvScrollBarExt1
            // 
            this.ppvScrollBarExt1.BackColor = System.Drawing.Color.Transparent;
            this.ppvScrollBarExt1.BaseColor = System.Drawing.Color.Gainsboro;
            this.ppvScrollBarExt1.BaseRadius = 0;
            this.ppvScrollBarExt1.BaseSize = 2;
            this.ppvScrollBarExt1.ButtonColor = System.Drawing.Color.DodgerBlue;
            this.ppvScrollBarExt1.ButtonHoverColor = System.Drawing.Color.DeepSkyBlue;
            this.ppvScrollBarExt1.ButtonRadius = 10;
            this.ppvScrollBarExt1.ButtonSize = 10;
            this.ppvScrollBarExt1.LargeChange = 10;
            this.ppvScrollBarExt1.Location = new System.Drawing.Point(15, 25);
            this.ppvScrollBarExt1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ppvScrollBarExt1.Maximum = 100;
            this.ppvScrollBarExt1.Minimum = 0;
            this.ppvScrollBarExt1.Name = "ppvScrollBarExt1";
            this.ppvScrollBarExt1.Size = new System.Drawing.Size(20, 402);
            this.ppvScrollBarExt1.SmallChange = 1;
            this.ppvScrollBarExt1.TabIndex = 33;
            this.ppvScrollBarExt1.Value = 0;
            this.ppvScrollBarExt1.VisibleValue = 10;
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.Color.White;
            this.tabPage4.Controls.Add(this.flowLayoutPanel1);
            this.tabPage4.Controls.Add(this.ppGroupBox5);
            this.tabPage4.Controls.Add(this.ppGroupBox4);
            this.tabPage4.Controls.Add(this.ppGroupBox3);
            this.tabPage4.Location = new System.Drawing.Point(0, 30);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(928, 675);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "容器";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoScroll = true;
            this.flowLayoutPanel1.Controls.Add(this.ppFlodPanel1);
            this.flowLayoutPanel1.Controls.Add(this.ppFlodPanel2);
            this.flowLayoutPanel1.Controls.Add(this.ppFlodPanel3);
            this.flowLayoutPanel1.Controls.Add(this.ppFlodPanel4);
            this.flowLayoutPanel1.Controls.Add(this.ppFlodPanel5);
            this.flowLayoutPanel1.Controls.Add(this.ppFlodPanel6);
            this.flowLayoutPanel1.Controls.Add(this.ppFlodPanel7);
            this.flowLayoutPanel1.Controls.Add(this.ppFlodPanel8);
            this.flowLayoutPanel1.Controls.Add(this.ppFlodPanel9);
            this.flowLayoutPanel1.Controls.Add(this.ppFlodPanel10);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(485, 345);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(440, 327);
            this.flowLayoutPanel1.TabIndex = 4;
            // 
            // ppFlodPanel1
            // 
            this.ppFlodPanel1.AnimeTime = 100;
            this.ppFlodPanel1.ArrowHoverImg = ((System.Drawing.Image)(resources.GetObject("ppFlodPanel1.ArrowHoverImg")));
            this.ppFlodPanel1.ArrowImg = ((System.Drawing.Image)(resources.GetObject("ppFlodPanel1.ArrowImg")));
            this.ppFlodPanel1.ArrowOffset = new System.Drawing.Point(-5, 0);
            this.ppFlodPanel1.ArrowSize = new System.Drawing.Size(13, 13);
            this.ppFlodPanel1.BorderColor = System.Drawing.Color.DodgerBlue;
            this.ppFlodPanel1.Controls.Add(this.ppButton42);
            this.ppFlodPanel1.FlodHeight = 106;
            this.ppFlodPanel1.IsFold = false;
            this.ppFlodPanel1.Location = new System.Drawing.Point(1, 1);
            this.ppFlodPanel1.Margin = new System.Windows.Forms.Padding(1);
            this.ppFlodPanel1.Name = "ppFlodPanel1";
            this.ppFlodPanel1.Radius = 0;
            this.ppFlodPanel1.ShowArrow = false;
            this.ppFlodPanel1.Size = new System.Drawing.Size(161, 106);
            this.ppFlodPanel1.TabIndex = 3;
            this.ppFlodPanel1.TitleBaseColor = System.Drawing.Color.LightGray;
            this.ppFlodPanel1.TitleBorderColor = System.Drawing.Color.Red;
            this.ppFlodPanel1.TitleHeight = 30;
            this.ppFlodPanel1.UseAnime = true;
            // 
            // ppButton42
            // 
            this.ppButton42.BackColor = System.Drawing.Color.White;
            this.ppButton42.BaseColor = System.Drawing.Color.White;
            this.ppButton42.DialogResult = System.Windows.Forms.DialogResult.None;
            this.ppButton42.IcoDown = null;
            this.ppButton42.IcoIn = null;
            this.ppButton42.IcoRegular = null;
            this.ppButton42.IcoSize = new System.Drawing.Size(15, 15);
            this.ppButton42.LineColor = System.Drawing.Color.LightGray;
            this.ppButton42.LineWidth = 1;
            this.ppButton42.Location = new System.Drawing.Point(1, 30);
            this.ppButton42.MouseDownBaseColor = System.Drawing.Color.LightGray;
            this.ppButton42.MouseDownLineColor = System.Drawing.Color.Silver;
            this.ppButton42.MouseDownTextColor = System.Drawing.Color.Black;
            this.ppButton42.MouseInBaseColor = System.Drawing.Color.LightGray;
            this.ppButton42.MouseInLineColor = System.Drawing.Color.Silver;
            this.ppButton42.MouseInTextColor = System.Drawing.Color.Black;
            this.ppButton42.Name = "ppButton42";
            this.ppButton42.Radius = 3;
            this.ppButton42.RegularBaseColor = System.Drawing.Color.White;
            this.ppButton42.RegularLineColor = System.Drawing.Color.LightGray;
            this.ppButton42.RegularTextColor = System.Drawing.Color.Black;
            this.ppButton42.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppButton42.Size = new System.Drawing.Size(75, 30);
            this.ppButton42.TabIndex = 1;
            this.ppButton42.Text = "ppButton42";
            this.ppButton42.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.ppButton42.TextColor = System.Drawing.Color.Black;
            this.ppButton42.TextDrawMode = PPSkin.Enums.DrawMode.Anti;
            this.ppButton42.TipAutoCloseTime = 5000;
            this.ppButton42.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppButton42.TipCloseOnLeave = true;
            this.ppButton42.TipDeviation = new System.Drawing.Size(0, 0);
            this.ppButton42.TipFontSize = 10;
            this.ppButton42.TipFontText = null;
            this.ppButton42.TipForeColor = System.Drawing.Color.White;
            this.ppButton42.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.ppButton42.TipText = "aaaa";
            this.ppButton42.TipTopMoust = true;
            this.ppButton42.UseSpecial = true;
            this.ppButton42.UseVisualStyleBackColor = true;
            this.ppButton42.Xoffset = 0;
            this.ppButton42.XoffsetIco = 0;
            this.ppButton42.Yoffset = 0;
            this.ppButton42.YoffsetIco = 0;
            // 
            // ppFlodPanel2
            // 
            this.ppFlodPanel2.AnimeTime = 100;
            this.ppFlodPanel2.ArrowHoverImg = ((System.Drawing.Image)(resources.GetObject("ppFlodPanel2.ArrowHoverImg")));
            this.ppFlodPanel2.ArrowImg = ((System.Drawing.Image)(resources.GetObject("ppFlodPanel2.ArrowImg")));
            this.ppFlodPanel2.ArrowOffset = new System.Drawing.Point(-5, 0);
            this.ppFlodPanel2.ArrowSize = new System.Drawing.Size(13, 13);
            this.ppFlodPanel2.BorderColor = System.Drawing.Color.DodgerBlue;
            this.ppFlodPanel2.Controls.Add(this.ppButton44);
            this.ppFlodPanel2.FlodHeight = 106;
            this.ppFlodPanel2.IsFold = false;
            this.ppFlodPanel2.Location = new System.Drawing.Point(164, 1);
            this.ppFlodPanel2.Margin = new System.Windows.Forms.Padding(1);
            this.ppFlodPanel2.Name = "ppFlodPanel2";
            this.ppFlodPanel2.Radius = 0;
            this.ppFlodPanel2.ShowArrow = false;
            this.ppFlodPanel2.Size = new System.Drawing.Size(161, 106);
            this.ppFlodPanel2.TabIndex = 4;
            this.ppFlodPanel2.TitleBaseColor = System.Drawing.Color.LightGray;
            this.ppFlodPanel2.TitleBorderColor = System.Drawing.Color.Red;
            this.ppFlodPanel2.TitleHeight = 30;
            this.ppFlodPanel2.UseAnime = true;
            // 
            // ppButton44
            // 
            this.ppButton44.BackColor = System.Drawing.Color.White;
            this.ppButton44.BaseColor = System.Drawing.Color.White;
            this.ppButton44.DialogResult = System.Windows.Forms.DialogResult.None;
            this.ppButton44.IcoDown = null;
            this.ppButton44.IcoIn = null;
            this.ppButton44.IcoRegular = null;
            this.ppButton44.IcoSize = new System.Drawing.Size(15, 15);
            this.ppButton44.LineColor = System.Drawing.Color.LightGray;
            this.ppButton44.LineWidth = 1;
            this.ppButton44.Location = new System.Drawing.Point(16, 48);
            this.ppButton44.MouseDownBaseColor = System.Drawing.Color.LightGray;
            this.ppButton44.MouseDownLineColor = System.Drawing.Color.Silver;
            this.ppButton44.MouseDownTextColor = System.Drawing.Color.Black;
            this.ppButton44.MouseInBaseColor = System.Drawing.Color.LightGray;
            this.ppButton44.MouseInLineColor = System.Drawing.Color.Silver;
            this.ppButton44.MouseInTextColor = System.Drawing.Color.Black;
            this.ppButton44.Name = "ppButton44";
            this.ppButton44.Radius = 3;
            this.ppButton44.RegularBaseColor = System.Drawing.Color.White;
            this.ppButton44.RegularLineColor = System.Drawing.Color.LightGray;
            this.ppButton44.RegularTextColor = System.Drawing.Color.Black;
            this.ppButton44.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppButton44.Size = new System.Drawing.Size(75, 30);
            this.ppButton44.TabIndex = 1;
            this.ppButton44.Text = "ppButton44";
            this.ppButton44.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.ppButton44.TextColor = System.Drawing.Color.Black;
            this.ppButton44.TextDrawMode = PPSkin.Enums.DrawMode.Anti;
            this.ppButton44.TipAutoCloseTime = 5000;
            this.ppButton44.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppButton44.TipCloseOnLeave = true;
            this.ppButton44.TipDeviation = new System.Drawing.Size(0, 0);
            this.ppButton44.TipFontSize = 10;
            this.ppButton44.TipFontText = null;
            this.ppButton44.TipForeColor = System.Drawing.Color.White;
            this.ppButton44.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.ppButton44.TipText = "aaaa";
            this.ppButton44.TipTopMoust = true;
            this.ppButton44.UseSpecial = true;
            this.ppButton44.UseVisualStyleBackColor = true;
            this.ppButton44.Xoffset = 0;
            this.ppButton44.XoffsetIco = 0;
            this.ppButton44.Yoffset = 0;
            this.ppButton44.YoffsetIco = 0;
            // 
            // ppFlodPanel3
            // 
            this.ppFlodPanel3.AnimeTime = 100;
            this.ppFlodPanel3.ArrowHoverImg = ((System.Drawing.Image)(resources.GetObject("ppFlodPanel3.ArrowHoverImg")));
            this.ppFlodPanel3.ArrowImg = ((System.Drawing.Image)(resources.GetObject("ppFlodPanel3.ArrowImg")));
            this.ppFlodPanel3.ArrowOffset = new System.Drawing.Point(-5, 0);
            this.ppFlodPanel3.ArrowSize = new System.Drawing.Size(13, 13);
            this.ppFlodPanel3.BorderColor = System.Drawing.Color.DodgerBlue;
            this.ppFlodPanel3.Controls.Add(this.ppButton45);
            this.ppFlodPanel3.FlodHeight = 106;
            this.ppFlodPanel3.IsFold = false;
            this.ppFlodPanel3.Location = new System.Drawing.Point(1, 109);
            this.ppFlodPanel3.Margin = new System.Windows.Forms.Padding(1);
            this.ppFlodPanel3.Name = "ppFlodPanel3";
            this.ppFlodPanel3.Radius = 10;
            this.ppFlodPanel3.ShowArrow = false;
            this.ppFlodPanel3.Size = new System.Drawing.Size(161, 106);
            this.ppFlodPanel3.TabIndex = 5;
            this.ppFlodPanel3.TitleBaseColor = System.Drawing.Color.LightGray;
            this.ppFlodPanel3.TitleBorderColor = System.Drawing.Color.Red;
            this.ppFlodPanel3.TitleHeight = 30;
            this.ppFlodPanel3.UseAnime = true;
            // 
            // ppButton45
            // 
            this.ppButton45.BackColor = System.Drawing.Color.White;
            this.ppButton45.BaseColor = System.Drawing.Color.White;
            this.ppButton45.DialogResult = System.Windows.Forms.DialogResult.None;
            this.ppButton45.IcoDown = null;
            this.ppButton45.IcoIn = null;
            this.ppButton45.IcoRegular = null;
            this.ppButton45.IcoSize = new System.Drawing.Size(15, 15);
            this.ppButton45.LineColor = System.Drawing.Color.LightGray;
            this.ppButton45.LineWidth = 1;
            this.ppButton45.Location = new System.Drawing.Point(16, 48);
            this.ppButton45.MouseDownBaseColor = System.Drawing.Color.LightGray;
            this.ppButton45.MouseDownLineColor = System.Drawing.Color.Silver;
            this.ppButton45.MouseDownTextColor = System.Drawing.Color.Black;
            this.ppButton45.MouseInBaseColor = System.Drawing.Color.LightGray;
            this.ppButton45.MouseInLineColor = System.Drawing.Color.Silver;
            this.ppButton45.MouseInTextColor = System.Drawing.Color.Black;
            this.ppButton45.Name = "ppButton45";
            this.ppButton45.Radius = 3;
            this.ppButton45.RegularBaseColor = System.Drawing.Color.White;
            this.ppButton45.RegularLineColor = System.Drawing.Color.LightGray;
            this.ppButton45.RegularTextColor = System.Drawing.Color.Black;
            this.ppButton45.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppButton45.Size = new System.Drawing.Size(75, 30);
            this.ppButton45.TabIndex = 1;
            this.ppButton45.Text = "ppButton45";
            this.ppButton45.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.ppButton45.TextColor = System.Drawing.Color.Black;
            this.ppButton45.TextDrawMode = PPSkin.Enums.DrawMode.Anti;
            this.ppButton45.TipAutoCloseTime = 5000;
            this.ppButton45.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppButton45.TipCloseOnLeave = true;
            this.ppButton45.TipDeviation = new System.Drawing.Size(0, 0);
            this.ppButton45.TipFontSize = 10;
            this.ppButton45.TipFontText = null;
            this.ppButton45.TipForeColor = System.Drawing.Color.White;
            this.ppButton45.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.ppButton45.TipText = "aaaa";
            this.ppButton45.TipTopMoust = true;
            this.ppButton45.UseSpecial = true;
            this.ppButton45.UseVisualStyleBackColor = true;
            this.ppButton45.Xoffset = 0;
            this.ppButton45.XoffsetIco = 0;
            this.ppButton45.Yoffset = 0;
            this.ppButton45.YoffsetIco = 0;
            // 
            // ppFlodPanel4
            // 
            this.ppFlodPanel4.AnimeTime = 100;
            this.ppFlodPanel4.ArrowHoverImg = ((System.Drawing.Image)(resources.GetObject("ppFlodPanel4.ArrowHoverImg")));
            this.ppFlodPanel4.ArrowImg = ((System.Drawing.Image)(resources.GetObject("ppFlodPanel4.ArrowImg")));
            this.ppFlodPanel4.ArrowOffset = new System.Drawing.Point(-5, 0);
            this.ppFlodPanel4.ArrowSize = new System.Drawing.Size(13, 13);
            this.ppFlodPanel4.BorderColor = System.Drawing.Color.DodgerBlue;
            this.ppFlodPanel4.Controls.Add(this.ppButton46);
            this.ppFlodPanel4.FlodHeight = 106;
            this.ppFlodPanel4.IsFold = false;
            this.ppFlodPanel4.Location = new System.Drawing.Point(164, 109);
            this.ppFlodPanel4.Margin = new System.Windows.Forms.Padding(1);
            this.ppFlodPanel4.Name = "ppFlodPanel4";
            this.ppFlodPanel4.Radius = 10;
            this.ppFlodPanel4.ShowArrow = false;
            this.ppFlodPanel4.Size = new System.Drawing.Size(161, 106);
            this.ppFlodPanel4.TabIndex = 6;
            this.ppFlodPanel4.TitleBaseColor = System.Drawing.Color.LightGray;
            this.ppFlodPanel4.TitleBorderColor = System.Drawing.Color.Red;
            this.ppFlodPanel4.TitleHeight = 30;
            this.ppFlodPanel4.UseAnime = true;
            // 
            // ppButton46
            // 
            this.ppButton46.BackColor = System.Drawing.Color.White;
            this.ppButton46.BaseColor = System.Drawing.Color.White;
            this.ppButton46.DialogResult = System.Windows.Forms.DialogResult.None;
            this.ppButton46.IcoDown = null;
            this.ppButton46.IcoIn = null;
            this.ppButton46.IcoRegular = null;
            this.ppButton46.IcoSize = new System.Drawing.Size(15, 15);
            this.ppButton46.LineColor = System.Drawing.Color.LightGray;
            this.ppButton46.LineWidth = 1;
            this.ppButton46.Location = new System.Drawing.Point(16, 48);
            this.ppButton46.MouseDownBaseColor = System.Drawing.Color.LightGray;
            this.ppButton46.MouseDownLineColor = System.Drawing.Color.Silver;
            this.ppButton46.MouseDownTextColor = System.Drawing.Color.Black;
            this.ppButton46.MouseInBaseColor = System.Drawing.Color.LightGray;
            this.ppButton46.MouseInLineColor = System.Drawing.Color.Silver;
            this.ppButton46.MouseInTextColor = System.Drawing.Color.Black;
            this.ppButton46.Name = "ppButton46";
            this.ppButton46.Radius = 3;
            this.ppButton46.RegularBaseColor = System.Drawing.Color.White;
            this.ppButton46.RegularLineColor = System.Drawing.Color.LightGray;
            this.ppButton46.RegularTextColor = System.Drawing.Color.Black;
            this.ppButton46.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppButton46.Size = new System.Drawing.Size(75, 30);
            this.ppButton46.TabIndex = 1;
            this.ppButton46.Text = "ppButton46";
            this.ppButton46.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.ppButton46.TextColor = System.Drawing.Color.Black;
            this.ppButton46.TextDrawMode = PPSkin.Enums.DrawMode.Anti;
            this.ppButton46.TipAutoCloseTime = 5000;
            this.ppButton46.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppButton46.TipCloseOnLeave = true;
            this.ppButton46.TipDeviation = new System.Drawing.Size(0, 0);
            this.ppButton46.TipFontSize = 10;
            this.ppButton46.TipFontText = null;
            this.ppButton46.TipForeColor = System.Drawing.Color.White;
            this.ppButton46.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.ppButton46.TipText = "aaaa";
            this.ppButton46.TipTopMoust = true;
            this.ppButton46.UseSpecial = true;
            this.ppButton46.UseVisualStyleBackColor = true;
            this.ppButton46.Xoffset = 0;
            this.ppButton46.XoffsetIco = 0;
            this.ppButton46.Yoffset = 0;
            this.ppButton46.YoffsetIco = 0;
            // 
            // ppFlodPanel5
            // 
            this.ppFlodPanel5.AnimeTime = 100;
            this.ppFlodPanel5.ArrowHoverImg = ((System.Drawing.Image)(resources.GetObject("ppFlodPanel5.ArrowHoverImg")));
            this.ppFlodPanel5.ArrowImg = ((System.Drawing.Image)(resources.GetObject("ppFlodPanel5.ArrowImg")));
            this.ppFlodPanel5.ArrowOffset = new System.Drawing.Point(-5, 0);
            this.ppFlodPanel5.ArrowSize = new System.Drawing.Size(13, 13);
            this.ppFlodPanel5.BorderColor = System.Drawing.Color.DodgerBlue;
            this.ppFlodPanel5.Controls.Add(this.ppButton47);
            this.ppFlodPanel5.FlodHeight = 106;
            this.ppFlodPanel5.IsFold = false;
            this.ppFlodPanel5.Location = new System.Drawing.Point(1, 217);
            this.ppFlodPanel5.Margin = new System.Windows.Forms.Padding(1);
            this.ppFlodPanel5.Name = "ppFlodPanel5";
            this.ppFlodPanel5.Radius = 0;
            this.ppFlodPanel5.ShowArrow = false;
            this.ppFlodPanel5.Size = new System.Drawing.Size(161, 106);
            this.ppFlodPanel5.TabIndex = 7;
            this.ppFlodPanel5.TitleBaseColor = System.Drawing.Color.LightGray;
            this.ppFlodPanel5.TitleBorderColor = System.Drawing.Color.Red;
            this.ppFlodPanel5.TitleHeight = 30;
            this.ppFlodPanel5.UseAnime = true;
            // 
            // ppButton47
            // 
            this.ppButton47.BackColor = System.Drawing.Color.White;
            this.ppButton47.BaseColor = System.Drawing.Color.White;
            this.ppButton47.DialogResult = System.Windows.Forms.DialogResult.None;
            this.ppButton47.IcoDown = null;
            this.ppButton47.IcoIn = null;
            this.ppButton47.IcoRegular = null;
            this.ppButton47.IcoSize = new System.Drawing.Size(15, 15);
            this.ppButton47.LineColor = System.Drawing.Color.LightGray;
            this.ppButton47.LineWidth = 1;
            this.ppButton47.Location = new System.Drawing.Point(16, 48);
            this.ppButton47.MouseDownBaseColor = System.Drawing.Color.LightGray;
            this.ppButton47.MouseDownLineColor = System.Drawing.Color.Silver;
            this.ppButton47.MouseDownTextColor = System.Drawing.Color.Black;
            this.ppButton47.MouseInBaseColor = System.Drawing.Color.LightGray;
            this.ppButton47.MouseInLineColor = System.Drawing.Color.Silver;
            this.ppButton47.MouseInTextColor = System.Drawing.Color.Black;
            this.ppButton47.Name = "ppButton47";
            this.ppButton47.Radius = 3;
            this.ppButton47.RegularBaseColor = System.Drawing.Color.White;
            this.ppButton47.RegularLineColor = System.Drawing.Color.LightGray;
            this.ppButton47.RegularTextColor = System.Drawing.Color.Black;
            this.ppButton47.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppButton47.Size = new System.Drawing.Size(75, 30);
            this.ppButton47.TabIndex = 1;
            this.ppButton47.Text = "ppButton47";
            this.ppButton47.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.ppButton47.TextColor = System.Drawing.Color.Black;
            this.ppButton47.TextDrawMode = PPSkin.Enums.DrawMode.Anti;
            this.ppButton47.TipAutoCloseTime = 5000;
            this.ppButton47.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppButton47.TipCloseOnLeave = true;
            this.ppButton47.TipDeviation = new System.Drawing.Size(0, 0);
            this.ppButton47.TipFontSize = 10;
            this.ppButton47.TipFontText = null;
            this.ppButton47.TipForeColor = System.Drawing.Color.White;
            this.ppButton47.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.ppButton47.TipText = "aaaa";
            this.ppButton47.TipTopMoust = true;
            this.ppButton47.UseSpecial = true;
            this.ppButton47.UseVisualStyleBackColor = true;
            this.ppButton47.Xoffset = 0;
            this.ppButton47.XoffsetIco = 0;
            this.ppButton47.Yoffset = 0;
            this.ppButton47.YoffsetIco = 0;
            // 
            // ppFlodPanel6
            // 
            this.ppFlodPanel6.AnimeTime = 100;
            this.ppFlodPanel6.ArrowHoverImg = ((System.Drawing.Image)(resources.GetObject("ppFlodPanel6.ArrowHoverImg")));
            this.ppFlodPanel6.ArrowImg = ((System.Drawing.Image)(resources.GetObject("ppFlodPanel6.ArrowImg")));
            this.ppFlodPanel6.ArrowOffset = new System.Drawing.Point(-5, 0);
            this.ppFlodPanel6.ArrowSize = new System.Drawing.Size(13, 13);
            this.ppFlodPanel6.BorderColor = System.Drawing.Color.DodgerBlue;
            this.ppFlodPanel6.Controls.Add(this.ppButton48);
            this.ppFlodPanel6.FlodHeight = 106;
            this.ppFlodPanel6.IsFold = false;
            this.ppFlodPanel6.Location = new System.Drawing.Point(164, 217);
            this.ppFlodPanel6.Margin = new System.Windows.Forms.Padding(1);
            this.ppFlodPanel6.Name = "ppFlodPanel6";
            this.ppFlodPanel6.Radius = 0;
            this.ppFlodPanel6.ShowArrow = false;
            this.ppFlodPanel6.Size = new System.Drawing.Size(161, 106);
            this.ppFlodPanel6.TabIndex = 8;
            this.ppFlodPanel6.TitleBaseColor = System.Drawing.Color.LightGray;
            this.ppFlodPanel6.TitleBorderColor = System.Drawing.Color.Red;
            this.ppFlodPanel6.TitleHeight = 30;
            this.ppFlodPanel6.UseAnime = true;
            // 
            // ppButton48
            // 
            this.ppButton48.BackColor = System.Drawing.Color.White;
            this.ppButton48.BaseColor = System.Drawing.Color.White;
            this.ppButton48.DialogResult = System.Windows.Forms.DialogResult.None;
            this.ppButton48.IcoDown = null;
            this.ppButton48.IcoIn = null;
            this.ppButton48.IcoRegular = null;
            this.ppButton48.IcoSize = new System.Drawing.Size(15, 15);
            this.ppButton48.LineColor = System.Drawing.Color.LightGray;
            this.ppButton48.LineWidth = 1;
            this.ppButton48.Location = new System.Drawing.Point(16, 48);
            this.ppButton48.MouseDownBaseColor = System.Drawing.Color.LightGray;
            this.ppButton48.MouseDownLineColor = System.Drawing.Color.Silver;
            this.ppButton48.MouseDownTextColor = System.Drawing.Color.Black;
            this.ppButton48.MouseInBaseColor = System.Drawing.Color.LightGray;
            this.ppButton48.MouseInLineColor = System.Drawing.Color.Silver;
            this.ppButton48.MouseInTextColor = System.Drawing.Color.Black;
            this.ppButton48.Name = "ppButton48";
            this.ppButton48.Radius = 3;
            this.ppButton48.RegularBaseColor = System.Drawing.Color.White;
            this.ppButton48.RegularLineColor = System.Drawing.Color.LightGray;
            this.ppButton48.RegularTextColor = System.Drawing.Color.Black;
            this.ppButton48.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppButton48.Size = new System.Drawing.Size(75, 30);
            this.ppButton48.TabIndex = 1;
            this.ppButton48.Text = "ppButton48";
            this.ppButton48.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.ppButton48.TextColor = System.Drawing.Color.Black;
            this.ppButton48.TextDrawMode = PPSkin.Enums.DrawMode.Anti;
            this.ppButton48.TipAutoCloseTime = 5000;
            this.ppButton48.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppButton48.TipCloseOnLeave = true;
            this.ppButton48.TipDeviation = new System.Drawing.Size(0, 0);
            this.ppButton48.TipFontSize = 10;
            this.ppButton48.TipFontText = null;
            this.ppButton48.TipForeColor = System.Drawing.Color.White;
            this.ppButton48.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.ppButton48.TipText = "aaaa";
            this.ppButton48.TipTopMoust = true;
            this.ppButton48.UseSpecial = true;
            this.ppButton48.UseVisualStyleBackColor = true;
            this.ppButton48.Xoffset = 0;
            this.ppButton48.XoffsetIco = 0;
            this.ppButton48.Yoffset = 0;
            this.ppButton48.YoffsetIco = 0;
            // 
            // ppFlodPanel7
            // 
            this.ppFlodPanel7.AnimeTime = 100;
            this.ppFlodPanel7.ArrowHoverImg = ((System.Drawing.Image)(resources.GetObject("ppFlodPanel7.ArrowHoverImg")));
            this.ppFlodPanel7.ArrowImg = ((System.Drawing.Image)(resources.GetObject("ppFlodPanel7.ArrowImg")));
            this.ppFlodPanel7.ArrowOffset = new System.Drawing.Point(-5, 0);
            this.ppFlodPanel7.ArrowSize = new System.Drawing.Size(13, 13);
            this.ppFlodPanel7.BorderColor = System.Drawing.Color.DodgerBlue;
            this.ppFlodPanel7.Controls.Add(this.ppButton49);
            this.ppFlodPanel7.FlodHeight = 106;
            this.ppFlodPanel7.IsFold = false;
            this.ppFlodPanel7.Location = new System.Drawing.Point(1, 325);
            this.ppFlodPanel7.Margin = new System.Windows.Forms.Padding(1);
            this.ppFlodPanel7.Name = "ppFlodPanel7";
            this.ppFlodPanel7.Radius = 0;
            this.ppFlodPanel7.ShowArrow = false;
            this.ppFlodPanel7.Size = new System.Drawing.Size(161, 106);
            this.ppFlodPanel7.TabIndex = 9;
            this.ppFlodPanel7.TitleBaseColor = System.Drawing.Color.LightGray;
            this.ppFlodPanel7.TitleBorderColor = System.Drawing.Color.Red;
            this.ppFlodPanel7.TitleHeight = 30;
            this.ppFlodPanel7.UseAnime = true;
            // 
            // ppButton49
            // 
            this.ppButton49.BackColor = System.Drawing.Color.White;
            this.ppButton49.BaseColor = System.Drawing.Color.White;
            this.ppButton49.DialogResult = System.Windows.Forms.DialogResult.None;
            this.ppButton49.IcoDown = null;
            this.ppButton49.IcoIn = null;
            this.ppButton49.IcoRegular = null;
            this.ppButton49.IcoSize = new System.Drawing.Size(15, 15);
            this.ppButton49.LineColor = System.Drawing.Color.LightGray;
            this.ppButton49.LineWidth = 1;
            this.ppButton49.Location = new System.Drawing.Point(16, 48);
            this.ppButton49.MouseDownBaseColor = System.Drawing.Color.LightGray;
            this.ppButton49.MouseDownLineColor = System.Drawing.Color.Silver;
            this.ppButton49.MouseDownTextColor = System.Drawing.Color.Black;
            this.ppButton49.MouseInBaseColor = System.Drawing.Color.LightGray;
            this.ppButton49.MouseInLineColor = System.Drawing.Color.Silver;
            this.ppButton49.MouseInTextColor = System.Drawing.Color.Black;
            this.ppButton49.Name = "ppButton49";
            this.ppButton49.Radius = 3;
            this.ppButton49.RegularBaseColor = System.Drawing.Color.White;
            this.ppButton49.RegularLineColor = System.Drawing.Color.LightGray;
            this.ppButton49.RegularTextColor = System.Drawing.Color.Black;
            this.ppButton49.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppButton49.Size = new System.Drawing.Size(75, 30);
            this.ppButton49.TabIndex = 1;
            this.ppButton49.Text = "ppButton49";
            this.ppButton49.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.ppButton49.TextColor = System.Drawing.Color.Black;
            this.ppButton49.TextDrawMode = PPSkin.Enums.DrawMode.Anti;
            this.ppButton49.TipAutoCloseTime = 5000;
            this.ppButton49.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppButton49.TipCloseOnLeave = true;
            this.ppButton49.TipDeviation = new System.Drawing.Size(0, 0);
            this.ppButton49.TipFontSize = 10;
            this.ppButton49.TipFontText = null;
            this.ppButton49.TipForeColor = System.Drawing.Color.White;
            this.ppButton49.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.ppButton49.TipText = "aaaa";
            this.ppButton49.TipTopMoust = true;
            this.ppButton49.UseSpecial = true;
            this.ppButton49.UseVisualStyleBackColor = true;
            this.ppButton49.Xoffset = 0;
            this.ppButton49.XoffsetIco = 0;
            this.ppButton49.Yoffset = 0;
            this.ppButton49.YoffsetIco = 0;
            // 
            // ppFlodPanel8
            // 
            this.ppFlodPanel8.AnimeTime = 100;
            this.ppFlodPanel8.ArrowHoverImg = ((System.Drawing.Image)(resources.GetObject("ppFlodPanel8.ArrowHoverImg")));
            this.ppFlodPanel8.ArrowImg = ((System.Drawing.Image)(resources.GetObject("ppFlodPanel8.ArrowImg")));
            this.ppFlodPanel8.ArrowOffset = new System.Drawing.Point(-5, 0);
            this.ppFlodPanel8.ArrowSize = new System.Drawing.Size(13, 13);
            this.ppFlodPanel8.BorderColor = System.Drawing.Color.DodgerBlue;
            this.ppFlodPanel8.Controls.Add(this.ppButton50);
            this.ppFlodPanel8.FlodHeight = 106;
            this.ppFlodPanel8.IsFold = false;
            this.ppFlodPanel8.Location = new System.Drawing.Point(164, 325);
            this.ppFlodPanel8.Margin = new System.Windows.Forms.Padding(1);
            this.ppFlodPanel8.Name = "ppFlodPanel8";
            this.ppFlodPanel8.Radius = 0;
            this.ppFlodPanel8.ShowArrow = false;
            this.ppFlodPanel8.Size = new System.Drawing.Size(161, 106);
            this.ppFlodPanel8.TabIndex = 10;
            this.ppFlodPanel8.TitleBaseColor = System.Drawing.Color.LightGray;
            this.ppFlodPanel8.TitleBorderColor = System.Drawing.Color.Red;
            this.ppFlodPanel8.TitleHeight = 30;
            this.ppFlodPanel8.UseAnime = true;
            // 
            // ppButton50
            // 
            this.ppButton50.BackColor = System.Drawing.Color.White;
            this.ppButton50.BaseColor = System.Drawing.Color.White;
            this.ppButton50.DialogResult = System.Windows.Forms.DialogResult.None;
            this.ppButton50.IcoDown = null;
            this.ppButton50.IcoIn = null;
            this.ppButton50.IcoRegular = null;
            this.ppButton50.IcoSize = new System.Drawing.Size(15, 15);
            this.ppButton50.LineColor = System.Drawing.Color.LightGray;
            this.ppButton50.LineWidth = 1;
            this.ppButton50.Location = new System.Drawing.Point(16, 48);
            this.ppButton50.MouseDownBaseColor = System.Drawing.Color.LightGray;
            this.ppButton50.MouseDownLineColor = System.Drawing.Color.Silver;
            this.ppButton50.MouseDownTextColor = System.Drawing.Color.Black;
            this.ppButton50.MouseInBaseColor = System.Drawing.Color.LightGray;
            this.ppButton50.MouseInLineColor = System.Drawing.Color.Silver;
            this.ppButton50.MouseInTextColor = System.Drawing.Color.Black;
            this.ppButton50.Name = "ppButton50";
            this.ppButton50.Radius = 3;
            this.ppButton50.RegularBaseColor = System.Drawing.Color.White;
            this.ppButton50.RegularLineColor = System.Drawing.Color.LightGray;
            this.ppButton50.RegularTextColor = System.Drawing.Color.Black;
            this.ppButton50.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppButton50.Size = new System.Drawing.Size(75, 30);
            this.ppButton50.TabIndex = 1;
            this.ppButton50.Text = "ppButton50";
            this.ppButton50.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.ppButton50.TextColor = System.Drawing.Color.Black;
            this.ppButton50.TextDrawMode = PPSkin.Enums.DrawMode.Anti;
            this.ppButton50.TipAutoCloseTime = 5000;
            this.ppButton50.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppButton50.TipCloseOnLeave = true;
            this.ppButton50.TipDeviation = new System.Drawing.Size(0, 0);
            this.ppButton50.TipFontSize = 10;
            this.ppButton50.TipFontText = null;
            this.ppButton50.TipForeColor = System.Drawing.Color.White;
            this.ppButton50.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.ppButton50.TipText = "aaaa";
            this.ppButton50.TipTopMoust = true;
            this.ppButton50.UseSpecial = true;
            this.ppButton50.UseVisualStyleBackColor = true;
            this.ppButton50.Xoffset = 0;
            this.ppButton50.XoffsetIco = 0;
            this.ppButton50.Yoffset = 0;
            this.ppButton50.YoffsetIco = 0;
            // 
            // ppFlodPanel9
            // 
            this.ppFlodPanel9.AnimeTime = 100;
            this.ppFlodPanel9.ArrowHoverImg = ((System.Drawing.Image)(resources.GetObject("ppFlodPanel9.ArrowHoverImg")));
            this.ppFlodPanel9.ArrowImg = ((System.Drawing.Image)(resources.GetObject("ppFlodPanel9.ArrowImg")));
            this.ppFlodPanel9.ArrowOffset = new System.Drawing.Point(-5, 0);
            this.ppFlodPanel9.ArrowSize = new System.Drawing.Size(13, 13);
            this.ppFlodPanel9.BorderColor = System.Drawing.Color.DodgerBlue;
            this.ppFlodPanel9.Controls.Add(this.ppButton51);
            this.ppFlodPanel9.FlodHeight = 106;
            this.ppFlodPanel9.IsFold = false;
            this.ppFlodPanel9.Location = new System.Drawing.Point(1, 433);
            this.ppFlodPanel9.Margin = new System.Windows.Forms.Padding(1);
            this.ppFlodPanel9.Name = "ppFlodPanel9";
            this.ppFlodPanel9.Radius = 0;
            this.ppFlodPanel9.ShowArrow = false;
            this.ppFlodPanel9.Size = new System.Drawing.Size(161, 106);
            this.ppFlodPanel9.TabIndex = 11;
            this.ppFlodPanel9.TitleBaseColor = System.Drawing.Color.LightGray;
            this.ppFlodPanel9.TitleBorderColor = System.Drawing.Color.Red;
            this.ppFlodPanel9.TitleHeight = 30;
            this.ppFlodPanel9.UseAnime = true;
            // 
            // ppButton51
            // 
            this.ppButton51.BackColor = System.Drawing.Color.White;
            this.ppButton51.BaseColor = System.Drawing.Color.White;
            this.ppButton51.DialogResult = System.Windows.Forms.DialogResult.None;
            this.ppButton51.IcoDown = null;
            this.ppButton51.IcoIn = null;
            this.ppButton51.IcoRegular = null;
            this.ppButton51.IcoSize = new System.Drawing.Size(15, 15);
            this.ppButton51.LineColor = System.Drawing.Color.LightGray;
            this.ppButton51.LineWidth = 1;
            this.ppButton51.Location = new System.Drawing.Point(16, 48);
            this.ppButton51.MouseDownBaseColor = System.Drawing.Color.LightGray;
            this.ppButton51.MouseDownLineColor = System.Drawing.Color.Silver;
            this.ppButton51.MouseDownTextColor = System.Drawing.Color.Black;
            this.ppButton51.MouseInBaseColor = System.Drawing.Color.LightGray;
            this.ppButton51.MouseInLineColor = System.Drawing.Color.Silver;
            this.ppButton51.MouseInTextColor = System.Drawing.Color.Black;
            this.ppButton51.Name = "ppButton51";
            this.ppButton51.Radius = 3;
            this.ppButton51.RegularBaseColor = System.Drawing.Color.White;
            this.ppButton51.RegularLineColor = System.Drawing.Color.LightGray;
            this.ppButton51.RegularTextColor = System.Drawing.Color.Black;
            this.ppButton51.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppButton51.Size = new System.Drawing.Size(75, 30);
            this.ppButton51.TabIndex = 1;
            this.ppButton51.Text = "ppButton51";
            this.ppButton51.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.ppButton51.TextColor = System.Drawing.Color.Black;
            this.ppButton51.TextDrawMode = PPSkin.Enums.DrawMode.Anti;
            this.ppButton51.TipAutoCloseTime = 5000;
            this.ppButton51.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppButton51.TipCloseOnLeave = true;
            this.ppButton51.TipDeviation = new System.Drawing.Size(0, 0);
            this.ppButton51.TipFontSize = 10;
            this.ppButton51.TipFontText = null;
            this.ppButton51.TipForeColor = System.Drawing.Color.White;
            this.ppButton51.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.ppButton51.TipText = "aaaa";
            this.ppButton51.TipTopMoust = true;
            this.ppButton51.UseSpecial = true;
            this.ppButton51.UseVisualStyleBackColor = true;
            this.ppButton51.Xoffset = 0;
            this.ppButton51.XoffsetIco = 0;
            this.ppButton51.Yoffset = 0;
            this.ppButton51.YoffsetIco = 0;
            // 
            // ppFlodPanel10
            // 
            this.ppFlodPanel10.AnimeTime = 100;
            this.ppFlodPanel10.ArrowHoverImg = ((System.Drawing.Image)(resources.GetObject("ppFlodPanel10.ArrowHoverImg")));
            this.ppFlodPanel10.ArrowImg = ((System.Drawing.Image)(resources.GetObject("ppFlodPanel10.ArrowImg")));
            this.ppFlodPanel10.ArrowOffset = new System.Drawing.Point(-5, 0);
            this.ppFlodPanel10.ArrowSize = new System.Drawing.Size(13, 13);
            this.ppFlodPanel10.BorderColor = System.Drawing.Color.DodgerBlue;
            this.ppFlodPanel10.Controls.Add(this.ppButton52);
            this.ppFlodPanel10.FlodHeight = 106;
            this.ppFlodPanel10.IsFold = false;
            this.ppFlodPanel10.Location = new System.Drawing.Point(164, 433);
            this.ppFlodPanel10.Margin = new System.Windows.Forms.Padding(1);
            this.ppFlodPanel10.Name = "ppFlodPanel10";
            this.ppFlodPanel10.Radius = 0;
            this.ppFlodPanel10.ShowArrow = false;
            this.ppFlodPanel10.Size = new System.Drawing.Size(161, 106);
            this.ppFlodPanel10.TabIndex = 12;
            this.ppFlodPanel10.TitleBaseColor = System.Drawing.Color.LightGray;
            this.ppFlodPanel10.TitleBorderColor = System.Drawing.Color.Red;
            this.ppFlodPanel10.TitleHeight = 30;
            this.ppFlodPanel10.UseAnime = true;
            // 
            // ppButton52
            // 
            this.ppButton52.BackColor = System.Drawing.Color.White;
            this.ppButton52.BaseColor = System.Drawing.Color.White;
            this.ppButton52.DialogResult = System.Windows.Forms.DialogResult.None;
            this.ppButton52.IcoDown = null;
            this.ppButton52.IcoIn = null;
            this.ppButton52.IcoRegular = null;
            this.ppButton52.IcoSize = new System.Drawing.Size(15, 15);
            this.ppButton52.LineColor = System.Drawing.Color.LightGray;
            this.ppButton52.LineWidth = 1;
            this.ppButton52.Location = new System.Drawing.Point(16, 48);
            this.ppButton52.MouseDownBaseColor = System.Drawing.Color.LightGray;
            this.ppButton52.MouseDownLineColor = System.Drawing.Color.Silver;
            this.ppButton52.MouseDownTextColor = System.Drawing.Color.Black;
            this.ppButton52.MouseInBaseColor = System.Drawing.Color.LightGray;
            this.ppButton52.MouseInLineColor = System.Drawing.Color.Silver;
            this.ppButton52.MouseInTextColor = System.Drawing.Color.Black;
            this.ppButton52.Name = "ppButton52";
            this.ppButton52.Radius = 3;
            this.ppButton52.RegularBaseColor = System.Drawing.Color.White;
            this.ppButton52.RegularLineColor = System.Drawing.Color.LightGray;
            this.ppButton52.RegularTextColor = System.Drawing.Color.Black;
            this.ppButton52.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppButton52.Size = new System.Drawing.Size(75, 30);
            this.ppButton52.TabIndex = 1;
            this.ppButton52.Text = "ppButton52";
            this.ppButton52.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.ppButton52.TextColor = System.Drawing.Color.Black;
            this.ppButton52.TextDrawMode = PPSkin.Enums.DrawMode.Anti;
            this.ppButton52.TipAutoCloseTime = 5000;
            this.ppButton52.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppButton52.TipCloseOnLeave = true;
            this.ppButton52.TipDeviation = new System.Drawing.Size(0, 0);
            this.ppButton52.TipFontSize = 10;
            this.ppButton52.TipFontText = null;
            this.ppButton52.TipForeColor = System.Drawing.Color.White;
            this.ppButton52.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.ppButton52.TipText = "aaaa";
            this.ppButton52.TipTopMoust = true;
            this.ppButton52.UseSpecial = true;
            this.ppButton52.UseVisualStyleBackColor = true;
            this.ppButton52.Xoffset = 0;
            this.ppButton52.XoffsetIco = 0;
            this.ppButton52.Yoffset = 0;
            this.ppButton52.YoffsetIco = 0;
            // 
            // ppGroupBox5
            // 
            this.ppGroupBox5.BackColor = System.Drawing.Color.White;
            this.ppGroupBox5.BaseColor = System.Drawing.Color.White;
            this.ppGroupBox5.BorderColor = System.Drawing.Color.Gray;
            this.ppGroupBox5.BorderWidth = 1;
            this.ppGroupBox5.Controls.Add(this.ppMovePanel1);
            this.ppGroupBox5.Location = new System.Drawing.Point(6, 345);
            this.ppGroupBox5.Name = "ppGroupBox5";
            this.ppGroupBox5.Radius = 10;
            this.ppGroupBox5.Size = new System.Drawing.Size(473, 327);
            this.ppGroupBox5.TabIndex = 2;
            this.ppGroupBox5.TabStop = false;
            this.ppGroupBox5.Text = "可移动Panel";
            this.ppGroupBox5.TextAlignment = System.Drawing.StringAlignment.Center;
            this.ppGroupBox5.TextDrawMode = PPSkin.PPGroupBox.DrawMode.Clear;
            this.ppGroupBox5.TilteOffset = 0;
            this.ppGroupBox5.TitleBaseColor = System.Drawing.Color.White;
            this.ppGroupBox5.TitleBorderColor = System.Drawing.Color.Gray;
            this.ppGroupBox5.TitleRadius = 10;
            // 
            // ppMovePanel1
            // 
            this.ppMovePanel1.AutoScaleMode = System.Windows.Forms.AutoSizeMode.GrowOnly;
            this.ppMovePanel1.BackColor = System.Drawing.SystemColors.Control;
            this.ppMovePanel1.ChildLockStr = "MoveLock";
            this.ppMovePanel1.Controls.Add(this.ppCheckBox21);
            this.ppMovePanel1.Controls.Add(this.ppComboboxEx5);
            this.ppMovePanel1.Controls.Add(this.ppPanel7);
            this.ppMovePanel1.Controls.Add(this.ppButton31);
            this.ppMovePanel1.Location = new System.Drawing.Point(18, 34);
            this.ppMovePanel1.MoveLock = false;
            this.ppMovePanel1.Name = "ppMovePanel1";
            this.ppMovePanel1.ResolutionX = 1920;
            this.ppMovePanel1.ResolutionY = 1080;
            this.ppMovePanel1.Size = new System.Drawing.Size(443, 282);
            this.ppMovePanel1.TabIndex = 12;
            // 
            // ppCheckBox21
            // 
            this.ppCheckBox21.BaseColor = System.Drawing.Color.Gray;
            this.ppCheckBox21.ButtonColor = System.Drawing.Color.White;
            this.ppCheckBox21.ButtonRadius = 25;
            this.ppCheckBox21.buttonRectHeight = 25;
            this.ppCheckBox21.buttonRectWidth = 25;
            this.ppCheckBox21.buttonRectX = 4;
            this.ppCheckBox21.buttonRectY = 4;
            this.ppCheckBox21.Checked = false;
            this.ppCheckBox21.CheckedBaseColor = System.Drawing.Color.DodgerBlue;
            this.ppCheckBox21.CheckedColor = System.Drawing.Color.Red;
            this.ppCheckBox21.CheckedImage = null;
            this.ppCheckBox21.CheckedText = "Lock";
            this.ppCheckBox21.ImageSize = new System.Drawing.Size(15, 15);
            this.ppCheckBox21.Location = new System.Drawing.Point(3, 3);
            this.ppCheckBox21.Name = "ppCheckBox21";
            this.ppCheckBox21.Radius = 30;
            this.ppCheckBox21.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppCheckBox21.Size = new System.Drawing.Size(82, 33);
            this.ppCheckBox21.TabIndex = 2;
            this.ppCheckBox21.Tag = "MoveLock";
            this.ppCheckBox21.TextDrawMode = PPSkin.Enums.DrawMode.Anti;
            this.ppCheckBox21.UnCheckedBaseColor = System.Drawing.Color.Gray;
            this.ppCheckBox21.UnCheckedColor = System.Drawing.Color.White;
            this.ppCheckBox21.UnCheckedImage = null;
            this.ppCheckBox21.UnCheckedText = "UnLock";
            this.ppCheckBox21.UseAnimation = false;
            this.ppCheckBox21.Xoffset = 0;
            this.ppCheckBox21.Yoffset = 0;
            this.ppCheckBox21.CheckedChanged += new System.EventHandler(this.ppCheckBox21_CheckedChanged);
            // 
            // ppComboboxEx5
            // 
            this.ppComboboxEx5.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ppComboboxEx5.BaseColor = System.Drawing.Color.White;
            this.ppComboboxEx5.BorderColor = System.Drawing.Color.LightGray;
            this.ppComboboxEx5.BorderWidth = 1F;
            this.ppComboboxEx5.DropbaseColor = System.Drawing.Color.White;
            this.ppComboboxEx5.DropborderColor = System.Drawing.Color.LightGray;
            this.ppComboboxEx5.DropbuttonColor = System.Drawing.Color.White;
            this.ppComboboxEx5.DropbuttonMouseDownColor = System.Drawing.Color.DimGray;
            this.ppComboboxEx5.DropbuttonMouseInColor = System.Drawing.Color.LightGray;
            this.ppComboboxEx5.DropbuttonRadius = 5;
            this.ppComboboxEx5.DropbuttonSelectColor = System.Drawing.Color.DimGray;
            this.ppComboboxEx5.DropformRadius = 5;
            this.ppComboboxEx5.DropTextAlign = PPSkin.PPComboboxEx.Align.LEFT;
            this.ppComboboxEx5.Location = new System.Drawing.Point(3, 55);
            this.ppComboboxEx5.MaxDropDownItems = 8;
            this.ppComboboxEx5.Name = "ppComboboxEx5";
            this.ppComboboxEx5.Radius = 5;
            this.ppComboboxEx5.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppComboboxEx5.SelectedIndex = -1;
            this.ppComboboxEx5.SelectedItem = null;
            this.ppComboboxEx5.SelectedText = "";
            this.ppComboboxEx5.SelectedValue = null;
            this.ppComboboxEx5.Size = new System.Drawing.Size(167, 33);
            this.ppComboboxEx5.TabIndex = 0;
            this.ppComboboxEx5.TextAlign = PPSkin.PPComboboxEx.Align.LEFT;
            this.ppComboboxEx5.TextDrawMode = PPSkin.Enums.DrawMode.Anti;
            // 
            // ppPanel7
            // 
            this.ppPanel7.BaseColor = System.Drawing.Color.White;
            this.ppPanel7.BorderColor = System.Drawing.Color.LightGray;
            this.ppPanel7.BorderWidth = 1;
            this.ppPanel7.Location = new System.Drawing.Point(224, 14);
            this.ppPanel7.Name = "ppPanel7";
            this.ppPanel7.Radius = 5;
            this.ppPanel7.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppPanel7.ShowBorder = true;
            this.ppPanel7.Size = new System.Drawing.Size(200, 98);
            this.ppPanel7.TabIndex = 1;
            // 
            // ppButton31
            // 
            this.ppButton31.BackColor = System.Drawing.Color.White;
            this.ppButton31.BaseColor = System.Drawing.Color.White;
            this.ppButton31.DialogResult = System.Windows.Forms.DialogResult.None;
            this.ppButton31.IcoDown = null;
            this.ppButton31.IcoIn = null;
            this.ppButton31.IcoRegular = null;
            this.ppButton31.IcoSize = new System.Drawing.Size(15, 15);
            this.ppButton31.LineColor = System.Drawing.Color.LightGray;
            this.ppButton31.LineWidth = 1;
            this.ppButton31.Location = new System.Drawing.Point(39, 159);
            this.ppButton31.MouseDownBaseColor = System.Drawing.Color.LightGray;
            this.ppButton31.MouseDownLineColor = System.Drawing.Color.Silver;
            this.ppButton31.MouseDownTextColor = System.Drawing.Color.Black;
            this.ppButton31.MouseInBaseColor = System.Drawing.Color.LightGray;
            this.ppButton31.MouseInLineColor = System.Drawing.Color.Silver;
            this.ppButton31.MouseInTextColor = System.Drawing.Color.Black;
            this.ppButton31.Name = "ppButton31";
            this.ppButton31.Radius = 3;
            this.ppButton31.RegularBaseColor = System.Drawing.Color.White;
            this.ppButton31.RegularLineColor = System.Drawing.Color.LightGray;
            this.ppButton31.RegularTextColor = System.Drawing.Color.Black;
            this.ppButton31.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppButton31.Size = new System.Drawing.Size(156, 46);
            this.ppButton31.TabIndex = 0;
            this.ppButton31.Text = "Button";
            this.ppButton31.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.ppButton31.TextColor = System.Drawing.Color.Black;
            this.ppButton31.TextDrawMode = PPSkin.Enums.DrawMode.Anti;
            this.ppButton31.TipAutoCloseTime = 5000;
            this.ppButton31.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppButton31.TipCloseOnLeave = true;
            this.ppButton31.TipDeviation = new System.Drawing.Size(0, 0);
            this.ppButton31.TipFontSize = 10;
            this.ppButton31.TipFontText = null;
            this.ppButton31.TipForeColor = System.Drawing.Color.White;
            this.ppButton31.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.ppButton31.TipText = null;
            this.ppButton31.TipTopMoust = true;
            this.ppButton31.UseSpecial = true;
            this.ppButton31.UseVisualStyleBackColor = false;
            this.ppButton31.Xoffset = 0;
            this.ppButton31.XoffsetIco = 0;
            this.ppButton31.Yoffset = 0;
            this.ppButton31.YoffsetIco = 0;
            // 
            // ppGroupBox4
            // 
            this.ppGroupBox4.BackColor = System.Drawing.Color.White;
            this.ppGroupBox4.BaseColor = System.Drawing.Color.White;
            this.ppGroupBox4.BorderColor = System.Drawing.Color.Gray;
            this.ppGroupBox4.BorderWidth = 1;
            this.ppGroupBox4.Controls.Add(this.ppPanel4);
            this.ppGroupBox4.Controls.Add(this.ppPanel3);
            this.ppGroupBox4.Controls.Add(this.ppPanel5);
            this.ppGroupBox4.Controls.Add(this.ppPanel6);
            this.ppGroupBox4.Location = new System.Drawing.Point(462, 3);
            this.ppGroupBox4.Name = "ppGroupBox4";
            this.ppGroupBox4.Radius = 10;
            this.ppGroupBox4.Size = new System.Drawing.Size(464, 336);
            this.ppGroupBox4.TabIndex = 1;
            this.ppGroupBox4.TabStop = false;
            this.ppGroupBox4.Text = "Panel";
            this.ppGroupBox4.TextAlignment = System.Drawing.StringAlignment.Center;
            this.ppGroupBox4.TextDrawMode = PPSkin.PPGroupBox.DrawMode.Clear;
            this.ppGroupBox4.TilteOffset = 0;
            this.ppGroupBox4.TitleBaseColor = System.Drawing.Color.White;
            this.ppGroupBox4.TitleBorderColor = System.Drawing.Color.Gray;
            this.ppGroupBox4.TitleRadius = 10;
            // 
            // ppPanel4
            // 
            this.ppPanel4.BaseColor = System.Drawing.Color.White;
            this.ppPanel4.BorderColor = System.Drawing.Color.LightGray;
            this.ppPanel4.BorderWidth = 3;
            this.ppPanel4.Location = new System.Drawing.Point(267, 186);
            this.ppPanel4.Name = "ppPanel4";
            this.ppPanel4.Radius = 10;
            this.ppPanel4.RoundStyle = PPSkin.Enums.RoundStyle.BottomRight;
            this.ppPanel4.ShowBorder = true;
            this.ppPanel4.Size = new System.Drawing.Size(178, 135);
            this.ppPanel4.TabIndex = 13;
            // 
            // ppPanel3
            // 
            this.ppPanel3.BaseColor = System.Drawing.Color.White;
            this.ppPanel3.BorderColor = System.Drawing.Color.LightGray;
            this.ppPanel3.BorderWidth = 1;
            this.ppPanel3.Location = new System.Drawing.Point(20, 186);
            this.ppPanel3.Name = "ppPanel3";
            this.ppPanel3.Radius = 10;
            this.ppPanel3.RoundStyle = PPSkin.Enums.RoundStyle.BottomLeft;
            this.ppPanel3.ShowBorder = true;
            this.ppPanel3.Size = new System.Drawing.Size(178, 135);
            this.ppPanel3.TabIndex = 12;
            // 
            // ppPanel5
            // 
            this.ppPanel5.BaseColor = System.Drawing.Color.White;
            this.ppPanel5.BorderColor = System.Drawing.Color.LightGray;
            this.ppPanel5.BorderWidth = 1;
            this.ppPanel5.Location = new System.Drawing.Point(267, 34);
            this.ppPanel5.Name = "ppPanel5";
            this.ppPanel5.Radius = 10;
            this.ppPanel5.RoundStyle = PPSkin.Enums.RoundStyle.TopRight;
            this.ppPanel5.ShowBorder = true;
            this.ppPanel5.Size = new System.Drawing.Size(178, 135);
            this.ppPanel5.TabIndex = 11;
            // 
            // ppPanel6
            // 
            this.ppPanel6.BaseColor = System.Drawing.SystemColors.Control;
            this.ppPanel6.BorderColor = System.Drawing.Color.LightGray;
            this.ppPanel6.BorderWidth = 1;
            this.ppPanel6.Location = new System.Drawing.Point(20, 34);
            this.ppPanel6.Name = "ppPanel6";
            this.ppPanel6.Radius = 5;
            this.ppPanel6.RoundStyle = PPSkin.Enums.RoundStyle.TopLeft;
            this.ppPanel6.ShowBorder = true;
            this.ppPanel6.Size = new System.Drawing.Size(178, 135);
            this.ppPanel6.TabIndex = 10;
            // 
            // ppGroupBox3
            // 
            this.ppGroupBox3.BackColor = System.Drawing.Color.White;
            this.ppGroupBox3.BaseColor = System.Drawing.Color.White;
            this.ppGroupBox3.BorderColor = System.Drawing.Color.Gray;
            this.ppGroupBox3.BorderWidth = 1;
            this.ppGroupBox3.Controls.Add(this.ppGroupBox7);
            this.ppGroupBox3.Controls.Add(this.ppGroupBox6);
            this.ppGroupBox3.Controls.Add(this.ppGroupBox8);
            this.ppGroupBox3.Controls.Add(this.ppGroupBox9);
            this.ppGroupBox3.Location = new System.Drawing.Point(6, 3);
            this.ppGroupBox3.Name = "ppGroupBox3";
            this.ppGroupBox3.Radius = 10;
            this.ppGroupBox3.Size = new System.Drawing.Size(433, 336);
            this.ppGroupBox3.TabIndex = 0;
            this.ppGroupBox3.TabStop = false;
            this.ppGroupBox3.Text = "GroupBox";
            this.ppGroupBox3.TextAlignment = System.Drawing.StringAlignment.Center;
            this.ppGroupBox3.TextDrawMode = PPSkin.PPGroupBox.DrawMode.Clear;
            this.ppGroupBox3.TilteOffset = 0;
            this.ppGroupBox3.TitleBaseColor = System.Drawing.Color.White;
            this.ppGroupBox3.TitleBorderColor = System.Drawing.Color.Gray;
            this.ppGroupBox3.TitleRadius = 10;
            // 
            // ppGroupBox7
            // 
            this.ppGroupBox7.BackColor = System.Drawing.Color.White;
            this.ppGroupBox7.BaseColor = System.Drawing.Color.LightCoral;
            this.ppGroupBox7.BorderColor = System.Drawing.Color.Red;
            this.ppGroupBox7.BorderWidth = 1;
            this.ppGroupBox7.Location = new System.Drawing.Point(225, 186);
            this.ppGroupBox7.Name = "ppGroupBox7";
            this.ppGroupBox7.Radius = 10;
            this.ppGroupBox7.Size = new System.Drawing.Size(178, 125);
            this.ppGroupBox7.TabIndex = 9;
            this.ppGroupBox7.TabStop = false;
            this.ppGroupBox7.Text = "PPGroupbox";
            this.ppGroupBox7.TextAlignment = System.Drawing.StringAlignment.Center;
            this.ppGroupBox7.TextDrawMode = PPSkin.PPGroupBox.DrawMode.Clear;
            this.ppGroupBox7.TilteOffset = 0;
            this.ppGroupBox7.TitleBaseColor = System.Drawing.Color.Coral;
            this.ppGroupBox7.TitleBorderColor = System.Drawing.Color.Chartreuse;
            this.ppGroupBox7.TitleRadius = 10;
            // 
            // ppGroupBox6
            // 
            this.ppGroupBox6.BackColor = System.Drawing.Color.White;
            this.ppGroupBox6.BaseColor = System.Drawing.Color.White;
            this.ppGroupBox6.BorderColor = System.Drawing.Color.Red;
            this.ppGroupBox6.BorderWidth = 4;
            this.ppGroupBox6.Location = new System.Drawing.Point(18, 186);
            this.ppGroupBox6.Name = "ppGroupBox6";
            this.ppGroupBox6.Radius = 10;
            this.ppGroupBox6.Size = new System.Drawing.Size(178, 125);
            this.ppGroupBox6.TabIndex = 8;
            this.ppGroupBox6.TabStop = false;
            this.ppGroupBox6.Text = "PPGroupbox";
            this.ppGroupBox6.TextAlignment = System.Drawing.StringAlignment.Center;
            this.ppGroupBox6.TextDrawMode = PPSkin.PPGroupBox.DrawMode.Clear;
            this.ppGroupBox6.TilteOffset = 0;
            this.ppGroupBox6.TitleBaseColor = System.Drawing.Color.White;
            this.ppGroupBox6.TitleBorderColor = System.Drawing.Color.Chartreuse;
            this.ppGroupBox6.TitleRadius = 10;
            // 
            // ppGroupBox8
            // 
            this.ppGroupBox8.BackColor = System.Drawing.Color.White;
            this.ppGroupBox8.BaseColor = System.Drawing.Color.White;
            this.ppGroupBox8.BorderColor = System.Drawing.Color.Gray;
            this.ppGroupBox8.BorderWidth = 1;
            this.ppGroupBox8.Location = new System.Drawing.Point(225, 44);
            this.ppGroupBox8.Name = "ppGroupBox8";
            this.ppGroupBox8.Radius = 0;
            this.ppGroupBox8.Size = new System.Drawing.Size(178, 125);
            this.ppGroupBox8.TabIndex = 7;
            this.ppGroupBox8.TabStop = false;
            this.ppGroupBox8.Text = "PPGroupbox";
            this.ppGroupBox8.TextAlignment = System.Drawing.StringAlignment.Center;
            this.ppGroupBox8.TextDrawMode = PPSkin.PPGroupBox.DrawMode.Clear;
            this.ppGroupBox8.TilteOffset = 0;
            this.ppGroupBox8.TitleBaseColor = System.Drawing.Color.White;
            this.ppGroupBox8.TitleBorderColor = System.Drawing.Color.Gray;
            this.ppGroupBox8.TitleRadius = 0;
            // 
            // ppGroupBox9
            // 
            this.ppGroupBox9.BackColor = System.Drawing.Color.White;
            this.ppGroupBox9.BaseColor = System.Drawing.Color.White;
            this.ppGroupBox9.BorderColor = System.Drawing.Color.Black;
            this.ppGroupBox9.BorderWidth = 1;
            this.ppGroupBox9.Location = new System.Drawing.Point(18, 44);
            this.ppGroupBox9.Name = "ppGroupBox9";
            this.ppGroupBox9.Radius = 10;
            this.ppGroupBox9.Size = new System.Drawing.Size(178, 125);
            this.ppGroupBox9.TabIndex = 6;
            this.ppGroupBox9.TabStop = false;
            this.ppGroupBox9.Text = "PPGroupbox";
            this.ppGroupBox9.TextAlignment = System.Drawing.StringAlignment.Center;
            this.ppGroupBox9.TextDrawMode = PPSkin.PPGroupBox.DrawMode.Clear;
            this.ppGroupBox9.TilteOffset = 0;
            this.ppGroupBox9.TitleBaseColor = System.Drawing.Color.White;
            this.ppGroupBox9.TitleBorderColor = System.Drawing.Color.Gray;
            this.ppGroupBox9.TitleRadius = 10;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.ppTreeView3);
            this.tabPage5.Controls.Add(this.ppTreeView1);
            this.tabPage5.Controls.Add(this.ppTreeView2);
            this.tabPage5.Location = new System.Drawing.Point(0, 30);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(928, 675);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "树";
            // 
            // ppTreeView3
            // 
            this.ppTreeView3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.ppTreeView3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.ppTreeView3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ppTreeView3.CheckBoxes = true;
            this.ppTreeView3.Cursor = System.Windows.Forms.Cursors.Default;
            this.ppTreeView3.DrawMode = System.Windows.Forms.TreeViewDrawMode.OwnerDrawAll;
            this.ppTreeView3.HotTracking = true;
            this.ppTreeView3.IconOffset = new System.Drawing.Point(0, 0);
            this.ppTreeView3.IconSize = new System.Drawing.Size(26, 26);
            this.ppTreeView3.ItemHeight = 30;
            this.ppTreeView3.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.ppTreeView3.Location = new System.Drawing.Point(615, 3);
            this.ppTreeView3.Name = "ppTreeView3";
            this.ppTreeView3.NodeBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.ppTreeView3.NodeBorderColor = System.Drawing.Color.Gray;
            this.ppTreeView3.NodeFont = new System.Drawing.Font("微软雅黑", 11F);
            this.ppTreeView3.NodeMouseInBackColor = System.Drawing.Color.SkyBlue;
            treeNode64.Name = "节点19";
            treeNode64.Text = "节点19";
            treeNode65.Name = "节点20";
            treeNode65.Text = "节点20";
            treeNode66.ImageIndex = 1;
            treeNode66.Name = "节点5";
            treeNode66.Text = "节点5";
            treeNode67.Name = "节点6";
            treeNode67.Text = "节点6";
            treeNode68.ImageIndex = 0;
            treeNode68.Name = "节点0";
            treeNode68.Text = "节点0";
            treeNode69.Name = "节点7";
            treeNode69.Text = "节点7";
            treeNode70.Name = "节点8";
            treeNode70.Text = "节点8";
            treeNode71.Name = "节点9";
            treeNode71.Text = "节点9";
            treeNode72.ImageIndex = 1;
            treeNode72.Name = "节点1";
            treeNode72.Text = "节点1";
            treeNode73.Name = "节点13";
            treeNode73.Text = "节点13";
            treeNode74.Name = "节点2";
            treeNode74.Text = "节点2";
            treeNode75.Name = "节点10";
            treeNode75.Text = "节点10";
            treeNode76.Name = "节点16";
            treeNode76.Text = "节点16";
            treeNode77.Name = "节点17";
            treeNode77.Text = "节点17";
            treeNode78.Name = "节点18";
            treeNode78.Text = "节点18";
            treeNode79.Name = "节点11";
            treeNode79.Text = "节点11";
            treeNode80.Name = "节点12";
            treeNode80.Text = "节点12";
            treeNode81.Name = "节点3";
            treeNode81.Text = "节点3";
            treeNode82.Name = "节点14";
            treeNode82.Text = "节点14";
            treeNode83.Name = "节点15";
            treeNode83.Text = "节点15";
            treeNode84.Name = "节点4";
            treeNode84.Text = "节点4";
            this.ppTreeView3.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode68,
            treeNode72,
            treeNode74,
            treeNode81,
            treeNode84});
            this.ppTreeView3.NodeSelectBackColor = System.Drawing.Color.DeepSkyBlue;
            this.ppTreeView3.NodeSelectBarColor = System.Drawing.Color.Magenta;
            this.ppTreeView3.NodeSelectBorderColor = System.Drawing.Color.Gray;
            this.ppTreeView3.NodeSelectTextColor = System.Drawing.Color.Aqua;
            this.ppTreeView3.NodeTextColor = System.Drawing.Color.Red;
            this.ppTreeView3.PPImageList = null;
            this.ppTreeView3.RootBackColor = System.Drawing.Color.DarkSalmon;
            this.ppTreeView3.RootFont = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ppTreeView3.RootTextColor = System.Drawing.Color.DarkTurquoise;
            this.ppTreeView3.ShowCheckBox = true;
            this.ppTreeView3.ShowPlusMinus = false;
            this.ppTreeView3.Size = new System.Drawing.Size(197, 644);
            this.ppTreeView3.TabIndex = 7;
            this.ppTreeView3.TextDrawMode = PPSkin.PPTreeView.drawMode.Clear;
            // 
            // ppTreeView1
            // 
            this.ppTreeView1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.ppTreeView1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.ppTreeView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ppTreeView1.CheckBoxes = true;
            this.ppTreeView1.Cursor = System.Windows.Forms.Cursors.Default;
            this.ppTreeView1.DrawMode = System.Windows.Forms.TreeViewDrawMode.OwnerDrawAll;
            this.ppTreeView1.HotTracking = true;
            this.ppTreeView1.IconOffset = new System.Drawing.Point(0, 0);
            this.ppTreeView1.IconSize = new System.Drawing.Size(26, 26);
            this.ppTreeView1.ItemHeight = 30;
            this.ppTreeView1.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.ppTreeView1.Location = new System.Drawing.Point(341, 3);
            this.ppTreeView1.Name = "ppTreeView1";
            this.ppTreeView1.NodeBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.ppTreeView1.NodeBorderColor = System.Drawing.Color.Gray;
            this.ppTreeView1.NodeFont = new System.Drawing.Font("微软雅黑", 11F);
            this.ppTreeView1.NodeMouseInBackColor = System.Drawing.Color.DimGray;
            treeNode1.Name = "节点19";
            treeNode1.Text = "节点19";
            treeNode2.Name = "节点20";
            treeNode2.Text = "节点20";
            treeNode3.ImageIndex = 1;
            treeNode3.Name = "节点5";
            treeNode3.Text = "节点5";
            treeNode4.Name = "节点6";
            treeNode4.Text = "节点6";
            treeNode5.ImageIndex = 0;
            treeNode5.Name = "节点0";
            treeNode5.Text = "节点0";
            treeNode6.Name = "节点7";
            treeNode6.Text = "节点7";
            treeNode7.Name = "节点8";
            treeNode7.Text = "节点8";
            treeNode8.Name = "节点9";
            treeNode8.Text = "节点9";
            treeNode9.ImageIndex = 1;
            treeNode9.Name = "节点1";
            treeNode9.Text = "节点1";
            treeNode10.Name = "节点13";
            treeNode10.Text = "节点13";
            treeNode11.Name = "节点2";
            treeNode11.Text = "节点2";
            treeNode12.Name = "节点10";
            treeNode12.Text = "节点10";
            treeNode13.Name = "节点16";
            treeNode13.Text = "节点16";
            treeNode14.Name = "节点17";
            treeNode14.Text = "节点17";
            treeNode15.Name = "节点18";
            treeNode15.Text = "节点18";
            treeNode16.Name = "节点11";
            treeNode16.Text = "节点11";
            treeNode17.Name = "节点12";
            treeNode17.Text = "节点12";
            treeNode18.Name = "节点3";
            treeNode18.Text = "节点3";
            treeNode19.Name = "节点14";
            treeNode19.Text = "节点14";
            treeNode20.Name = "节点15";
            treeNode20.Text = "节点15";
            treeNode21.Name = "节点4";
            treeNode21.Text = "节点4";
            this.ppTreeView1.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode5,
            treeNode9,
            treeNode11,
            treeNode18,
            treeNode21});
            this.ppTreeView1.NodeSelectBackColor = System.Drawing.Color.DimGray;
            this.ppTreeView1.NodeSelectBarColor = System.Drawing.Color.DodgerBlue;
            this.ppTreeView1.NodeSelectBorderColor = System.Drawing.Color.Gray;
            this.ppTreeView1.NodeSelectTextColor = System.Drawing.Color.White;
            this.ppTreeView1.NodeTextColor = System.Drawing.Color.White;
            this.ppTreeView1.PPImageList = null;
            this.ppTreeView1.RootBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.ppTreeView1.RootFont = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ppTreeView1.RootTextColor = System.Drawing.Color.DodgerBlue;
            this.ppTreeView1.ShowCheckBox = false;
            this.ppTreeView1.ShowPlusMinus = false;
            this.ppTreeView1.Size = new System.Drawing.Size(197, 644);
            this.ppTreeView1.TabIndex = 6;
            this.ppTreeView1.TextDrawMode = PPSkin.PPTreeView.drawMode.Clear;
            // 
            // ppTreeView2
            // 
            this.ppTreeView2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.ppTreeView2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.ppTreeView2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ppTreeView2.CheckBoxes = true;
            this.ppTreeView2.Cursor = System.Windows.Forms.Cursors.Default;
            this.ppTreeView2.DrawMode = System.Windows.Forms.TreeViewDrawMode.OwnerDrawAll;
            this.ppTreeView2.HotTracking = true;
            this.ppTreeView2.IconOffset = new System.Drawing.Point(0, 0);
            this.ppTreeView2.IconSize = new System.Drawing.Size(26, 26);
            this.ppTreeView2.ItemHeight = 30;
            this.ppTreeView2.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.ppTreeView2.Location = new System.Drawing.Point(75, 3);
            this.ppTreeView2.Name = "ppTreeView2";
            this.ppTreeView2.NodeBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.ppTreeView2.NodeBorderColor = System.Drawing.Color.Gray;
            this.ppTreeView2.NodeFont = new System.Drawing.Font("微软雅黑", 11F);
            this.ppTreeView2.NodeMouseInBackColor = System.Drawing.Color.DimGray;
            treeNode22.Name = "节点19";
            treeNode22.Text = "节点19";
            treeNode23.Name = "节点20";
            treeNode23.Text = "节点20";
            treeNode24.ImageIndex = 1;
            treeNode24.Name = "节点5";
            treeNode24.Text = "节点5";
            treeNode25.Name = "节点6";
            treeNode25.Text = "节点6";
            treeNode26.ImageIndex = 0;
            treeNode26.Name = "节点0";
            treeNode26.Text = "节点0";
            treeNode27.Name = "节点7";
            treeNode27.Text = "节点7";
            treeNode28.Name = "节点8";
            treeNode28.Text = "节点8";
            treeNode29.Name = "节点9";
            treeNode29.Text = "节点9";
            treeNode30.ImageIndex = 1;
            treeNode30.Name = "节点1";
            treeNode30.Text = "节点1";
            treeNode31.Name = "节点13";
            treeNode31.Text = "节点13";
            treeNode32.Name = "节点2";
            treeNode32.Text = "节点2";
            treeNode33.Name = "节点10";
            treeNode33.Text = "节点10";
            treeNode34.Name = "节点16";
            treeNode34.Text = "节点16";
            treeNode35.Name = "节点17";
            treeNode35.Text = "节点17";
            treeNode36.Name = "节点18";
            treeNode36.Text = "节点18";
            treeNode37.Name = "节点11";
            treeNode37.Text = "节点11";
            treeNode38.Name = "节点12";
            treeNode38.Text = "节点12";
            treeNode39.Name = "节点3";
            treeNode39.Text = "节点3";
            treeNode40.Name = "节点14";
            treeNode40.Text = "节点14";
            treeNode41.Name = "节点15";
            treeNode41.Text = "节点15";
            treeNode42.Name = "节点4";
            treeNode42.Text = "节点4";
            this.ppTreeView2.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode26,
            treeNode30,
            treeNode32,
            treeNode39,
            treeNode42});
            this.ppTreeView2.NodeSelectBackColor = System.Drawing.Color.DimGray;
            this.ppTreeView2.NodeSelectBarColor = System.Drawing.Color.DodgerBlue;
            this.ppTreeView2.NodeSelectBorderColor = System.Drawing.Color.Gray;
            this.ppTreeView2.NodeSelectTextColor = System.Drawing.Color.White;
            this.ppTreeView2.NodeTextColor = System.Drawing.Color.White;
            this.ppTreeView2.PPImageList = null;
            this.ppTreeView2.RootBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.ppTreeView2.RootFont = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ppTreeView2.RootTextColor = System.Drawing.Color.DodgerBlue;
            this.ppTreeView2.ShowCheckBox = false;
            this.ppTreeView2.ShowPlusMinus = false;
            this.ppTreeView2.Size = new System.Drawing.Size(197, 644);
            this.ppTreeView2.TabIndex = 5;
            this.ppTreeView2.TextDrawMode = PPSkin.PPTreeView.drawMode.Clear;
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.ppTabControl2);
            this.tabPage6.Location = new System.Drawing.Point(0, 30);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(928, 675);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "选项卡";
            // 
            // ppTabControl2
            // 
            this.ppTabControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ppTabControl2.BaseColor = System.Drawing.Color.WhiteSmoke;
            this.ppTabControl2.Controls.Add(this.tabPage8);
            this.ppTabControl2.Controls.Add(this.tabPage9);
            this.ppTabControl2.Cursor = System.Windows.Forms.Cursors.Default;
            this.ppTabControl2.ItemSize = new System.Drawing.Size(100, 30);
            this.ppTabControl2.Location = new System.Drawing.Point(27, 27);
            this.ppTabControl2.Name = "ppTabControl2";
            this.ppTabControl2.SelectedIndex = 0;
            this.ppTabControl2.SelectLineHeight = 1;
            this.ppTabControl2.ShowCloseButton = true;
            this.ppTabControl2.ShowSelectLine = false;
            this.ppTabControl2.ShowTab = true;
            this.ppTabControl2.ShowTabBorder = false;
            this.ppTabControl2.Size = new System.Drawing.Size(885, 621);
            this.ppTabControl2.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.ppTabControl2.TabBackColor = System.Drawing.Color.WhiteSmoke;
            this.ppTabControl2.TabBorderColor = System.Drawing.Color.LightGray;
            this.ppTabControl2.TabCloseButtonColor = System.Drawing.Color.DimGray;
            this.ppTabControl2.TabFont = new System.Drawing.Font("微软雅黑", 10F);
            this.ppTabControl2.TabInCloseButtonColor = System.Drawing.Color.DodgerBlue;
            this.ppTabControl2.TabIndex = 1;
            this.ppTabControl2.TabSelectBackColor = System.Drawing.Color.White;
            this.ppTabControl2.TabSelectBorderColor = System.Drawing.Color.DarkGray;
            this.ppTabControl2.TabSelectLineColor = System.Drawing.Color.DodgerBlue;
            this.ppTabControl2.TabSelectTextColor = System.Drawing.Color.DodgerBlue;
            this.ppTabControl2.TabTextAlign = PPSkin.PPTabControl.TextAlign.CENTER;
            this.ppTabControl2.TabTextColor = System.Drawing.Color.Black;
            this.ppTabControl2.TabTextOffset = new System.Drawing.Point(0, 0);
            this.ppTabControl2.TextDrawMode = PPSkin.PPTabControl.drawMode.Clear;
            this.ppTabControl2.TitleBaseColor = System.Drawing.Color.WhiteSmoke;
            // 
            // tabPage8
            // 
            this.tabPage8.BackColor = System.Drawing.Color.LightSalmon;
            this.tabPage8.Controls.Add(this.ppTabControl6);
            this.tabPage8.Controls.Add(this.ppTabControl5);
            this.tabPage8.Controls.Add(this.ppTabControl3);
            this.tabPage8.Controls.Add(this.ppTabControl4);
            this.tabPage8.Location = new System.Drawing.Point(0, 30);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage8.Size = new System.Drawing.Size(885, 591);
            this.tabPage8.TabIndex = 0;
            this.tabPage8.Text = "tabPage8";
            // 
            // ppTabControl6
            // 
            this.ppTabControl6.Alignment = System.Windows.Forms.TabAlignment.Bottom;
            this.ppTabControl6.BaseColor = System.Drawing.Color.WhiteSmoke;
            this.ppTabControl6.Controls.Add(this.tabPage16);
            this.ppTabControl6.Controls.Add(this.tabPage17);
            this.ppTabControl6.Cursor = System.Windows.Forms.Cursors.Default;
            this.ppTabControl6.ItemSize = new System.Drawing.Size(100, 30);
            this.ppTabControl6.Location = new System.Drawing.Point(461, 28);
            this.ppTabControl6.Name = "ppTabControl6";
            this.ppTabControl6.SelectedIndex = 0;
            this.ppTabControl6.SelectLineHeight = 4;
            this.ppTabControl6.ShowCloseButton = true;
            this.ppTabControl6.ShowSelectLine = true;
            this.ppTabControl6.ShowTab = true;
            this.ppTabControl6.ShowTabBorder = false;
            this.ppTabControl6.Size = new System.Drawing.Size(400, 250);
            this.ppTabControl6.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.ppTabControl6.TabBackColor = System.Drawing.Color.WhiteSmoke;
            this.ppTabControl6.TabBorderColor = System.Drawing.Color.LightGray;
            this.ppTabControl6.TabCloseButtonColor = System.Drawing.Color.DimGray;
            this.ppTabControl6.TabFont = new System.Drawing.Font("微软雅黑", 10F);
            this.ppTabControl6.TabInCloseButtonColor = System.Drawing.Color.DodgerBlue;
            this.ppTabControl6.TabIndex = 4;
            this.ppTabControl6.TabSelectBackColor = System.Drawing.Color.White;
            this.ppTabControl6.TabSelectBorderColor = System.Drawing.Color.DarkGray;
            this.ppTabControl6.TabSelectLineColor = System.Drawing.Color.DodgerBlue;
            this.ppTabControl6.TabSelectTextColor = System.Drawing.Color.DodgerBlue;
            this.ppTabControl6.TabTextAlign = PPSkin.PPTabControl.TextAlign.CENTER;
            this.ppTabControl6.TabTextColor = System.Drawing.Color.Black;
            this.ppTabControl6.TabTextOffset = new System.Drawing.Point(0, 0);
            this.ppTabControl6.TextDrawMode = PPSkin.PPTabControl.drawMode.Clear;
            this.ppTabControl6.TitleBaseColor = System.Drawing.Color.WhiteSmoke;
            // 
            // tabPage16
            // 
            this.tabPage16.Location = new System.Drawing.Point(0, 0);
            this.tabPage16.Name = "tabPage16";
            this.tabPage16.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage16.Size = new System.Drawing.Size(400, 220);
            this.tabPage16.TabIndex = 0;
            this.tabPage16.Text = "tabPage16";
            // 
            // tabPage17
            // 
            this.tabPage17.Location = new System.Drawing.Point(0, 0);
            this.tabPage17.Name = "tabPage17";
            this.tabPage17.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage17.Size = new System.Drawing.Size(400, 220);
            this.tabPage17.TabIndex = 1;
            this.tabPage17.Text = "tabPage17";
            // 
            // ppTabControl5
            // 
            this.ppTabControl5.Alignment = System.Windows.Forms.TabAlignment.Right;
            this.ppTabControl5.BaseColor = System.Drawing.Color.WhiteSmoke;
            this.ppTabControl5.Controls.Add(this.tabPage14);
            this.ppTabControl5.Controls.Add(this.tabPage15);
            this.ppTabControl5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ppTabControl5.ItemSize = new System.Drawing.Size(30, 100);
            this.ppTabControl5.Location = new System.Drawing.Point(461, 307);
            this.ppTabControl5.Multiline = true;
            this.ppTabControl5.Name = "ppTabControl5";
            this.ppTabControl5.SelectedIndex = 0;
            this.ppTabControl5.SelectLineHeight = 4;
            this.ppTabControl5.ShowCloseButton = false;
            this.ppTabControl5.ShowSelectLine = true;
            this.ppTabControl5.ShowTab = true;
            this.ppTabControl5.ShowTabBorder = false;
            this.ppTabControl5.Size = new System.Drawing.Size(400, 250);
            this.ppTabControl5.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.ppTabControl5.TabBackColor = System.Drawing.Color.WhiteSmoke;
            this.ppTabControl5.TabBorderColor = System.Drawing.Color.LightGray;
            this.ppTabControl5.TabCloseButtonColor = System.Drawing.Color.DimGray;
            this.ppTabControl5.TabFont = new System.Drawing.Font("微软雅黑", 10F);
            this.ppTabControl5.TabInCloseButtonColor = System.Drawing.Color.DodgerBlue;
            this.ppTabControl5.TabIndex = 3;
            this.ppTabControl5.TabSelectBackColor = System.Drawing.Color.White;
            this.ppTabControl5.TabSelectBorderColor = System.Drawing.Color.DarkGray;
            this.ppTabControl5.TabSelectLineColor = System.Drawing.Color.DodgerBlue;
            this.ppTabControl5.TabSelectTextColor = System.Drawing.Color.DodgerBlue;
            this.ppTabControl5.TabTextAlign = PPSkin.PPTabControl.TextAlign.CENTER;
            this.ppTabControl5.TabTextColor = System.Drawing.Color.Black;
            this.ppTabControl5.TabTextOffset = new System.Drawing.Point(0, 0);
            this.ppTabControl5.TextDrawMode = PPSkin.PPTabControl.drawMode.Clear;
            this.ppTabControl5.TitleBaseColor = System.Drawing.Color.WhiteSmoke;
            // 
            // tabPage14
            // 
            this.tabPage14.Location = new System.Drawing.Point(0, 0);
            this.tabPage14.Name = "tabPage14";
            this.tabPage14.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage14.Size = new System.Drawing.Size(300, 250);
            this.tabPage14.TabIndex = 0;
            this.tabPage14.Text = "tabPage14";
            // 
            // tabPage15
            // 
            this.tabPage15.Location = new System.Drawing.Point(0, 0);
            this.tabPage15.Name = "tabPage15";
            this.tabPage15.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage15.Size = new System.Drawing.Size(300, 250);
            this.tabPage15.TabIndex = 1;
            this.tabPage15.Text = "tabPage15";
            // 
            // ppTabControl3
            // 
            this.ppTabControl3.BaseColor = System.Drawing.Color.WhiteSmoke;
            this.ppTabControl3.Controls.Add(this.tabPage10);
            this.ppTabControl3.Controls.Add(this.tabPage11);
            this.ppTabControl3.Cursor = System.Windows.Forms.Cursors.Default;
            this.ppTabControl3.ItemSize = new System.Drawing.Size(100, 30);
            this.ppTabControl3.Location = new System.Drawing.Point(43, 28);
            this.ppTabControl3.Name = "ppTabControl3";
            this.ppTabControl3.SelectedIndex = 0;
            this.ppTabControl3.SelectLineHeight = 4;
            this.ppTabControl3.ShowCloseButton = true;
            this.ppTabControl3.ShowSelectLine = true;
            this.ppTabControl3.ShowTab = true;
            this.ppTabControl3.ShowTabBorder = false;
            this.ppTabControl3.Size = new System.Drawing.Size(400, 250);
            this.ppTabControl3.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.ppTabControl3.TabBackColor = System.Drawing.Color.WhiteSmoke;
            this.ppTabControl3.TabBorderColor = System.Drawing.Color.LightGray;
            this.ppTabControl3.TabCloseButtonColor = System.Drawing.Color.DimGray;
            this.ppTabControl3.TabFont = new System.Drawing.Font("微软雅黑", 10F);
            this.ppTabControl3.TabInCloseButtonColor = System.Drawing.Color.DodgerBlue;
            this.ppTabControl3.TabIndex = 1;
            this.ppTabControl3.TabSelectBackColor = System.Drawing.Color.White;
            this.ppTabControl3.TabSelectBorderColor = System.Drawing.Color.DarkGray;
            this.ppTabControl3.TabSelectLineColor = System.Drawing.Color.DodgerBlue;
            this.ppTabControl3.TabSelectTextColor = System.Drawing.Color.DodgerBlue;
            this.ppTabControl3.TabTextAlign = PPSkin.PPTabControl.TextAlign.CENTER;
            this.ppTabControl3.TabTextColor = System.Drawing.Color.Black;
            this.ppTabControl3.TabTextOffset = new System.Drawing.Point(0, 0);
            this.ppTabControl3.TextDrawMode = PPSkin.PPTabControl.drawMode.Clear;
            this.ppTabControl3.TitleBaseColor = System.Drawing.Color.WhiteSmoke;
            // 
            // tabPage10
            // 
            this.tabPage10.Location = new System.Drawing.Point(0, 30);
            this.tabPage10.Name = "tabPage10";
            this.tabPage10.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage10.Size = new System.Drawing.Size(400, 220);
            this.tabPage10.TabIndex = 0;
            this.tabPage10.Text = "tabPage10";
            // 
            // tabPage11
            // 
            this.tabPage11.Location = new System.Drawing.Point(0, 30);
            this.tabPage11.Name = "tabPage11";
            this.tabPage11.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage11.Size = new System.Drawing.Size(400, 220);
            this.tabPage11.TabIndex = 1;
            this.tabPage11.Text = "tabPage11";
            // 
            // ppTabControl4
            // 
            this.ppTabControl4.Alignment = System.Windows.Forms.TabAlignment.Left;
            this.ppTabControl4.BaseColor = System.Drawing.Color.WhiteSmoke;
            this.ppTabControl4.Controls.Add(this.tabPage12);
            this.ppTabControl4.Controls.Add(this.tabPage13);
            this.ppTabControl4.Cursor = System.Windows.Forms.Cursors.Default;
            this.ppTabControl4.ItemSize = new System.Drawing.Size(30, 100);
            this.ppTabControl4.Location = new System.Drawing.Point(43, 307);
            this.ppTabControl4.Multiline = true;
            this.ppTabControl4.Name = "ppTabControl4";
            this.ppTabControl4.SelectedIndex = 0;
            this.ppTabControl4.SelectLineHeight = 4;
            this.ppTabControl4.ShowCloseButton = false;
            this.ppTabControl4.ShowSelectLine = true;
            this.ppTabControl4.ShowTab = true;
            this.ppTabControl4.ShowTabBorder = false;
            this.ppTabControl4.Size = new System.Drawing.Size(400, 250);
            this.ppTabControl4.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.ppTabControl4.TabBackColor = System.Drawing.Color.WhiteSmoke;
            this.ppTabControl4.TabBorderColor = System.Drawing.Color.LightGray;
            this.ppTabControl4.TabCloseButtonColor = System.Drawing.Color.DimGray;
            this.ppTabControl4.TabFont = new System.Drawing.Font("微软雅黑", 10F);
            this.ppTabControl4.TabInCloseButtonColor = System.Drawing.Color.DodgerBlue;
            this.ppTabControl4.TabIndex = 2;
            this.ppTabControl4.TabSelectBackColor = System.Drawing.Color.White;
            this.ppTabControl4.TabSelectBorderColor = System.Drawing.Color.DarkGray;
            this.ppTabControl4.TabSelectLineColor = System.Drawing.Color.DodgerBlue;
            this.ppTabControl4.TabSelectTextColor = System.Drawing.Color.DodgerBlue;
            this.ppTabControl4.TabTextAlign = PPSkin.PPTabControl.TextAlign.CENTER;
            this.ppTabControl4.TabTextColor = System.Drawing.Color.Black;
            this.ppTabControl4.TabTextOffset = new System.Drawing.Point(0, 0);
            this.ppTabControl4.TextDrawMode = PPSkin.PPTabControl.drawMode.Clear;
            this.ppTabControl4.TitleBaseColor = System.Drawing.Color.WhiteSmoke;
            // 
            // tabPage12
            // 
            this.tabPage12.Location = new System.Drawing.Point(100, 0);
            this.tabPage12.Name = "tabPage12";
            this.tabPage12.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage12.Size = new System.Drawing.Size(300, 250);
            this.tabPage12.TabIndex = 0;
            this.tabPage12.Text = "tabPage12";
            // 
            // tabPage13
            // 
            this.tabPage13.Location = new System.Drawing.Point(100, 0);
            this.tabPage13.Name = "tabPage13";
            this.tabPage13.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage13.Size = new System.Drawing.Size(300, 250);
            this.tabPage13.TabIndex = 1;
            this.tabPage13.Text = "tabPage13";
            // 
            // tabPage9
            // 
            this.tabPage9.BackColor = System.Drawing.Color.Turquoise;
            this.tabPage9.Controls.Add(this.ppTabControl7);
            this.tabPage9.Controls.Add(this.ppTabControl8);
            this.tabPage9.Controls.Add(this.ppTabControl9);
            this.tabPage9.Controls.Add(this.ppTabControl10);
            this.tabPage9.Location = new System.Drawing.Point(0, 30);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage9.Size = new System.Drawing.Size(885, 591);
            this.tabPage9.TabIndex = 1;
            this.tabPage9.Text = "tabPage9";
            // 
            // ppTabControl7
            // 
            this.ppTabControl7.Alignment = System.Windows.Forms.TabAlignment.Bottom;
            this.ppTabControl7.BaseColor = System.Drawing.Color.White;
            this.ppTabControl7.Controls.Add(this.tabPage18);
            this.ppTabControl7.Controls.Add(this.tabPage19);
            this.ppTabControl7.Cursor = System.Windows.Forms.Cursors.Default;
            this.ppTabControl7.ItemSize = new System.Drawing.Size(100, 30);
            this.ppTabControl7.Location = new System.Drawing.Point(453, 31);
            this.ppTabControl7.Name = "ppTabControl7";
            this.ppTabControl7.SelectedIndex = 0;
            this.ppTabControl7.SelectLineHeight = 4;
            this.ppTabControl7.ShowCloseButton = true;
            this.ppTabControl7.ShowSelectLine = true;
            this.ppTabControl7.ShowTab = true;
            this.ppTabControl7.ShowTabBorder = false;
            this.ppTabControl7.Size = new System.Drawing.Size(400, 250);
            this.ppTabControl7.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.ppTabControl7.TabBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.ppTabControl7.TabBorderColor = System.Drawing.Color.LightGray;
            this.ppTabControl7.TabCloseButtonColor = System.Drawing.Color.White;
            this.ppTabControl7.TabFont = new System.Drawing.Font("微软雅黑", 10F);
            this.ppTabControl7.TabInCloseButtonColor = System.Drawing.Color.DodgerBlue;
            this.ppTabControl7.TabIndex = 8;
            this.ppTabControl7.TabSelectBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(71)))), ((int)(((byte)(71)))));
            this.ppTabControl7.TabSelectBorderColor = System.Drawing.Color.DarkGray;
            this.ppTabControl7.TabSelectLineColor = System.Drawing.Color.DodgerBlue;
            this.ppTabControl7.TabSelectTextColor = System.Drawing.Color.DodgerBlue;
            this.ppTabControl7.TabTextAlign = PPSkin.PPTabControl.TextAlign.CENTER;
            this.ppTabControl7.TabTextColor = System.Drawing.Color.White;
            this.ppTabControl7.TabTextOffset = new System.Drawing.Point(0, 0);
            this.ppTabControl7.TextDrawMode = PPSkin.PPTabControl.drawMode.Clear;
            this.ppTabControl7.TitleBaseColor = System.Drawing.Color.WhiteSmoke;
            // 
            // tabPage18
            // 
            this.tabPage18.Location = new System.Drawing.Point(0, 0);
            this.tabPage18.Name = "tabPage18";
            this.tabPage18.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage18.Size = new System.Drawing.Size(400, 220);
            this.tabPage18.TabIndex = 0;
            this.tabPage18.Text = "tabPage18";
            // 
            // tabPage19
            // 
            this.tabPage19.Location = new System.Drawing.Point(0, 0);
            this.tabPage19.Name = "tabPage19";
            this.tabPage19.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage19.Size = new System.Drawing.Size(400, 220);
            this.tabPage19.TabIndex = 1;
            this.tabPage19.Text = "tabPage19";
            // 
            // ppTabControl8
            // 
            this.ppTabControl8.Alignment = System.Windows.Forms.TabAlignment.Right;
            this.ppTabControl8.BaseColor = System.Drawing.Color.White;
            this.ppTabControl8.Controls.Add(this.tabPage20);
            this.ppTabControl8.Controls.Add(this.tabPage21);
            this.ppTabControl8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ppTabControl8.ItemSize = new System.Drawing.Size(30, 100);
            this.ppTabControl8.Location = new System.Drawing.Point(453, 310);
            this.ppTabControl8.Multiline = true;
            this.ppTabControl8.Name = "ppTabControl8";
            this.ppTabControl8.SelectedIndex = 0;
            this.ppTabControl8.SelectLineHeight = 4;
            this.ppTabControl8.ShowCloseButton = false;
            this.ppTabControl8.ShowSelectLine = true;
            this.ppTabControl8.ShowTab = true;
            this.ppTabControl8.ShowTabBorder = false;
            this.ppTabControl8.Size = new System.Drawing.Size(400, 250);
            this.ppTabControl8.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.ppTabControl8.TabBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.ppTabControl8.TabBorderColor = System.Drawing.Color.LightGray;
            this.ppTabControl8.TabCloseButtonColor = System.Drawing.Color.White;
            this.ppTabControl8.TabFont = new System.Drawing.Font("微软雅黑", 10F);
            this.ppTabControl8.TabInCloseButtonColor = System.Drawing.Color.DodgerBlue;
            this.ppTabControl8.TabIndex = 7;
            this.ppTabControl8.TabSelectBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(71)))), ((int)(((byte)(71)))));
            this.ppTabControl8.TabSelectBorderColor = System.Drawing.Color.DarkGray;
            this.ppTabControl8.TabSelectLineColor = System.Drawing.Color.DodgerBlue;
            this.ppTabControl8.TabSelectTextColor = System.Drawing.Color.DodgerBlue;
            this.ppTabControl8.TabTextAlign = PPSkin.PPTabControl.TextAlign.CENTER;
            this.ppTabControl8.TabTextColor = System.Drawing.Color.White;
            this.ppTabControl8.TabTextOffset = new System.Drawing.Point(0, 0);
            this.ppTabControl8.TextDrawMode = PPSkin.PPTabControl.drawMode.Clear;
            this.ppTabControl8.TitleBaseColor = System.Drawing.Color.WhiteSmoke;
            // 
            // tabPage20
            // 
            this.tabPage20.Location = new System.Drawing.Point(0, 0);
            this.tabPage20.Name = "tabPage20";
            this.tabPage20.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage20.Size = new System.Drawing.Size(300, 250);
            this.tabPage20.TabIndex = 0;
            this.tabPage20.Text = "tabPage20";
            // 
            // tabPage21
            // 
            this.tabPage21.Location = new System.Drawing.Point(0, 0);
            this.tabPage21.Name = "tabPage21";
            this.tabPage21.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage21.Size = new System.Drawing.Size(300, 250);
            this.tabPage21.TabIndex = 1;
            this.tabPage21.Text = "tabPage21";
            // 
            // ppTabControl9
            // 
            this.ppTabControl9.BaseColor = System.Drawing.Color.White;
            this.ppTabControl9.Controls.Add(this.tabPage22);
            this.ppTabControl9.Controls.Add(this.tabPage23);
            this.ppTabControl9.Cursor = System.Windows.Forms.Cursors.Default;
            this.ppTabControl9.ItemSize = new System.Drawing.Size(100, 30);
            this.ppTabControl9.Location = new System.Drawing.Point(35, 31);
            this.ppTabControl9.Name = "ppTabControl9";
            this.ppTabControl9.SelectedIndex = 0;
            this.ppTabControl9.SelectLineHeight = 4;
            this.ppTabControl9.ShowCloseButton = true;
            this.ppTabControl9.ShowSelectLine = true;
            this.ppTabControl9.ShowTab = true;
            this.ppTabControl9.ShowTabBorder = false;
            this.ppTabControl9.Size = new System.Drawing.Size(400, 250);
            this.ppTabControl9.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.ppTabControl9.TabBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.ppTabControl9.TabBorderColor = System.Drawing.Color.LightGray;
            this.ppTabControl9.TabCloseButtonColor = System.Drawing.Color.White;
            this.ppTabControl9.TabFont = new System.Drawing.Font("微软雅黑", 10F);
            this.ppTabControl9.TabInCloseButtonColor = System.Drawing.Color.DodgerBlue;
            this.ppTabControl9.TabIndex = 5;
            this.ppTabControl9.TabSelectBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(71)))), ((int)(((byte)(71)))));
            this.ppTabControl9.TabSelectBorderColor = System.Drawing.Color.DarkGray;
            this.ppTabControl9.TabSelectLineColor = System.Drawing.Color.DodgerBlue;
            this.ppTabControl9.TabSelectTextColor = System.Drawing.Color.DodgerBlue;
            this.ppTabControl9.TabTextAlign = PPSkin.PPTabControl.TextAlign.CENTER;
            this.ppTabControl9.TabTextColor = System.Drawing.Color.White;
            this.ppTabControl9.TabTextOffset = new System.Drawing.Point(0, 0);
            this.ppTabControl9.TextDrawMode = PPSkin.PPTabControl.drawMode.Clear;
            this.ppTabControl9.TitleBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            // 
            // tabPage22
            // 
            this.tabPage22.Location = new System.Drawing.Point(0, 30);
            this.tabPage22.Name = "tabPage22";
            this.tabPage22.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage22.Size = new System.Drawing.Size(400, 220);
            this.tabPage22.TabIndex = 0;
            this.tabPage22.Text = "tabPage22";
            // 
            // tabPage23
            // 
            this.tabPage23.Location = new System.Drawing.Point(0, 30);
            this.tabPage23.Name = "tabPage23";
            this.tabPage23.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage23.Size = new System.Drawing.Size(400, 220);
            this.tabPage23.TabIndex = 1;
            this.tabPage23.Text = "tabPage23";
            // 
            // ppTabControl10
            // 
            this.ppTabControl10.Alignment = System.Windows.Forms.TabAlignment.Left;
            this.ppTabControl10.BaseColor = System.Drawing.Color.White;
            this.ppTabControl10.Controls.Add(this.tabPage24);
            this.ppTabControl10.Controls.Add(this.tabPage25);
            this.ppTabControl10.Cursor = System.Windows.Forms.Cursors.Default;
            this.ppTabControl10.ItemSize = new System.Drawing.Size(30, 100);
            this.ppTabControl10.Location = new System.Drawing.Point(35, 310);
            this.ppTabControl10.Multiline = true;
            this.ppTabControl10.Name = "ppTabControl10";
            this.ppTabControl10.SelectedIndex = 0;
            this.ppTabControl10.SelectLineHeight = 4;
            this.ppTabControl10.ShowCloseButton = false;
            this.ppTabControl10.ShowSelectLine = true;
            this.ppTabControl10.ShowTab = true;
            this.ppTabControl10.ShowTabBorder = false;
            this.ppTabControl10.Size = new System.Drawing.Size(400, 250);
            this.ppTabControl10.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.ppTabControl10.TabBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.ppTabControl10.TabBorderColor = System.Drawing.Color.LightGray;
            this.ppTabControl10.TabCloseButtonColor = System.Drawing.Color.White;
            this.ppTabControl10.TabFont = new System.Drawing.Font("微软雅黑", 10F);
            this.ppTabControl10.TabInCloseButtonColor = System.Drawing.Color.DodgerBlue;
            this.ppTabControl10.TabIndex = 6;
            this.ppTabControl10.TabSelectBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(71)))), ((int)(((byte)(71)))));
            this.ppTabControl10.TabSelectBorderColor = System.Drawing.Color.DarkGray;
            this.ppTabControl10.TabSelectLineColor = System.Drawing.Color.DodgerBlue;
            this.ppTabControl10.TabSelectTextColor = System.Drawing.Color.DodgerBlue;
            this.ppTabControl10.TabTextAlign = PPSkin.PPTabControl.TextAlign.CENTER;
            this.ppTabControl10.TabTextColor = System.Drawing.Color.White;
            this.ppTabControl10.TabTextOffset = new System.Drawing.Point(0, 0);
            this.ppTabControl10.TextDrawMode = PPSkin.PPTabControl.drawMode.Clear;
            this.ppTabControl10.TitleBaseColor = System.Drawing.Color.WhiteSmoke;
            // 
            // tabPage24
            // 
            this.tabPage24.Location = new System.Drawing.Point(100, 0);
            this.tabPage24.Name = "tabPage24";
            this.tabPage24.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage24.Size = new System.Drawing.Size(300, 250);
            this.tabPage24.TabIndex = 0;
            this.tabPage24.Text = "tabPage24";
            // 
            // tabPage25
            // 
            this.tabPage25.Location = new System.Drawing.Point(100, 0);
            this.tabPage25.Name = "tabPage25";
            this.tabPage25.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage25.Size = new System.Drawing.Size(300, 250);
            this.tabPage25.TabIndex = 1;
            this.tabPage25.Text = "tabPage25";
            // 
            // tabPage7
            // 
            this.tabPage7.BackColor = System.Drawing.Color.White;
            this.tabPage7.Controls.Add(this.pphScrollBarExt10);
            this.tabPage7.Controls.Add(this.ppvScrollBarExt11);
            this.tabPage7.Controls.Add(this.pphScrollBarExt9);
            this.tabPage7.Controls.Add(this.ppvScrollBarExt10);
            this.tabPage7.Controls.Add(this.pphScrollBarExt8);
            this.tabPage7.Controls.Add(this.ppvScrollBarExt9);
            this.tabPage7.Controls.Add(this.pphScrollBarExt7);
            this.tabPage7.Controls.Add(this.ppvScrollBarExt8);
            this.tabPage7.Controls.Add(this.ppDataGridView4);
            this.tabPage7.Controls.Add(this.ppDataGridView3);
            this.tabPage7.Controls.Add(this.ppDataGridView2);
            this.tabPage7.Controls.Add(this.ppDataGridView1);
            this.tabPage7.Location = new System.Drawing.Point(0, 30);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Size = new System.Drawing.Size(928, 675);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "表";
            // 
            // pphScrollBarExt10
            // 
            this.pphScrollBarExt10.BackColor = System.Drawing.Color.Transparent;
            this.pphScrollBarExt10.BaseColor = System.Drawing.Color.Gainsboro;
            this.pphScrollBarExt10.BaseRadius = 0;
            this.pphScrollBarExt10.BaseSize = 1;
            this.pphScrollBarExt10.ButtonColor = System.Drawing.Color.DodgerBlue;
            this.pphScrollBarExt10.ButtonHoverColor = System.Drawing.Color.DeepSkyBlue;
            this.pphScrollBarExt10.ButtonRadius = 6;
            this.pphScrollBarExt10.ButtonSize = 6;
            this.pphScrollBarExt10.LargeChange = 10;
            this.pphScrollBarExt10.Location = new System.Drawing.Point(472, 650);
            this.pphScrollBarExt10.Margin = new System.Windows.Forms.Padding(0);
            this.pphScrollBarExt10.Maximum = 100;
            this.pphScrollBarExt10.Minimum = 0;
            this.pphScrollBarExt10.Name = "pphScrollBarExt10";
            this.pphScrollBarExt10.Size = new System.Drawing.Size(441, 17);
            this.pphScrollBarExt10.SmallChange = 1;
            this.pphScrollBarExt10.TabIndex = 19;
            this.pphScrollBarExt10.Value = 0;
            this.pphScrollBarExt10.VisibleValue = 10;
            // 
            // ppvScrollBarExt11
            // 
            this.ppvScrollBarExt11.BackColor = System.Drawing.Color.Transparent;
            this.ppvScrollBarExt11.BaseColor = System.Drawing.Color.Gainsboro;
            this.ppvScrollBarExt11.BaseRadius = 0;
            this.ppvScrollBarExt11.BaseSize = 1;
            this.ppvScrollBarExt11.ButtonColor = System.Drawing.Color.DodgerBlue;
            this.ppvScrollBarExt11.ButtonHoverColor = System.Drawing.Color.DeepSkyBlue;
            this.ppvScrollBarExt11.ButtonRadius = 6;
            this.ppvScrollBarExt11.ButtonSize = 6;
            this.ppvScrollBarExt11.LargeChange = 10;
            this.ppvScrollBarExt11.Location = new System.Drawing.Point(913, 372);
            this.ppvScrollBarExt11.Margin = new System.Windows.Forms.Padding(0);
            this.ppvScrollBarExt11.Maximum = 100;
            this.ppvScrollBarExt11.Minimum = 0;
            this.ppvScrollBarExt11.Name = "ppvScrollBarExt11";
            this.ppvScrollBarExt11.Size = new System.Drawing.Size(17, 278);
            this.ppvScrollBarExt11.SmallChange = 1;
            this.ppvScrollBarExt11.TabIndex = 18;
            this.ppvScrollBarExt11.Value = 0;
            this.ppvScrollBarExt11.VisibleValue = 10;
            // 
            // pphScrollBarExt9
            // 
            this.pphScrollBarExt9.BackColor = System.Drawing.Color.Transparent;
            this.pphScrollBarExt9.BaseColor = System.Drawing.Color.Gainsboro;
            this.pphScrollBarExt9.BaseRadius = 10;
            this.pphScrollBarExt9.BaseSize = 10;
            this.pphScrollBarExt9.ButtonColor = System.Drawing.Color.DodgerBlue;
            this.pphScrollBarExt9.ButtonHoverColor = System.Drawing.Color.DeepSkyBlue;
            this.pphScrollBarExt9.ButtonRadius = 10;
            this.pphScrollBarExt9.ButtonSize = 10;
            this.pphScrollBarExt9.LargeChange = 10;
            this.pphScrollBarExt9.Location = new System.Drawing.Point(16, 650);
            this.pphScrollBarExt9.Margin = new System.Windows.Forms.Padding(0);
            this.pphScrollBarExt9.Maximum = 100;
            this.pphScrollBarExt9.Minimum = 0;
            this.pphScrollBarExt9.Name = "pphScrollBarExt9";
            this.pphScrollBarExt9.Size = new System.Drawing.Size(439, 17);
            this.pphScrollBarExt9.SmallChange = 1;
            this.pphScrollBarExt9.TabIndex = 17;
            this.pphScrollBarExt9.Value = 0;
            this.pphScrollBarExt9.VisibleValue = 10;
            // 
            // ppvScrollBarExt10
            // 
            this.ppvScrollBarExt10.BackColor = System.Drawing.Color.Transparent;
            this.ppvScrollBarExt10.BaseColor = System.Drawing.Color.Gainsboro;
            this.ppvScrollBarExt10.BaseRadius = 10;
            this.ppvScrollBarExt10.BaseSize = 10;
            this.ppvScrollBarExt10.ButtonColor = System.Drawing.Color.DodgerBlue;
            this.ppvScrollBarExt10.ButtonHoverColor = System.Drawing.Color.DeepSkyBlue;
            this.ppvScrollBarExt10.ButtonRadius = 10;
            this.ppvScrollBarExt10.ButtonSize = 10;
            this.ppvScrollBarExt10.LargeChange = 10;
            this.ppvScrollBarExt10.Location = new System.Drawing.Point(452, 372);
            this.ppvScrollBarExt10.Margin = new System.Windows.Forms.Padding(0);
            this.ppvScrollBarExt10.Maximum = 100;
            this.ppvScrollBarExt10.Minimum = 0;
            this.ppvScrollBarExt10.Name = "ppvScrollBarExt10";
            this.ppvScrollBarExt10.Size = new System.Drawing.Size(17, 278);
            this.ppvScrollBarExt10.SmallChange = 1;
            this.ppvScrollBarExt10.TabIndex = 16;
            this.ppvScrollBarExt10.Value = 0;
            this.ppvScrollBarExt10.VisibleValue = 10;
            // 
            // pphScrollBarExt8
            // 
            this.pphScrollBarExt8.BackColor = System.Drawing.Color.Transparent;
            this.pphScrollBarExt8.BaseColor = System.Drawing.Color.Gainsboro;
            this.pphScrollBarExt8.BaseRadius = 0;
            this.pphScrollBarExt8.BaseSize = 1;
            this.pphScrollBarExt8.ButtonColor = System.Drawing.Color.DodgerBlue;
            this.pphScrollBarExt8.ButtonHoverColor = System.Drawing.Color.DeepSkyBlue;
            this.pphScrollBarExt8.ButtonRadius = 10;
            this.pphScrollBarExt8.ButtonSize = 9;
            this.pphScrollBarExt8.LargeChange = 10;
            this.pphScrollBarExt8.Location = new System.Drawing.Point(469, 308);
            this.pphScrollBarExt8.Margin = new System.Windows.Forms.Padding(0);
            this.pphScrollBarExt8.Maximum = 100;
            this.pphScrollBarExt8.Minimum = 0;
            this.pphScrollBarExt8.Name = "pphScrollBarExt8";
            this.pphScrollBarExt8.Size = new System.Drawing.Size(441, 17);
            this.pphScrollBarExt8.SmallChange = 1;
            this.pphScrollBarExt8.TabIndex = 15;
            this.pphScrollBarExt8.Value = 0;
            this.pphScrollBarExt8.VisibleValue = 10;
            // 
            // ppvScrollBarExt9
            // 
            this.ppvScrollBarExt9.BackColor = System.Drawing.Color.Transparent;
            this.ppvScrollBarExt9.BaseColor = System.Drawing.Color.Gainsboro;
            this.ppvScrollBarExt9.BaseRadius = 0;
            this.ppvScrollBarExt9.BaseSize = 1;
            this.ppvScrollBarExt9.ButtonColor = System.Drawing.Color.DodgerBlue;
            this.ppvScrollBarExt9.ButtonHoverColor = System.Drawing.Color.DeepSkyBlue;
            this.ppvScrollBarExt9.ButtonRadius = 10;
            this.ppvScrollBarExt9.ButtonSize = 9;
            this.ppvScrollBarExt9.LargeChange = 10;
            this.ppvScrollBarExt9.Location = new System.Drawing.Point(913, 27);
            this.ppvScrollBarExt9.Margin = new System.Windows.Forms.Padding(0);
            this.ppvScrollBarExt9.Maximum = 100;
            this.ppvScrollBarExt9.Minimum = 0;
            this.ppvScrollBarExt9.Name = "ppvScrollBarExt9";
            this.ppvScrollBarExt9.Size = new System.Drawing.Size(17, 278);
            this.ppvScrollBarExt9.SmallChange = 1;
            this.ppvScrollBarExt9.TabIndex = 14;
            this.ppvScrollBarExt9.Value = 0;
            this.ppvScrollBarExt9.VisibleValue = 10;
            // 
            // pphScrollBarExt7
            // 
            this.pphScrollBarExt7.BackColor = System.Drawing.Color.Transparent;
            this.pphScrollBarExt7.BaseColor = System.Drawing.Color.DarkGray;
            this.pphScrollBarExt7.BaseRadius = 0;
            this.pphScrollBarExt7.BaseSize = 1;
            this.pphScrollBarExt7.ButtonColor = System.Drawing.Color.Lime;
            this.pphScrollBarExt7.ButtonHoverColor = System.Drawing.Color.SpringGreen;
            this.pphScrollBarExt7.ButtonRadius = 10;
            this.pphScrollBarExt7.ButtonSize = 9;
            this.pphScrollBarExt7.LargeChange = 10;
            this.pphScrollBarExt7.Location = new System.Drawing.Point(16, 308);
            this.pphScrollBarExt7.Margin = new System.Windows.Forms.Padding(0);
            this.pphScrollBarExt7.Maximum = 100;
            this.pphScrollBarExt7.Minimum = 0;
            this.pphScrollBarExt7.Name = "pphScrollBarExt7";
            this.pphScrollBarExt7.Size = new System.Drawing.Size(433, 17);
            this.pphScrollBarExt7.SmallChange = 1;
            this.pphScrollBarExt7.TabIndex = 13;
            this.pphScrollBarExt7.Value = 0;
            this.pphScrollBarExt7.VisibleValue = 10;
            // 
            // ppvScrollBarExt8
            // 
            this.ppvScrollBarExt8.BackColor = System.Drawing.Color.Transparent;
            this.ppvScrollBarExt8.BaseColor = System.Drawing.Color.DarkGray;
            this.ppvScrollBarExt8.BaseRadius = 0;
            this.ppvScrollBarExt8.BaseSize = 1;
            this.ppvScrollBarExt8.ButtonColor = System.Drawing.Color.Lime;
            this.ppvScrollBarExt8.ButtonHoverColor = System.Drawing.Color.SpringGreen;
            this.ppvScrollBarExt8.ButtonRadius = 10;
            this.ppvScrollBarExt8.ButtonSize = 9;
            this.ppvScrollBarExt8.LargeChange = 10;
            this.ppvScrollBarExt8.Location = new System.Drawing.Point(449, 27);
            this.ppvScrollBarExt8.Margin = new System.Windows.Forms.Padding(0);
            this.ppvScrollBarExt8.Maximum = 100;
            this.ppvScrollBarExt8.Minimum = 0;
            this.ppvScrollBarExt8.Name = "ppvScrollBarExt8";
            this.ppvScrollBarExt8.Size = new System.Drawing.Size(17, 278);
            this.ppvScrollBarExt8.SmallChange = 1;
            this.ppvScrollBarExt8.TabIndex = 12;
            this.ppvScrollBarExt8.Value = 0;
            this.ppvScrollBarExt8.VisibleValue = 10;
            // 
            // ppDataGridView4
            // 
            this.ppDataGridView4.AllowUserToAddRows = false;
            this.ppDataGridView4.AllowUserToDeleteRows = false;
            this.ppDataGridView4.AutoWidth = true;
            this.ppDataGridView4.BackgroundColor = System.Drawing.Color.White;
            this.ppDataGridView4.BorderColor = System.Drawing.Color.LightGray;
            this.ppDataGridView4.BorderRadius = 10;
            this.ppDataGridView4.CellDefaultColor = System.Drawing.Color.White;
            this.ppDataGridView4.CellEvenColor = System.Drawing.Color.WhiteSmoke;
            this.ppDataGridView4.CellFont = new System.Drawing.Font("微软雅黑", 10F);
            this.ppDataGridView4.CellForeColor = System.Drawing.Color.Black;
            this.ppDataGridView4.CellSelectBackColorBegin = System.Drawing.Color.White;
            this.ppDataGridView4.CellSelectBackColorEnd = System.Drawing.Color.Silver;
            this.ppDataGridView4.ColumnBackColorBegin = System.Drawing.Color.White;
            this.ppDataGridView4.ColumnBackColorEnd = System.Drawing.Color.LightGray;
            this.ppDataGridView4.ColumnFont = new System.Drawing.Font("微软雅黑", 11F);
            this.ppDataGridView4.ColumnForeColor = System.Drawing.Color.Black;
            this.ppDataGridView4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ppDataGridView4.EnableEvenBackColor = false;
            this.ppDataGridView4.FirstColumnText = "Num.";
            this.ppDataGridView4.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.ppDataGridView4.Location = new System.Drawing.Point(472, 369);
            this.ppDataGridView4.Name = "ppDataGridView4";
            this.ppDataGridView4.ReadOnly = true;
            this.ppDataGridView4.RowTemplate.Height = 30;
            this.ppDataGridView4.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.ppDataGridView4.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ppDataGridView4.ShowBorder = true;
            this.ppDataGridView4.ShowCellBorder = true;
            this.ppDataGridView4.ShowColumnCheckBox = false;
            this.ppDataGridView4.Size = new System.Drawing.Size(441, 278);
            this.ppDataGridView4.TabIndex = 7;
            this.ppDataGridView4.TextAlign = PPSkin.PPDataGridView.textAlign.Center;
            this.ppDataGridView4.TextAlignColumn = PPSkin.PPDataGridView.textAlign.Center;
            this.ppDataGridView4.TextAlignRowHead = PPSkin.PPDataGridView.textAlign.Center;
            this.ppDataGridView4.TextDrawMode = PPSkin.PPDataGridView.DrawMode.Clear;
            // 
            // ppDataGridView3
            // 
            this.ppDataGridView3.AllowUserToAddRows = false;
            this.ppDataGridView3.AllowUserToDeleteRows = false;
            this.ppDataGridView3.AutoWidth = true;
            this.ppDataGridView3.BackgroundColor = System.Drawing.Color.White;
            this.ppDataGridView3.BorderColor = System.Drawing.Color.DodgerBlue;
            this.ppDataGridView3.BorderRadius = 10;
            this.ppDataGridView3.CellDefaultColor = System.Drawing.Color.AliceBlue;
            this.ppDataGridView3.CellEvenColor = System.Drawing.Color.CornflowerBlue;
            this.ppDataGridView3.CellFont = new System.Drawing.Font("微软雅黑", 10F);
            this.ppDataGridView3.CellForeColor = System.Drawing.Color.Black;
            this.ppDataGridView3.CellSelectBackColorBegin = System.Drawing.Color.Aqua;
            this.ppDataGridView3.CellSelectBackColorEnd = System.Drawing.Color.Turquoise;
            this.ppDataGridView3.ColumnBackColorBegin = System.Drawing.Color.DeepSkyBlue;
            this.ppDataGridView3.ColumnBackColorEnd = System.Drawing.Color.DodgerBlue;
            this.ppDataGridView3.ColumnFont = new System.Drawing.Font("微软雅黑", 11F);
            this.ppDataGridView3.ColumnForeColor = System.Drawing.Color.Black;
            this.ppDataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ppDataGridView3.EnableEvenBackColor = true;
            this.ppDataGridView3.FirstColumnText = "No.";
            this.ppDataGridView3.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.ppDataGridView3.Location = new System.Drawing.Point(16, 369);
            this.ppDataGridView3.Name = "ppDataGridView3";
            this.ppDataGridView3.ReadOnly = true;
            this.ppDataGridView3.RowTemplate.Height = 30;
            this.ppDataGridView3.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.ppDataGridView3.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ppDataGridView3.ShowBorder = false;
            this.ppDataGridView3.ShowCellBorder = false;
            this.ppDataGridView3.ShowColumnCheckBox = false;
            this.ppDataGridView3.Size = new System.Drawing.Size(433, 278);
            this.ppDataGridView3.TabIndex = 6;
            this.ppDataGridView3.TextAlign = PPSkin.PPDataGridView.textAlign.Center;
            this.ppDataGridView3.TextAlignColumn = PPSkin.PPDataGridView.textAlign.Center;
            this.ppDataGridView3.TextAlignRowHead = PPSkin.PPDataGridView.textAlign.Center;
            this.ppDataGridView3.TextDrawMode = PPSkin.PPDataGridView.DrawMode.Clear;
            // 
            // ppDataGridView2
            // 
            this.ppDataGridView2.AllowUserToAddRows = false;
            this.ppDataGridView2.AllowUserToDeleteRows = false;
            this.ppDataGridView2.AutoWidth = true;
            this.ppDataGridView2.BackgroundColor = System.Drawing.Color.White;
            this.ppDataGridView2.BorderColor = System.Drawing.Color.LightGray;
            this.ppDataGridView2.BorderRadius = 0;
            this.ppDataGridView2.CellDefaultColor = System.Drawing.Color.White;
            this.ppDataGridView2.CellEvenColor = System.Drawing.Color.WhiteSmoke;
            this.ppDataGridView2.CellFont = new System.Drawing.Font("微软雅黑", 10F);
            this.ppDataGridView2.CellForeColor = System.Drawing.Color.Black;
            this.ppDataGridView2.CellSelectBackColorBegin = System.Drawing.Color.White;
            this.ppDataGridView2.CellSelectBackColorEnd = System.Drawing.Color.Silver;
            this.ppDataGridView2.ColumnBackColorBegin = System.Drawing.Color.White;
            this.ppDataGridView2.ColumnBackColorEnd = System.Drawing.Color.LightGray;
            this.ppDataGridView2.ColumnFont = new System.Drawing.Font("微软雅黑", 11F);
            this.ppDataGridView2.ColumnForeColor = System.Drawing.Color.Black;
            this.ppDataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ppDataGridView2.EnableEvenBackColor = false;
            this.ppDataGridView2.FirstColumnText = "Num.";
            this.ppDataGridView2.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.ppDataGridView2.Location = new System.Drawing.Point(472, 27);
            this.ppDataGridView2.Name = "ppDataGridView2";
            this.ppDataGridView2.ReadOnly = true;
            this.ppDataGridView2.RowTemplate.Height = 23;
            this.ppDataGridView2.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.ppDataGridView2.ShowBorder = true;
            this.ppDataGridView2.ShowCellBorder = true;
            this.ppDataGridView2.ShowColumnCheckBox = false;
            this.ppDataGridView2.Size = new System.Drawing.Size(441, 278);
            this.ppDataGridView2.TabIndex = 5;
            this.ppDataGridView2.TextAlign = PPSkin.PPDataGridView.textAlign.Center;
            this.ppDataGridView2.TextAlignColumn = PPSkin.PPDataGridView.textAlign.Center;
            this.ppDataGridView2.TextAlignRowHead = PPSkin.PPDataGridView.textAlign.Center;
            this.ppDataGridView2.TextDrawMode = PPSkin.PPDataGridView.DrawMode.Clear;
            // 
            // ppDataGridView1
            // 
            this.ppDataGridView1.AllowUserToAddRows = false;
            this.ppDataGridView1.AllowUserToDeleteRows = false;
            this.ppDataGridView1.AutoWidth = true;
            this.ppDataGridView1.BackgroundColor = System.Drawing.Color.White;
            this.ppDataGridView1.BorderColor = System.Drawing.Color.LightGray;
            this.ppDataGridView1.BorderRadius = 0;
            this.ppDataGridView1.CellDefaultColor = System.Drawing.Color.LightCoral;
            this.ppDataGridView1.CellEvenColor = System.Drawing.Color.OrangeRed;
            this.ppDataGridView1.CellFont = new System.Drawing.Font("微软雅黑", 10F);
            this.ppDataGridView1.CellForeColor = System.Drawing.Color.Black;
            this.ppDataGridView1.CellSelectBackColorBegin = System.Drawing.Color.MediumTurquoise;
            this.ppDataGridView1.CellSelectBackColorEnd = System.Drawing.Color.LightSeaGreen;
            this.ppDataGridView1.ColumnBackColorBegin = System.Drawing.Color.PaleGreen;
            this.ppDataGridView1.ColumnBackColorEnd = System.Drawing.Color.LimeGreen;
            this.ppDataGridView1.ColumnFont = new System.Drawing.Font("微软雅黑", 11F);
            this.ppDataGridView1.ColumnForeColor = System.Drawing.Color.Black;
            this.ppDataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ppDataGridView1.EnableEvenBackColor = true;
            this.ppDataGridView1.FirstColumnText = "Num.";
            this.ppDataGridView1.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.ppDataGridView1.Location = new System.Drawing.Point(16, 27);
            this.ppDataGridView1.Name = "ppDataGridView1";
            this.ppDataGridView1.ReadOnly = true;
            this.ppDataGridView1.RowTemplate.Height = 30;
            this.ppDataGridView1.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.ppDataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ppDataGridView1.ShowBorder = true;
            this.ppDataGridView1.ShowCellBorder = true;
            this.ppDataGridView1.ShowColumnCheckBox = false;
            this.ppDataGridView1.Size = new System.Drawing.Size(433, 278);
            this.ppDataGridView1.TabIndex = 4;
            this.ppDataGridView1.TextAlign = PPSkin.PPDataGridView.textAlign.Center;
            this.ppDataGridView1.TextAlignColumn = PPSkin.PPDataGridView.textAlign.Center;
            this.ppDataGridView1.TextAlignRowHead = PPSkin.PPDataGridView.textAlign.Center;
            this.ppDataGridView1.TextDrawMode = PPSkin.PPDataGridView.DrawMode.Clear;
            // 
            // tabPage26
            // 
            this.tabPage26.BackColor = System.Drawing.Color.White;
            this.tabPage26.Controls.Add(this.ppButton35);
            this.tabPage26.Controls.Add(this.unlockerPanel3);
            this.tabPage26.Controls.Add(this.unlockerPanel2);
            this.tabPage26.Controls.Add(this.unlockerPanel1);
            this.tabPage26.Controls.Add(this.ledBar1);
            this.tabPage26.Controls.Add(this.ledNum14);
            this.tabPage26.Controls.Add(this.ledNum13);
            this.tabPage26.Controls.Add(this.ledNum12);
            this.tabPage26.Controls.Add(this.ledNum11);
            this.tabPage26.Controls.Add(this.ledNum10);
            this.tabPage26.Controls.Add(this.ledNum9);
            this.tabPage26.Controls.Add(this.ledNum8);
            this.tabPage26.Controls.Add(this.ledNum7);
            this.tabPage26.Controls.Add(this.ledNum6);
            this.tabPage26.Controls.Add(this.ledNum5);
            this.tabPage26.Controls.Add(this.ledNum4);
            this.tabPage26.Controls.Add(this.ledNum3);
            this.tabPage26.Controls.Add(this.ledNum2);
            this.tabPage26.Controls.Add(this.ledNum1);
            this.tabPage26.Location = new System.Drawing.Point(0, 30);
            this.tabPage26.Name = "tabPage26";
            this.tabPage26.Size = new System.Drawing.Size(928, 675);
            this.tabPage26.TabIndex = 7;
            this.tabPage26.Text = "其他控件";
            // 
            // ppButton35
            // 
            this.ppButton35.BackColor = System.Drawing.Color.White;
            this.ppButton35.BaseColor = System.Drawing.Color.White;
            this.ppButton35.DialogResult = System.Windows.Forms.DialogResult.None;
            this.ppButton35.IcoDown = null;
            this.ppButton35.IcoIn = null;
            this.ppButton35.IcoRegular = null;
            this.ppButton35.IcoSize = new System.Drawing.Size(15, 15);
            this.ppButton35.LineColor = System.Drawing.Color.LightGray;
            this.ppButton35.LineWidth = 1;
            this.ppButton35.Location = new System.Drawing.Point(68, 618);
            this.ppButton35.MouseDownBaseColor = System.Drawing.Color.LightGray;
            this.ppButton35.MouseDownLineColor = System.Drawing.Color.Silver;
            this.ppButton35.MouseDownTextColor = System.Drawing.Color.Black;
            this.ppButton35.MouseInBaseColor = System.Drawing.Color.LightGray;
            this.ppButton35.MouseInLineColor = System.Drawing.Color.Silver;
            this.ppButton35.MouseInTextColor = System.Drawing.Color.Black;
            this.ppButton35.Name = "ppButton35";
            this.ppButton35.Radius = 3;
            this.ppButton35.RegularBaseColor = System.Drawing.Color.White;
            this.ppButton35.RegularLineColor = System.Drawing.Color.LightGray;
            this.ppButton35.RegularTextColor = System.Drawing.Color.Black;
            this.ppButton35.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppButton35.Size = new System.Drawing.Size(75, 30);
            this.ppButton35.TabIndex = 37;
            this.ppButton35.Text = "截图";
            this.ppButton35.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.ppButton35.TextColor = System.Drawing.Color.Black;
            this.ppButton35.TextDrawMode = PPSkin.Enums.DrawMode.Anti;
            this.ppButton35.TipAutoCloseTime = 5000;
            this.ppButton35.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppButton35.TipCloseOnLeave = true;
            this.ppButton35.TipDeviation = new System.Drawing.Size(0, 0);
            this.ppButton35.TipFontSize = 10;
            this.ppButton35.TipFontText = null;
            this.ppButton35.TipForeColor = System.Drawing.Color.White;
            this.ppButton35.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.ppButton35.TipText = null;
            this.ppButton35.TipTopMoust = true;
            this.ppButton35.UseSpecial = true;
            this.ppButton35.UseVisualStyleBackColor = false;
            this.ppButton35.Xoffset = 0;
            this.ppButton35.XoffsetIco = 0;
            this.ppButton35.Yoffset = 0;
            this.ppButton35.YoffsetIco = 0;
            this.ppButton35.Click += new System.EventHandler(this.ppButton35_Click);
            // 
            // unlockerPanel3
            // 
            this.unlockerPanel3.ButtonSize = new System.Drawing.Size(70, 70);
            this.unlockerPanel3.LineColor = System.Drawing.Color.DodgerBlue;
            this.unlockerPanel3.LineErrorColor = System.Drawing.Color.Red;
            this.unlockerPanel3.LinePassColor = System.Drawing.Color.Green;
            this.unlockerPanel3.LineWidth = 20;
            this.unlockerPanel3.Location = new System.Drawing.Point(625, 307);
            this.unlockerPanel3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.unlockerPanel3.MinimumSize = new System.Drawing.Size(175, 212);
            this.unlockerPanel3.Name = "unlockerPanel3";
            this.unlockerPanel3.NormalColor = System.Drawing.Color.Orange;
            this.unlockerPanel3.Password = "1234";
            this.unlockerPanel3.Size = new System.Drawing.Size(266, 266);
            this.unlockerPanel3.TabIndex = 36;
            // 
            // unlockerPanel2
            // 
            this.unlockerPanel2.ButtonSize = new System.Drawing.Size(30, 30);
            this.unlockerPanel2.LineColor = System.Drawing.Color.DeepSkyBlue;
            this.unlockerPanel2.LineErrorColor = System.Drawing.Color.DarkOrange;
            this.unlockerPanel2.LinePassColor = System.Drawing.Color.LawnGreen;
            this.unlockerPanel2.LineWidth = 10;
            this.unlockerPanel2.Location = new System.Drawing.Point(353, 307);
            this.unlockerPanel2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.unlockerPanel2.MinimumSize = new System.Drawing.Size(175, 212);
            this.unlockerPanel2.Name = "unlockerPanel2";
            this.unlockerPanel2.NormalColor = System.Drawing.Color.HotPink;
            this.unlockerPanel2.Password = "123456789";
            this.unlockerPanel2.Size = new System.Drawing.Size(266, 266);
            this.unlockerPanel2.TabIndex = 35;
            // 
            // unlockerPanel1
            // 
            this.unlockerPanel1.ButtonSize = new System.Drawing.Size(50, 50);
            this.unlockerPanel1.LineColor = System.Drawing.Color.DodgerBlue;
            this.unlockerPanel1.LineErrorColor = System.Drawing.Color.Red;
            this.unlockerPanel1.LinePassColor = System.Drawing.Color.Green;
            this.unlockerPanel1.LineWidth = 20;
            this.unlockerPanel1.Location = new System.Drawing.Point(52, 307);
            this.unlockerPanel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.unlockerPanel1.MinimumSize = new System.Drawing.Size(175, 212);
            this.unlockerPanel1.Name = "unlockerPanel1";
            this.unlockerPanel1.NormalColor = System.Drawing.Color.Silver;
            this.unlockerPanel1.Password = "1234";
            this.unlockerPanel1.Size = new System.Drawing.Size(266, 266);
            this.unlockerPanel1.TabIndex = 34;
            // 
            // ledBar1
            // 
            this.ledBar1.CharInterval = 2;
            this.ledBar1.CharNum = 23;
            this.ledBar1.Location = new System.Drawing.Point(19, 184);
            this.ledBar1.Name = "ledBar1";
            this.ledBar1.NumColor = System.Drawing.Color.DodgerBlue;
            this.ledBar1.Size = new System.Drawing.Size(774, 59);
            this.ledBar1.SpaceWidth = 2;
            this.ledBar1.TabIndex = 33;
            // 
            // ledNum14
            // 
            this.ledNum14.Location = new System.Drawing.Point(864, 29);
            this.ledNum14.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ledNum14.Name = "ledNum14";
            this.ledNum14.NumChar = '_';
            this.ledNum14.NumColor = System.Drawing.Color.Red;
            this.ledNum14.Size = new System.Drawing.Size(59, 118);
            this.ledNum14.SpaceWidth = 4;
            this.ledNum14.TabIndex = 32;
            // 
            // ledNum13
            // 
            this.ledNum13.Location = new System.Drawing.Point(799, 29);
            this.ledNum13.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ledNum13.Name = "ledNum13";
            this.ledNum13.NumChar = ':';
            this.ledNum13.NumColor = System.Drawing.Color.OrangeRed;
            this.ledNum13.Size = new System.Drawing.Size(59, 118);
            this.ledNum13.SpaceWidth = 4;
            this.ledNum13.TabIndex = 31;
            // 
            // ledNum12
            // 
            this.ledNum12.Location = new System.Drawing.Point(734, 29);
            this.ledNum12.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ledNum12.Name = "ledNum12";
            this.ledNum12.NumChar = '.';
            this.ledNum12.NumColor = System.Drawing.Color.Chocolate;
            this.ledNum12.Size = new System.Drawing.Size(59, 118);
            this.ledNum12.SpaceWidth = 4;
            this.ledNum12.TabIndex = 30;
            // 
            // ledNum11
            // 
            this.ledNum11.Location = new System.Drawing.Point(669, 29);
            this.ledNum11.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ledNum11.Name = "ledNum11";
            this.ledNum11.NumChar = '-';
            this.ledNum11.NumColor = System.Drawing.Color.DarkOrange;
            this.ledNum11.Size = new System.Drawing.Size(59, 118);
            this.ledNum11.SpaceWidth = 4;
            this.ledNum11.TabIndex = 29;
            // 
            // ledNum10
            // 
            this.ledNum10.Location = new System.Drawing.Point(604, 29);
            this.ledNum10.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ledNum10.Name = "ledNum10";
            this.ledNum10.NumChar = '0';
            this.ledNum10.NumColor = System.Drawing.Color.Orange;
            this.ledNum10.Size = new System.Drawing.Size(59, 118);
            this.ledNum10.SpaceWidth = 4;
            this.ledNum10.TabIndex = 28;
            // 
            // ledNum9
            // 
            this.ledNum9.Location = new System.Drawing.Point(539, 29);
            this.ledNum9.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ledNum9.Name = "ledNum9";
            this.ledNum9.NumChar = '9';
            this.ledNum9.NumColor = System.Drawing.Color.Goldenrod;
            this.ledNum9.Size = new System.Drawing.Size(59, 118);
            this.ledNum9.SpaceWidth = 4;
            this.ledNum9.TabIndex = 27;
            // 
            // ledNum8
            // 
            this.ledNum8.Location = new System.Drawing.Point(474, 29);
            this.ledNum8.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ledNum8.Name = "ledNum8";
            this.ledNum8.NumChar = '8';
            this.ledNum8.NumColor = System.Drawing.Color.Gold;
            this.ledNum8.Size = new System.Drawing.Size(59, 118);
            this.ledNum8.SpaceWidth = 4;
            this.ledNum8.TabIndex = 26;
            // 
            // ledNum7
            // 
            this.ledNum7.Location = new System.Drawing.Point(409, 29);
            this.ledNum7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ledNum7.Name = "ledNum7";
            this.ledNum7.NumChar = '7';
            this.ledNum7.NumColor = System.Drawing.Color.Yellow;
            this.ledNum7.Size = new System.Drawing.Size(59, 118);
            this.ledNum7.SpaceWidth = 4;
            this.ledNum7.TabIndex = 25;
            // 
            // ledNum6
            // 
            this.ledNum6.Location = new System.Drawing.Point(344, 29);
            this.ledNum6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ledNum6.Name = "ledNum6";
            this.ledNum6.NumChar = '6';
            this.ledNum6.NumColor = System.Drawing.Color.MediumSpringGreen;
            this.ledNum6.Size = new System.Drawing.Size(59, 118);
            this.ledNum6.SpaceWidth = 4;
            this.ledNum6.TabIndex = 24;
            // 
            // ledNum5
            // 
            this.ledNum5.Location = new System.Drawing.Point(279, 29);
            this.ledNum5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ledNum5.Name = "ledNum5";
            this.ledNum5.NumChar = '5';
            this.ledNum5.NumColor = System.Drawing.Color.PaleTurquoise;
            this.ledNum5.Size = new System.Drawing.Size(59, 118);
            this.ledNum5.SpaceWidth = 4;
            this.ledNum5.TabIndex = 23;
            // 
            // ledNum4
            // 
            this.ledNum4.Location = new System.Drawing.Point(214, 29);
            this.ledNum4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ledNum4.Name = "ledNum4";
            this.ledNum4.NumChar = '4';
            this.ledNum4.NumColor = System.Drawing.Color.Cyan;
            this.ledNum4.Size = new System.Drawing.Size(59, 118);
            this.ledNum4.SpaceWidth = 4;
            this.ledNum4.TabIndex = 22;
            // 
            // ledNum3
            // 
            this.ledNum3.Location = new System.Drawing.Point(149, 29);
            this.ledNum3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ledNum3.Name = "ledNum3";
            this.ledNum3.NumChar = '3';
            this.ledNum3.NumColor = System.Drawing.Color.DarkTurquoise;
            this.ledNum3.Size = new System.Drawing.Size(59, 118);
            this.ledNum3.SpaceWidth = 4;
            this.ledNum3.TabIndex = 21;
            // 
            // ledNum2
            // 
            this.ledNum2.Location = new System.Drawing.Point(84, 29);
            this.ledNum2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ledNum2.Name = "ledNum2";
            this.ledNum2.NumChar = '2';
            this.ledNum2.NumColor = System.Drawing.Color.DeepSkyBlue;
            this.ledNum2.Size = new System.Drawing.Size(59, 118);
            this.ledNum2.SpaceWidth = 4;
            this.ledNum2.TabIndex = 20;
            // 
            // ledNum1
            // 
            this.ledNum1.Location = new System.Drawing.Point(19, 26);
            this.ledNum1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ledNum1.Name = "ledNum1";
            this.ledNum1.NumChar = '1';
            this.ledNum1.NumColor = System.Drawing.Color.DodgerBlue;
            this.ledNum1.Size = new System.Drawing.Size(59, 118);
            this.ledNum1.SpaceWidth = 4;
            this.ledNum1.TabIndex = 19;
            // 
            // tabPage27
            // 
            this.tabPage27.BackColor = System.Drawing.Color.White;
            this.tabPage27.Controls.Add(this.ppButton54);
            this.tabPage27.Controls.Add(this.ppButton53);
            this.tabPage27.Controls.Add(this.ppNumericUpDown5);
            this.tabPage27.Controls.Add(this.ppRoundProgressBar9);
            this.tabPage27.Controls.Add(this.ppComboboxEx6);
            this.tabPage27.Controls.Add(this.panel2);
            this.tabPage27.Controls.Add(this.ppButton43);
            this.tabPage27.Controls.Add(this.ppLabel1);
            this.tabPage27.Controls.Add(this.ppNumericUpDown4);
            this.tabPage27.Controls.Add(this.ppRadioButton11);
            this.tabPage27.Controls.Add(this.ppRadioButton10);
            this.tabPage27.Controls.Add(this.ppRadioButton9);
            this.tabPage27.Controls.Add(this.panel1);
            this.tabPage27.Controls.Add(this.ppButton34);
            this.tabPage27.Location = new System.Drawing.Point(0, 30);
            this.tabPage27.Name = "tabPage27";
            this.tabPage27.Size = new System.Drawing.Size(928, 675);
            this.tabPage27.TabIndex = 8;
            this.tabPage27.Text = "动画";
            // 
            // ppButton54
            // 
            this.ppButton54.BackColor = System.Drawing.Color.White;
            this.ppButton54.BaseColor = System.Drawing.Color.White;
            this.ppButton54.DialogResult = System.Windows.Forms.DialogResult.None;
            this.ppButton54.IcoDown = null;
            this.ppButton54.IcoIn = null;
            this.ppButton54.IcoRegular = null;
            this.ppButton54.IcoSize = new System.Drawing.Size(15, 15);
            this.ppButton54.LineColor = System.Drawing.Color.LightGray;
            this.ppButton54.LineWidth = 1;
            this.ppButton54.Location = new System.Drawing.Point(821, 488);
            this.ppButton54.MouseDownBaseColor = System.Drawing.Color.LightGray;
            this.ppButton54.MouseDownLineColor = System.Drawing.Color.Silver;
            this.ppButton54.MouseDownTextColor = System.Drawing.Color.Black;
            this.ppButton54.MouseInBaseColor = System.Drawing.Color.LightGray;
            this.ppButton54.MouseInLineColor = System.Drawing.Color.Silver;
            this.ppButton54.MouseInTextColor = System.Drawing.Color.Black;
            this.ppButton54.Name = "ppButton54";
            this.ppButton54.Radius = 3;
            this.ppButton54.RegularBaseColor = System.Drawing.Color.White;
            this.ppButton54.RegularLineColor = System.Drawing.Color.LightGray;
            this.ppButton54.RegularTextColor = System.Drawing.Color.Black;
            this.ppButton54.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppButton54.Size = new System.Drawing.Size(75, 30);
            this.ppButton54.TabIndex = 20;
            this.ppButton54.Text = "选择字体";
            this.ppButton54.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.ppButton54.TextColor = System.Drawing.Color.Black;
            this.ppButton54.TextDrawMode = PPSkin.Enums.DrawMode.Anti;
            this.ppButton54.TipAutoCloseTime = 5000;
            this.ppButton54.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppButton54.TipCloseOnLeave = true;
            this.ppButton54.TipDeviation = new System.Drawing.Size(0, 0);
            this.ppButton54.TipFontSize = 10;
            this.ppButton54.TipFontText = null;
            this.ppButton54.TipForeColor = System.Drawing.Color.White;
            this.ppButton54.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.ppButton54.TipText = null;
            this.ppButton54.TipTopMoust = true;
            this.ppButton54.UseSpecial = true;
            this.ppButton54.UseVisualStyleBackColor = false;
            this.ppButton54.Xoffset = 0;
            this.ppButton54.XoffsetIco = 0;
            this.ppButton54.Yoffset = 0;
            this.ppButton54.YoffsetIco = 0;
            this.ppButton54.Click += new System.EventHandler(this.ppButton54_Click);
            // 
            // ppButton53
            // 
            this.ppButton53.BackColor = System.Drawing.Color.White;
            this.ppButton53.BaseColor = System.Drawing.Color.White;
            this.ppButton53.DialogResult = System.Windows.Forms.DialogResult.None;
            this.ppButton53.IcoDown = null;
            this.ppButton53.IcoIn = null;
            this.ppButton53.IcoRegular = null;
            this.ppButton53.IcoSize = new System.Drawing.Size(15, 15);
            this.ppButton53.LineColor = System.Drawing.Color.LightGray;
            this.ppButton53.LineWidth = 1;
            this.ppButton53.Location = new System.Drawing.Point(108, 456);
            this.ppButton53.MouseDownBaseColor = System.Drawing.Color.LightGray;
            this.ppButton53.MouseDownLineColor = System.Drawing.Color.Silver;
            this.ppButton53.MouseDownTextColor = System.Drawing.Color.Black;
            this.ppButton53.MouseInBaseColor = System.Drawing.Color.LightGray;
            this.ppButton53.MouseInLineColor = System.Drawing.Color.Silver;
            this.ppButton53.MouseInTextColor = System.Drawing.Color.Black;
            this.ppButton53.Name = "ppButton53";
            this.ppButton53.Radius = 3;
            this.ppButton53.RegularBaseColor = System.Drawing.Color.White;
            this.ppButton53.RegularLineColor = System.Drawing.Color.LightGray;
            this.ppButton53.RegularTextColor = System.Drawing.Color.Black;
            this.ppButton53.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppButton53.Size = new System.Drawing.Size(75, 30);
            this.ppButton53.TabIndex = 19;
            this.ppButton53.Text = "开始";
            this.ppButton53.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.ppButton53.TextColor = System.Drawing.Color.Black;
            this.ppButton53.TextDrawMode = PPSkin.Enums.DrawMode.Anti;
            this.ppButton53.TipAutoCloseTime = 5000;
            this.ppButton53.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppButton53.TipCloseOnLeave = true;
            this.ppButton53.TipDeviation = new System.Drawing.Size(0, 0);
            this.ppButton53.TipFontSize = 10;
            this.ppButton53.TipFontText = null;
            this.ppButton53.TipForeColor = System.Drawing.Color.White;
            this.ppButton53.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.ppButton53.TipText = null;
            this.ppButton53.TipTopMoust = true;
            this.ppButton53.UseSpecial = true;
            this.ppButton53.UseVisualStyleBackColor = false;
            this.ppButton53.Xoffset = 0;
            this.ppButton53.XoffsetIco = 0;
            this.ppButton53.Yoffset = 0;
            this.ppButton53.YoffsetIco = 0;
            this.ppButton53.Click += new System.EventHandler(this.ppButton53_Click);
            // 
            // ppNumericUpDown5
            // 
            this.ppNumericUpDown5.BackColor = System.Drawing.Color.White;
            this.ppNumericUpDown5.BaseColor = System.Drawing.Color.White;
            this.ppNumericUpDown5.BorderColor = System.Drawing.Color.LightGray;
            this.ppNumericUpDown5.BorderWidth = 1F;
            this.ppNumericUpDown5.ButtonPicDown = ((System.Drawing.Bitmap)(resources.GetObject("ppNumericUpDown5.ButtonPicDown")));
            this.ppNumericUpDown5.ButtonPicDownHover = ((System.Drawing.Bitmap)(resources.GetObject("ppNumericUpDown5.ButtonPicDownHover")));
            this.ppNumericUpDown5.ButtonPicUP = ((System.Drawing.Bitmap)(resources.GetObject("ppNumericUpDown5.ButtonPicUP")));
            this.ppNumericUpDown5.ButtonPicUPHover = ((System.Drawing.Bitmap)(resources.GetObject("ppNumericUpDown5.ButtonPicUPHover")));
            this.ppNumericUpDown5.ButtonSize = new System.Drawing.Size(13, 13);
            this.ppNumericUpDown5.Location = new System.Drawing.Point(30, 454);
            this.ppNumericUpDown5.Maxinum = 100;
            this.ppNumericUpDown5.Mininum = 0;
            this.ppNumericUpDown5.Name = "ppNumericUpDown5";
            this.ppNumericUpDown5.PadLeftCount = 0;
            this.ppNumericUpDown5.Radius = 5;
            this.ppNumericUpDown5.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppNumericUpDown5.ShowBorder = true;
            this.ppNumericUpDown5.Size = new System.Drawing.Size(53, 32);
            this.ppNumericUpDown5.TabIndex = 18;
            this.ppNumericUpDown5.Value = 0;
            // 
            // ppRoundProgressBar9
            // 
            this.ppRoundProgressBar9.BaseColor = System.Drawing.Color.LightGray;
            this.ppRoundProgressBar9.CircleWidth = 16;
            this.ppRoundProgressBar9.Direction = PPSkin.PPRoundProgressBar.DIRECTION.ClockWise;
            this.ppRoundProgressBar9.InsideColor = System.Drawing.Color.LightGray;
            this.ppRoundProgressBar9.Location = new System.Drawing.Point(30, 509);
            this.ppRoundProgressBar9.Maximum = ((long)(100));
            this.ppRoundProgressBar9.Minimum = ((long)(0));
            this.ppRoundProgressBar9.Name = "ppRoundProgressBar9";
            this.ppRoundProgressBar9.ShowType = PPSkin.PPRoundProgressBar.SHOWType.RING;
            this.ppRoundProgressBar9.Size = new System.Drawing.Size(150, 150);
            this.ppRoundProgressBar9.StartAngle = 0;
            this.ppRoundProgressBar9.Step = ((long)(1));
            this.ppRoundProgressBar9.TabIndex = 17;
            this.ppRoundProgressBar9.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.ppRoundProgressBar9.Value = ((long)(0));
            this.ppRoundProgressBar9.ValueColor = System.Drawing.Color.DodgerBlue;
            this.ppRoundProgressBar9.ValueType = PPSkin.PPRoundProgressBar.VALUETYPE.PERCENT;
            this.ppRoundProgressBar9.ValueChanged += new System.EventHandler(this.ppRoundProgressBar9_ValueChanged);
            // 
            // ppComboboxEx6
            // 
            this.ppComboboxEx6.BackColor = System.Drawing.Color.White;
            this.ppComboboxEx6.BaseColor = System.Drawing.Color.White;
            this.ppComboboxEx6.BorderColor = System.Drawing.Color.LightGray;
            this.ppComboboxEx6.BorderWidth = 1F;
            this.ppComboboxEx6.DropbaseColor = System.Drawing.Color.White;
            this.ppComboboxEx6.DropborderColor = System.Drawing.Color.LightGray;
            this.ppComboboxEx6.DropbuttonColor = System.Drawing.Color.White;
            this.ppComboboxEx6.DropbuttonMouseDownColor = System.Drawing.Color.DimGray;
            this.ppComboboxEx6.DropbuttonMouseInColor = System.Drawing.Color.LightGray;
            this.ppComboboxEx6.DropbuttonRadius = 5;
            this.ppComboboxEx6.DropbuttonSelectColor = System.Drawing.Color.DimGray;
            this.ppComboboxEx6.DropformRadius = 5;
            this.ppComboboxEx6.DropTextAlign = PPSkin.PPComboboxEx.Align.LEFT;
            this.ppComboboxEx6.Items.AddRange(new object[] {
            "UniformMotion",
            "EaseIn",
            "EaseOut",
            "EaseBoth",
            "BackIn",
            "BackOut",
            "BackBoth",
            "BounceIn",
            "BounceOut",
            "BounceBoth",
            "ElasticIn",
            "ElasticOut",
            "ElasticBoth",
            "QuadraticIn",
            "QuadraticOut"});
            this.ppComboboxEx6.Location = new System.Drawing.Point(16, 15);
            this.ppComboboxEx6.MaxDropDownItems = 8;
            this.ppComboboxEx6.Name = "ppComboboxEx6";
            this.ppComboboxEx6.Radius = 5;
            this.ppComboboxEx6.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppComboboxEx6.SelectedIndex = -1;
            this.ppComboboxEx6.SelectedItem = null;
            this.ppComboboxEx6.SelectedText = "";
            this.ppComboboxEx6.SelectedValue = null;
            this.ppComboboxEx6.Size = new System.Drawing.Size(167, 33);
            this.ppComboboxEx6.TabIndex = 16;
            this.ppComboboxEx6.TextAlign = PPSkin.PPComboboxEx.Align.LEFT;
            this.ppComboboxEx6.TextDrawMode = PPSkin.Enums.DrawMode.Anti;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Location = new System.Drawing.Point(809, 15);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(26, 30);
            this.panel2.TabIndex = 15;
            this.panel2.Click += new System.EventHandler(this.panel2_Click);
            // 
            // ppButton43
            // 
            this.ppButton43.BackColor = System.Drawing.Color.White;
            this.ppButton43.BaseColor = System.Drawing.Color.White;
            this.ppButton43.DialogResult = System.Windows.Forms.DialogResult.None;
            this.ppButton43.IcoDown = null;
            this.ppButton43.IcoIn = null;
            this.ppButton43.IcoRegular = null;
            this.ppButton43.IcoSize = new System.Drawing.Size(15, 15);
            this.ppButton43.LineColor = System.Drawing.Color.LightGray;
            this.ppButton43.LineWidth = 1;
            this.ppButton43.Location = new System.Drawing.Point(841, 15);
            this.ppButton43.MouseDownBaseColor = System.Drawing.Color.LightGray;
            this.ppButton43.MouseDownLineColor = System.Drawing.Color.Silver;
            this.ppButton43.MouseDownTextColor = System.Drawing.Color.Black;
            this.ppButton43.MouseInBaseColor = System.Drawing.Color.LightGray;
            this.ppButton43.MouseInLineColor = System.Drawing.Color.Silver;
            this.ppButton43.MouseInTextColor = System.Drawing.Color.Black;
            this.ppButton43.Name = "ppButton43";
            this.ppButton43.Radius = 3;
            this.ppButton43.RegularBaseColor = System.Drawing.Color.White;
            this.ppButton43.RegularLineColor = System.Drawing.Color.LightGray;
            this.ppButton43.RegularTextColor = System.Drawing.Color.Black;
            this.ppButton43.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppButton43.Size = new System.Drawing.Size(75, 30);
            this.ppButton43.TabIndex = 14;
            this.ppButton43.Text = "开始";
            this.ppButton43.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.ppButton43.TextColor = System.Drawing.Color.Black;
            this.ppButton43.TextDrawMode = PPSkin.Enums.DrawMode.Anti;
            this.ppButton43.TipAutoCloseTime = 5000;
            this.ppButton43.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppButton43.TipCloseOnLeave = true;
            this.ppButton43.TipDeviation = new System.Drawing.Size(0, 0);
            this.ppButton43.TipFontSize = 10;
            this.ppButton43.TipFontText = null;
            this.ppButton43.TipForeColor = System.Drawing.Color.White;
            this.ppButton43.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.ppButton43.TipText = null;
            this.ppButton43.TipTopMoust = true;
            this.ppButton43.UseSpecial = true;
            this.ppButton43.UseVisualStyleBackColor = false;
            this.ppButton43.Xoffset = 0;
            this.ppButton43.XoffsetIco = 0;
            this.ppButton43.Yoffset = 0;
            this.ppButton43.YoffsetIco = 0;
            this.ppButton43.Click += new System.EventHandler(this.ppButton43_Click);
            // 
            // ppLabel1
            // 
            this.ppLabel1.AutoSize = true;
            this.ppLabel1.BackColor = System.Drawing.Color.White;
            this.ppLabel1.Location = new System.Drawing.Point(27, 75);
            this.ppLabel1.Name = "ppLabel1";
            this.ppLabel1.Size = new System.Drawing.Size(56, 17);
            this.ppLabel1.TabIndex = 13;
            this.ppLabel1.Text = "动画时间";
            this.ppLabel1.TextDrawMode = PPSkin.Enums.DrawMode.Anti;
            // 
            // ppNumericUpDown4
            // 
            this.ppNumericUpDown4.BackColor = System.Drawing.Color.White;
            this.ppNumericUpDown4.BaseColor = System.Drawing.Color.White;
            this.ppNumericUpDown4.BorderColor = System.Drawing.Color.LightGray;
            this.ppNumericUpDown4.BorderWidth = 1F;
            this.ppNumericUpDown4.ButtonPicDown = ((System.Drawing.Bitmap)(resources.GetObject("ppNumericUpDown4.ButtonPicDown")));
            this.ppNumericUpDown4.ButtonPicDownHover = ((System.Drawing.Bitmap)(resources.GetObject("ppNumericUpDown4.ButtonPicDownHover")));
            this.ppNumericUpDown4.ButtonPicUP = ((System.Drawing.Bitmap)(resources.GetObject("ppNumericUpDown4.ButtonPicUP")));
            this.ppNumericUpDown4.ButtonPicUPHover = ((System.Drawing.Bitmap)(resources.GetObject("ppNumericUpDown4.ButtonPicUPHover")));
            this.ppNumericUpDown4.ButtonSize = new System.Drawing.Size(13, 13);
            this.ppNumericUpDown4.Location = new System.Drawing.Point(89, 68);
            this.ppNumericUpDown4.Maxinum = 10000;
            this.ppNumericUpDown4.Mininum = 10;
            this.ppNumericUpDown4.Name = "ppNumericUpDown4";
            this.ppNumericUpDown4.PadLeftCount = 0;
            this.ppNumericUpDown4.Radius = 5;
            this.ppNumericUpDown4.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppNumericUpDown4.ShowBorder = true;
            this.ppNumericUpDown4.Size = new System.Drawing.Size(94, 32);
            this.ppNumericUpDown4.TabIndex = 12;
            this.ppNumericUpDown4.Value = 2000;
            // 
            // ppRadioButton11
            // 
            this.ppRadioButton11.AutoSize = true;
            this.ppRadioButton11.BackColor = System.Drawing.Color.White;
            this.ppRadioButton11.CheckAlign = PPSkin.PPRadioButton.Align.Left;
            this.ppRadioButton11.CheckedForeColor = System.Drawing.Color.DodgerBlue;
            this.ppRadioButton11.CheckedPic = ((System.Drawing.Image)(resources.GetObject("ppRadioButton11.CheckedPic")));
            this.ppRadioButton11.Location = new System.Drawing.Point(418, 20);
            this.ppRadioButton11.Name = "ppRadioButton11";
            this.ppRadioButton11.PicOffset = new System.Drawing.Point(0, 0);
            this.ppRadioButton11.PicSize = new System.Drawing.Size(15, 15);
            this.ppRadioButton11.Size = new System.Drawing.Size(58, 21);
            this.ppRadioButton11.TabIndex = 11;
            this.ppRadioButton11.TabStop = true;
            this.ppRadioButton11.Text = "type3";
            this.ppRadioButton11.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.ppRadioButton11.UnCheckedForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.ppRadioButton11.unCheckedPic = ((System.Drawing.Image)(resources.GetObject("ppRadioButton11.unCheckedPic")));
            this.ppRadioButton11.UseVisualStyleBackColor = false;
            // 
            // ppRadioButton10
            // 
            this.ppRadioButton10.AutoSize = true;
            this.ppRadioButton10.BackColor = System.Drawing.Color.White;
            this.ppRadioButton10.CheckAlign = PPSkin.PPRadioButton.Align.Left;
            this.ppRadioButton10.CheckedForeColor = System.Drawing.Color.DodgerBlue;
            this.ppRadioButton10.CheckedPic = ((System.Drawing.Image)(resources.GetObject("ppRadioButton10.CheckedPic")));
            this.ppRadioButton10.Location = new System.Drawing.Point(354, 20);
            this.ppRadioButton10.Name = "ppRadioButton10";
            this.ppRadioButton10.PicOffset = new System.Drawing.Point(0, 0);
            this.ppRadioButton10.PicSize = new System.Drawing.Size(15, 15);
            this.ppRadioButton10.Size = new System.Drawing.Size(58, 21);
            this.ppRadioButton10.TabIndex = 10;
            this.ppRadioButton10.TabStop = true;
            this.ppRadioButton10.Text = "type2";
            this.ppRadioButton10.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.ppRadioButton10.UnCheckedForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.ppRadioButton10.unCheckedPic = ((System.Drawing.Image)(resources.GetObject("ppRadioButton10.unCheckedPic")));
            this.ppRadioButton10.UseVisualStyleBackColor = false;
            // 
            // ppRadioButton9
            // 
            this.ppRadioButton9.AutoSize = true;
            this.ppRadioButton9.BackColor = System.Drawing.Color.White;
            this.ppRadioButton9.CheckAlign = PPSkin.PPRadioButton.Align.Left;
            this.ppRadioButton9.Checked = true;
            this.ppRadioButton9.CheckedForeColor = System.Drawing.Color.DodgerBlue;
            this.ppRadioButton9.CheckedPic = ((System.Drawing.Image)(resources.GetObject("ppRadioButton9.CheckedPic")));
            this.ppRadioButton9.Location = new System.Drawing.Point(290, 20);
            this.ppRadioButton9.Name = "ppRadioButton9";
            this.ppRadioButton9.PicOffset = new System.Drawing.Point(0, 0);
            this.ppRadioButton9.PicSize = new System.Drawing.Size(15, 15);
            this.ppRadioButton9.Size = new System.Drawing.Size(58, 21);
            this.ppRadioButton9.TabIndex = 9;
            this.ppRadioButton9.TabStop = true;
            this.ppRadioButton9.Text = "type1";
            this.ppRadioButton9.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.ppRadioButton9.UnCheckedForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.ppRadioButton9.unCheckedPic = ((System.Drawing.Image)(resources.GetObject("ppRadioButton9.unCheckedPic")));
            this.ppRadioButton9.UseVisualStyleBackColor = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Red;
            this.panel1.Location = new System.Drawing.Point(200, 200);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(100, 100);
            this.panel1.TabIndex = 8;
            // 
            // ppButton34
            // 
            this.ppButton34.BackColor = System.Drawing.Color.White;
            this.ppButton34.BaseColor = System.Drawing.Color.White;
            this.ppButton34.DialogResult = System.Windows.Forms.DialogResult.None;
            this.ppButton34.IcoDown = null;
            this.ppButton34.IcoIn = null;
            this.ppButton34.IcoRegular = null;
            this.ppButton34.IcoSize = new System.Drawing.Size(15, 15);
            this.ppButton34.LineColor = System.Drawing.Color.LightGray;
            this.ppButton34.LineWidth = 1;
            this.ppButton34.Location = new System.Drawing.Point(189, 18);
            this.ppButton34.MouseDownBaseColor = System.Drawing.Color.LightGray;
            this.ppButton34.MouseDownLineColor = System.Drawing.Color.Silver;
            this.ppButton34.MouseDownTextColor = System.Drawing.Color.Black;
            this.ppButton34.MouseInBaseColor = System.Drawing.Color.LightGray;
            this.ppButton34.MouseInLineColor = System.Drawing.Color.Silver;
            this.ppButton34.MouseInTextColor = System.Drawing.Color.Black;
            this.ppButton34.Name = "ppButton34";
            this.ppButton34.Radius = 3;
            this.ppButton34.RegularBaseColor = System.Drawing.Color.White;
            this.ppButton34.RegularLineColor = System.Drawing.Color.LightGray;
            this.ppButton34.RegularTextColor = System.Drawing.Color.Black;
            this.ppButton34.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppButton34.Size = new System.Drawing.Size(75, 30);
            this.ppButton34.TabIndex = 7;
            this.ppButton34.Text = "开始";
            this.ppButton34.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.ppButton34.TextColor = System.Drawing.Color.Black;
            this.ppButton34.TextDrawMode = PPSkin.Enums.DrawMode.Anti;
            this.ppButton34.TipAutoCloseTime = 5000;
            this.ppButton34.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppButton34.TipCloseOnLeave = true;
            this.ppButton34.TipDeviation = new System.Drawing.Size(0, 0);
            this.ppButton34.TipFontSize = 10;
            this.ppButton34.TipFontText = null;
            this.ppButton34.TipForeColor = System.Drawing.Color.White;
            this.ppButton34.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.ppButton34.TipText = null;
            this.ppButton34.TipTopMoust = true;
            this.ppButton34.UseSpecial = true;
            this.ppButton34.UseVisualStyleBackColor = false;
            this.ppButton34.Xoffset = 0;
            this.ppButton34.XoffsetIco = 0;
            this.ppButton34.Yoffset = 0;
            this.ppButton34.YoffsetIco = 0;
            this.ppButton34.Click += new System.EventHandler(this.ppButton34_Click);
            // 
            // tabPage28
            // 
            this.tabPage28.BackColor = System.Drawing.Color.White;
            this.tabPage28.Controls.Add(this.panel3);
            this.tabPage28.Controls.Add(this.ppButton41);
            this.tabPage28.Controls.Add(this.ppComboboxEx9);
            this.tabPage28.Controls.Add(this.ppButton40);
            this.tabPage28.Controls.Add(this.ppRichTextbox5);
            this.tabPage28.Controls.Add(this.ppPanel8);
            this.tabPage28.Controls.Add(this.ppButton33);
            this.tabPage28.Controls.Add(this.ppButton32);
            this.tabPage28.Controls.Add(this.ppButton36);
            this.tabPage28.Controls.Add(this.ppButton37);
            this.tabPage28.Controls.Add(this.ppButton38);
            this.tabPage28.Controls.Add(this.ppButton39);
            this.tabPage28.Location = new System.Drawing.Point(0, 30);
            this.tabPage28.Name = "tabPage28";
            this.tabPage28.Size = new System.Drawing.Size(928, 675);
            this.tabPage28.TabIndex = 9;
            this.tabPage28.Text = "遮罩控件";
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Location = new System.Drawing.Point(467, 437);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(97, 52);
            this.panel3.TabIndex = 21;
            this.panel3.MouseEnter += new System.EventHandler(this.panel3_MouseEnter);
            this.panel3.MouseLeave += new System.EventHandler(this.panel3_MouseLeave);
            // 
            // ppButton41
            // 
            this.ppButton41.BackColor = System.Drawing.Color.White;
            this.ppButton41.BaseColor = System.Drawing.Color.White;
            this.ppButton41.DialogResult = System.Windows.Forms.DialogResult.None;
            this.ppButton41.IcoDown = null;
            this.ppButton41.IcoIn = null;
            this.ppButton41.IcoRegular = null;
            this.ppButton41.IcoSize = new System.Drawing.Size(15, 15);
            this.ppButton41.LineColor = System.Drawing.Color.LightGray;
            this.ppButton41.LineWidth = 1;
            this.ppButton41.Location = new System.Drawing.Point(38, 452);
            this.ppButton41.MouseDownBaseColor = System.Drawing.Color.LightGray;
            this.ppButton41.MouseDownLineColor = System.Drawing.Color.Silver;
            this.ppButton41.MouseDownTextColor = System.Drawing.Color.Black;
            this.ppButton41.MouseInBaseColor = System.Drawing.Color.LightGray;
            this.ppButton41.MouseInLineColor = System.Drawing.Color.Silver;
            this.ppButton41.MouseInTextColor = System.Drawing.Color.Black;
            this.ppButton41.Name = "ppButton41";
            this.ppButton41.Radius = 3;
            this.ppButton41.RegularBaseColor = System.Drawing.Color.White;
            this.ppButton41.RegularLineColor = System.Drawing.Color.LightGray;
            this.ppButton41.RegularTextColor = System.Drawing.Color.Black;
            this.ppButton41.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppButton41.Size = new System.Drawing.Size(75, 30);
            this.ppButton41.TabIndex = 20;
            this.ppButton41.Text = "窗体1";
            this.ppButton41.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.ppButton41.TextColor = System.Drawing.Color.Black;
            this.ppButton41.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.ppButton41.TipAutoCloseTime = 5000;
            this.ppButton41.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppButton41.TipCloseOnLeave = true;
            this.ppButton41.TipDeviation = new System.Drawing.Size(0, 0);
            this.ppButton41.TipFontSize = 10;
            this.ppButton41.TipFontText = null;
            this.ppButton41.TipForeColor = System.Drawing.Color.White;
            this.ppButton41.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.ppButton41.TipText = null;
            this.ppButton41.TipTopMoust = true;
            this.ppButton41.UseSpecial = true;
            this.ppButton41.UseVisualStyleBackColor = false;
            this.ppButton41.Xoffset = 0;
            this.ppButton41.XoffsetIco = 0;
            this.ppButton41.Yoffset = 0;
            this.ppButton41.YoffsetIco = 0;
            this.ppButton41.Click += new System.EventHandler(this.ppButton41_Click);
            // 
            // ppComboboxEx9
            // 
            this.ppComboboxEx9.BackColor = System.Drawing.Color.White;
            this.ppComboboxEx9.BaseColor = System.Drawing.Color.White;
            this.ppComboboxEx9.BorderColor = System.Drawing.Color.LightGray;
            this.ppComboboxEx9.BorderWidth = 1F;
            this.ppComboboxEx9.DropbaseColor = System.Drawing.Color.White;
            this.ppComboboxEx9.DropborderColor = System.Drawing.Color.LightGray;
            this.ppComboboxEx9.DropbuttonColor = System.Drawing.Color.White;
            this.ppComboboxEx9.DropbuttonMouseDownColor = System.Drawing.Color.DimGray;
            this.ppComboboxEx9.DropbuttonMouseInColor = System.Drawing.Color.LightGray;
            this.ppComboboxEx9.DropbuttonRadius = 5;
            this.ppComboboxEx9.DropbuttonSelectColor = System.Drawing.Color.DimGray;
            this.ppComboboxEx9.DropformRadius = 5;
            this.ppComboboxEx9.DropTextAlign = PPSkin.PPComboboxEx.Align.LEFT;
            this.ppComboboxEx9.Location = new System.Drawing.Point(509, 237);
            this.ppComboboxEx9.MaxDropDownItems = 8;
            this.ppComboboxEx9.Name = "ppComboboxEx9";
            this.ppComboboxEx9.Radius = 10;
            this.ppComboboxEx9.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppComboboxEx9.SelectedIndex = -1;
            this.ppComboboxEx9.SelectedItem = null;
            this.ppComboboxEx9.SelectedText = "";
            this.ppComboboxEx9.SelectedValue = null;
            this.ppComboboxEx9.Size = new System.Drawing.Size(201, 54);
            this.ppComboboxEx9.TabIndex = 19;
            this.ppComboboxEx9.TextAlign = PPSkin.PPComboboxEx.Align.LEFT;
            this.ppComboboxEx9.TextDrawMode = PPSkin.Enums.DrawMode.Anti;
            // 
            // ppButton40
            // 
            this.ppButton40.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ppButton40.BackColor = System.Drawing.Color.White;
            this.ppButton40.BaseColor = System.Drawing.Color.White;
            this.ppButton40.DialogResult = System.Windows.Forms.DialogResult.None;
            this.ppButton40.IcoDown = null;
            this.ppButton40.IcoIn = null;
            this.ppButton40.IcoRegular = null;
            this.ppButton40.IcoSize = new System.Drawing.Size(15, 15);
            this.ppButton40.LineColor = System.Drawing.Color.LightGray;
            this.ppButton40.LineWidth = 1;
            this.ppButton40.Location = new System.Drawing.Point(506, 141);
            this.ppButton40.MouseDownBaseColor = System.Drawing.Color.LightGray;
            this.ppButton40.MouseDownLineColor = System.Drawing.Color.Silver;
            this.ppButton40.MouseDownTextColor = System.Drawing.Color.Black;
            this.ppButton40.MouseInBaseColor = System.Drawing.Color.LightGray;
            this.ppButton40.MouseInLineColor = System.Drawing.Color.Silver;
            this.ppButton40.MouseInTextColor = System.Drawing.Color.Black;
            this.ppButton40.Name = "ppButton40";
            this.ppButton40.Radius = 10;
            this.ppButton40.RegularBaseColor = System.Drawing.Color.White;
            this.ppButton40.RegularLineColor = System.Drawing.Color.LightGray;
            this.ppButton40.RegularTextColor = System.Drawing.Color.Black;
            this.ppButton40.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppButton40.Size = new System.Drawing.Size(143, 66);
            this.ppButton40.TabIndex = 18;
            this.ppButton40.Text = "ppButton40";
            this.ppButton40.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.ppButton40.TextColor = System.Drawing.Color.Black;
            this.ppButton40.TextDrawMode = PPSkin.Enums.DrawMode.Anti;
            this.ppButton40.TipAutoCloseTime = 5000;
            this.ppButton40.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppButton40.TipCloseOnLeave = true;
            this.ppButton40.TipDeviation = new System.Drawing.Size(0, 0);
            this.ppButton40.TipFontSize = 10;
            this.ppButton40.TipFontText = null;
            this.ppButton40.TipForeColor = System.Drawing.Color.White;
            this.ppButton40.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.ppButton40.TipText = null;
            this.ppButton40.TipTopMoust = true;
            this.ppButton40.UseSpecial = true;
            this.ppButton40.UseVisualStyleBackColor = false;
            this.ppButton40.Xoffset = 0;
            this.ppButton40.XoffsetIco = 0;
            this.ppButton40.Yoffset = 0;
            this.ppButton40.YoffsetIco = 0;
            // 
            // ppRichTextbox5
            // 
            this.ppRichTextbox5.AcceptsTab = false;
            this.ppRichTextbox5.AutoWordSelection = false;
            this.ppRichTextbox5.BackColor = System.Drawing.SystemColors.Control;
            this.ppRichTextbox5.BaseColor = System.Drawing.Color.White;
            this.ppRichTextbox5.BorderColor = System.Drawing.Color.Black;
            this.ppRichTextbox5.BorderWidth = 1F;
            this.ppRichTextbox5.BulletIndent = 0;
            this.ppRichTextbox5.DetectUrls = true;
            this.ppRichTextbox5.EnableAutoDrogDrop = false;
            this.ppRichTextbox5.HideSelection = true;
            this.ppRichTextbox5.Location = new System.Drawing.Point(273, 139);
            this.ppRichTextbox5.MaxLength = 2147483647;
            this.ppRichTextbox5.Multiline = true;
            this.ppRichTextbox5.Name = "ppRichTextbox5";
            this.ppRichTextbox5.Radius = 10;
            this.ppRichTextbox5.ReadOnly = false;
            this.ppRichTextbox5.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppRichTextbox5.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            this.ppRichTextbox5.SelectionColor = System.Drawing.Color.Black;
            this.ppRichTextbox5.SelectionStart = 0;
            this.ppRichTextbox5.ShortcutsEnabled = true;
            this.ppRichTextbox5.ShowBorder = true;
            this.ppRichTextbox5.ShowSelectionMargin = false;
            this.ppRichTextbox5.Size = new System.Drawing.Size(212, 213);
            this.ppRichTextbox5.TabIndex = 17;
            this.ppRichTextbox5.VScrollBar_BaseColor = System.Drawing.Color.Gainsboro;
            this.ppRichTextbox5.VScrollBar_BaseRadius = 0;
            this.ppRichTextbox5.VScrollBar_BaseSize = 2;
            this.ppRichTextbox5.VScrollBar_ButtonColor = System.Drawing.Color.DodgerBlue;
            this.ppRichTextbox5.VScrollBar_ButtonHoverColor = System.Drawing.Color.DeepSkyBlue;
            this.ppRichTextbox5.VScrollBar_ButtonRadius = 10;
            this.ppRichTextbox5.VScrollBar_ButtonSize = 10;
            this.ppRichTextbox5.WordWrap = true;
            this.ppRichTextbox5.ZoomFactor = 1F;
            // 
            // ppPanel8
            // 
            this.ppPanel8.BackColor = System.Drawing.Color.White;
            this.ppPanel8.BaseColor = System.Drawing.SystemColors.Control;
            this.ppPanel8.BorderColor = System.Drawing.Color.LightGray;
            this.ppPanel8.BorderWidth = 1;
            this.ppPanel8.Location = new System.Drawing.Point(42, 141);
            this.ppPanel8.Name = "ppPanel8";
            this.ppPanel8.Radius = 20;
            this.ppPanel8.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppPanel8.ShowBorder = true;
            this.ppPanel8.Size = new System.Drawing.Size(210, 211);
            this.ppPanel8.TabIndex = 16;
            // 
            // ppButton33
            // 
            this.ppButton33.BackColor = System.Drawing.Color.White;
            this.ppButton33.BaseColor = System.Drawing.Color.White;
            this.ppButton33.DialogResult = System.Windows.Forms.DialogResult.None;
            this.ppButton33.IcoDown = null;
            this.ppButton33.IcoIn = null;
            this.ppButton33.IcoRegular = null;
            this.ppButton33.IcoSize = new System.Drawing.Size(15, 15);
            this.ppButton33.LineColor = System.Drawing.Color.LightGray;
            this.ppButton33.LineWidth = 1;
            this.ppButton33.Location = new System.Drawing.Point(523, 38);
            this.ppButton33.MouseDownBaseColor = System.Drawing.Color.LightGray;
            this.ppButton33.MouseDownLineColor = System.Drawing.Color.Silver;
            this.ppButton33.MouseDownTextColor = System.Drawing.Color.Black;
            this.ppButton33.MouseInBaseColor = System.Drawing.Color.LightGray;
            this.ppButton33.MouseInLineColor = System.Drawing.Color.Silver;
            this.ppButton33.MouseInTextColor = System.Drawing.Color.Black;
            this.ppButton33.Name = "ppButton33";
            this.ppButton33.Radius = 3;
            this.ppButton33.RegularBaseColor = System.Drawing.Color.White;
            this.ppButton33.RegularLineColor = System.Drawing.Color.LightGray;
            this.ppButton33.RegularTextColor = System.Drawing.Color.Black;
            this.ppButton33.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppButton33.Size = new System.Drawing.Size(75, 30);
            this.ppButton33.TabIndex = 15;
            this.ppButton33.Text = "控件遮罩4";
            this.ppButton33.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.ppButton33.TextColor = System.Drawing.Color.Black;
            this.ppButton33.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.ppButton33.TipAutoCloseTime = 5000;
            this.ppButton33.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppButton33.TipCloseOnLeave = true;
            this.ppButton33.TipDeviation = new System.Drawing.Size(0, 0);
            this.ppButton33.TipFontSize = 10;
            this.ppButton33.TipFontText = null;
            this.ppButton33.TipForeColor = System.Drawing.Color.White;
            this.ppButton33.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.ppButton33.TipText = null;
            this.ppButton33.TipTopMoust = true;
            this.ppButton33.UseSpecial = true;
            this.ppButton33.UseVisualStyleBackColor = false;
            this.ppButton33.Xoffset = 0;
            this.ppButton33.XoffsetIco = 0;
            this.ppButton33.Yoffset = 0;
            this.ppButton33.YoffsetIco = 0;
            this.ppButton33.Click += new System.EventHandler(this.ppButton33_Click);
            // 
            // ppButton32
            // 
            this.ppButton32.BackColor = System.Drawing.Color.White;
            this.ppButton32.BaseColor = System.Drawing.Color.White;
            this.ppButton32.DialogResult = System.Windows.Forms.DialogResult.None;
            this.ppButton32.IcoDown = null;
            this.ppButton32.IcoIn = null;
            this.ppButton32.IcoRegular = null;
            this.ppButton32.IcoSize = new System.Drawing.Size(15, 15);
            this.ppButton32.LineColor = System.Drawing.Color.LightGray;
            this.ppButton32.LineWidth = 1;
            this.ppButton32.Location = new System.Drawing.Point(425, 38);
            this.ppButton32.MouseDownBaseColor = System.Drawing.Color.LightGray;
            this.ppButton32.MouseDownLineColor = System.Drawing.Color.Silver;
            this.ppButton32.MouseDownTextColor = System.Drawing.Color.Black;
            this.ppButton32.MouseInBaseColor = System.Drawing.Color.LightGray;
            this.ppButton32.MouseInLineColor = System.Drawing.Color.Silver;
            this.ppButton32.MouseInTextColor = System.Drawing.Color.Black;
            this.ppButton32.Name = "ppButton32";
            this.ppButton32.Radius = 3;
            this.ppButton32.RegularBaseColor = System.Drawing.Color.White;
            this.ppButton32.RegularLineColor = System.Drawing.Color.LightGray;
            this.ppButton32.RegularTextColor = System.Drawing.Color.Black;
            this.ppButton32.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppButton32.Size = new System.Drawing.Size(75, 30);
            this.ppButton32.TabIndex = 14;
            this.ppButton32.Text = "控件遮罩3";
            this.ppButton32.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.ppButton32.TextColor = System.Drawing.Color.Black;
            this.ppButton32.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.ppButton32.TipAutoCloseTime = 5000;
            this.ppButton32.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppButton32.TipCloseOnLeave = true;
            this.ppButton32.TipDeviation = new System.Drawing.Size(0, 0);
            this.ppButton32.TipFontSize = 10;
            this.ppButton32.TipFontText = null;
            this.ppButton32.TipForeColor = System.Drawing.Color.White;
            this.ppButton32.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.ppButton32.TipText = null;
            this.ppButton32.TipTopMoust = true;
            this.ppButton32.UseSpecial = true;
            this.ppButton32.UseVisualStyleBackColor = false;
            this.ppButton32.Xoffset = 0;
            this.ppButton32.XoffsetIco = 0;
            this.ppButton32.Yoffset = 0;
            this.ppButton32.YoffsetIco = 0;
            this.ppButton32.Click += new System.EventHandler(this.ppButton32_Click);
            // 
            // ppButton36
            // 
            this.ppButton36.BackColor = System.Drawing.Color.White;
            this.ppButton36.BaseColor = System.Drawing.Color.White;
            this.ppButton36.DialogResult = System.Windows.Forms.DialogResult.None;
            this.ppButton36.IcoDown = null;
            this.ppButton36.IcoIn = null;
            this.ppButton36.IcoRegular = null;
            this.ppButton36.IcoSize = new System.Drawing.Size(15, 15);
            this.ppButton36.LineColor = System.Drawing.Color.LightGray;
            this.ppButton36.LineWidth = 1;
            this.ppButton36.Location = new System.Drawing.Point(328, 38);
            this.ppButton36.MouseDownBaseColor = System.Drawing.Color.LightGray;
            this.ppButton36.MouseDownLineColor = System.Drawing.Color.Silver;
            this.ppButton36.MouseDownTextColor = System.Drawing.Color.Black;
            this.ppButton36.MouseInBaseColor = System.Drawing.Color.LightGray;
            this.ppButton36.MouseInLineColor = System.Drawing.Color.Silver;
            this.ppButton36.MouseInTextColor = System.Drawing.Color.Black;
            this.ppButton36.Name = "ppButton36";
            this.ppButton36.Radius = 3;
            this.ppButton36.RegularBaseColor = System.Drawing.Color.White;
            this.ppButton36.RegularLineColor = System.Drawing.Color.LightGray;
            this.ppButton36.RegularTextColor = System.Drawing.Color.Black;
            this.ppButton36.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppButton36.Size = new System.Drawing.Size(75, 30);
            this.ppButton36.TabIndex = 13;
            this.ppButton36.Text = "控件遮罩2";
            this.ppButton36.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.ppButton36.TextColor = System.Drawing.Color.Black;
            this.ppButton36.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.ppButton36.TipAutoCloseTime = 5000;
            this.ppButton36.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppButton36.TipCloseOnLeave = true;
            this.ppButton36.TipDeviation = new System.Drawing.Size(0, 0);
            this.ppButton36.TipFontSize = 10;
            this.ppButton36.TipFontText = null;
            this.ppButton36.TipForeColor = System.Drawing.Color.White;
            this.ppButton36.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.ppButton36.TipText = null;
            this.ppButton36.TipTopMoust = true;
            this.ppButton36.UseSpecial = true;
            this.ppButton36.UseVisualStyleBackColor = false;
            this.ppButton36.Xoffset = 0;
            this.ppButton36.XoffsetIco = 0;
            this.ppButton36.Yoffset = 0;
            this.ppButton36.YoffsetIco = 0;
            this.ppButton36.Click += new System.EventHandler(this.ppButton36_Click);
            // 
            // ppButton37
            // 
            this.ppButton37.BackColor = System.Drawing.Color.White;
            this.ppButton37.BaseColor = System.Drawing.Color.White;
            this.ppButton37.DialogResult = System.Windows.Forms.DialogResult.None;
            this.ppButton37.IcoDown = null;
            this.ppButton37.IcoIn = null;
            this.ppButton37.IcoRegular = null;
            this.ppButton37.IcoSize = new System.Drawing.Size(15, 15);
            this.ppButton37.LineColor = System.Drawing.Color.LightGray;
            this.ppButton37.LineWidth = 1;
            this.ppButton37.Location = new System.Drawing.Point(237, 38);
            this.ppButton37.MouseDownBaseColor = System.Drawing.Color.LightGray;
            this.ppButton37.MouseDownLineColor = System.Drawing.Color.Silver;
            this.ppButton37.MouseDownTextColor = System.Drawing.Color.Black;
            this.ppButton37.MouseInBaseColor = System.Drawing.Color.LightGray;
            this.ppButton37.MouseInLineColor = System.Drawing.Color.Silver;
            this.ppButton37.MouseInTextColor = System.Drawing.Color.Black;
            this.ppButton37.Name = "ppButton37";
            this.ppButton37.Radius = 3;
            this.ppButton37.RegularBaseColor = System.Drawing.Color.White;
            this.ppButton37.RegularLineColor = System.Drawing.Color.LightGray;
            this.ppButton37.RegularTextColor = System.Drawing.Color.Black;
            this.ppButton37.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppButton37.Size = new System.Drawing.Size(75, 30);
            this.ppButton37.TabIndex = 12;
            this.ppButton37.Text = "控件遮罩";
            this.ppButton37.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.ppButton37.TextColor = System.Drawing.Color.Black;
            this.ppButton37.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.ppButton37.TipAutoCloseTime = 5000;
            this.ppButton37.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppButton37.TipCloseOnLeave = true;
            this.ppButton37.TipDeviation = new System.Drawing.Size(0, 0);
            this.ppButton37.TipFontSize = 10;
            this.ppButton37.TipFontText = null;
            this.ppButton37.TipForeColor = System.Drawing.Color.White;
            this.ppButton37.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.ppButton37.TipText = null;
            this.ppButton37.TipTopMoust = true;
            this.ppButton37.UseSpecial = true;
            this.ppButton37.UseVisualStyleBackColor = false;
            this.ppButton37.Xoffset = 0;
            this.ppButton37.XoffsetIco = 0;
            this.ppButton37.Yoffset = 0;
            this.ppButton37.YoffsetIco = 0;
            this.ppButton37.Click += new System.EventHandler(this.ppButton37_Click);
            // 
            // ppButton38
            // 
            this.ppButton38.BackColor = System.Drawing.Color.White;
            this.ppButton38.BaseColor = System.Drawing.Color.White;
            this.ppButton38.DialogResult = System.Windows.Forms.DialogResult.None;
            this.ppButton38.IcoDown = null;
            this.ppButton38.IcoIn = null;
            this.ppButton38.IcoRegular = null;
            this.ppButton38.IcoSize = new System.Drawing.Size(15, 15);
            this.ppButton38.LineColor = System.Drawing.Color.LightGray;
            this.ppButton38.LineWidth = 1;
            this.ppButton38.Location = new System.Drawing.Point(133, 38);
            this.ppButton38.MouseDownBaseColor = System.Drawing.Color.LightGray;
            this.ppButton38.MouseDownLineColor = System.Drawing.Color.Silver;
            this.ppButton38.MouseDownTextColor = System.Drawing.Color.Black;
            this.ppButton38.MouseInBaseColor = System.Drawing.Color.LightGray;
            this.ppButton38.MouseInLineColor = System.Drawing.Color.Silver;
            this.ppButton38.MouseInTextColor = System.Drawing.Color.Black;
            this.ppButton38.Name = "ppButton38";
            this.ppButton38.Radius = 3;
            this.ppButton38.RegularBaseColor = System.Drawing.Color.White;
            this.ppButton38.RegularLineColor = System.Drawing.Color.LightGray;
            this.ppButton38.RegularTextColor = System.Drawing.Color.Black;
            this.ppButton38.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppButton38.Size = new System.Drawing.Size(75, 30);
            this.ppButton38.TabIndex = 11;
            this.ppButton38.Text = "窗体遮罩2";
            this.ppButton38.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.ppButton38.TextColor = System.Drawing.Color.Black;
            this.ppButton38.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.ppButton38.TipAutoCloseTime = 5000;
            this.ppButton38.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppButton38.TipCloseOnLeave = true;
            this.ppButton38.TipDeviation = new System.Drawing.Size(0, 0);
            this.ppButton38.TipFontSize = 10;
            this.ppButton38.TipFontText = null;
            this.ppButton38.TipForeColor = System.Drawing.Color.White;
            this.ppButton38.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.ppButton38.TipText = null;
            this.ppButton38.TipTopMoust = true;
            this.ppButton38.UseSpecial = true;
            this.ppButton38.UseVisualStyleBackColor = false;
            this.ppButton38.Xoffset = 0;
            this.ppButton38.XoffsetIco = 0;
            this.ppButton38.Yoffset = 0;
            this.ppButton38.YoffsetIco = 0;
            this.ppButton38.Click += new System.EventHandler(this.ppButton38_Click);
            // 
            // ppButton39
            // 
            this.ppButton39.BackColor = System.Drawing.Color.White;
            this.ppButton39.BaseColor = System.Drawing.Color.White;
            this.ppButton39.DialogResult = System.Windows.Forms.DialogResult.None;
            this.ppButton39.IcoDown = null;
            this.ppButton39.IcoIn = null;
            this.ppButton39.IcoRegular = null;
            this.ppButton39.IcoSize = new System.Drawing.Size(15, 15);
            this.ppButton39.LineColor = System.Drawing.Color.LightGray;
            this.ppButton39.LineWidth = 1;
            this.ppButton39.Location = new System.Drawing.Point(38, 38);
            this.ppButton39.MouseDownBaseColor = System.Drawing.Color.LightGray;
            this.ppButton39.MouseDownLineColor = System.Drawing.Color.Silver;
            this.ppButton39.MouseDownTextColor = System.Drawing.Color.Black;
            this.ppButton39.MouseInBaseColor = System.Drawing.Color.LightGray;
            this.ppButton39.MouseInLineColor = System.Drawing.Color.Silver;
            this.ppButton39.MouseInTextColor = System.Drawing.Color.Black;
            this.ppButton39.Name = "ppButton39";
            this.ppButton39.Radius = 3;
            this.ppButton39.RegularBaseColor = System.Drawing.Color.White;
            this.ppButton39.RegularLineColor = System.Drawing.Color.LightGray;
            this.ppButton39.RegularTextColor = System.Drawing.Color.Black;
            this.ppButton39.RoundStyle = PPSkin.Enums.RoundStyle.All;
            this.ppButton39.Size = new System.Drawing.Size(75, 30);
            this.ppButton39.TabIndex = 10;
            this.ppButton39.Text = "窗体遮罩";
            this.ppButton39.TextAlign = PPSkin.PPButton.Align.CENTER;
            this.ppButton39.TextColor = System.Drawing.Color.Black;
            this.ppButton39.TextDrawMode = PPSkin.Enums.DrawMode.Clear;
            this.ppButton39.TipAutoCloseTime = 5000;
            this.ppButton39.TipBackColor = System.Drawing.Color.DodgerBlue;
            this.ppButton39.TipCloseOnLeave = true;
            this.ppButton39.TipDeviation = new System.Drawing.Size(0, 0);
            this.ppButton39.TipFontSize = 10;
            this.ppButton39.TipFontText = null;
            this.ppButton39.TipForeColor = System.Drawing.Color.White;
            this.ppButton39.TipLocation = PPSkin.AnchorTipsLocation.TOP;
            this.ppButton39.TipText = null;
            this.ppButton39.TipTopMoust = true;
            this.ppButton39.UseSpecial = true;
            this.ppButton39.UseVisualStyleBackColor = false;
            this.ppButton39.Xoffset = 0;
            this.ppButton39.XoffsetIco = 0;
            this.ppButton39.Yoffset = 0;
            this.ppButton39.YoffsetIco = 0;
            this.ppButton39.Click += new System.EventHandler(this.ppButton39_Click);
            // 
            // ppPanel2
            // 
            this.ppPanel2.BackColor = System.Drawing.Color.DodgerBlue;
            this.ppPanel2.BaseColor = System.Drawing.Color.White;
            this.ppPanel2.BorderColor = System.Drawing.Color.LightGray;
            this.ppPanel2.BorderWidth = 1;
            this.ppPanel2.Controls.Add(this.ppTabControl1);
            this.ppPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ppPanel2.Location = new System.Drawing.Point(151, 32);
            this.ppPanel2.Name = "ppPanel2";
            this.ppPanel2.Radius = 7;
            this.ppPanel2.RoundStyle = PPSkin.Enums.RoundStyle.BottomRight;
            this.ppPanel2.ShowBorder = false;
            this.ppPanel2.Size = new System.Drawing.Size(928, 705);
            this.ppPanel2.TabIndex = 3;
            // 
            // ppPanel1
            // 
            this.ppPanel1.BackColor = System.Drawing.Color.DodgerBlue;
            this.ppPanel1.BaseColor = System.Drawing.SystemColors.ControlDarkDark;
            this.ppPanel1.BorderColor = System.Drawing.Color.LightGray;
            this.ppPanel1.BorderWidth = 1;
            this.ppPanel1.Controls.Add(this.ppTabButtonBar1);
            this.ppPanel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.ppPanel1.Location = new System.Drawing.Point(1, 32);
            this.ppPanel1.Name = "ppPanel1";
            this.ppPanel1.Radius = 4;
            this.ppPanel1.RoundStyle = PPSkin.Enums.RoundStyle.BottomLeft;
            this.ppPanel1.ShowBorder = false;
            this.ppPanel1.Size = new System.Drawing.Size(150, 705);
            this.ppPanel1.TabIndex = 2;
            // 
            // Demo
            // 
            this.AnimeTimeClose = 300;
            this.AnimeTimeLoad = 300;
            this.AnimeTypeClose = PPSkin.PPForm.AnimeType.AW_BLEND;
            this.AnimeTypeLoad = PPSkin.PPForm.AnimeType.AW_BLEND;
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(1080, 738);
            this.Controls.Add(this.ppPanel2);
            this.Controls.Add(this.ppPanel1);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "Demo";
            this.Padding = new System.Windows.Forms.Padding(1, 32, 1, 1);
            this.Radius = 5;
            this.ShadowColor = System.Drawing.Color.DodgerBlue;
            this.ShadowWidth = 8;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Demo";
            this.TransparencyKey = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(144)))), ((int)(((byte)(250)))));
            this.Load += new System.EventHandler(this.Demo_Load);
            this.ppTabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.ppGroupBox10.ResumeLayout(false);
            this.ppGroupBox2.ResumeLayout(false);
            this.ppGroupBox2.PerformLayout();
            this.ppGroupBox1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ppFlodPanel1.ResumeLayout(false);
            this.ppFlodPanel2.ResumeLayout(false);
            this.ppFlodPanel3.ResumeLayout(false);
            this.ppFlodPanel4.ResumeLayout(false);
            this.ppFlodPanel5.ResumeLayout(false);
            this.ppFlodPanel6.ResumeLayout(false);
            this.ppFlodPanel7.ResumeLayout(false);
            this.ppFlodPanel8.ResumeLayout(false);
            this.ppFlodPanel9.ResumeLayout(false);
            this.ppFlodPanel10.ResumeLayout(false);
            this.ppGroupBox5.ResumeLayout(false);
            this.ppMovePanel1.ResumeLayout(false);
            this.ppGroupBox4.ResumeLayout(false);
            this.ppGroupBox3.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            this.tabPage6.ResumeLayout(false);
            this.ppTabControl2.ResumeLayout(false);
            this.tabPage8.ResumeLayout(false);
            this.ppTabControl6.ResumeLayout(false);
            this.ppTabControl5.ResumeLayout(false);
            this.ppTabControl3.ResumeLayout(false);
            this.ppTabControl4.ResumeLayout(false);
            this.tabPage9.ResumeLayout(false);
            this.ppTabControl7.ResumeLayout(false);
            this.ppTabControl8.ResumeLayout(false);
            this.ppTabControl9.ResumeLayout(false);
            this.ppTabControl10.ResumeLayout(false);
            this.tabPage7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ppDataGridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ppDataGridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ppDataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ppDataGridView1)).EndInit();
            this.tabPage26.ResumeLayout(false);
            this.tabPage27.ResumeLayout(false);
            this.tabPage27.PerformLayout();
            this.tabPage28.ResumeLayout(false);
            this.ppPanel2.ResumeLayout(false);
            this.ppPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private PPSkin.PPTabButtonBar ppTabButtonBar1;
        private PPSkin.PPTabControl ppTabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private PPSkin.PPPanel ppPanel2;
        private PPSkin.PPGroupBox ppGroupBox1;
        private PPSkin.PPButton ppButton5;
        private PPSkin.PPButton ppButton4;
        private PPSkin.PPButton ppButton3;
        private PPSkin.PPButton ppButton2;
        private PPSkin.PPButton ppButton1;
        private PPSkin.PPButton ppButton6;
        private PPSkin.PPButton ppButton7;
        private PPSkin.PPButton ppButton8;
        private PPSkin.PPButton ppButton9;
        private PPSkin.PPButton ppButton10;
        private PPSkin.PPButton ppButton11;
        private PPSkin.PPButton ppButton12;
        private PPSkin.PPButton ppButton13;
        private PPSkin.PPButton ppButton14;
        private PPSkin.PPButton ppButton15;
        private PPSkin.PPButton ppButton16;
        private PPSkin.PPButton ppButton17;
        private PPSkin.PPButton ppButton18;
        private PPSkin.PPButton ppButton19;
        private PPSkin.PPButton ppButton20;
        private PPSkin.PPButton ppButton21;
        private PPSkin.PPButton ppButton22;
        private PPSkin.PPButton ppButton23;
        private PPSkin.PPButton ppButton24;
        private PPSkin.PPButton ppButton25;
        private PPSkin.PPButton ppButton26;
        private PPSkin.PPButton ppButton27;
        private PPSkin.PPButton ppButton28;
        private PPSkin.PPButton ppButton29;
        private PPSkin.PPButton ppButton30;
        private PPSkin.PPGroupBox ppGroupBox2;
        private PPSkin.PPCheckBox ppCheckBox5;
        private PPSkin.PPCheckBox ppCheckBox4;
        private PPSkin.PPCheckBox ppCheckBox3;
        private PPSkin.PPCheckBox ppCheckBox2;
        private PPSkin.PPCheckBox ppCheckBox1;
        private PPSkin.PPCheckBox ppCheckBox6;
        private PPSkin.PPCheckBox ppCheckBox7;
        private PPSkin.PPCheckBox ppCheckBox8;
        private PPSkin.PPCheckBox ppCheckBox9;
        private PPSkin.PPCheckBox ppCheckBox10;
        private PPSkin.PPCheckBox ppCheckBox11;
        private PPSkin.PPCheckBox ppCheckBox12;
        private PPSkin.PPCheckBox ppCheckBox13;
        private PPSkin.PPCheckBox ppCheckBox14;
        private PPSkin.PPCheckBox ppCheckBox15;
        private PPSkin.PPCheckBox ppCheckBox16;
        private PPSkin.PPCheckBox ppCheckBox17;
        private PPSkin.PPCheckBox ppCheckBox18;
        private PPSkin.PPCheckBox ppCheckBox19;
        private PPSkin.PPCheckBox ppCheckBox20;
        private PPSkin.PPCheckBoxEx ppCheckBoxEx1;
        private PPSkin.PPCheckBoxEx ppCheckBoxEx2;
        private PPSkin.PPCheckBoxEx ppCheckBoxEx3;
        private PPSkin.PPCheckBoxEx ppCheckBoxEx4;
        private PPSkin.PPRadioButton ppRadioButton1;
        private PPSkin.PPRadioButton ppRadioButton2;
        private PPSkin.PPRadioButton ppRadioButton4;
        private PPSkin.PPRadioButton ppRadioButton3;
        private PPSkin.PPComboboxEx ppComboboxEx4;
        private PPSkin.PPComboboxEx ppComboboxEx3;
        private PPSkin.PPComboboxEx ppComboboxEx2;
        private PPSkin.PPComboboxEx ppComboboxEx1;
        private PPSkin.PPCombobox ppCombobox4;
        private PPSkin.PPCombobox ppCombobox3;
        private PPSkin.PPCombobox ppCombobox2;
        private PPSkin.PPCombobox ppCombobox1;
        private PPSkin.PPDateTimePicker ppDateTimePicker5;
        private PPSkin.PPDateTimePicker ppDateTimePicker6;
        private PPSkin.PPDateTimePicker ppDateTimePicker7;
        private PPSkin.PPDateTimePicker ppDateTimePicker8;
        private PPSkin.PPDateTimePicker ppDateTimePicker1;
        private PPSkin.PPDateTimePicker ppDateTimePicker2;
        private PPSkin.PPDateTimePicker ppDateTimePicker3;
        private PPSkin.PPDateTimePicker ppDateTimePicker4;
        private PPSkin.PPDateTimePicker ppDateTimePicker9;
        private PPSkin.PPDateTimePicker ppDateTimePicker10;
        private PPSkin.PPDateTimePicker ppDateTimePicker11;
        private PPSkin.PPDateTimePicker ppDateTimePicker12;
        private PPSkin.PPDateTimePicker ppDateTimePicker13;
        private PPSkin.PPDateTimePicker ppDateTimePicker14;
        private PPSkin.PPDateTimePicker ppDateTimePicker15;
        private PPSkin.PPDateTimePicker ppDateTimePicker16;
        private System.Windows.Forms.TabPage tabPage3;
        private PPSkin.PPHScrollBarExt pphScrollBarExt5;
        private PPSkin.PPHScrollBarExt pphScrollBarExt4;
        private PPSkin.PPWaveProgressBar ppWaveProgressBar7;
        private PPSkin.PPWaveProgressBar ppWaveProgressBar6;
        private PPSkin.PPWaveProgressBar ppWaveProgressBar5;
        private PPSkin.PPWaveProgressBar ppWaveProgressBar4;
        private PPSkin.PPWaveProgressBar ppWaveProgressBar3;
        private PPSkin.PPWaveProgressBar ppWaveProgressBar2;
        private PPSkin.PPWaveProgressBar ppWaveProgressBar1;
        private PPSkin.PPTrackBar ppTrackBar3;
        private PPSkin.PPTrackBar ppTrackBar2;
        private PPSkin.PPTrackBar ppTrackBar1;
        private PPSkin.PPRoundProgressBar ppRoundProgressBar8;
        private PPSkin.PPRoundProgressBar ppRoundProgressBar7;
        private PPSkin.PPRoundProgressBar ppRoundProgressBar6;
        private PPSkin.PPRoundProgressBar ppRoundProgressBar5;
        private PPSkin.PPRoundProgressBar ppRoundProgressBar4;
        private PPSkin.PPRoundProgressBar ppRoundProgressBar3;
        private PPSkin.PPRoundProgressBar ppRoundProgressBar2;
        private PPSkin.PPRoundProgressBar ppRoundProgressBar1;
        private PPSkin.PPProgressBar ppProgressBar3;
        private PPSkin.PPProgressBar ppProgressBar2;
        private PPSkin.PPProgressBar ppProgressBar1;
        private PPSkin.PPHScrollBarExt pphScrollBarExt3;
        private PPSkin.PPHScrollBarExt pphScrollBarExt2;
        private PPSkin.PPHScrollBarExt pphScrollBarExt1;
        private PPSkin.PPVScrollBarExt ppvScrollBarExt6;
        private PPSkin.PPVScrollBarExt ppvScrollBarExt5;
        private PPSkin.PPVScrollBarExt ppvScrollBarExt4;
        private PPSkin.PPVScrollBarExt ppvScrollBarExt3;
        private PPSkin.PPVScrollBarExt ppvScrollBarExt2;
        private PPSkin.PPVScrollBarExt ppvScrollBarExt1;
        private System.Windows.Forms.TabPage tabPage4;
        private PPSkin.PPGroupBox ppGroupBox3;
        private PPSkin.PPGroupBox ppGroupBox5;
        private PPSkin.PPGroupBox ppGroupBox4;
        private PPSkin.PPGroupBox ppGroupBox7;
        private PPSkin.PPGroupBox ppGroupBox6;
        private PPSkin.PPGroupBox ppGroupBox8;
        private PPSkin.PPGroupBox ppGroupBox9;
        private PPSkin.PPPanel ppPanel4;
        private PPSkin.PPPanel ppPanel3;
        private PPSkin.PPPanel ppPanel5;
        private PPSkin.PPPanel ppPanel6;
        private PPSkin.PPMovePanel ppMovePanel1;
        private PPSkin.PPPanel ppPanel7;
        private PPSkin.PPButton ppButton31;
        private PPSkin.PPComboboxEx ppComboboxEx5;
        private PPSkin.PPCheckBox ppCheckBox21;
        private PPSkin.PPNumericUpDown ppNumericUpDown3;
        private PPSkin.PPNumericUpDown ppNumericUpDown2;
        private PPSkin.PPNumericUpDown ppNumericUpDown1;
        private PPSkin.PPTextbox ppTextbox5;
        private PPSkin.PPTextbox ppTextbox4;
        private PPSkin.PPTextbox ppTextbox3;
        private PPSkin.PPTextbox ppTextbox2;
        private PPSkin.PPTextbox ppTextbox1;
        private PPSkin.PPTextbox Textbox;
        private PPSkin.PPRichTextbox ppRichTextbox1;
        private PPSkin.PPGroupBox ppGroupBox10;
        private System.Windows.Forms.TabPage tabPage5;
        private PPSkin.PPTreeView ppTreeView3;
        private PPSkin.PPTreeView ppTreeView1;
        private PPSkin.PPTreeView ppTreeView2;
        private System.Windows.Forms.TabPage tabPage6;
        private PPSkin.PPTabControl ppTabControl2;
        private System.Windows.Forms.TabPage tabPage8;
        private PPSkin.PPTabControl ppTabControl6;
        private System.Windows.Forms.TabPage tabPage16;
        private System.Windows.Forms.TabPage tabPage17;
        private PPSkin.PPTabControl ppTabControl5;
        private System.Windows.Forms.TabPage tabPage14;
        private System.Windows.Forms.TabPage tabPage15;
        private PPSkin.PPTabControl ppTabControl3;
        private System.Windows.Forms.TabPage tabPage10;
        private System.Windows.Forms.TabPage tabPage11;
        private PPSkin.PPTabControl ppTabControl4;
        private System.Windows.Forms.TabPage tabPage12;
        private System.Windows.Forms.TabPage tabPage13;
        private System.Windows.Forms.TabPage tabPage9;
        private PPSkin.PPTabControl ppTabControl7;
        private System.Windows.Forms.TabPage tabPage18;
        private System.Windows.Forms.TabPage tabPage19;
        private PPSkin.PPTabControl ppTabControl8;
        private System.Windows.Forms.TabPage tabPage20;
        private System.Windows.Forms.TabPage tabPage21;
        private PPSkin.PPTabControl ppTabControl9;
        private System.Windows.Forms.TabPage tabPage22;
        private System.Windows.Forms.TabPage tabPage23;
        private PPSkin.PPTabControl ppTabControl10;
        private System.Windows.Forms.TabPage tabPage24;
        private System.Windows.Forms.TabPage tabPage25;
        private System.Windows.Forms.TabPage tabPage7;
        private PPSkin.PPDataGridView ppDataGridView4;
        private PPSkin.PPDataGridView ppDataGridView3;
        private PPSkin.PPDataGridView ppDataGridView2;
        private PPSkin.PPDataGridView ppDataGridView1;
        private PPSkin.PPHScrollBarExt pphScrollBarExt10;
        private PPSkin.PPVScrollBarExt ppvScrollBarExt11;
        private PPSkin.PPHScrollBarExt pphScrollBarExt9;
        private PPSkin.PPVScrollBarExt ppvScrollBarExt10;
        private PPSkin.PPHScrollBarExt pphScrollBarExt8;
        private PPSkin.PPVScrollBarExt ppvScrollBarExt9;
        private PPSkin.PPHScrollBarExt pphScrollBarExt7;
        private PPSkin.PPVScrollBarExt ppvScrollBarExt8;
        private System.Windows.Forms.TabPage tabPage26;
        private PPSkin.PPButton ppButton35;
        private PPSkin.UnlockerPanel unlockerPanel3;
        private PPSkin.UnlockerPanel unlockerPanel2;
        private PPSkin.UnlockerPanel unlockerPanel1;
        private PPSkin.LedBar ledBar1;
        private PPSkin.LedNum ledNum14;
        private PPSkin.LedNum ledNum13;
        private PPSkin.LedNum ledNum12;
        private PPSkin.LedNum ledNum11;
        private PPSkin.LedNum ledNum10;
        private PPSkin.LedNum ledNum9;
        private PPSkin.LedNum ledNum8;
        private PPSkin.LedNum ledNum7;
        private PPSkin.LedNum ledNum6;
        private PPSkin.LedNum ledNum5;
        private PPSkin.LedNum ledNum4;
        private PPSkin.LedNum ledNum3;
        private PPSkin.LedNum ledNum2;
        private PPSkin.LedNum ledNum1;
        private System.Windows.Forms.TabPage tabPage27;
        private PPSkin.PPRadioButton ppRadioButton11;
        private PPSkin.PPRadioButton ppRadioButton10;
        private PPSkin.PPRadioButton ppRadioButton9;
        private System.Windows.Forms.Panel panel1;
        private PPSkin.PPButton ppButton34;
        private PPSkin.PPNumericUpDown ppNumericUpDown4;
        private PPSkin.PPLabel ppLabel1;
        private System.Windows.Forms.TabPage tabPage28;
        private PPSkin.PPButton ppButton33;
        private PPSkin.PPButton ppButton32;
        private PPSkin.PPButton ppButton36;
        private PPSkin.PPButton ppButton37;
        private PPSkin.PPButton ppButton38;
        private PPSkin.PPButton ppButton39;
        private PPSkin.PPComboboxEx ppComboboxEx9;
        private PPSkin.PPButton ppButton40;
        private PPSkin.PPRichTextbox ppRichTextbox5;
        private PPSkin.PPPanel ppPanel8;
        private PPSkin.PPButton ppButton41;
        private PPSkin.PPPanel ppPanel1;
        private PPSkin.PPFlodPanel ppFlodPanel1;
        private PPSkin.PPButton ppButton42;
        private PPSkin.PPButton ppButton43;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private PPSkin.PPFlodPanel ppFlodPanel2;
        private PPSkin.PPButton ppButton44;
        private PPSkin.PPFlodPanel ppFlodPanel3;
        private PPSkin.PPButton ppButton45;
        private PPSkin.PPFlodPanel ppFlodPanel4;
        private PPSkin.PPButton ppButton46;
        private PPSkin.PPFlodPanel ppFlodPanel5;
        private PPSkin.PPButton ppButton47;
        private PPSkin.PPFlodPanel ppFlodPanel6;
        private PPSkin.PPButton ppButton48;
        private PPSkin.PPFlodPanel ppFlodPanel7;
        private PPSkin.PPButton ppButton49;
        private PPSkin.PPFlodPanel ppFlodPanel8;
        private PPSkin.PPButton ppButton50;
        private PPSkin.PPFlodPanel ppFlodPanel9;
        private PPSkin.PPButton ppButton51;
        private PPSkin.PPFlodPanel ppFlodPanel10;
        private PPSkin.PPButton ppButton52;
        private PPSkin.PPComboboxEx ppComboboxEx6;
        private PPSkin.PPNumericUpDown ppNumericUpDown5;
        private PPSkin.PPRoundProgressBar ppRoundProgressBar9;
        private PPSkin.PPButton ppButton53;
        private System.Windows.Forms.Panel panel3;
        private PPSkin.PPButton ppButton54;
        private System.Windows.Forms.FontDialog fontDialog1;
    }
}