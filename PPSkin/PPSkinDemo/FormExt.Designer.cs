﻿namespace PPSkinDemo
{
    partial class FormExt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // FormExt
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Fade_In = true;
            this.Fade_Out = true;
            this.Name = "FormExt";
            this.Radius = 8;
            this.ShadowColor = System.Drawing.Color.DodgerBlue;
            this.Text = "FormExt";
            this.TransparencyKey = System.Drawing.Color.DodgerBlue;
            this.ResumeLayout(false);

        }

        #endregion
    }
}